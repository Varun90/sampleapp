import UIKit
import Cosmos
import SlideMenuControllerSwift
import IQKeyboardManagerSwift
class CustomeFinishRideAlert: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    @IBOutlet var ratingTableView: UITableView!
    @IBOutlet var submitBtn: UIButton!
    var rootCon : UIViewController!
    var nav : UINavigationController!
    var starRating = 0.0
    var alertMessage = ""
    var totalPrice = ""
    var rideId = ""
    var ratingArray = [3.5,4.0,4.5,5.0]
    var opinion_notes = "Opinion...".localized
    override func viewDidLoad() {
        super.viewDidLoad()
        print(alertMessage)
        
        alertMessage = "Finish Ride Message".localized
        alertMessage = alertMessage.replacingOccurrences(of: "%@", with: totalPrice)
        alertMessage = alertMessage.replacingOccurrences(of: "Your-Taxi", with: "<i>YOUR</i><b>TAXI</b>")
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.title = "RATING".localized
        submitBtn.setTitle("Send".localized, for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setRatingDidFinish()
    {
        self.dismiss(animated: false) {
            if self.rootCon is DriverDetailsController{
                (self.rootCon as! DriverDetailsController).backAction()
            }
            else if self.rootCon is CurrentRidesViewController{
                _ = self.nav.popViewController(animated: false)
                let visibleViewController1 : UIViewController = (self.delegate?.getVisibleViewController(self.delegate?.window!.rootViewController)!)!
                
                if visibleViewController1 is SlideMenuController
                {
                    let slideMenuVC1 = (visibleViewController1 as! SlideMenuController)
                    let nav1 = (slideMenuVC1.mainViewController as! UINavigationController)
                    let rootCon = nav1.viewControllers.last
                    if rootCon is DriverDetailsController
                    {
                        (rootCon as! DriverDetailsController).backAction()
                    }
                }
            }
        }
    }
    func sendRatingToServer() {
        if self.starRating < 3.5 {
            let randomIndex = Int(arc4random_uniform(UInt32(ratingArray.count)))
            self.starRating = ratingArray[randomIndex]
        }
        var opinionComment = ""
        if opinion_notes != "Opinion...".localized
        {
            opinionComment = opinion_notes
        }
        let Request: WebserviceHelper = WebserviceHelper()
        let riderID = Request.getUserDefaultsStandard()
        var parameters = [
            "BookingId":rideId,
            "RiderId": riderID,
            "Rating" : self.starRating,
            "Comment" : opinionComment,
            "Language" : systemLanguage
            ] as [String : Any]
        if checkConnection(){
            showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            printParameterLog(apiName: KSET_DRIVER_RATING, parameter: parameters, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KSET_DRIVER_RATING, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    if responseContent != nil
                    {
                        let status = responseContent?.value(forKey: "Status") as? Bool
                        if(status == true)
                        {
                            UserDefaults.standard.set(nil, forKey: KRIDER_FEEDBACK_INFO)
                            UserDefaults.standard.synchronize()
                            
                            self.showSimpleAlertControllerWithOkAction(nil, message: responseContent!.value(forKey: "Message") as? String ?? "", callback: {
                                self.setRatingDidFinish()
                            })
                        }
                        else{
                            self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func submitButtonAction(_ sender: Any) {
        if self.starRating > 0.0 {
            self.view.endEditing(true)
            sendRatingToServer()
        }
        else {
            self.showSimpleAlertController(nil, message: "First rate please.".localized)
        }
        
    }
    
    //MARK: - Tableview methods
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingTitleCell") as! RatingTitleCell
            drawShadow(cell.shadowView)
            cell.descriptionTextView.attributedText = stringFromHtml(string: alertMessage, fontSize: "16", colorType: "#009578")
            
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingStarCell") as! RatingStarCell
            drawShadow(cell.shadowView)
            cell.titleLabel.text = "Your opinion is important to us.".localized
            cell.discriptionLabel.text = "Rating...".localized
            cell.ratingView.didFinishTouchingCosmos = {
                rating in
                self.starRating = rating
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingFeedbackCell") as! RatingFeedbackCell
            drawShadow(cell.shadowView)
            cell.opinionNoteField.delegate = self
            cell.opinionNoteField.autocorrectionType = .no
            cell.titleLable.text = "What we need to improve?".localized
            cell.opinionNoteField.tag = indexPath.row
            if opinion_notes != "Opinion...".localized
            {
                cell.opinionNoteField.text = opinion_notes
                cell.opinionNoteField.textColor = kDARK_OLIVE_COLOR
            }
            else
            {
                cell.opinionNoteField.text = opinion_notes.localized
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 180
        }
        return 144
    }
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.textColor == UIColor.darkGray {
            textView.text = nil
            textView.textColor = kDARK_OLIVE_COLOR
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        let value = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if value.isEmpty {
            textView.text = "Opinion...".localized
            textView.textColor = UIColor.darkGray
            opinion_notes = "Opinion...".localized
        }
        else
        {
            opinion_notes = textView.text!
            textView.setContentOffset(.zero, animated: false)
        }
    }
}
class RatingTitleCell: UITableViewCell {
    @IBOutlet var shadowView: UIView!
    @IBOutlet var descriptionTextView: UITextView!
}
class RatingStarCell: UITableViewCell {
    @IBOutlet var shadowView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var discriptionLabel: UILabel!
    @IBOutlet var ratingView: CosmosView!
}
class RatingFeedbackCell: UITableViewCell {
    @IBOutlet var shadowView: UIView!
    @IBOutlet var opinionNoteField: UITextView!
    @IBOutlet var titleLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        opinionNoteField.textColor = UIColor.darkGray
        opinionNoteField.text = "Opinion...".localized
    }
}



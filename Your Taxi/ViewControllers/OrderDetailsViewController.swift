

import UIKit
import KDCircularProgress

class OrderDetailsViewController: BaseViewController{

    var riderInfoDic: NSDictionary?
    
    @IBOutlet weak var circularProgressView: KDCircularProgress!
    @IBOutlet weak var timerLable: UILabel!
    @IBOutlet var riderDurationTitle: UILabel!
    @IBOutlet var riderDateTimeLabel: UITextField!
    @IBOutlet var riderDateTimeLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var riderDurationLable: UILabel!
    @IBOutlet weak var pickdtLable: UILabel!
    @IBOutlet weak var carTypeLable: UILabel!
    @IBOutlet weak var pickupLocationLable: UILabel!
    @IBOutlet weak var timeoutmgsView: UIView!
    @IBOutlet weak var orderTypeLabel : UILabel!
    @IBOutlet weak var orderType : UILabel!
    @IBOutlet var rideAcceptBTN: UIButton!
    @IBOutlet var rideRejectBTN: UIButton!
    @IBOutlet var rideMissedLable: UILabel!
    @IBOutlet var carTypeTitleInsBooking: NSLayoutConstraint!
    @IBOutlet var carTypeValueInsBooking: NSLayoutConstraint!
    
    @IBOutlet var carTypeTitlePreBooking: NSLayoutConstraint!
    @IBOutlet var carTypeValuePreBooking: NSLayoutConstraint!
    
    var timer = Timer()
    var currentCount = 0.0
    let maxCount = 20.0 //40.0
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var dicLocation : NSDictionary?
    var missedDicLocation : NSDictionary?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.title = "Ride Request".localized
        rideAcceptBTN.setTitle("Accept".localized, for: .normal)
        rideRejectBTN.setTitle("Reject".localized, for: .normal)
        rideMissedLable.text = "MissOrderMsgKey".localized
        circularProgressView.angle = 0
        
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
        })
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(OrderDetailsViewController.increaseProgressButtonTapped), userInfo: nil, repeats: true)
        
      print("riderInfoDic", riderInfoDic)
        var orderTypeValue = riderInfoDic?.value(forKey: "order_type") as? String
        if orderTypeValue == "Instant booking" || orderTypeValue == "Sofort Bestellung" || orderTypeValue == "Sofortbestellung" {
            orderTypeValue = "For Now".localized
            UserDefaults.standard.removeObject(forKey: "Time")
            UserDefaults.standard.removeObject(forKey: "rideStatus")
            UserDefaults.standard.synchronize()
            riderDurationLable.isHidden = false
            
            var rTotalTime = "0"
            var rTotalKm = "0.0"
            
            if let totalTime = riderInfoDic?.value(forKey: "total_time") as? String {
                rTotalTime = totalTime
            }
            
            if let totalKm = riderInfoDic?.value(forKey: "total_km") as? NSInteger {
                rTotalKm = "\(totalKm)"
            }
            
            if let totalKm = riderInfoDic?.value(forKey: "total_km") as? Double {
                rTotalKm = "\(totalKm)"
            }
            
            if let totalKm = riderInfoDic?.value(forKey: "total_km") as? NSString {
                rTotalKm = totalKm as String
            }
            
            
            let minsList = rTotalTime.components(separatedBy: " ")
            if minsList.count == 2 {
                let timeInt = Int(minsList[0])! + 3
                rTotalTime = "\(minsList[0]) - \(timeInt)"
            }
            
            //riderDurationLable.text = "\("Rider".localized) \(rTotalTime) & \(rTotalKm) km \("away".localized)"
            riderDurationLable.text = "\("PassengerTime".localized) \(rTotalTime) \("PassengerTotalTime".localized)"
            riderDateTimeLabelHeight.constant = 0
        }
        else{
            orderTypeValue = "Later Booking".localized
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.locale = Locale(identifier: "en_GB")
            let time = riderInfoDic!["pick_up_date"] as? String
            var dateObj = dateFormatter.date(from: time!)
            
            if dateObj == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
                dateObj = dateFormatter.date(from: time!)
            }
            
            dateFormatter.dateFormat = "dd MMMM yyyy"
            
            
            let dateString = dateFormatter.string(from: dateObj!)
            
            riderDurationLable.text=dateString
            
            dateFormatter.dateFormat = "HH:mm"
            
            let timeString = dateFormatter.string(from: dateObj!)
            riderDateTimeLabelHeight.constant = 30
            
            riderDateTimeLabel.text=timeString + " o'clock".localized
        }

        orderType.text = orderTypeValue
        print(riderInfoDic!)
        if let value = riderInfoDic?.value(forKey: "car_type") as? String {

        carTypeLable.text = value
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        if appdelegate.rideRequestStatus == .rideAccepted {
            acceptBtnAction(riderInfoDic!)
        }else if appdelegate.rideRequestStatus == .rideRejected {
            rejectBtnAction(riderInfoDic!)
        }
        
        appdelegate.rideRequestStatus = nil
    }
    
    func backAction(){
        handleTap()
    }
    
    func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func increaseProgressButtonTapped()-> Void {
        if currentCount != maxCount {
            currentCount += 1
            let newAngleValue = newAngle()
            circularProgressView.animate(Double(newAngleValue), duration: 1.0, completion: nil)
            let str = NSString(format:"%.0fs", currentCount)
            timerLable.text=str as String
        
        }else{
            delegate!.stopSound()
            if timer.isValid {
                timer.invalidate()
            }
            
            callMissedOrRejectAPI(status: 5)
        }
    }
    
    func showBackOption() {
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(self.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
    }

    func newAngle() -> Int {
        return Int(360 * (currentCount / maxCount))
    }

    @IBAction func acceptBtnAction(_ sender: AnyObject) {
    
        delegate!.stopSound()
        if checkConnection(){
            if timer.isValid {
                timer.invalidate()
            }
            
            UserDefaults.standard.synchronize()
            if (UserDefaults.standard.object(forKey: kNEW_LOCATION) != nil) {
                dicLocation = UserDefaults.standard.object(forKey: kNEW_LOCATION) as! NSDictionary
            }
           
            let parameters = [
                "Platform":1,
                "Status":1,
                "DriverId":getLoginDriverID(),
                "RideId": riderInfoDic?.value(forKey: "ride_id") as? NSInteger ?? 0,
                "Latitude" : dicLocation?.value(forKey: "lat"),
                "Longitude" : dicLocation?.value(forKey: "long")
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            printParameterLog(apiName: KACCEPTREJECT_URL, parameter: parameters, headerDict: getRequestHeaders())
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KACCEPTREJECT_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Error", message: error?.localized ?? "")
                    return;
                }
                
                let responseObject = getResponseDictionaryWithResult(result: result)
                print(responseObject)
                
                if let status = responseObject["Status"] as? Bool {
                    
                    if status {
                        
                        self.showSimpleAlertControllerWithOkAction(nil, message: responseObject["Message"] as? String ?? "Ride accepted successfully.", callback: { action -> Void in
                            self.dismiss(animated: false, completion: {
                                if UIApplication.topViewController()?.navigationController != nil {
                                    let array = UIApplication.topViewController()?.navigationController?.viewControllers
                                    for view in array! {
                                        if view is HomeViewController {
                                            UIApplication.topViewController()?.navigationController?.popToViewController(view, animated: false)
                                        }
                                    }
                                }
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "InstantBooking"), object: nil)
                                return
                            })
                        })
                    }
                    
                    else {
                        self.alreadyLogoutAction(response: responseObject as! [String : AnyObject])
                    }
                }
            }
        }
        else {
            showAlertForInternetConnectionOff()
        }
    }
    
    
    func alreadyLogoutAction(response:[String : AnyObject]){
        //  : Handle Driver Logout
        if let value = response["is_already_logout"] as? String {
            if value == "1" && UserDefaults.standard.bool(forKey: KRIDER_LOGIN) && !KAPP_RIDER{
                self.delegate?.forceFullyLogoutDriverAlert(response["Message"] as! String)
            }
        }
        else{
            if let message = response["Message"] as? String {
                self.showSimpleAlertControllerWithOkAction(nil, message: message, callback: { [weak self] in
                    self?.dismiss(animated: true, completion: nil)
                })
            }
        }
    }
    
    @IBAction func rejectBtnAction(_ sender: AnyObject) {
        callMissedOrRejectAPI(status: 2)
    }
    
    func callMissedOrRejectAPI(status : NSInteger) {
        
        delegate!.stopSound()
        if checkConnection(){
            if timer.isValid {
                timer.invalidate()
            }
            UserDefaults.standard.synchronize()
            if (UserDefaults.standard.object(forKey: kNEW_LOCATION) != nil) {
                missedDicLocation = UserDefaults.standard.object(forKey: kNEW_LOCATION) as! NSDictionary
            }

            let parameters = [
                "Platform":1,
                "Status":status,
                "DriverId":getLoginDriverID(),
                "RideId": riderInfoDic?.value(forKey: "ride_id") as? NSInteger ?? 0,
                "Latitude" : dicLocation?.value(forKey: "lat"),
                "Longitude" : dicLocation?.value(forKey: "long")
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            printParameterLog(apiName: KACCEPTREJECT_URL, parameter: parameters, headerDict: getRequestHeaders())
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KACCEPTREJECT_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Error", message: error?.localized ?? "")
                    return;
                }
                
                let responseContent = getResponseDictionaryWithResult(result: result)
                print(responseContent)
                
                
                if status == 5 {//Missed Ride API
                    self.timeoutmgsView.isHidden=false
                    self.view.bringSubview(toFront: self.timeoutmgsView)
                    self.showBackOption()
                    return
                }
                
                if let message = responseContent["Errors"] as? String {
                    self.showSimpleAlertControllerWithOkAction(nil, message: message, callback: {[weak self] in
                        self?.dismiss(animated: true, completion: nil)
                    })
                    return
                }
                
                let status = responseContent["status"] as? Bool ?? false
                if status {
                    if let message = responseContent["Message"] as? String {
                        self.showSimpleAlertControllerWithOkAction(nil, message: message, callback: {[weak self] in
                            self?.dismiss(animated: true, completion: nil)
                        })
                    }
                }
                else {
                    self.alreadyLogoutAction(response: responseContent as! [String : AnyObject])
                }
            }
        }
        else{
            showAlertForInternetConnectionOff()
        }
    }
}

 import UIKit
 import SlideMenuControllerSwift
 import AccountKit
 import MapKit
 import INTULocationManager
 import KDCircularProgress
 import CoreLocation
 import Crashlytics
 import GoogleMaps
 
 let CURRENTRIDEWSTAG = 1
 let LOGOUTWSTAG = 2
 let CURRENTTRIPWSTAG = 3
 let CANCELRIDETAG = 4
 let RIDEDETAILS = 5
 let PENDINGRIDE = 6
 var rotation = 0.0
 let MERCATOR_RADIUS = 85445659.44705395
 let MAX_GOOGLE_LEVELS = 20.0
 class HomeViewController: BaseViewController, CLLocationManagerDelegate, UITextFieldDelegate{
    
    // MARK: - UIButton Properties
    @IBOutlet var rideMapView: GMSMapView!
    
    @IBOutlet var currentRideButton: UIButton!
    @IBOutlet var driverPaymentButton: UIButton!
    @IBOutlet var pendingRideButton : UIButton!
    @IBOutlet var myRideButton: UIButton!
    @IBOutlet var bookRideButton: UIButton!
    @IBOutlet var logoutButton: UIButton!
    @IBOutlet var pinButton: UIButton!
    @IBOutlet weak var changeCarBtn : UIButton!
    @IBOutlet weak var editPickupLocation : UIButton!
    @IBOutlet weak var dropUpAddressBtn: UIButton!
    @IBOutlet var pickUpAddressBtn: UIButton!
    @IBOutlet weak var editDropupLocation : UIButton!
    @IBOutlet var deleteDriverProfileBtn: UIButton!
    
     let movement = ARCarMovement()
    
    @IBOutlet var laterBookingCount: UILabel!
    @IBOutlet var laterBookingCountWidth: NSLayoutConstraint!
    
    // MARK: - UIView Properties
    @IBOutlet weak var PinView: UIView!
    @IBOutlet weak var activeCarView : UIView!
    @IBOutlet weak var carTypeView : UIView!
    @IBOutlet weak var rideDetailsView : UIView!
    @IBOutlet weak var rideDetailsCornerView : UIView!
    @IBOutlet weak var timeView : UIView!
    @IBOutlet weak var pickUpView : UIView!
    @IBOutlet weak var dropUpView : UIView!
    
    // MARK: - UILabel Properties
    @IBOutlet weak var carName : UILabel!
    @IBOutlet weak var carTypeName : UILabel!
    @IBOutlet weak var plateNo : UILabel!
    @IBOutlet weak var driverName : UILabel!
    @IBOutlet weak var reachTimelbl: UILabel!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var refLabel : UILabel!
    @IBOutlet var pinBottomLbl: UILabel!
    
    @IBOutlet var lblPickupTitle: UILabel!
    @IBOutlet var lblDestinationTitle: UILabel!
    
    @IBOutlet var imgPickupLine: UIImageView!
    
    
    @IBOutlet var carTypeBudgetBtn: UIButton!
    @IBOutlet var carTypeBusinessBtn: UIButton!
    @IBOutlet var carTypeComfortBtn: UIButton!
    // MARK: - Bottom Popup Properties
    
    @IBOutlet var budgetButton: UIButton!
    @IBOutlet var businessButton: UIButton!
    @IBOutlet var comfortButton: UIButton!
    @IBOutlet var budgetTimeLbl: UILabel!
    @IBOutlet var businessTimeLbl: UILabel!
    @IBOutlet var comfortTimeLbl: UILabel!
    @IBOutlet var budgetFixPriceLbl: UILabel!
    @IBOutlet var businessFixPriceLbl: UILabel!
    @IBOutlet var comfortFixPriceLbl: UILabel!
    @IBOutlet var budgetPersonLbl: UILabel!
    @IBOutlet var businessPersonLbl: UILabel!
    @IBOutlet var comfortPersonLbl: UILabel!
    @IBOutlet var categoryTimeView: UIView!
    @IBOutlet var categoryTimeSubView: UIView!
    @IBOutlet var bugetCarTimeLbl: UILabel!
    @IBOutlet var businessCarTimeLbl: UILabel!
    @IBOutlet var comfortCarTimeLbl: UILabel!
    @IBOutlet var bugetTimeIndicator: UIActivityIndicatorView!
    @IBOutlet var businessTimeIndicator: UIActivityIndicatorView!
    @IBOutlet var comfortTimeIndicator: UIActivityIndicatorView!
    
    // MARK: - Other Properties
    @IBOutlet weak var pinImage : UIImageView!
    @IBOutlet weak var driverProfileImage : UIImageView!
    
    @IBOutlet weak var driverProfileIndicator : UIActivityIndicatorView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pickUpAddressbtn: UITextField!
    @IBOutlet weak var dropUpAddressField: UITextField!
    @IBOutlet var pinViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Define Variables
    var driverActiveCarsList = [ActiveCar]()
    
    var driverSwitch : UISwitch!
    var driverStatus : UILabel!
    let locationManager = CLLocationManager()
    var identifier = ""
    var polyline = GMSPolyline()
    var sourceMarker = GMSMarker()
    var destinationMarker = GMSMarker()
//    var mapView : MKMapView!
    var accountKit: AKFAccountKit!
    var isInitialized = false
    var pickUpLocationData = SelectedLocation()
    var dropUpLocationData = SelectedLocation()
    var reachTimeToRider = ""
    var isEditingLocation : Bool = false
    var PointAnnotation : CustomAnnotation!
    var PointAnnotationView : MKAnnotationView!
    var changeTextOf = "PickUp"
    var carType : String = "1"
    var fromBookAlert = " "
    var driverAnnotation : CustomAnnotation!
    var pinAnnotationView:MKAnnotationView!
    var fileurl : URL!
    var pickUpisSet : Bool = false
    var dropUpisSet : Bool = false
    var isCarAvailable : Bool = false
    var localAllowed: Bool = false
    var isSetDropPin: Bool = false
    var priceMinDetailsDict : NSDictionary!
    var driverListDict : NSDictionary! = NSDictionary()
    var carCategoryTimeDict : NSDictionary?
    var resultArray: NSMutableArray = []
    var userIDString: String?
    var userIDDisString: String?
    var FareresultArray: NSMutableArray = []
    var FareEstimationresultArray: NSMutableArray = []
    var customDriverListDict : NSDictionary! = NSDictionary()
    let mapManager = MapManager()
    var registerDict : NSMutableDictionary!
    var tempDict : NSMutableDictionary!
    var tempDictNew : NSMutableDictionary!
    var isPerformLogout : Bool = false
    var isAddressSelected : Bool = false
    var pickUpDict : NSMutableDictionary = ["City" : "" , "Country" : ""]
    var rotation = 0.0
    var btnPaymentYOURTAXI = ""
    
    // MARK: - Overrided Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        movement.delegate = self
        homeVcDidLoad = true
        identifier = "MyPin"
        self.navigationItem.setHidesBackButton(true, animated: false)
        delegate?.homeViewController = self
        
        if KAPP_RIDER {
            
            self.dropUpAddressBtn.isEnabled = false
            
            rideMapView.delegate = self
            rideMapView.settings.rotateGestures = false
            rideMapView.isMyLocationEnabled = false

            do {
                //self.animateViewToCurrentLocation(coordinate: (locationManager.location?.coordinate)!)
            } catch {
                print(error.localizedDescription)
            }

            imgPickupLine.isHidden = false
            dropUpView.isHidden = false
            refLabel.isHidden = true
            self.setScreenLayout()
            self.showHUD()
            dropUpAddressField.text = "Set Destination.".localized
            editPickupLocation.isHidden = true
            editDropupLocation.isHidden = true
            isEditingLocation = false
            
            self.lblPickupTitle.text = "From:".localized
            self.lblDestinationTitle.text = "To:".localized
            
            self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!)
            carTypeView.isHidden = false

            PinView.isHidden = false
            timeView.isHidden = true
            if CLLocationManager.locationServicesEnabled() {
                switch(CLLocationManager.authorizationStatus()) {
                case .notDetermined, .restricted, .denied:
                    localAllowed = true
                    let alertController = UIAlertController(
                        title: "Background Location Access Disabled".localized,
                        message: "In order to be notified, please open this app's settings and set location access to 'Always'.".localized,
                        preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    
                    let openAction = UIAlertAction(title: "Open Settings".localized, style: .default) { (action) in
                        if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(url as URL)
                        }
                    }
                    alertController.addAction(openAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                case .authorizedAlways, .authorizedWhenInUse:
                    
                    if (delegate?.userLocation?.coordinate != nil){
                        
                        let centerCoordinate = CLLocationCoordinate2DMake((delegate?.userLocation?.coordinate.latitude)!, (delegate?.userLocation?.coordinate.longitude)!)
                        self.setRegionOnMapview(centerCoordinate)
                        
                        self.pickUpLocationData.setLocationData(latitude: "\(String(describing: delegate?.userLocation?.coordinate.latitude))", longitude: String(describing: delegate?.userLocation?.coordinate.longitude), address: " ")
                        
                        
                        self.hideHUD()
                    }
                    else
                    {
                        locationManager.delegate = self
                        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
                        locationManager.requestAlwaysAuthorization()
                        locationManager.startUpdatingLocation()
                        
                        delegate?.userLocation = self.locationManager.location
                        
                        if(delegate?.userLocation != nil){
                            let centerCoordinate = CLLocationCoordinate2DMake((delegate?.userLocation?.coordinate.latitude)!, (delegate?.userLocation?.coordinate.longitude)!)
                            
                            self.setRegionOnMapview(centerCoordinate)
                        }
                        self.hideHUD()
                    }
                }
            } else {
                print("Location services are not enabled".localized)
            }
            
            self.rideDetailsView.isHidden = true
            
            rideDetailsCornerView.layer.borderColor = UIColor.lightGray.cgColor
            rideDetailsCornerView.layer.borderWidth = 1.0
            rideDetailsCornerView.layer.cornerRadius = 4.0
            rideDetailsCornerView.layer.masksToBounds = true
            
            categoryTimeSubView.layer.borderColor = kDARK_OLIVE_COLOR.cgColor
            categoryTimeSubView.layer.borderWidth = 1.0
            
            
            //bugetCarTimeLbl.textColor = UIColor.black
            bugetCarTimeLbl.text = "-"
            
            
            //businessCarTimeLbl.textColor = UIColor.black
            businessCarTimeLbl.text = "-"
            
            
            //comfortCarTimeLbl.textColor = UIColor.black
            comfortCarTimeLbl.text = "-"
        }
        else
        {
            laterBookingCount.isHidden = true
            laterBookingCount.layer.borderWidth = 2
            laterBookingCount.layer.borderColor = UIColor.white.cgColor
            laterBookingCountWidth.constant = laterBookingCount.frame.height
            laterBookingCount.layer.cornerRadius = laterBookingCount.frame.height/2
            laterBookingCount.layer.masksToBounds = true
            laterBookingCount.textAlignment = .center
            laterBookingCount.layoutIfNeeded()
        }
        setData()
        if !KAPP_RIDER{
        self.checkAppVersion()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        delegate?.carType = "1"
        if KAPP_RIDER
        {
            self.PinView.layer.borderWidth = 3
            self.PinView.layer.borderColor = UIColor.white.cgColor
            self.title = "YOURTAXI".localized
            
            rideDetailsView.isHidden = pickUpisSet && dropUpisSet ? false : true
            
            slideMenuController()?.addLeftGestures()
            NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.removeAnnotation(_:)), name: NSNotification.Name(rawValue: "RideBooked"), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "dropUpLocationSelected"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.setDropUpPin(_:)), name:NSNotification.Name(rawValue: "dropUpLocationSelected"), object: nil)
            
            self.setNavigationBarTitle()
        }
        else
        {
            preBookingCountApi()
            getCarDetails()
            self.title = "Driver".localized
            NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "InstantBooking"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.currentRideAction(_:)), name: NSNotification.Name(rawValue: "InstantBooking"), object: nil)
        }
        
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "NotifyAppVersion"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkAppVersion), name: NSNotification.Name(rawValue: "NotifyAppVersion"), object: nil)
        
        locationManager.startUpdatingLocation()
        locationManager1.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if KAPP_RIDER {
            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
            if appDelegate.resultArray.count != 0//allAnnotations.count != 0
            {
                if pickUpLocationData.latitude != nil && dropUpLocationData.address != nil && changeTextOf != "PickUp" && changeTextOf != "DropUp"
                {
                    if !isSetDropPin{
                        openCarPriceAndMinDetailsView()
                    }
                }
                else{
                    carTypeView.isHidden = false
                }
            }
            isSetDropPin = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title = ""
        if(KAPP_RIDER){
            locationManager.stopUpdatingLocation()
            locationManager1.stopUpdatingLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func checkAppVersion() {
        var appName = "ios_"
        if KAPP_RIDER{
            appName = appName+"rider"
        }else{
            appName = appName+"driver"
        }
        
        let parameters = [
            "AppName":appName,"Language":systemLanguage] as [String : Any]
        
        self.showHUD()
        
        let Request: WebserviceHelper = WebserviceHelper()
        
        let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
        
        let headerDict = KAPP_RIDER ? Request.getHeaderForRider(parameters: parameters) : getRequestHeaders()
        
        Request.CustomHTTPPostJSONWithHeader(url: KAPPVERSION, jsonString: requestJsonString as NSString, withIdicator: false, header:headerDict) { (result, error) in
            
            self.hideHUD()
            
            if error != nil {
                self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                return;
            }
            let responseObject : NSDictionary = self.convertStringToDictionary(text: result)!
            
            let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
            let responseContent : [String:Any] = convertStringToArray(text: decryptedData!)![0] as! [String : Any]
            print("response----",responseContent )
            
            //Get the Result Set
            let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"]! as! String
            print(appVersion)
            
            let appStoreVersion : Double = Double(responseContent["Version"] as! String)!
            if (Double(appVersion)! < appStoreVersion) {
                self.showSimpleAlertControllerWithOkAction("Alert", message: "ForceUpdateMessage".localized, callback: {
                    
                    var urlString = ""
                    if KAPP_RIDER{
                        urlString = "https://itunes.apple.com/us/app/your-taxi/id1116102714?ls=1&mt=8"
                    }else{
                        urlString = "https://itunes.apple.com/us/app/your-taxi-driver/id1116102701?ls=1&mt=8"
                    }
                    
                    if let url = URL(string: urlString) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            if UIApplication.shared.canOpenURL(url as URL) {
                                UIApplication.shared.openURL(url as URL)
                            }
                        }
                    }
                    
                })
            }
        }
    }
    func removeAnnotation(_ notification : Notification) {
        print("Ride Booked")
    }
    
    func setOptionTitles() {
        
        let titleDeleteProfile = "DELETE PROFILE".localized
        deleteDriverProfileBtn.setTitle(titleDeleteProfile, for: UIControlState())
        
        let titleChangeCar = "Change Car".localized
        changeCarBtn.setTitle(titleChangeCar, for: UIControlState())
        
        let titleCurrnetRide = "Instant Booking".localized
        currentRideButton.setTitle(titleCurrnetRide, for: UIControlState())
        
        
        btnPaymentYOURTAXI = "Payment to Yourtaxi".localized
        btnPaymentYOURTAXI = btnPaymentYOURTAXI.replacingOccurrences(of: "YOURTAXI", with: "<i>YOUR</i><b>TAXI</b>")
        driverPaymentButton.setAttributedTitle(stringFromHtml(string: btnPaymentYOURTAXI, fontSize: "16", colorType: "#ffffff"), for: .normal)
        
        let titleLogout = "LogoutKey".localized
        logoutButton.setTitle(titleLogout, for: UIControlState())
    }
    
    // MARK: - SETUP DATA
    func setData() {
        
        currentRideButton.titleLabel?.adjustsFontSizeToFitWidth = true;
        currentRideButton.layer.cornerRadius = 20;
        currentRideButton.clipsToBounds = true;
        
        logoutButton.titleLabel?.adjustsFontSizeToFitWidth = true;
        logoutButton.layer.cornerRadius = 20;
        logoutButton.clipsToBounds = true;
        if KAPP_RIDER{
            myRideButton.setTitle("RIDE HISTORY".localized, for: UIControlState())
            bookRideButton.setTitle("BOOK A RIDE".localized, for: UIControlState())
            myRideButton.titleLabel?.adjustsFontSizeToFitWidth = true;
            myRideButton.layer.cornerRadius = 20;
            myRideButton.clipsToBounds = true;
            bookRideButton.titleLabel?.adjustsFontSizeToFitWidth = true;
            bookRideButton.layer.cornerRadius = 20;
            bookRideButton.clipsToBounds = true;
        }else{
            setOptionTitles()
            driverPaymentButton.titleLabel?.adjustsFontSizeToFitWidth = true;
            driverPaymentButton.layer.cornerRadius = 20;
            driverPaymentButton.clipsToBounds = true;

            // Driver availability switch
            let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as? String
            let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
            let userDict = self.convertStringToDictionary(text: decryt!)!
            
            //Kalai
            print(userDict)
            driverSwitch = UISwitch()
            driverSwitch.tintColor = UIColor.white
            driverSwitch.onTintColor = UIColor(red: 59/255.0, green: 146/255.0, blue: 27/255.0, alpha: 1.0)
            driverSwitch.thumbTintColor = UIColor.white
            driverSwitch.addTarget(self, action: #selector(HomeViewController.driverSwitchChangeAction(_:)), for: UIControlEvents.valueChanged)
            let rightBarButton = UIBarButtonItem(customView: driverSwitch)
            self.navigationItem.rightBarButtonItem = rightBarButton
            
            driverStatus = UILabel(frame: CGRect(x: 0, y: 0, width: 90, height: 40))
            driverStatus.textColor = UIColor.white
            driverStatus.text = "Online".localized
            let leftBarButton = UIBarButtonItem(customView: driverStatus)
            self.navigationItem.leftBarButtonItem = leftBarButton
            
            if userDict.value(forKey: "Availability") as? NSInteger == 1 {
                driverSwitch.setOn(true, animated: false)
                driverStatus.text = "Online".localized
                locationManager1.startUpdatingLocation()
            }
            else {
                driverSwitch.setOn(false, animated: false)
                driverStatus.text = "Offline".localized
                locationManager1.stopUpdatingLocation()
            }
           
        }
    }
    func callUserInformationAPIWithUserID(userID: NSInteger) {
        
        let parameters = [
            "UserId":userID] as [String : Any]
        
        self.showHUD()
        
        let Request: WebserviceHelper = WebserviceHelper()
        
        let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
        
        Request.CustomHTTPPostJSONWithHeader(url: kGET_DRIVER_DETAILS, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
            
            self.hideHUD()
            
            if error != nil {
                self.hideHUD()
                self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                return;
            }
            let responseContent = getResponseDictionaryWithResult(result: result)
            print(responseContent)
            if self.setLoginUserDataWithDictionary(responseDict: responseContent){
                self.setData()
                self.getCarDetails()
            }
        }
    }
    
    func setLoginUserDataWithDictionary(responseDict: NSDictionary) -> Bool {
        
        print(responseDict)
        
        if (responseDict["user"] as? NSDictionary) != nil {
            
            let kUserDict = responseDict["user"] as! NSDictionary
            
            encrypt(data: kUserDict, key: KUSER_DETAILS)
            UserDefaults.standard.set(true, forKey: KRIDER_LOGIN)
            
            if let bankDict = responseDict["bank"] as? NSDictionary {
                encrypt(data: bankDict, key: KUSER_BANK_DETAILS)
            }
            
            UserDefaults.standard.synchronize()
            return true
        }
        
        self.showSimpleAlertController("Fail".localized, message:"Server response error".localized)
        return false
    }
    
    
    //MARK:- API CALLING
    func preBookingCountApi(){
        
        if(checkConnection()){
            
            let parameters = [
                "UserId":getLoginDriverID()] as [String : Any]
            
            self.showHUD()
            
            let Request: WebserviceHelper = WebserviceHelper()
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: PREBOOKING_COUNT, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    print(error?.localized ?? "")
                    self.showSimpleAlertController("Alert", message: error?.localized ?? "")
                }
                
                let responseContent = getResponseDictionaryWithResult(result: result)
                print(responseContent)
                
                guard let count = responseContent["TotalRides"] as? NSInteger else {
                    self.laterBookingCount.text = "0"
                    self.laterBookingCount.isHidden = true
                    return
                }
                self.laterBookingCount.text = count > 0 ? "\(count)" : "0"
                self.laterBookingCount.isHidden = true
            }
        }
    }
    
    // MARK: - SETUP UI METHODS
    func setScreenLayout() {
        carTypeBudgetBtn.backgroundColor = kLIGHT_OLIVE_COLOR
        carTypeBudgetBtn.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        carTypeBudgetBtn.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        
        carTypeBusinessBtn.backgroundColor = kLIGHT_OLIVE_COLOR
        carTypeBusinessBtn.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        carTypeBusinessBtn.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        
        carTypeComfortBtn.backgroundColor = kLIGHT_OLIVE_COLOR
        carTypeComfortBtn.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        carTypeComfortBtn.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        
        carTypeBudgetBtn.backgroundColor = kLIGHT_SELECT_COLOR
        carTypeBudgetBtn.tag = 2
    }
    
    func setNavigationBarTitle() {
        let titleLabel = UILabel()
        
        let italicAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.italicSystemFont(ofSize: 17)]
        let boldAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 17)]
        
        let partOne = NSMutableAttributedString(string: "YOUR", attributes: italicAttributes)
        let partTwo = NSMutableAttributedString(string: "TAXI", attributes: boldAttributes)
        
        let combination = NSMutableAttributedString()
        combination.append(partOne)
        combination.append(partTwo)
        
        titleLabel.attributedText = combination
        titleLabel.sizeToFit()
        self.navigationItem.titleView = titleLabel
    }
    
    
    func setDropUpPin(_ notification : Notification) {
        
        if let dic = notification.object as? NSMutableDictionary {
            isAddressSelected = true
            let value = dic["address_type"] as! String
            DispatchQueue.main.async {
                let coordinates = dic["coordinates"] as! CLLocationCoordinate2D
                let address = dic["address"] as! String
                print("Address :", address)
                
                CLGeocoder().reverseGeocodeLocation(CLLocation.init(latitude: coordinates.latitude, longitude: coordinates.longitude), completionHandler: {(placemarks, error) -> Void in
                    
                    if error != nil {
                        print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                        self.hideHUD()
                        return
                    }
                    
                    if (placemarks?.count)! > 0 {
                        
                        var placeMark: CLPlacemark!
                        placeMark = placemarks?[0]
                        var countryName = placeMark.country
                        let countryISOCode = placeMark.isoCountryCode
                        var city = countryISOCode == "IN" || countryISOCode == "in" ? placeMark.subAdministrativeArea : placeMark.administrativeArea
                        if city == nil {
                            city = "unknown city"
                        }
                        
                        if countryName == nil {
                            countryName = "unknown country"
                        }
                        
                        if value == "drop_up" {
                            let pickCountry = self.pickUpDict["Country"] as! String
                            let pickCity = self.pickUpDict["City"] as! String
                            
                            self.setServiceLocationAvailability(type: 2, pickupCity: pickCity, pickupCountry: pickCountry, dropCity: city!, dropCountry: countryName!, notification, isFromMapView: false, latitude: coordinates.latitude, longitude: coordinates.longitude)
                            print("city:", pickCountry + "  " + pickCity)
                        } else {
                            self.setServiceLocationAvailability(type: 1, pickupCity: city!, pickupCountry:countryName!, dropCity: "", dropCountry: "", notification, isFromMapView: false, latitude: coordinates.latitude, longitude: coordinates.longitude)
                            self.pickUpDict.setValue(city, forKey: "City")
                            self.pickUpDict.setValue(countryName, forKey: "Country")
                        }
                    }
                })
            }
        }
            
        else {
            if let str = notification.object as? String {
                
                DispatchQueue.main.async {
                    
                    self.changeTextOf = str
                    
                    self.pinImage.image = UIImage(named: str == "DropUp" ? "dropupPin" : "pickupPin")
                    self.PinView.backgroundColor = str == "DropUp" ? UIColor.red : kLIGHT_OLIVE_COLOR
                    self.titlelbl.text = str == "DropUp" ? "Set Destination".localized : "Set pickup".localized
                    
                    self.pinImage.isHidden = false
                    self.PinView.isHidden = false
                    self.timeView.isHidden = self.changeTextOf == "PickUp" ? false : true
                    //                        self.timeView.isHidden = true
                    
                    if str == "DropUp" {
                        self.pickUpAddressbtn.isEnabled = false
                    }else {
                        self.dropUpAddressBtn.isEnabled = false
                    }
                }
            }
        }
    }
    
    //MARK:- CUSTOM METHODS
    func ProceedWithSelectLocation(_ notification : Notification)
    {
        isSetDropPin = true
        if let dic = notification.object as? NSMutableDictionary
        {
            let value = dic["address_type"] as! String
            if value == "drop_up"{
                DispatchQueue.main.async {
                    let coordinates = dic["coordinates"] as! CLLocationCoordinate2D
                    let address = dic["address"] as! String
                    self.dropUpisSet = true
                    self.dropPinOnLocation(coordinates: coordinates, address: address)
                    self.dropUpAddressBtn.isEnabled = false
                    self.editPickupLocation.isUserInteractionEnabled = true
                    self.editDropupLocation.isHidden = false
                }
            }
            else {
                DispatchQueue.main.async {
                    print("set pick up address")
                    let coordinates = dic["coordinates"] as! CLLocationCoordinate2D
                    let address = dic["address"] as! String
                    self.pickUpAddressbtn.text = address
                    self.pickUpLocationData.setLocationData(latitude: "\(coordinates.latitude)", longitude: "\(coordinates.longitude)", address: address.trimmingCharacters(in: .whitespaces))
                    self.proceedWithMapLocation()
                    self.setRegionOnMapview(coordinates)
                }
            }
        }
        else {
            if let str = notification.object as? String {
                DispatchQueue.main.async {
                    self.changeTextOf = str
                    self.pinImage.image = UIImage(named: str == "DropUp" ? "dropupPin" : "pickupPin")
                    self.PinView.backgroundColor = str == "DropUp" ? UIColor.red : kLIGHT_OLIVE_COLOR
                    self.titlelbl.text = str == "DropUp" ? "Set Destination".localized : "Set pickup".localized
                    self.pinImage.isHidden = false
                    self.PinView.isHidden = false
                    self.timeView.isHidden = true
                    if str == "DropUp" {
                        self.pickUpAddressbtn.isEnabled = false
                    }else {
                        self.dropUpAddressBtn.isEnabled = false
                    }
                }
            }
        }
    }

    func didFinishPriceInformation(coordinates: CLLocationCoordinate2D?, address : String) {
        
        self.dropUpAddressField.text = address
        
        self.dropUpLocationData.setLocationData(latitude: "\(coordinates!.latitude)", longitude: "\(coordinates!.longitude)", address: address)
        
        destinationMarker.map = nil
        destinationMarker = GMSMarker()
        destinationMarker.position = CLLocationCoordinate2D(latitude: (self.dropUpLocationData.latitude as NSString).doubleValue, longitude: (self.dropUpLocationData.longitude as NSString).doubleValue)
        destinationMarker.icon = UIImage(named:  "dropupPin")
        destinationMarker.map = rideMapView
    }
    
    func dropPinOnLocation(coordinates: CLLocationCoordinate2D?, address : String) {
        if(checkConnection()) {
            if coordinates != nil {
                self.hideHUD()
                self.pinImage.isHidden = true
                self.editDropupLocation.isHidden = false
                let pickup_latlong = "\(self.pickUpLocationData.latitude!),\(self.pickUpLocationData.longitude!)"
                let dropup_latlong = "\(coordinates!.latitude),\(coordinates!.longitude)"
                self.isEditingLocation = false
                self.changeTextOf = " "
                
                var parameters = [
                    "PickupLatitude": pickUpLocationData.latitude!,
                    "DropLatitude": coordinates!.latitude,
                    "PickupLongitude":pickUpLocationData.longitude!,
                    "DropLongitude":coordinates!.longitude,
                    //"RideType": "2",
                    "RentalPlanId": "",
                    // "CarTypeId": "1"
                    ] as [String : Any]
                let jsonString = returnJsonString(param: parameters)
                var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                parameters = ["data":encryptedData!]
                let jsonParam = returnJsonString(param: parameters)
                self.showHUD()
                let Request: WebserviceHelper = WebserviceHelper()
                let headerDict = Request.getHeaderForRider(parameters: parameters)
                printJsonStringLog(apiName: KGET_MIN_PRICE, parameter: jsonParam as NSString, headerDict: headerDict)
                Request.CustomHTTPPostJSONWithHeader(url: KGET_MIN_PRICE, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                    self.hideHUD()
                    if ((error) != nil) {
                        print(error.debugDescription)
                    }else{
                        if result == "[]" {
                            self.showSimpleAlertController(nil, message: "Something Went Wrong".localized)
                            self.hideHUD()
                            return
                        }
                        let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                        
                        guard let data = responseObject.value(forKey: "data") as? String else {
                            self.showSimpleAlertController(nil, message: "Something Went Wrong".localized)
                            self.hideHUD()
                            return
                        }
                        
                        let decryptedData = AESCrypt.decrypt(data, password: SECRET_KEY)
                        print("DropPinOnLocation GetFareEstimate :", decryptedData ?? "")
                        
                        guard let list = self.convertToDictionary(text: decryptedData!) as? [[String:Any]]  else {
                            
                            var alertMsg = "Something went wrong".localized
                            if let list = self.convertToDictionary(text: decryptedData!) as? NSDictionary, let message = list["Message"] as? NSString {
                                alertMsg = message as String
                            }
                            //                            self.showRouteOnMap()
                            self.showSimpleAlertController(nil, message: alertMsg)
                            
                            if self.changeTextOf == "PickUp" {
                                self.pickUpAddressbtn.text = "Choose Location"
                            }else {
                                self.dropUpAddressField.text = "Choose Location"
                            }
                            
                            return
                        }
                        
                        self.didFinishPriceInformation(coordinates: coordinates, address: address)
                        
                        self.FareresultArray = NSMutableArray()
                        for data in list {
                            self.FareresultArray.add(data as [String:Any])
                        }
                        if self.FareresultArray.count > 0{
                            var status = "success" as! String
                            if status == "success"{
                                for i in 0..<self.FareresultArray.count{
                                    self.customDriverListDict = self.FareresultArray.object(at: i) as! NSDictionary
                                }
                                self.priceMinDetailsDict =  self.customDriverListDict
                                self.driverListDict = self.customDriverListDict
                                
                                
                                self.hideHUD()
                                self.showRouteOnMap()
                                self.openCarPriceAndMinDetailsView()
                                
                            }else{
                                //When admin delete rider profile
                                let responseContent = Request.convertStringToDictionary(text: result )
                                if let isDeleted = (responseContent?.value(forKey: "response") as! NSDictionary).value(forKey: "flag") as? Int{
                                    if(isDeleted == 1){
                                        self.moveToSingupController(message: (responseContent?.value(forKey: "response") as! NSDictionary).value(forKey: "message") as? String ?? "")
                                    }
                                }else{
                                    let messageAlert = responseContent?.value(forKey: "response") as? NSDictionary
                                    self.showSimpleAlertController(nil, message: messageAlert?.value(forKey: "Message") as? String ?? "")
                                }
                            }
                        }
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func openCarPriceAndMinDetailsView() {
        
        print("openCarPriceAndMinDetailsView")
        if priceMinDetailsDict != nil{
            carTypeView.isHidden = true
            categoryTimeView.isHidden = true
            self.rideDetailsView.isHidden = true
            for (_, object) in self.FareresultArray.enumerated() {
                
                self.priceMinDetailsDict = object as? NSDictionary
                if let value = priceMinDetailsDict.value(forKey: "CarTypeId") as? Int32{
                    if(value == 1){
                        if let time = priceMinDetailsDict.value(forKey: "TimeToReach") as? String {
                            budgetTimeLbl.text = time.isEmpty ? "No taxi available".localized: time
                            let price = (priceMinDetailsDict.value(forKey: "Currency") as? String)! + " " + (priceMinDetailsDict.value(forKey: "RidePrice") as? String)!
                            budgetFixPriceLbl.text = price
                            let paragraphStyle = NSMutableParagraphStyle()
                            paragraphStyle.alignment = .center
                            
                            let myString = NSMutableAttributedString(string:"", attributes: [NSParagraphStyleAttributeName: paragraphStyle])
                            let attachment = NSTextAttachment()
                            attachment.image = #imageLiteral(resourceName: "Member")
                            attachment.bounds = CGRect(x: -1, y: -2, width: 16, height: 16)
                            let carCapacityAttachmentString = NSAttributedString(attachment: attachment)
                            myString.append(carCapacityAttachmentString)
                            let carCapacity = priceMinDetailsDict.value(forKey: "CarTypeCapacity") as? Int
                            let carTypeCapacity = NSMutableAttributedString(string:"x\(carCapacity ?? 0)")
                            myString.append(carTypeCapacity)
                            let breaketStr = NSAttributedString(string: "  ")
                            myString.append(breaketStr)
                            let bagCapacity = priceMinDetailsDict.value(forKey: "BagCapacity") as? Int
                            let attachment1 = NSTextAttachment()
                            attachment1.image = #imageLiteral(resourceName: "ic_baggage_estimate")
                            attachment1.bounds = CGRect(x: -1, y: -2, width: 16, height: 16)
                            let bagAttachmentString = NSAttributedString(attachment: attachment1)
                            myString.append(bagAttachmentString)
                            let myString1 = NSMutableAttributedString(string:"x\(bagCapacity!)")
                            myString.append(myString1)
                            self.budgetPersonLbl.attributedText = myString
                        }
                    }
                    
                    if(value == 2){
                        if let time = priceMinDetailsDict.value(forKey: "TimeToReach") as? String{
                            businessTimeLbl.text = time.isEmpty ? "No taxi available".localized: time
                            let price = (priceMinDetailsDict.value(forKey: "Currency") as? String)! + " " + (priceMinDetailsDict.value(forKey: "RidePrice") as? String)!
                            businessFixPriceLbl.text = price
                            
                            let myString = NSMutableAttributedString()
                            let attachment = NSTextAttachment()
                            attachment.image = #imageLiteral(resourceName: "Member")
                            attachment.bounds = CGRect(x: -1, y: -2, width: 16, height: 16)
                            let carCapacityAttachmentString = NSAttributedString(attachment: attachment)
                            myString.append(carCapacityAttachmentString)
                            let carCapacity = priceMinDetailsDict.value(forKey: "CarTypeCapacity") as? Int
                            let carTypeCapacity = NSMutableAttributedString(string:"x\(carCapacity ?? 0)")
                            myString.append(carTypeCapacity)
                            let breaketStr = NSAttributedString(string: "  ")
                            myString.append(breaketStr)
                            let bagCapacity = priceMinDetailsDict.value(forKey: "BagCapacity") as? Int
                            let attachment1 = NSTextAttachment()
                            attachment1.image = #imageLiteral(resourceName: "ic_baggage_estimate")
                            attachment1.bounds = CGRect(x: -1, y: -2, width: 16, height: 16)
                            let bagAttachmentString = NSAttributedString(attachment: attachment1)
                            myString.append(bagAttachmentString)
                            let myString1 = NSMutableAttributedString(string:"x\(bagCapacity!)")
                            myString.append(myString1)
                            self.businessPersonLbl.attributedText = myString

                        }
                    }
                    
                    if(value == 3){
                        if let time = priceMinDetailsDict.value(forKey: "TimeToReach") as? String {
                            comfortTimeLbl.text = time.isEmpty ? "No taxi available".localized: time
                            let price = (priceMinDetailsDict.value(forKey: "Currency") as? String)! + " " + (priceMinDetailsDict.value(forKey: "RidePrice") as? String)!
                            comfortFixPriceLbl.text = price
                            
                            let myString = NSMutableAttributedString()
                            let attachment = NSTextAttachment()
                            attachment.image = #imageLiteral(resourceName: "Member")
                            attachment.bounds = CGRect(x: -1, y: -2, width: 16, height: 16)
                            let carCapacityAttachmentString = NSAttributedString(attachment: attachment)
                            myString.append(carCapacityAttachmentString)
                            let carCapacity = priceMinDetailsDict.value(forKey: "CarTypeCapacity") as? Int
                            let carTypeCapacity = NSMutableAttributedString(string:"x\(carCapacity ?? 0)")
                            myString.append(carTypeCapacity)
                            let breaketStr = NSAttributedString(string: "  ")
                            myString.append(breaketStr)
                            let bagCapacity = priceMinDetailsDict.value(forKey: "BagCapacity") as? Int
                            let attachment1 = NSTextAttachment()
                            attachment1.image = #imageLiteral(resourceName: "ic_baggage_estimate")
                            attachment1.bounds = CGRect(x: -1, y: -2, width: 16, height: 16)
                            let bagAttachmentString = NSAttributedString(attachment: attachment1)
                            myString.append(bagAttachmentString)
                            let myString1 = NSMutableAttributedString(string:"x\(bagCapacity!)")
                            myString.append(myString1)
                            self.comfortPersonLbl.attributedText = myString
                        }
                    }
                }
            }
            let frame1 = CGRect(x: 0, y: self.view.frame.size.height, width: 0, height: 0)
            let originalFrame1 = self.rideDetailsView.frame
            self.rideDetailsView.frame = frame1
            
            UIView.animate(withDuration: 3.0, delay: 0, options:.curveEaseInOut, animations: {
                self.rideDetailsView.frame = originalFrame1
                self.rideDetailsView.isHidden = false
            }, completion: { finished in})
        }
        
    }
    
    // MARK: - BUTTON ACTION METHODS
    
    @IBAction func showCarList(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CarDetailsViewController") as! CarDetailsViewController
        vc.carDetailsArray = self.driverActiveCarsList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func deleteProfileAction(_ sender: UIButton) {
        
        if(checkConnection()) {
            
            let alertController = UIAlertController(title: nil, message: "Are you sure you want to delete your account?".localized, preferredStyle: .alert)
            
            let yesAction = UIAlertAction(title: "Yes".localized, style: .default, handler: { (action) in
                self.deleteDriverProfileApi()
            })
            let noAction = UIAlertAction(title: "No".localized, style: .default, handler: nil)

            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            hideHUD()
            showAlertForInternetConnectionOff()
        }
    }
    
    func setServiceLocationAvailability(type:Int, pickupCity: String, pickupCountry: String, dropCity: String, dropCountry: String, _ notification : Notification? = nil, isFromMapView : Bool, latitude: Double, longitude: Double){
        if type != 2{
            if(checkConnection()) {
                var parameters = [
                    "Type" : type,
                    "PickupCity" : pickupCity,
                    "PickupCountry" : pickupCountry,
                    "DropCity" : dropCity,
                    "DropCountry" : dropCountry,
                    "Latitude" : String(latitude),
                    "Longitude" : String(longitude)
                    ] as [String : Any]
                
                self.showHUD()
                
                let jsonString = returnJsonString(param: parameters)
                
                let encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                
                parameters = ["data":encryptedData!]
                
                let jsonParam = returnJsonString(param: parameters)
                
                let Request: WebserviceHelper = WebserviceHelper()
                let headerDict = Request.getHeaderForRider(parameters: parameters)
                printJsonStringLog(apiName: KCheckServiceAvailability, parameter: jsonString as NSString, headerDict: headerDict)
                Request.CustomHTTPPostJSONWithHeader(url: KCheckServiceAvailability, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict ) { (result, error) in
                   self.hideHUD()
                    if ((error) != nil){
                        print(error.debugDescription)
                    } else{
                        
                        let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                        
                        let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                        
                        let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                        
                        print("ServiceAvailability :", responseContent ?? "")
                        if ((responseContent?.value(forKey: "Status") as? Bool) == false) {
                            DispatchQueue.main.async {
                            self.timeView.isHidden = true
                            self.PinView.isHidden = false
                            self.PinView.backgroundColor = UIColor.red
                            self.categoryTimeView.isHidden = true
                            
                            self.titlelbl.text = "Service not available for this location".localized
                            self.pinViewHeightConstraint.constant = 45.0
                            self.PinView.layer.cornerRadius = 22.5
                            self.pinBottomLbl.isHidden = true
                            }
                        }
                        else {
                            isFromMapView ? self.proceedWithMapLocation() : self.ProceedWithSelectLocation(notification!)
                        }
                    }
                }
            }else{
                self.showAlertForInternetConnectionOff()
            }
        }else {
            isFromMapView ? self.proceedWithMapLocation() : self.ProceedWithSelectLocation(notification!)
        }
    }
    
    func checkMapLocationServiceAvailability(value : String) {
        
        DispatchQueue.main.async {
            
            let currentPosition = self.rideMapView.camera.target
            
            CLGeocoder().reverseGeocodeLocation(CLLocation.init(latitude: currentPosition.latitude, longitude: currentPosition.longitude), completionHandler: {(placemarks, error) -> Void in
                
                if error != nil {
                    print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                    self.hideHUD()
                    return
                }
                
                if (placemarks?.count)! > 0 {
                    
                    var placeMark: CLPlacemark!
                    placeMark = placemarks?[0]
                    var countryName = placeMark.country
                    let countryISOCode = placeMark.isoCountryCode
                    var city = countryISOCode == "IN" || countryISOCode == "in" ? placeMark.subAdministrativeArea : placeMark.administrativeArea
                    
                    if city == nil {
                        city = "unknown city"
                    }
                    
                    if countryName == nil {
                        countryName = "unknown country"
                    }
                    
                    if value == "drop_up" {
                        let pickCountry = self.pickUpDict["Country"] as! String
                        let pickCity = self.pickUpDict["City"] as! String
                        
                        self.setServiceLocationAvailability(type: 2, pickupCity: pickCity, pickupCountry: pickCountry, dropCity: city!, dropCountry: countryName!, nil, isFromMapView: true, latitude: currentPosition.latitude, longitude: currentPosition.longitude)
                    } else {
                        self.setServiceLocationAvailability(type: 1, pickupCity: city!, pickupCountry:countryName!, dropCity: "", dropCountry: "", nil, isFromMapView: true,latitude: currentPosition.latitude, longitude: currentPosition.longitude)
                        
                        self.pickUpDict.setValue(city, forKey: "City")
                        self.pickUpDict.setValue(countryName, forKey: "Country")
                        
                       // print("city:", city! + "  " + countryName!)
                    }
                }
            })
        }
    }
    
    
    @IBAction func setLocationButtonClicked(_ sender: UIButton) {
        let value = changeTextOf == "PickUp" ? "pick_up ": "drop_up"
        checkMapLocationServiceAvailability(value: value)
    }
    
    func proceedWithMapLocation() {
        
        if pickUpAddressbtn.text == "" || pickUpAddressbtn.text!.localized == "Getting Location".localized || pickUpLocationData.latitude == nil
        {
            self.showSimpleAlertController(nil, message: "Getting Location".localized)
        }
        else
        {
            if changeTextOf == "PickUp" {
                changeTextOf = " "
                sourceMarker.map = nil
                sourceMarker = GMSMarker()
                sourceMarker.position = CLLocationCoordinate2D(latitude: (pickUpLocationData.latitude as NSString).doubleValue, longitude: (pickUpLocationData.longitude as NSString).doubleValue)
                sourceMarker.icon = UIImage(named:  "pickupPin")
                sourceMarker.map = rideMapView
                
                pickUpisSet = true
                pickUpAddressBtn.isEnabled = false
                if isEditingLocation == false
                {
                    dropUpAddressField.text = "Set Destination.".localized
                    dropUpAddressBtn.isEnabled = true
                    pinImage.image = UIImage(named: "dropupPin")
                    pinImage.isHidden = true
                    PinView.isHidden = true
                    timeView.isHidden = true
                    refLabel.isHidden = false
                    dropUpView.isHidden = false
                    editPickupLocation.isHidden = false
                    imgPickupLine.isHidden = false
                }
                else if isEditingLocation == true && dropUpisSet == true
                {
                    getFareEstimate()
                    imgPickupLine.isHidden = false
                    dropUpView.isHidden = false
                    refLabel.isHidden = false
                    editPickupLocation.isHidden = false
                    isEditingLocation = false
                    self.editDropupLocation.isUserInteractionEnabled = true
                    self.PinView.isHidden = true
                    self.timeView.isHidden = true
                    self.pinImage.isHidden = true
                }
                else if isEditingLocation == true
                {
                    dropUpAddressField.text = "Set Destination.".localized
                    dropUpAddressBtn.isEnabled = true
                    
                    imgPickupLine.isHidden = false
                    dropUpView.isHidden = false
                    refLabel.isHidden = false
                    editPickupLocation.isHidden = false
                    isEditingLocation = false
                    self.editDropupLocation.isUserInteractionEnabled = true
                    self.PinView.isHidden = true
                    self.timeView.isHidden = true
                    self.pinImage.isHidden = true
                }
            }
            else if changeTextOf == "DropUp"
            {
                if dropUpLocationData.latitude == nil
                {
                    self.showSimpleAlertController(nil, message: "Getting Location".localized)
                }
                else
                {
                    dropUpAddressBtn.isEnabled = false
                    editDropupLocation.isHidden = false
                    editDropupLocation.isUserInteractionEnabled = true
                    self.editPickupLocation.isHidden = false
                    self.editDropupLocation.isHidden = false
                    
                    destinationMarker.map = nil
                    destinationMarker = GMSMarker()
                    destinationMarker.position = CLLocationCoordinate2D(latitude: (dropUpLocationData.latitude as NSString).doubleValue, longitude: (dropUpLocationData.longitude as NSString).doubleValue)
                    destinationMarker.icon = UIImage(named:  "dropupPin")
                    destinationMarker.map = rideMapView
                    
                    getFareEstimate()
                    
                    if isEditingLocation == true
                    {
                        isEditingLocation = false
                        self.editPickupLocation.isUserInteractionEnabled = true
                    }
                    dropUpisSet = true
                    self.PinView.alpha = 0.0
                    self.PinView.isHidden = true
                    self.timeView.isHidden = true
                    self.pinImage.isHidden = true
                    changeTextOf = " "
                    
                }
            }
            if pickUpisSet == true && dropUpisSet == true
            {
            }
        }
    }
    
    func showRouteOnMap()
    {
        self.PinView.isHidden = true
        self.timeView.isHidden = true
        
        let origin = "\(String(Double(pickUpLocationData.latitude!)!)),\(Double(pickUpLocationData.longitude!)!)"
        let destination = "\(String(Double(dropUpLocationData.latitude!)!)),\(Double(dropUpLocationData.longitude!)!)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=false&mode=driving&key=\(GOOGLE_WEB_API_KEY)"
        print(url)
        URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { (data, response, error) -> Void in
            if((error) != nil) {
                return
            }
            
            do {
                let responseObject = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(responseObject ?? "")
                
                let routes = responseObject!["routes"] as? [[String:AnyObject]]
                
                for route in routes!
                {
                    print(route)
                    let routeOverviewPolyline = route["overview_polyline"] as? NSDictionary
                    let points = "\(routeOverviewPolyline?["points"] as AnyObject)"
                    print()
                    let path = GMSPath.init(fromEncodedPath: points)
                    self.polyline.path = path
                    self.polyline.map = self.rideMapView
                    self.polyline.strokeWidth = 8.0
                    self.polyline.strokeColor = UIColor(red: 0.0/255.0, green: 179.0/255.0, blue: 253.0/255.0, alpha: 1.0)
                    //Zoom into the source and desstination location
                    DispatchQueue.main.async {
                        var bounds = GMSCoordinateBounds.init(coordinate: self.sourceMarker.position, coordinate: self.destinationMarker.position)
                        bounds = bounds.includingPath(path!)
                        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(174.0, 30.0, 170.0, 30.0))
                        self.rideMapView.animate(with: cameraUpdate)
                        //Update the pick up and Drop pin based on end point
                        self.setupMarkersAfterRoute(route: route)
                    }
                }
                
            } catch let error as NSError {
                print(error)
            }
        }).resume()
    }
    
    func setupMarkersAfterRoute(route : [String:AnyObject]) {
        var directionList : NSMutableArray = []
        // Get distance
        let legs = route["legs"] as! NSArray
        let leg = legs[0] as! NSDictionary
        
        guard let steps = leg["steps"] as? NSArray else {
            return
        }
        
        directionList = steps.mutableCopy() as! NSMutableArray
        print(directionList)
        
        if directionList.count > 0 {
            let startStep = directionList.firstObject as! NSDictionary
            let endStep = directionList.lastObject as! NSDictionary
            
            if let sourceDict = startStep["start_location"] as? NSDictionary, let lat = sourceDict["lat"] as? Double, let lng = sourceDict["lng"] as? Double  {
                let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                //sourceMarker.position = location
                setUpDriverMarkerWithNewLocation(location: location)
            }
            
            if let destinationDict = endStep["end_location"] as? NSDictionary, let lat = destinationDict["lat"] as? Double, let lng = destinationDict["lng"] as? Double  {
                let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                destinationMarker.position = location
            }
        }
    }
    func setUpDriverMarkerWithNewLocation(location : CLLocationCoordinate2D) {
        
        movement.arCarMovement(marker: sourceMarker, oldCoordinate: sourceMarker.position, newCoordinate: location, mapView: rideMapView, bearing: self.rotation, movingDuration: 0.5)
    }
    @IBAction func editPickUpLocationClicked(_ sender: UIButton) {
        
        //Hide the route and source marker if exists
        self.polyline.map = nil
        sourceMarker.map = nil
        PinView.isHidden = true
        timeView.isHidden = true
        
        carTypeView.isHidden = false
        categoryTimeView.isHidden = false
        self.rideDetailsView.isHidden = true
        pickUpisSet = false
        self.PinView.backgroundColor = kLIGHT_OLIVE_COLOR
        editDropupLocation.isUserInteractionEnabled = false
        editPickupLocation.isHidden = true
        
        let pickUpLocationSelect = self.storyboard?.instantiateViewController(withIdentifier: "DropUpLocationController") as! DropUpLocationController
        pickUpLocationSelect.locationType = "pick_up"
        pickUpLocationSelect.locationToEdit = self.pickUpAddressbtn.text!
        let RootVC = UINavigationController(rootViewController: pickUpLocationSelect)
        UIApplication.topViewController()?.present(RootVC, animated: true) {
            self.isEditingLocation = true
            self.changeTextOf = "PickUp"
            self.pinImage.isHidden = false
            self.pinImage.image = UIImage(named: "pickupPin")
            
            if let dLat = self.pickUpLocationData.latitude, let dLong = self.pickUpLocationData.longitude {
                let centerCoordinate = CLLocationCoordinate2DMake(Double(dLat)!, Double(dLong)!)
                self.setRegionOnMapview(centerCoordinate)
            }
        }
    }
    
    func logoutAlertYesAction()  {
        
        if(checkConnection()){
            
            let parameters = ["PlatForm": 1] as [String : Any]
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            Request.CustomHTTPPostJSONWithHeader(url: KLOGOUT_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders(), callback: { (result, error) in
                print("result", result)
                
                self.hideHUD()
                
                if(error != nil) {
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                
                let responseDict = getResponseDictionaryWithResult(result: result)
                print(responseDict)
                
                if let status = responseDict["Status"] as? Bool {
                    if status == true {
                        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                        appDelegate.performLogout()
                    }else{
                        self.goToAlreadyLogoutAction(response: responseDict as! [String : Any])
                    }
                    //Clear the user Email when logout
                    Crashlytics.sharedInstance().setUserEmail("")
                }
            })
        } else {
            self.showAlertForInternetConnectionOff()
        }
    }
    
    @IBAction func budgetButtonAction(_ sender: Any) {
        bookAction("1",withReachTime: budgetTimeLbl.text!)
    }
    @IBAction func businessButtonAction(_ sender: Any) {
        bookAction("2",withReachTime: businessTimeLbl.text!)
    }
    
    @IBAction func comfortButtonAction(_ sender: Any) {
        bookAction("3",withReachTime: comfortTimeLbl.text!)
    }
    func bookAction(_ carTypeString:String, withReachTime reachTime:String)
    {
        if KAPP_RIDER
        {
            print("reachTime")
            var forNowBooking = false
            if carTypeString == "1"{
                if let value = (FareresultArray[0] as! NSDictionary).value(forKey: "CarTypeId") as? Int32{
                    if(value == 1){
                        if value != 0{
                            forNowBooking = budgetTimeLbl.text! == "No taxi available".localized ? false : true
                        }
                    }
                }
            }
            else if carTypeString == "2"{
                if let value = (FareresultArray[1] as! NSDictionary).value(forKey: "CarTypeId") as? Int32{
                    if(value == 2){
                        if value != 0{
                            forNowBooking = businessTimeLbl.text! == "No taxi available".localized ? false : true
                        }
                    }
                }
            }
            else if carTypeString == "3"{
                if let value = (FareresultArray[2] as! NSDictionary).value(forKey: "CarTypeId") as? Int32{
                    if(value == 3){
                        if value != 0{
                            forNowBooking = comfortTimeLbl.text! == "No taxi available".localized ? false : true
                        }
                    }
                }
            }
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "OrderRideViewController") as! OrderRideViewController
            vc.pickUPLocation = pickUpLocationData
            vc.dropUPLocation = dropUpLocationData
            vc.driver_id = "0"
            vc.carType = carTypeString
            vc.reachTimeToRider = reachTime
            print("forNowBooking", forNowBooking)
            vc.forNowAvailable = forNowBooking
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    func driverSwitchChangeAction(_ sender: Any){
        let switchObj = sender as! UISwitch
        if switchObj.isOn{
            UIApplication.shared.isIdleTimerDisabled = true
            setDriverAvailibility("1")
        }
        else {
            UIApplication.shared.isIdleTimerDisabled = false
            setDriverAvailibility("0")
        }
    }
    
    func resetDestination() {
        DispatchQueue.main.async {
            self.polyline.map = nil
            self.animateViewToCurrentLocation(coordinate: self.sourceMarker.position)
        }
    }
    
    @IBAction func editDropUpLocationClicked(_ sender: UIButton) {
        
        //Hide the route and source marker if exists
        self.polyline.map = nil
        destinationMarker.map = nil
        PinView.isHidden = true
        timeView.isHidden = true
        
        carTypeView.isHidden = false
        categoryTimeView.isHidden = false
        self.rideDetailsView.isHidden = true
        
        self.PinView.backgroundColor = UIColor.red
        
        dropUpisSet = false
        
        editDropupLocation.isHidden = true
        editPickupLocation.isUserInteractionEnabled = false
        
        let dropUpLocationSelect = self.storyboard?.instantiateViewController(withIdentifier: "DropUpLocationController") as! DropUpLocationController
        dropUpLocationSelect.locationType = "drop_up"
        
        dropUpLocationSelect.locationToEdit = self.dropUpAddressField.text == "Set Destination." || self.dropUpAddressField.text == "Getting Location" || self.dropUpAddressField.text == "Choose Location" ? "" : self.dropUpAddressField.text!
        
        let RootVC = UINavigationController(rootViewController: dropUpLocationSelect)
        UIApplication.topViewController()?.present(RootVC, animated: true) {
            self.isEditingLocation = true
            self.changeTextOf = "DropUp"
            self.pinImage.isHidden = false
            self.pinImage.image = UIImage(named: "dropupPin")
            
            if let dLat = self.dropUpLocationData.latitude, let dLong = self.dropUpLocationData.longitude {
                let centerCoordinate = CLLocationCoordinate2DMake(Double(dLat)!, Double(dLong)!)
                self.setRegionOnMapview(centerCoordinate)
            }
        }
    }
    
    @IBAction func driverPaymentButtonAction(_ sender: Any) {
        getPayAmount()
    }
    
    
    @IBAction func currentRideAction(_ sender: AnyObject) {
        if KAPP_RIDER{
            callWebserviceForGettingCurrentRide()
        }
        else{
            callWebserviceForGettingCurrentTrip(CURRENTTRIPWSTAG)
        }
    }
    
    
    @IBAction func logoutAction(_ sender: AnyObject) {
        let alertController: UIAlertController = UIAlertController(title: nil, message: KAPP_RIDER ? "RiderLogOutMsgKey".localized : "LogOutMsgKey".localized, preferredStyle: .alert)
        let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
            self.logoutAlertYesAction()
        }
        let noAction: UIAlertAction = UIAlertAction(title: "No".localized, style: .default) { action -> Void in}
        
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func carTypeSelectionAction(_ sender: AnyObject) {
        
        let btnType = sender as! UIButton
        if btnType == carTypeBudgetBtn {
            carType = "1"
            if btnType.tag == 1
            {
                showHUD()
                getNearByAvailableDriver(carType: "1", location: (delegate?.userLocation)!)
            }
            carTypeBudgetBtn.tag = 2
            carTypeBusinessBtn.tag = 1
            carTypeComfortBtn.tag = 1
            
            carTypeBudgetBtn.backgroundColor = kLIGHT_SELECT_COLOR
            carTypeBusinessBtn.backgroundColor = kLIGHT_OLIVE_COLOR
            carTypeComfortBtn.backgroundColor = kLIGHT_OLIVE_COLOR
            
        }
        else if btnType == carTypeBusinessBtn {
            carType = "2"
            if btnType.tag == 1
            {
                showHUD()
                getNearByAvailableDriver(carType: "2", location: (delegate?.userLocation)!)
            }
            carTypeBudgetBtn.tag = 1
            carTypeBusinessBtn.tag = 2
            carTypeComfortBtn.tag = 1
            
            carTypeBudgetBtn.backgroundColor = kLIGHT_OLIVE_COLOR
            carTypeBusinessBtn.backgroundColor = kLIGHT_SELECT_COLOR
            carTypeComfortBtn.backgroundColor = kLIGHT_OLIVE_COLOR
        }
        else if btnType == carTypeComfortBtn {
            carType = "3"
            if btnType.tag == 1
            {
                showHUD()
                getNearByAvailableDriver(carType: "3", location: (delegate?.userLocation)!)
            }
            carTypeBudgetBtn.tag = 1
            carTypeBusinessBtn.tag = 1
            carTypeComfortBtn.tag = 2
            
            carTypeBudgetBtn.backgroundColor = kLIGHT_OLIVE_COLOR
            carTypeBusinessBtn.backgroundColor = kLIGHT_OLIVE_COLOR
            carTypeComfortBtn.backgroundColor = kLIGHT_SELECT_COLOR
        }
        
        self.mapView(rideMapView, idleAt: rideMapView.camera)
        delegate?.carType = carType
        if pickUpLocationData.latitude != nil && dropUpLocationData.address != nil && changeTextOf != "PickUp" && changeTextOf != "DropUp"
        {
            getFareEstimate()
        }
        
    }
    
    @IBAction func pickUpDropUpAction(_ sender: AnyObject) {
        
        let btnType = sender as! UIButton
        
        if btnType.tag == 4 {
            
            
        }else{
            
        }
    }
    
    @IBAction func rideDetailAction(_ sender: AnyObject) {
        callWebserviceForGettingCurrentTrip(RIDEDETAILS)
    }
    
    @IBAction func focusonUserLocation(_ sender: AnyObject) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                self.showLocationAllowAlert()
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                if delegate?.userLocation?.coordinate != nil {
                    
                    let center = CLLocationCoordinate2D(latitude: (delegate?.userLocation?.coordinate.latitude)!, longitude: (delegate?.userLocation?.coordinate.longitude)!)
                    print("\n User Current Location : \(String(describing: delegate?.userLocation)) \n")
                    print("\n Localion manager : \(String(describing: self.locationManager.location)) \n")
                    
                    self.setRegionOnMapview(center)
                }
            }
        } else {
            self.showLocationAllowAlert()
        }
    }
    
    @IBAction func dropUpAddressBtnClicked(_ sender: UIButton) {
        let dropUpLocationSelect = self.storyboard?.instantiateViewController(withIdentifier: "DropUpLocationController") as! DropUpLocationController
        dropUpLocationSelect.locationType = "drop_up"
        dropUpLocationSelect.locationToEdit = self.dropUpAddressField.text == "Set Destination." || self.dropUpAddressField.text == "Getting Location" || self.dropUpAddressField.text == "Choose Location" ? "" : self.dropUpAddressField.text!
        let RootVC = UINavigationController(rootViewController: dropUpLocationSelect)
        UIApplication.topViewController()?.present(RootVC, animated: true, completion: nil)
    }
    
    @IBAction func pickUpAddressBtnClicked(_ sender: Any) {
        let dropUpLocationSelect = self.storyboard?.instantiateViewController(withIdentifier: "DropUpLocationController") as! DropUpLocationController
        dropUpLocationSelect.locationType = "pick_up"
        dropUpLocationSelect.locationToEdit = self.pickUpAddressbtn.text!
        let RootVC = UINavigationController(rootViewController: dropUpLocationSelect)
        UIApplication.topViewController()?.present(RootVC, animated: true, completion: nil)
    }
    
    // MARK: - MENU ACTIONS METHODS
    func openSettingScreen(){
        let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsRiderVC") as? SettingsRiderVC
        self.navigationController?.pushViewController(settingVC!, animated: true)
    }
    func openPaymentDetailsScreen(){
        let paymentVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentDetailsController") as? PaymentDetailsController
        self.navigationController?.pushViewController(paymentVC!, animated: true)
    }
    
    func openPreBookingListScreen(){
        let preBookingVC = self.storyboard?.instantiateViewController(withIdentifier: "PreBookingListVC") as? PreBookingListVC
        self.navigationController?.pushViewController(preBookingVC!, animated: true)
    }
    
    func openRideHistoryListScreen(){
        let ridehistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "RideHistoryListVC") as? RideHistoryListVC
        self.navigationController?.pushViewController(ridehistoryVC!, animated: true)
    }
    
    // MARK:- WEB SERVICE METHODS
    func getFareEstimate()
    {
        if(checkConnection()){
            
            var parameters = [
                "PickupLatitude": pickUpLocationData.latitude!,
                "DropLatitude": dropUpLocationData.latitude!,
                "PickupLongitude":pickUpLocationData.longitude!,
                "DropLongitude":dropUpLocationData.longitude!,
                //"RideType": "2",
                "RentalPlanId": "",
                //"CarTypeId": "1"
                ] as [String : Any]
            
            let jsonString = returnJsonString(param: parameters)
            
            let encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            printJsonStringLog(apiName: KGET_MIN_PRICE, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KGET_MIN_PRICE, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                
                self.hideHUD()
                
                if ((error) != nil){
                    print(error.debugDescription)
                }else {
                    if result == "[]" {
                        self.showSimpleAlertController(nil, message: "Something Went Wrong".localized)
                        self.hideHUD()
                        return
                    }
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    guard let data = responseObject.value(forKey: "data") as? String else {
                        self.showSimpleAlertController(nil, message: "Something Went Wrong".localized)
                        return
                    }
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    print("GetFareEstimate :", decryptedData ?? "")
                    
                    if let list = self.convertToDictionary(text: decryptedData!) as? [[String:Any]] {
                        
                        self.FareresultArray = NSMutableArray()
                        for data in list {
                            self.FareresultArray.add(data as [String:Any])
                        }
                        
                        if self.FareresultArray.count > 0{
                            
                            for i in 0..<self.FareresultArray.count{
                                self.customDriverListDict = self.FareresultArray.object(at: i) as! NSDictionary
                            }
                            self.priceMinDetailsDict =  self.customDriverListDict
                            self.driverListDict = self.customDriverListDict
                            
                            self.showRouteOnMap()
                            self.openCarPriceAndMinDetailsView()
                        }
                        else{
                            let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                            let status = responseContent?.value(forKey: "Status") as? Bool
                            if(status == false)
                            {
                                self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                            }
                        }
                        
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
        
    }
    func callWebserviceForGettingCurrentRide(){
        if checkConnection(){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            let headerDict = [
                "Content-Type" : "application/json",
                "Authorization" : UserDefaults.standard.string(forKey: kAUTH_TOKEN)! as String,
                "Language" : systemLanguage
            ]
            self.showHUD()
            var parameters = [
                "UserId" : riderID,
                "Language" : systemLanguage
                ] as [String : Any]
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data": encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KCURRENTRIDE_URL, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KCURRENTRIDE_URL,jsonString: jsonParam as NSString, withIdicator: false, header: headerDict) { (result, error) in
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                
                let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                
                print("Current Ride :", responseContent ?? "")
                
                if let status = responseContent!["Status"] as? Bool{
                    if status == true
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DriverDetailsController") as! DriverDetailsController
                        let currentRideData = responseContent!["result"] as? NSDictionary
                        print("currentRideData", currentRideData)
                        vc.currentRideData = currentRideData?.mutableCopy() as? NSMutableDictionary
                        print("currentRideData Booking\(String(describing: currentRideData))")
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        if let message = responseContent?.value(forKey: "Message") as? String {
                            self.showSimpleAlertController(nil, message: message)
                            self.hideHUD()
                        }
                    }
                    
                }
            }
        }
        else{
            showAlertForInternetConnectionOff()
        }
    }
    
    func callWebserviceForGettingCurrentTrip(_ tag : NSInteger){
        
        if(checkConnection()) {
            
            let parameters = [
                "UserId":getLoginDriverID()
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            printParameterLog(apiName: KDRIVER_CURRENT_RIDEINFO_URL, parameter: parameters, headerDict: getRequestHeaders())
            
            Request.CustomHTTPPostJSONWithHeader(url: KDRIVER_CURRENT_RIDEINFO_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.hideHUD()
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                
                let responseContent = getResponseDictionaryWithResult(result: result)
                print(responseContent)
                
                if let message = responseContent["Errors"] as? String {
                    self.showSimpleAlertController(nil, message: message)
                    return
                }

                guard let message = responseContent["Status"] as? Bool else { return }
                
                if message == true {
                    
                    if(tag == RIDEDETAILS){
                        let rideInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "RideDetailsViewController") as? RideDetailsViewController
                        let rideDetails = responseContent["result"] as? NSDictionary
                        rideInfoVC?.currentTripInfo = rideDetails?.mutableCopy() as? NSMutableDictionary
                        self.navigationController?.pushViewController(rideInfoVC!, animated: true)
                    }else{
                        let rideInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "RideInfoViewController") as? RideInfoViewController
                        let currentTrip = responseContent["result"] as? NSDictionary
                        rideInfoVC?.currentTripInfo = currentTrip?.mutableCopy() as! NSMutableDictionary
                        self.navigationController?.pushViewController(rideInfoVC!, animated: true)
                    }
                } else {
                    UserDefaults.standard.removeObject(forKey: "Time")
                    UserDefaults.standard.removeObject(forKey: "rideStatus")
                    self.showSimpleAlertController(nil, message: (responseContent["Message"] as? String ?? "")!)
                }
            }
        } else {
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func deleteDriverProfileApi() {
        
        if checkConnection(){
            
            let parameters = [
                "UserId":getLoginDriverID(),
                "UpdatedBy":getLoginDriverID(),
                "Reason":"Driver"
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            printParameterLog(apiName: DELETE_DRIVER_PROFILE, parameter: parameters, headerDict: getRequestHeaders())
            
            Request.CustomHTTPPostJSONWithHeader(url: DELETE_DRIVER_PROFILE, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if (error == nil){
                    let responseContent = getResponseDictionaryWithResult(result: result)
                    print(responseContent)
                    
                    if let value = responseContent.value(forKey: "Message") as? String {
                        self.self.showSimpleAlertControllerWithOkAction(nil, message: value, callback: {
                            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                            appDelegate.performLogout()
                        })
                    }
                    
                } else {
                    self.hideHUD()
                    self.showSimpleAlertController("alert", message: (error?.localized)!)
                }
            }
        } else {
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func getCarDetails()
    {
        if checkConnection(){
            
            self.showHUD()
            
            let userDict = getCurrentLoginUserDict()
            
            //let userFName = userDict.value(forKey: "FirstName") as! String
            //let userLName = userDict.value(forKey: "LastName") as! String
            //let UserName = "\(userFName) \(userLName)"
            let UserName = userDict.value(forKey: "DisplayName") as? String ?? ""
            
            var driverRating = "0.0"
            
            if let value = userDict.value(forKey: "DriverRating") as? NSString {
                driverRating = value as String
            }
            
            if let value = userDict.value(forKey: "DriverRating") as? Double {
                driverRating = "\(value)"
            }
            
            let attachment = NSTextAttachment()
            attachment.image = #imageLiteral(resourceName: "driver_star")
            let attachmentString = NSAttributedString(attachment: attachment)
            let myString = NSMutableAttributedString(string: UserName)
            let rateStr = NSAttributedString(string: "\n(\(driverRating)")
            let breaketStr = NSAttributedString(string: ")")
            myString.append(rateStr)
            myString.append(attachmentString)
            myString.append(breaketStr)
            driverName.attributedText = myString
            
            if let profileImg = userDict.value(forKey: "ProfilePic") as? String {
                downloadImage(URL(string: profileImg)!, imageBtn: nil, imageView: self.driverProfileImage, indicator: self.driverProfileIndicator)
            }
            
            let parameters = [
                "UserId":getLoginDriverID()] as [String : Any]
            
            self.showHUD()
            
            let Request: WebserviceHelper = WebserviceHelper()
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KDRIVER_CAR_LIST, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                
                if ((error) != nil){
                    print(error.debugDescription)
                } else {
                    let responseContent = getResponseArrayWithResult(result: result)
                    
                    if responseContent.count > 0 {
                        
                        do {
                            let activeCarsData = try JSONSerialization.data(withJSONObject: responseContent, options: [])
                            let jsonDecoder = JSONDecoder()
                            let driversCarList = try? jsonDecoder.decode(DriversCarList.self, from: activeCarsData)
                            self.setActiveCarInformation(responseCarList: driversCarList!)
                            
                        } catch let error as NSError {
                            print(error)
                        }
                        print(responseContent)
                    } else {
                        self.showSimpleAlertController(nil, message: "Response Error or No cars available")
                    }
                }
                self.hideHUD()
            }
        }
        else{
            hideHUD()
            showAlertForInternetConnectionOff()
        }
    }
    
    func callWebserviceForGettingPendingTrip(_ tag : NSInteger){
        
        if(checkConnection()) {
            
            let parameters = [
                "DriverId":getLoginDriverID()
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KDRIVER_PENDING_RIDE, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                
                if result == "[]" {
                    self.showSimpleAlertController(nil, message: "No Prebooking".localized)
                    return
                }
                
                let responseContent = getResponseArrayWithResult(result: result)
                
                if responseContent.count > 0 {
                    
                    let pendingRideVC = self.storyboard?.instantiateViewController(withIdentifier: "PendingRidesViewController") as! PendingRidesViewController
                    pendingRideVC.pendingRideList = responseContent.mutableCopy() as! NSMutableArray
                    self.navigationController?.pushViewController(pendingRideVC, animated: true)
                    print(responseContent)
                } else {
                    self.showSimpleAlertController(nil, message: "No Prebooking".localized)
                }
            }
        } else {
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func setActiveCarInformation(responseCarList: DriversCarList) {
        
        self.driverActiveCarsList = responseCarList
        
        func getActiveCar()-> ActiveCar {
            for (index, carObj) in responseCarList.enumerated() {
                if carObj.selectedCar == 1 {
                    print("Activated Car Is :\(carObj.brand) at Index \(index)")
                    return carObj
                }
            }
            return responseCarList[0]
        }
        
        let activeCar = getActiveCar()
        
        self.carName.text = activeCar.brand
        self.plateNo.text = getPlateNumber(plateNumber: activeCar.plateNumber)
        self.carTypeName.text = activeCar.carTypeName
    }
    
    func getNearByAvailableDriver(carType: String, location: CLLocation) {
        
        DispatchQueue.global(qos: .background).async {
            if(self.checkConnection()){
                let latitudeStr = String(format: "%f", location.coordinate.latitude)
                let longitudeStr = String(format: "%f", location.coordinate.longitude)
                var parameters = [
                    "Latitude" : self.pickUpisSet ? self.pickUpLocationData.latitude : latitudeStr,
                    "Longitude" : self.pickUpisSet ? self.pickUpLocationData.longitude : longitudeStr
                    ] as [String:Any]
                //                    self.showHUD()
                let Request: WebserviceHelper = WebserviceHelper()
                let jsonString = returnJsonString(param: parameters)
                var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                let headerDict = Request.getHeaderForRider(parameters: parameters)
                parameters = ["data":encryptedData!]
                let jsonParam = returnJsonString(param: parameters)
               // printJsonStringLog(apiName: KNearestDriverList_URL, parameter: jsonParam as NSString, headerDict: headerDict)
                Request.CustomHTTPPostJSONWithHeader(url: KNearestDriverList_URL, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                    DispatchQueue.main.async {self.hideHUD()}
                    if ((error) != nil){
                        print(error?.localized ?? "")
                    }else{
                        let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                        guard let data = responseObject.value(forKey: "data") as? String else {
                            self.showSimpleAlertController(nil, message: "Something Went Wrong".localized)
                            self.hideHUD()
                            return
                        }
                        let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                        
                        DispatchQueue.main.async {
                            if let list = self.convertToDictionary(text: decryptedData!) as? [[String:Any]] {
                                self.resultArray = NSMutableArray()
                                for data in list {
                                    self.resultArray.add(data as [String:Any])
                                }
                            }
                            
                            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                            
                            if self.resultArray.count > 0 {
                                appDelegate.showDriversOnGoogleMap(LocationsArray: self.resultArray)
                            }else {
                                appDelegate.removeMarkers()
                            }
                        }
                    }
                }
            }else{
                self.hideHUD()
                self.showAlertForInternetConnectionOff()
            }
        }
    }
    
    func setDriverAvailibility(_ status: String) {
        
        if(checkConnection()) {
            
            let parameters = [
                "UserId":getLoginDriverID(),
                "Status":(status == "1") ? 1 : 0,
                "DeviceId": DEVICE_UUID,
                "PushToken":getPushTokenString()
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KSET_DRIVER_AVAILABILITY, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                
                if self.isPerformLogout {
                    self.logoutAlertYesAction()
                }
                else {
                    if ((error) != nil) {
                        print(error.debugDescription)
                        self.setDriverAvailibilityDidFinish(false)
                        return;
                    } else {
                        self.handleAvailablilityChangeResponseWithResult(result: result)
                    }
                }
            }
        } else {
            self.showAlertForInternetConnectionOff()
            driverSwitch.isOn = !driverSwitch.isOn
            UIApplication.shared.isIdleTimerDisabled = driverSwitch.isOn
        }
    }
    
    func handleAvailablilityChangeResponseWithResult(result: String) {
        
        let responseContent = getResponseDictionaryWithResult(result: result)
        print(responseContent)
        if let value = responseContent.value(forKey: "Status") as? Bool {
            
            if value == true {
                
                if let message = responseContent.value(forKey: "Message") as? String {
                    
                    if (message == "You are offline now") || (message == "Du bist jetzt offline") {
                        self.setDriverAvailibilityStatus(status: 0)
                        self.setDriverAvailibilityDidFinish(false)
                        self.locationManager.stopUpdatingLocation()
                        locationManager1.stopUpdatingLocation()
                    } else {
                        self.setDriverAvailibilityStatus(status: 1)
                        self.setDriverAvailibilityDidFinish(true)
                        locationManager1.startUpdatingLocation()
                    }
                }
            }
            else {
                
                self.setDriverAvailibilityStatus(status: driverSwitch.isOn ? 1 : 0)
                self.setDriverAvailibilityDidFinish(driverSwitch.isOn)
                
                guard let isAlreadyLogout = responseContent.value(forKey: "IsAlreadyLogout") as? String else {
                    let message = responseContent.value(forKey: "Message") as? String
                    self.showSimpleAlertController(nil, message: message!)
                    return
                }
                
                //  : Handle Driver Logout
                if isAlreadyLogout == "1" && UserDefaults.standard.bool(forKey: KRIDER_LOGIN) && !KAPP_RIDER {
                    self.delegate?.forceFullyLogoutDriverAlert(responseContent.value(forKey: "Message") as! String)
                }
            }
        }
        else{
            self.setDriverAvailibilityDidFinish(false)
        }
    }
    
    func getPayAmount() {
        
        if(checkConnection()){
            
            let parameters = [
                "DriverId":getLoginDriverID(),
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            printParameterLog(apiName: DRIVER_PAY_AMOUNT, parameter: parameters, headerDict: getRequestHeaders())
            
            Request.CustomHTTPPostJSONWithHeader(url: DRIVER_PAY_AMOUNT, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    print(error?.localized ?? "")
                    self.showSimpleAlertController("Alert", message: error?.localized ?? "")
                    return
                }
                
                let responseContent = getResponseDictionaryWithResult(result: result)
                print(responseContent)
                
                guard let status = responseContent.value(forKey: "Status") as? Bool else {
                    return
                }
                
                if status {
                    
                    if let totalPayAmount = responseContent["TotalPayAmount"] as? String {
                        
                        if totalPayAmount != "0.00" && totalPayAmount != "0.0" {
                            let paymentVC = self.storyboard?.instantiateViewController(withIdentifier: "paymentYourTaxiViewController") as? paymentYourTaxiViewController
                            paymentVC?.amountToPay = totalPayAmount
                            self.navigationController?.pushViewController(paymentVC!, animated: true)
                        }else {
                            if let message = responseContent.value(forKey: "Message") as? String  {
                                self.showSimpleAlertController(nil, message: message)
                            }
                        }
                    }
                } else {
                    if let message = responseContent.value(forKey: "Message") as? String  {
                        self.showSimpleAlertController(nil, message: message)
                    }
                }
            }
        } else {
            showAlertForInternetConnectionOff()
        }
    }
    
    //MARK:- ALREADY LOGOUT ACTION
    func goToAlreadyLogoutAction(response:[String:Any]){
        
        if let value = response["is_already_logout"] as? String {
            if value == "1" && UserDefaults.standard.bool(forKey: KRIDER_LOGIN) && !KAPP_RIDER{
                self.delegate?.forceFullyLogoutDriverAlert(response["message"] as! String)
            }
            else{
                self.showSimpleAlertController(nil, message: response["message"] as! String)
            }
        }
        else{
            self.showSimpleAlertController(nil, message: response["message"] as? String ?? "No message found from server.")
        }
    }
    
    func setDriverAvailibilityStatus(status:NSInteger){
        let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
        let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
        let userDict = self.convertStringToDictionary(text: decryt!)!
        var dict = userDict as! [String:Any]
        dict["Availability"] = status
        
        var encryptData = returnJsonString(param: dict)
        encryptData = AESCrypt.encrypt(encryptData, password: SECRET_KEY)
        UserDefaults.standard.set(encryptData, forKey: KUSER_DETAILS)
        UserDefaults.standard.synchronize()
    }
    
    func setDriverAvailibilityDidFinish(_ status:Bool){
        if !status {
            if driverSwitch.isOn{
                driverSwitch.setOn(false, animated: true)
                driverStatus.text = "Offline".localized
            }
            else if(!driverSwitch.isOn && !status){
                driverSwitch.setOn(false, animated: true)
                driverStatus.text = "Offline".localized
            }
            else{
                driverSwitch.setOn(true, animated: true)
                driverStatus.text = "Online".localized
            }
        }
        else{
            if driverSwitch.isOn{
                driverStatus.text = "Online".localized
            }
            else{
                driverStatus.text = "Offline".localized
            }
        }
    }
    
    // MARK: - Car category time
    func stopCarCategoryTime() {
        bugetCarTimeLbl.isHidden = false
        businessCarTimeLbl.isHidden = false
        comfortCarTimeLbl.isHidden = false
        
        bugetTimeIndicator.stopAnimating()
        businessTimeIndicator.stopAnimating()
        comfortTimeIndicator.stopAnimating()
        
        if carCategoryTimeDict != nil {
            if carCategoryTimeDict!["budget"] != nil {
                let bugetValue = carCategoryTimeDict?.value(forKey: "budget") as! NSDictionary
                if bugetValue.value(forKey: "TimeToReach") as! String == ""{
                    bugetCarTimeLbl.text = "No taxi available".localized
                }else{
                    bugetCarTimeLbl.text = ((bugetValue.value(forKey: "TimeToReach") as! NSString) as String).localized
                }
            }
            if carCategoryTimeDict!["business"] != nil {
                let businessValue = carCategoryTimeDict?.value(forKey: "business") as! NSDictionary
                if businessValue.value(forKey: "TimeToReach") as! String == ""{
                    businessCarTimeLbl.text = "No taxi available".localized
                }else{
                    businessCarTimeLbl.text = ((businessValue.value(forKey: "TimeToReach") as! NSString) as String).localized
                }
            }
            if carCategoryTimeDict!["comfort"] != nil {
                let comfortValue = carCategoryTimeDict?.value(forKey: "comfort") as! NSDictionary
                if comfortValue.value(forKey: "TimeToReach") as! String == ""{
                    comfortCarTimeLbl.text = "No taxi available".localized
                }else{
                    comfortCarTimeLbl.text = ((comfortValue.value(forKey: "TimeToReach") as! NSString) as String).localized
                }
            }
            
        }
        
    }
    func startCarCategoryTime()  {
        bugetCarTimeLbl.isHidden = true
        businessCarTimeLbl.isHidden = true
        comfortCarTimeLbl.isHidden = true
        
        bugetTimeIndicator.startAnimating()
        businessTimeIndicator.startAnimating()
        comfortTimeIndicator.startAnimating()
        
        categoryTimeView.isHidden = false
    }
    
    // MARK: - Mapview Update
    func setRegionOnMapview(_ coordinate:CLLocationCoordinate2D){
        print("Test :" , coordinate)
        self.animateViewToCurrentLocation(coordinate: coordinate)
    }
    
    func setCurrentLocation(){
        if changeTextOf == "PickUp" && localAllowed{
            localAllowed = false
            if delegate?.userLocation != nil{
                let centerCoordinate = CLLocationCoordinate2DMake((delegate?.userLocation?.coordinate.latitude)!, (delegate?.userLocation?.coordinate.longitude)!)
                self.setRegionOnMapview(centerCoordinate)
            }
        }
    }
 }
 
 //MARK:- EXTENSION GMSMAPVIEW DELEGATE
 extension HomeViewController : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("Target center position:\(mapView.camera.target)")
        if isInitialized {
            if self.changeTextOf == "PickUp"
            {
                self.pickUpAddressbtn.text = "Getting Location".localized
                startCarCategoryTime()
            }
            else if self.changeTextOf == "DropUp" && dropUpisSet == false
            {
                dropUpAddressField.text = "Getting Location".localized
            }
            self.PinView.isHidden = true
            self.timeView.isHidden = true
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        print("Target center position:\(position.target)")
        
        isCarAvailable = false
        self.pinBottomLbl.isHidden = true
        pinViewHeightConstraint.constant = 45.0
        PinView.layer.cornerRadius = 22.5
        UIApplication.shared.setStatusBarHidden(false, with: UIStatusBarAnimation.slide)
        
        if self.changeTextOf == "PickUp"
        {
            startCarCategoryTime()
        }
        else if self.changeTextOf == "DropUp"
        {
            self.activityIndicator.isHidden = true
            self.timeView.isHidden = true
        }
        
        UIView.animate(withDuration: 0.1, delay: 0.09, options: .curveEaseInOut, animations: {
            self.PinView.alpha = 1 // Here you will get the animation you want
            
        }, completion: { finished in
            // Here you hide it when animation done
            CLGeocoder().reverseGeocodeLocation(CLLocation.init(latitude: position.target.latitude, longitude: position.target.longitude), completionHandler: {(placemarks, error) -> Void in
                
                if error != nil {
                    print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                    self.hideHUD()
                    return
                }
                
                if (placemarks?.count)! > 0 {
                    
                    self.PinView.isHidden = self.isAddressSelected
                    
                    var placeMark: CLPlacemark!
                    placeMark = placemarks?[0]
                    //let countryName = placeMark.country
                    let countryISOCode = placeMark.isoCountryCode
                    
                    // Check country for texi are available or not
                    if(countryISOCode == "DE" || countryISOCode == "FR" || countryISOCode == "AT" || countryISOCode == "IT" || countryISOCode == "LI" || countryISOCode == "CH" || countryISOCode == "IN"){
                        if let formattedAddress = placeMark.addressDictionary!["FormattedAddressLines"] as? NSArray {
                            
                            let addressString = formattedAddress.componentsJoined(by: ", ")
                            
                            if self.changeTextOf == "PickUp"
                            {
                                
                                self.pickUpAddressbtn.text = addressString.trimmingCharacters(in: .whitespaces).localized
                                self.pickUpLocationData.setLocationData(latitude: "\(position.target.latitude)", longitude: "\(position.target.longitude)", address: addressString.trimmingCharacters(in: .whitespaces))
                                
                                self.timeView.isHidden = self.isAddressSelected
                                self.PinView.isHidden = self.isAddressSelected
                                
                                self.reachTimelbl.isHidden = true
                                self.titlelbl.text = "Set pickup".localized
                                self.pinViewHeightConstraint.constant = 45.0
                                //self.pinViewWidth.constant = 45.0
                                self.pickUpView.isHidden = false
                                self.pinButton.isEnabled = true
                                
                                self.activityIndicator.startAnimating()
                                if UserDefaults.standard.object(forKey: KUSER_DETAILS) != nil && !self.isAddressSelected {
                                    self.getReachTime(location: position.target, pickUpSet: true, delegatelatitude: "", delegatelongitude: "")
                                }
                            }
                            else if self.changeTextOf == "DropUp"
                            {
                                self.timeView.isHidden = true
                                self.titlelbl.text = "Set Destination".localized
                                
                                self.dropUpView.isHidden = false
                                
                                self.pinButton.isEnabled = true
                                self.dropUpAddressField.text = "\(addressString.trimmingCharacters(in: .whitespaces).localized)"
                                self.dropUpLocationData.setLocationData(latitude: "\(position.target.latitude)", longitude: "\(position.target.longitude)", address: addressString.trimmingCharacters(in: .whitespaces))
                            }
                            else
                            {
                                self.PinView.isHidden = true
                                self.timeView.isHidden = true
                            }
                        }
                    }
                    else {
                        self.timeView.isHidden = true
                        self.PinView.isHidden = false
                        self.titlelbl.text = "Your-Taxi not available".localized
                        self.pinButton.isEnabled = false
                        if self.changeTextOf == "PickUp"
                        {
                            self.pickUpView.isHidden = false
                        }
                        else if self.changeTextOf == "DropUp"{
                            self.dropUpView.isHidden = false
                        }
                        else{
                            self.PinView.isHidden = true
                            self.timeView.isHidden = true
                        }
                        self.categoryTimeView.isHidden = true
                    }
                }
                
                self.isAddressSelected = false
            })
        })
        
    }
 }
 
 
 //MARK:- EXTENSION MKMAPVIEW DELEGATE
 extension HomeViewController : MKMapViewDelegate {
    
    func getReachTime(location : CLLocationCoordinate2D, pickUpSet: Bool, delegatelatitude: String, delegatelongitude: String ) {
        print("loca:", delegatelatitude)
        print("loca:", delegatelongitude)
        if(checkConnection()){
            
            DispatchQueue.global(qos: .background).async {
                let latitudeStr = pickUpSet ? String(format: "%f", location.latitude) : delegatelatitude
                let longitudeStr = pickUpSet ? String(format: "%f", location.longitude) :
                    delegatelongitude
                var parameters:[String:Any] = [
                    "PickupLatitude": latitudeStr,
                    "PickupLongitude":longitudeStr,
                    ]
                self.hideHUD()
                let jsonString = returnJsonString(param: parameters)
                 print("location:", jsonString)
                let Request: WebserviceHelper = WebserviceHelper()
                let headerDict = Request.getHeaderForRider(parameters: parameters)
                var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                parameters = ["data":encryptedData!]
                let jsonParam = returnJsonString(param: parameters)
               // printJsonStringLog(apiName: KDriverReachingTime_URL, parameter: jsonParam as NSString, headerDict: headerDict)
                Request.CustomHTTPPostJSONWithHeader(url: KDriverReachingTime_URL, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                    DispatchQueue.main.async {
                        
                        if ((error) != nil){
                            print(error.debugDescription)
                            return;
                        }else{
                            let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                            guard let data = responseObject.value(forKey: "data") as? String else {
                                self.showSimpleAlertController(nil, message: "Something Went Wrong".localized)
                                return
                            }
                            let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                            
                            let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                           
                            self.activityIndicator.stopAnimating()
                            if(responseContent != nil){
                                let status = "success" as String
                                if status == "success"{
                                    
                                    let responseDict = responseContent as! NSDictionary
                                    
                                    
                                    self.carCategoryTimeDict = responseDict
                                    
                                    self.stopCarCategoryTime()
                                    
                                    var reachTimeMsg = ""
                                    var rechTimeStatus = false
                                    
                                    if self.delegate!.carType == "1"
                                    {
                                        if responseDict["budget"] != nil {
                                            let value = responseDict.value(forKey: "budget") as! NSDictionary
                                            reachTimeMsg = ((value.value(forKey: "TimeToReach") as! NSString) as String).localized
                                            let status = (value.value(forKey: "Status") as! Bool) as Bool
                                            if status == true {
                                                rechTimeStatus = true
                                            }
                                        }
                                    }
                                    else if self.delegate!.carType == "2"
                                    {
                                        if responseDict["business"] != nil {
                                            let value = responseDict.value(forKey: "business") as! NSDictionary
                                            reachTimeMsg = ((value.value(forKey: "TimeToReach") as! NSString) as String).localized
                                            let status = (value.value(forKey: "Status") as! Bool) as Bool
                                            if status == true {
                                                rechTimeStatus = true
                                            }
                                        }
                                    }
                                    else if self.delegate!.carType == "3"
                                    {
                                        if responseDict["comfort"] != nil {
                                            let value = responseDict.value(forKey: "comfort") as! NSDictionary
                                            reachTimeMsg = ((value.value(forKey: "TimeToReach") as! NSString) as String).localized
                                            let status = (value.value(forKey: "Status") as! Bool) as Bool
                                            if status == true {
                                                rechTimeStatus = true
                                            }
                                        }
                                    }
                                    
                                    if rechTimeStatus == true {
                                        
                                        self.pinButton.isUserInteractionEnabled = true
                                        self.isCarAvailable = true
                                        self.reachTimelbl.text = reachTimeMsg
                                        self.reachTimeToRider = reachTimeMsg
                                        self.pinViewHeightConstraint.constant = 45.0
                                        self.PinView.layer.cornerRadius = 22.5
                                        self.reachTimelbl.isHidden = false
                                        
                                        if (!self.timeView.isHidden){
                                            self.timeView.isHidden = false
                                            self.PinView.isHidden = false
                                            self.pinBottomLbl.isHidden = true
                                        }
                                    }
                                    else {
                                        self.isCarAvailable = false
                                        self.reachTimelbl.text = reachTimeMsg
                                        self.reachTimeToRider = reachTimeMsg
                                        self.pinViewHeightConstraint.constant = 60.0
                                        self.PinView.layer.cornerRadius = 30.0
                                        self.timeView.isHidden = true
                                        self.reachTimelbl.isHidden = true
                                        self.PinView.isHidden = false
                                        self.pinBottomLbl.isHidden = false
                                    }
                                }else{
                                    self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "Message") as? String ?? ""))
                                }
                            }
                        }
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.delegate?.userLocation = locations.last
        print("Current detected location \(String(describing: self.delegate?.userLocation))")
        print("Current location Course \(String(describing: self.delegate?.userLocation?.course))")
        
        if rideMapView != nil {
            if let course = self.delegate?.userLocation?.course {
               self.rotation = Double(course) ?? 0.0
                self.rotation = self.rotation < 0.0 ? 0.0 : self.rotation
                print(self.rotation)
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func checkLocationEnable(status :CLAuthorizationStatus){
        
        if CLLocationManager.locationServicesEnabled(){
            switch status {
            case .authorizedAlways , .authorizedWhenInUse:
                break;
            case .denied , .notDetermined , .restricted:
                
                let alertController = UIAlertController(
                    title: "Background Location Access Disabled".localized,
                    message: "In order to be notified, please open this app's settings and set location access to 'Always'.".localized,
                    preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
                alertController.addAction(cancelAction)
                
                let openAction = UIAlertAction(title: "Open Settings".localized, style: .default) { (action) in
                    if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                alertController.addAction(openAction)
                
                self.present(alertController, animated: true, completion: nil)
                return;
            }
        }
    }
    func animateViewToCurrentLocation(coordinate : CLLocationCoordinate2D) {
        print("coordinate:-", coordinate)
        let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: 15.0)
        rideMapView.camera = camera
        rideMapView.animate(to: camera)
        rideMapView.isMyLocationEnabled = true
        rideMapView.settings.myLocationButton = true
        rideMapView.settings.compassButton = true
    }
    
 }
 
 //MARK:- EXTENSION HOME VIEW CONTROLLER
 extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
 }
 extension HomeViewController : ARCarMovementDelegate {
    
    func arCarMovementMoved(_ marker: GMSMarker) {
        print("Delegate")
    }
 }
 
      

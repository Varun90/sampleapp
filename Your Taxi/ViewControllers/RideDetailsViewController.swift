

import UIKit

class RideDetailsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var tableview: UITableView!
    var currentTripInfo : NSMutableDictionary?
    var rideDetailAry = NSMutableArray()
    var titleAry = NSMutableArray()
    var tableviewCell = NSMutableArray()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Ride Details".localized
        
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(RideDetailsViewController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        
       setupData()
        
    }
    
    func setupData() {
        
        titleAry.add("Taxi/Limousine:".localized)
        titleAry.add("TotalPassengers:".localized)
        titleAry.add("Needchildseat(3-12 Year)?:".localized)
        titleAry.add("TotalLuggages&Handbages:".localized)
        titleAry.add("Smoking:".localized)
        titleAry.add("Wheelchair:".localized)
        titleAry.add("Animals:".localized)
        
        var valuesDic = NSMutableDictionary()
        valuesDic.setObject((currentTripInfo?.value(forKey: "taxi_or_limo"))!, forKey: titleAry.object(at: 0) as! String as NSCopying )
        rideDetailAry.add(valuesDic)
        
        valuesDic = NSMutableDictionary()
        valuesDic.setObject((currentTripInfo?.value(forKey: "passenger"))!, forKey: titleAry.object(at: 1) as! String as NSCopying)
        rideDetailAry.add(valuesDic)
        
        valuesDic = NSMutableDictionary()
        valuesDic.setObject((currentTripInfo?.value(forKey: "num_of_kids"))!, forKey: titleAry.object(at: 2) as! String as NSCopying)
        rideDetailAry.add(valuesDic)
        
        valuesDic = NSMutableDictionary()
        valuesDic.setObject((currentTripInfo?.value(forKey: "handbag"))!, forKey: titleAry.object(at: 3) as! String as NSCopying)
        rideDetailAry.add(valuesDic)
        
        valuesDic = NSMutableDictionary()
        valuesDic.setObject((currentTripInfo?.value(forKey: "smoke"))!, forKey: titleAry.object(at: 4) as! String as NSCopying)
        rideDetailAry.add(valuesDic)
        
        valuesDic = NSMutableDictionary()
        valuesDic.setObject((currentTripInfo?.value(forKey: "is_wheelchair"))!, forKey: titleAry.object(at: 5) as! String as NSCopying)
        rideDetailAry.add(valuesDic)
        
        valuesDic = NSMutableDictionary()
        valuesDic.setObject((currentTripInfo?.value(forKey: "is_animal"))!, forKey: titleAry.object(at: 6) as! String as NSCopying)
        rideDetailAry.add(valuesDic)
    }
    
    // MARK: - Actions Methods
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rideDetailAry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableviewCell.count > 0 {
         
            let cell = tableviewCell.object(at: indexPath.row) as! UITableViewCell
            let titleLbl = cell.viewWithTag(1) as! UILabel
            let keyString = titleAry.object(at: indexPath.row) as! String
            
            let titleSize = self.calculateContentHeight(CGSize(width: titleLbl.frame.size.width, height: 9999), contentText: keyString)
            
            
            let detailsLbl = cell.viewWithTag(2) as! UILabel
            let dicDetails = rideDetailAry.object(at: indexPath.row) as! NSDictionary
            
            if dicDetails.value(forKey: keyString) as? String != nil
            {
                let detailsSize = self.calculateContentHeight(CGSize(width: detailsLbl.frame.size.width, height: 9999), contentText: (dicDetails.value(forKey: keyString) as? String)!)
                if titleSize > detailsSize {
          
                    return titleSize+32
                }else{
                    
                  
                    return detailsSize+32
                }
            }            
        }
       
       return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell1:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as UITableViewCell!
       
        if !tableviewCell.contains(cell1) {
           tableviewCell.add(cell1)
        }
        
        let titleLbl = cell1.viewWithTag(1) as! UILabel
        let detailsLbl = cell1.viewWithTag(2) as! UILabel
        
        let keyString = titleAry.object(at: (indexPath as NSIndexPath).row) as! String
        titleLbl.text = keyString
        let dicDetails = rideDetailAry.object(at: (indexPath as NSIndexPath).row) as! NSDictionary
        detailsLbl.text = dicDetails.value(forKey: keyString) as? String
        
        return cell1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
          return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
         return 0.01
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

import UIKit
import GooglePlaces

protocol CurrentRideDelegate : NSObjectProtocol {
    func setLocationData(strLocation:String?,type:String)
}
class AddLocationViewController: BaseViewController, UITableViewDelegate, UITextFieldDelegate,UITableViewDataSource {
    
    @IBOutlet weak var txtFieldAddressType : UITextField!
    @IBOutlet weak var typeView : UIView!
    @IBOutlet weak var addressTypeViewHeight : NSLayoutConstraint!
    @IBOutlet weak var manualView : UIView!
    @IBOutlet weak var manualTextField : UITextField!
    @IBOutlet var dotView: UIView!
    @IBOutlet var btnSave: UIButton!
    weak var locationDelegate : CurrentRideDelegate?
    var searchResultTableView:UITableView!
    var mainView : UIView!
    var placesClient = GMSPlacesClient()
    var filter = GMSAutocompleteFilter()
    var resultArray: NSMutableArray = []
    var selectedTextField : UITextField?
    var selectedRow : NSInteger?
    var favId : Int32?
    var locationType = ""
    var addressType = ""
    var selectedAddress = ""
    var favouriteCount: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFieldAddressType.placeholder = "Address Type".localized
        typeView.isHidden = (addressType == "favourite") ?  false : true
        addressTypeViewHeight.constant = (addressType == "favourite") ?  50 : 0
        btnSave.setTitle("Save".localized, for: .normal)
        btnSave.isHidden = (addressType == "edit_dropup_address" || addressType == "edit_pickup_address") ?  true : false
        if locationType == "drop_up"{
            dotView.backgroundColor = UIColor(red: 230/255.0, green: 69/255.0, blue: 11/255.0, alpha: 1.0)
        }
        else{
            dotView.backgroundColor = kLIGHT_OLIVE_COLOR
        }
        
        if addressType == "home"{
            manualTextField.placeholder = "Enter home address.".localized
            self.title = "HOME ADDRESS".localized
        }
        else if addressType == "work"{
            manualTextField.placeholder = "Enter work address.".localized
            self.title = "WORK ADDRESS".localized
        }
        else if addressType == "edit_pickup_address"{
            manualTextField.placeholder = "Enter Pickup Location.".localized
            self.title = "SET PICKUP LOCATION".localized
        }
        else if addressType == "edit_dropup_address"{
            manualTextField.placeholder = "Enter Dropup Location.".localized
            self.title = "SET DESTINATION".localized
        }
        else{
            manualTextField.placeholder = "Enter favorite address.".localized
            self.title = "Favorites".localized
        }
        manualTextField.text = selectedAddress
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(AddLocationViewController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        self.slideMenuController()?.removeLeftGestures()
        initSearchTableView()
        manualTextField.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        manualTextField.becomeFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func backAction(){
        self.view.endEditing(true)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func initSearchTableView() -> Void {
        mainView = UIView.init(frame: CGRect(x: self.manualView.frame.origin.x, y: 100, width: self.manualView.frame.size.width, height: 200))
        mainView.autoresizingMask = [.flexibleTopMargin,.flexibleWidth]
        mainView.backgroundColor = UIColor.lightGray
        self.view.addSubview(mainView)
        
        searchResultTableView=UITableView.init(frame: CGRect(x: 1, y: 0, width: mainView.frame.size.width - 2,height: mainView.frame.size.height - 1))
        searchResultTableView.delegate = self
        searchResultTableView.dataSource=self
        searchResultTableView.tag=200
        searchResultTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        mainView.addSubview(searchResultTableView)
        mainView.isHidden=true
    }
    
    func placeAutocomplete(_ textField: NSString) {

        let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(GOOGLE_WEB_API_KEY)&input=\(textField)&components=country:ch|country:li|country:de|country:at|country:in"
        let Request: WebserviceHelper = WebserviceHelper()
        
        Request.HTTPGetJSON(url: urlString,withIdicator: false) { (result, error) in
            if ((error) != nil){
            }else{
                let responseContent = Request.convertStringToDictionary(text: result )
                if responseContent != nil {
                    if let value = responseContent?["predictions"] as? [[String:Any]] {
                        self.resultArray = NSMutableArray()
                        for data in value {
                            
                            self.resultArray.add((data as [String:Any])["description"] as! String)
                        }
                        if self.resultArray.count > 0{
                            
                            self.mainView.isHidden=false
                            self.view.bringSubview(toFront: self.mainView)
                            
                            self.searchResultTableView.reloadData()
                        }else{
                            self.mainView.isHidden=true
                        }
                    }
                    else {
                        self.mainView.isHidden=true
                    }
                }
                else {
                    self.mainView.isHidden=true
                }
            }
        }
    }
    // MARK: - TableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 34
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
        if resultArray.count>0 {
            cell.textLabel?.font=UIFont.systemFont(ofSize: 12)
            cell.textLabel?.text = resultArray[(indexPath as NSIndexPath).row] as? String
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        mainView.isHidden=true
        if resultArray.count>0 {
            let address = resultArray[(indexPath as NSIndexPath).row] as? String
            self.selectedTextField?.text = address
            self.selectedTextField?.resignFirstResponder()
            
            if(addressType == "edit_pickup_address" || addressType == "edit_dropup_address"){
                if(self.locationDelegate != nil){
                    self.locationDelegate?.setLocationData(strLocation: address, type: addressType)
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        if((addressType == "home" && self.favId != 0) || (addressType == "work" && self.favId != 0)){
            if manualTextField.text == "" {
                self.showSimpleAlertController(nil, message: addressType == "home" ? "Enter home address.".localized : "Enter work address.".localized)
            }else{
                UpdateFavoriteAddressToServer(manualTextField.text!)
            }
        }else if addressType == "favourite" && txtFieldAddressType.text == "" {
            self.showSimpleAlertController(nil, message: "Please enter the address type".localized)
        }else if manualTextField.text == "" {
            self.showSimpleAlertController(nil, message: addressType == "home" ? "Enter home address.".localized : addressType == "work" ? "Enter work address.".localized : "Enter favorite address.".localized)
        }else{
            setFavoriteAddressToServer(manualTextField.text!)
        }
    }
    
    func textFieldShouldBeginEditing(_ sender: UITextField) -> Bool {
        
        var height = CGFloat()
        if self.view.frame.size.width==320 && self.view.frame.size.height == 416 {
            height = 59
        }else  if self.view.frame.size.width==320 && self.view.frame.size.height == 504 {
            height=140
        }else {
            height=170
        }
        mainView.frame = CGRect(x: manualView.frame.origin.x ,y: manualView.frame.origin.y + manualView.frame.size.height + 1, width: self.manualView.frame.size.width,height: height)
        self.searchResultTableView.frame=CGRect(x: 1, y: 1, width: mainView.frame.size.width - 2, height: mainView.frame.size.height - 2)
        self.selectedTextField = sender
        filter = GMSAutocompleteFilter()
        filter.country = KSET_COUNTRY
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if newString.isEmpty {
            mainView.isHidden=true
            
        }else{
            placeAutocomplete(newString as NSString)
        }
        searchResultTableView.reloadData()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        mainView.isHidden=true
    }
    // MARK: - Webservice Methods
    func UpdateFavoriteAddressToServer(_ address:String)
    {
        if(checkConnection()){
            
            let addressTypeName = addressType == "favourite" ? txtFieldAddressType.text : addressType
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "FavouriteId" : self.favId,
                "UserId" : riderID,
                "FavouriteName" : addressTypeName,
                "LocationName" : address,
                ] as [String : Any]
            
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KSET_FAVOURITE_LOCATION, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KUPDATE_FAVOURITE_LOCATION, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    guard let data = responseObject.value(forKey: "data") as? String else {
                        self.showSimpleAlertController(nil, message: "Something Went Wrong")
                        return
                    }
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    print("SetFavoriteAddress :", responseContent ?? "")
                    if let value = responseContent?.value(forKey: "Status") as? Bool{
                        if value == true{
                            _ = self.navigationController?.popViewController(animated: true)
                        }else if value == false {
                            self.hideHUD()
                            if responseContent?.index(ofAccessibilityElement: "Errors") != nil {
                                if let ErrorMsg = responseContent?.value(forKey: "Errors") as? NSDictionary{
                                    if let fn = ErrorMsg.value(forKey: "FavouriteName") as? NSArray{
                                        self.showSimpleAlertController(nil, message: (fn[0] as? String)!)
                                    }
                                }
                            }
                            else{
                                self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "Message") as? String)!)
                            }
                        }
                        
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    func setFavoriteAddressToServer(_ address:String)
    {
        if(checkConnection()){
            
            let addressTypeName = addressType == "favourite" ? txtFieldAddressType.text : addressType
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "UserId" : riderID,
                "FavouriteName" : addressTypeName,
                "LocationName" : address,
                ] as [String : Any]
            
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KSET_FAVOURITE_LOCATION, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KSET_FAVOURITE_LOCATION, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    guard let data = responseObject.value(forKey: "data") as? String else {
                        self.showSimpleAlertController(nil, message: "Something Went Wrong")
                        return
                    }
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    print("SetFavoriteAddress :", responseContent ?? "")
                    if let value = responseContent?.value(forKey: "Status") as? Bool{
                        if value == true{
                            _ = self.navigationController?.popViewController(animated: true)
                        }else if value == false {
                            self.hideHUD()
                            if responseContent?.index(ofAccessibilityElement: "Errors") != nil {
                                if let ErrorMsg = responseContent?.value(forKey: "Errors") as? NSDictionary{
                                    if let fn = ErrorMsg.value(forKey: "FavouriteName") as? NSArray{
                                        self.showSimpleAlertController(nil, message: (fn[0] as? String)!)
                                    }
                                }
                            }
                            else{
                                self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "Message") as? String)!)
                            }
                        }
                        
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
}
// MARK: - GMSAutocompleteViewController
extension AddLocationViewController : GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if manualTextField.isFirstResponder
        {
            manualTextField.text = place.formattedAddress
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Swift.Error) {
        print("Error: \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }

    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}



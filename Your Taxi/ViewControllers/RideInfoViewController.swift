

let ADDWAITINGTIMETAG = 1
let ADDTRIPSTATUSTAG = 2
let CANCELTRIPTAG = 3
let FINISHTRIPTAG = 4
let FINISH = 5

import UIKit
import INTULocationManager

enum DriverRideWayStatus : String  {
    case driver_on_the_way
    case arrived_at_pick_up_location
    case waiting_for_rider
    case rider_on_board
    case ride_is_finished
}

class RideInfoViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource, SlideButtonDelegate,TrackLocationDelegate {
    
    
    // MARK: - PROPERTIES
    @IBOutlet var TripInfoTable: UITableView!
    @IBOutlet var TripConfirmTable: UITableView!
    @IBOutlet var infoControlSegment: UISegmentedControl!
    @IBOutlet var infoView: UIView!
    @IBOutlet var confirmView: UIView!
    @IBOutlet var addingView: UIView!
    @IBOutlet var waitingTimeTF: UITextField!
    @IBOutlet var moreKilometerTF: UITextField!
    @IBOutlet var waitingTimeLBL: UILabel!
    @IBOutlet var moreKilometerLBL: UILabel!
    @IBOutlet var updateRideBTN: UIButton!
    @IBOutlet var cancelTheTrip: UIButton!
    @IBOutlet var callToYourTaxiBtn: UIButton!
    @IBOutlet var callToYourTaxiBtnTrailing: NSLayoutConstraint!
    
    var pickerView: UIPickerView!
    var selectedTextField : UITextField?
    var pickerToolBar: UIToolbar!
    var infoTitleArray : NSMutableArray!
    var infoValuesArray : NSMutableArray!
    var confirmStatusArray : NSMutableArray!
    var currentTripInfo : NSMutableDictionary!
    var UserLocation : CLLocation?
    var paramterName = ""
    var fileURL : URL!
    var timeScheduled:Timer?
    var currentRideStatus = 2 //Driver Assigned
    var btnCallYOURTAXI = ""
    var dicLocation : NSDictionary?
    var driveCurrentTripTimer : Timer?
    // MARK: - VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Bookingdetails".localized
        
        infoTitleArray = NSMutableArray()
        infoValuesArray = NSMutableArray()
        confirmStatusArray = NSMutableArray()
        
        setScreenUI()
        setInfoTripData()
        setConfirmTripData()
        getUserCurrentLocation()
        
        if !KAPP_RIDER {
            cancelTheTrip.setTitle("Cancel Booking".localized, for: .normal)
            btnCallYOURTAXI = "Call Yourtaxi".localized
            btnCallYOURTAXI = btnCallYOURTAXI.replacingOccurrences(of: "YOURTAXI", with: "<i>YOUR</i><b>TAXI</b>")
            callToYourTaxiBtn.setAttributedTitle(stringFromHtml(string: btnCallYOURTAXI, fontSize: "16", colorType: "#ffffff"), for: .normal)
            NotificationCenter.default.removeObserver(HomeViewController.self, name: NSNotification.Name(rawValue: "InstantBooking"), object: nil)
        }
        
        let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        fileURL = dir?.appendingPathComponent("Distance.txt")

        TripConfirmTable.tableFooterView = UIView()
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        print("currentTripInfo", currentTripInfo)
        
        //Set the Current Ride Status
        if let rideStatus = currentTripInfo["RideStatus"] as? NSInteger {
            currentRideStatus = rideStatus
        }
        if let showArrive = currentTripInfo["ShowArriveBtn"] as? Bool {
            if showArrive{
                driveCurrentTripTimer?.invalidate()
                btnActiveInactive()
            }else{
                driveCurrentTripTimer = Timer.scheduledTimer(timeInterval: 15.0, target: self, selector: #selector(callWebserviceForGettingCurrentTrip), userInfo: nil, repeats: true)
            }
        }
        
     
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if !KAPP_RIDER {
            DispatchQueue.main.async {
                self.reloadTripInfoTable()
            }
            if UserDefaults.standard.value(forKey: "Time") != nil {
                if let privDate = UserDefaults.standard.value(forKey: "Time") as? NSDate {
                    let enterAppTime = NSDate()
                    let interval = NSInteger(enterAppTime.timeIntervalSince(privDate as Date))
                    //                let sec = interval % 60
                    print(" time call from appdel")
                    if 180 >= interval {
                        let remainingIntervalTime = 180 - interval
                        let timeIntr = TimeInterval(remainingIntervalTime)
                        timeScheduled = Timer.scheduledTimer(timeInterval: timeIntr, target: self, selector: #selector(btnActiveInactive), userInfo: nil, repeats: false)
                    } else {
                        Timer.scheduledTimer(timeInterval: 0.0, target: self, selector: #selector(btnActiveInactive), userInfo: nil, repeats: false)
                    }
                }
            }
        }
        
        self.title = "Bookingdetails".localized
        
        registerForKeyboardNotifications()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        //        self.title = ""
        unregisterForKeyboardNotifications()
        
        if timeScheduled != nil {
            timeScheduled?.invalidate()
            timeScheduled = nil
        }
        
    }
    
    override  func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !KAPP_RIDER {
          //  callToYourTaxiBtnTrailing.constant = isPreBookingRide() ? ((self.view.frame.size.width - 6) / 2) - (callToYourTaxiBtn.frame.size.width / 2) : 2
         //   cancelTheTrip.isHidden = isPreBookingRide() ? true : false
        }
    }
    
    //MARK:- MEMORY MANAGEMENT METHOD
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - TABLEVIEW DELEGATE/DATASOURCE METHODS
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return infoTitleArray.count
        }
        else if tableView.tag == 2 {
            return confirmStatusArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1 {
            
            var inputStr : NSString = "DefaultValue"
            
            if let value = infoValuesArray.object(at: (indexPath as NSIndexPath).row) as? String {
                inputStr = value as NSString
            }
            
            guard let aliasInputStr = infoValuesArray.object(at: (indexPath as NSIndexPath).row) as? NSString else {
                return 40
            }
            
            inputStr = aliasInputStr
            
            let cellSize = getTextSize(inputStr, fontSize: CGFloat(16.0), contrainSize: CGSize(width: tableView.frame.size.width - 180, height: CGFloat(Float.greatestFiniteMagnitude)))
            if cellSize.height > 40 {
                if indexPath.row == 4 {
                    return cellSize.height + 15
                } else if indexPath.row == 3 || indexPath.row == 5 {
                    TripInfoTable.estimatedRowHeight = 44
                    return UITableViewAutomaticDimension
                }
                return cellSize.height
            }else if indexPath.row == 4 {
                return 50
            }else if indexPath.row == 5 {
                return (currentTripInfo?.value(forKey: "RideType") as? NSInteger) ?? 1 == 1 ? 60 : 40
            } else if indexPath.row == 3 || indexPath.row == 5 {
                TripInfoTable.estimatedRowHeight = 44
                return UITableViewAutomaticDimension
            }
            return 40
        }
        else if tableView.tag == 2 {
            return 60
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //Info Trip Table
        if tableView.tag == 1 {
            var cell = UITableViewCell()
            var cellIdentifier : NSString
            if (indexPath as NSIndexPath).row == 3 || (indexPath as NSIndexPath).row == 4 || (indexPath as NSIndexPath).row == 5 || (indexPath as NSIndexPath).row == 6 {
                cellIdentifier = "InfoTripRedirectCell"
                cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier as String, for: indexPath)
                let button :UIButton = cell.viewWithTag(3) as! UIButton
                button.setBackgroundImage(UIImage(named:"btn_location"), for: UIControlState())
                
            }
            else{
                cellIdentifier = "InfoTripSimpleCell"
                cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier as String, for: indexPath)
            }
            
            let titleLabel = cell.viewWithTag(1) as! UILabel
            titleLabel.text = (infoTitleArray.object(at: (indexPath as NSIndexPath).row) as? String)?.localized
            titleLabel.adjustsFontSizeToFitWidth = true
            
            let valueLabel = cell.viewWithTag(2) as! UITextView
            
            valueLabel.text = "DefaultValue"

            if let aliasInputStr = infoValuesArray.object(at: (indexPath as NSIndexPath).row) as? String {
                valueLabel.text = aliasInputStr
            }
            let navigateBtn = cell.viewWithTag(3) as! UIButton
            navigateBtn.isHidden = true

            if (indexPath as NSIndexPath).row == 3 || (indexPath as NSIndexPath).row == 5 || (indexPath as NSIndexPath).row == 9 {
                titleLabel.sizeToFit()
                valueLabel.sizeToFit()
            }
            
            if (indexPath as NSIndexPath).row == 3 || (indexPath as NSIndexPath).row == 4 || (indexPath as NSIndexPath).row == 5 {
                
                let goBtn = cell.viewWithTag(4) as! MMSlidingButton
                let arrivedBtn = cell.viewWithTag(5) as! MMSlidingButton
                let startRideBtn = cell.viewWithTag(6) as! MMSlidingButton
                let completeRideBtn = cell.viewWithTag(7) as! UIButton
                completeRideBtn.setTitle("End Trip".localized, for: .normal)
                goBtn.delegate = self
                startRideBtn.delegate = self
                arrivedBtn.delegate = self
                navigateBtn.isHidden = true
                
                goBtn.dragPointButtonLabel.text           = ""
                arrivedBtn.dragPointButtonLabel.text      = ""
                startRideBtn.dragPointButtonLabel.text    = ""
                goBtn.buttonLabel.text           = "Go".localized
                arrivedBtn.buttonLabel.text      = "Arrived".localized
                startRideBtn.buttonLabel.text    = "Start Ride".localized
                
                goBtn.isHidden = true
                arrivedBtn.isHidden = true
                startRideBtn.isHidden = true
                completeRideBtn.isHidden = true
                
                if (indexPath as NSIndexPath).row == 3 {
                    titleLabel.isHidden = false
                    valueLabel.isHidden = false
                }
                
                if (indexPath as NSIndexPath).row == 4 {
                    titleLabel.isHidden = true
                    valueLabel.isHidden = true
                    
                    if UserDefaults.standard.value(forKey: "rideStatus") == nil {
                        goBtn.isHidden = false
                        arrivedBtn.isHidden = true
                        startRideBtn.isHidden = true
                        completeRideBtn.isHidden = true
                        UserDefaults.standard.set(false, forKey: "toLabel")
                    } else {
                        if UserDefaults.standard.value(forKey: "rideStatus") as? String == "Go" {
                            DispatchQueue.main.async {
                                goBtn.unlocked                       = false
                                goBtn.imageView.isHidden             = false
                                goBtn.dragPoint.backgroundColor      = UIColor.lightGray
                                goBtn.dragPointButtonLabel.textColor = UIColor.gray
                                goBtn.isUserInteractionEnabled = false
                                goBtn.isHidden = false
                            }
                            arrivedBtn.isHidden = true
                            startRideBtn.isHidden = true
                            completeRideBtn.isHidden = true
                            UserDefaults.standard.set(false, forKey: "toLabel")
                        }  else  if UserDefaults.standard.value(forKey: "rideStatus") as? String == "Arrived" {
                            goBtn.isHidden = true
                            arrivedBtn.isHidden = false
                            startRideBtn.isHidden = true
                            completeRideBtn.isHidden = true
                            UserDefaults.standard.set(false, forKey: "toLabel")
                        } else if UserDefaults.standard.value(forKey: "rideStatus") as? String == "StartRideon" {
                            arrivedBtn.isHidden = true
                            goBtn.isHidden = true
                            startRideBtn.isHidden = false
                            completeRideBtn.isHidden = true
                            UserDefaults.standard.set(false, forKey: "toLabel")
                        } else if UserDefaults.standard.value(forKey: "rideStatus") as? String == "StartRideoff" {
                            DispatchQueue.main.async {
                                startRideBtn.unlocked                       = false
                                startRideBtn.imageView.isHidden             = false
                                startRideBtn.dragPoint.backgroundColor      = UIColor.lightGray
                                startRideBtn.dragPointButtonLabel.textColor = UIColor.gray
                                startRideBtn.isUserInteractionEnabled = false
                            }
                            arrivedBtn.isHidden = true
                            goBtn.isHidden = true
                            startRideBtn.isHidden = false
                            completeRideBtn.isHidden = true
                            UserDefaults.standard.set(true, forKey: "toLabel")
                        }  else if UserDefaults.standard.value(forKey: "rideStatus") as? String == "completeRide" {
                            arrivedBtn.isHidden = true
                            goBtn.isHidden = true
                            startRideBtn.isHidden = true
                            completeRideBtn.isHidden = false
                            UserDefaults.standard.set(true, forKey: "toLabel")
                        }
                    }
                }
                
                if (indexPath as NSIndexPath).row == 5 {
                    if cellIdentifier == "InfoTripRedirectCell" {
                        let infoTripCell = TripInfoTable.dequeueReusableCell(withIdentifier: cellIdentifier as String) as? infoTripRedirectTableViewCell
                        infoTripCell?.goSlideBtnHeight.constant = 0
                    }
                    if UserDefaults.standard.value(forKey: "toLabel") as! Bool == false {
                        titleLabel.text = ""
                        valueLabel.text = ""
                        titleLabel.isHidden = true
                        valueLabel.isHidden = true
                    } else {
                        titleLabel.isHidden = false
                        valueLabel.isHidden = false
                    }
                }
            }

            if titleLabel.text == "Payment:".localized {
                let callBtn = cell.viewWithTag(3) as! UIButton
                callBtn.isHidden = false
                callBtn.isUserInteractionEnabled = false
                
                if let value = currentTripInfo.object(forKey: "PaymentType") as? NSInteger  {
                    callBtn.setBackgroundImage( (value == 1) ? #imageLiteral(resourceName: "cash") : #imageLiteral(resourceName: "card"), for: .normal)
                }
            }
            
            if titleLabel.text == "Passenger:".localized {
                
                let callBtn = cell.viewWithTag(3) as! UIButton
                callBtn.isHidden = false
                callBtn.setBackgroundImage(#imageLiteral(resourceName: "call"), for: .normal)
                
                let value = String(format: "%.1f", (currentTripInfo.value(forKey: "RiderRating") as? Double) ?? 0.0)
                if  value != nil || value != "0.0" {
                    
                    let attachment = NSTextAttachment()
                    attachment.image = #imageLiteral(resourceName: "driver_star")
                    let attachmentString = NSAttributedString(attachment: attachment)
                    
                    var passString = "DefaultValue"
                    
                    if let value = infoValuesArray.object(at: (indexPath as NSIndexPath).row) as? String {
                        passString = value
                    }
                    
                    let myString = NSMutableAttributedString(string: passString)
                    let rateStr = NSAttributedString(string: "(\(value)")
                    let breaketStr = NSAttributedString(string: ")")
                    myString.append(rateStr)
                    myString.append(attachmentString)
                    myString.append(breaketStr)
                    valueLabel.attributedText = myString
                }
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none;
            
            valueLabel.linkTextAttributes = [NSForegroundColorAttributeName: UIColor.init(red: 239/255.0, green: 117/255.0, blue: 33/255.0, alpha: 1.0)]
            valueLabel.textColor = UIColor.white
            return cell
        }
            
            //Confirm Trip Table
        else {
            var cell = UITableViewCell()
            var cellIdentifier : NSString
            cellIdentifier = "ConfirmTripCell"
            cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier as String, for: indexPath)
            let button :UIButton = cell.viewWithTag(1) as! UIButton
            
            if UserDefaults.standard.value(forKey: "RideIsStart") as? String == "1" && confirmStatusArray.object(at: (indexPath as NSIndexPath).row)as? String == "START RIDE".localized
            {
                button.isEnabled = false
            }
            else
            {
                button.isEnabled = true
            }
            
            if confirmStatusArray.object(at: (indexPath as NSIndexPath).row)as? String == "CALL TO YOUR TAXI".localized || confirmStatusArray.object(at: (indexPath as NSIndexPath).row)as? String == "CALL THE RIDER".localized
                
            {
                button.setImage(UIImage(named:"btn_call")?.withRenderingMode(.alwaysOriginal), for: UIControlState())
            }
            else
            {
                button.setImage(nil, for: UIControlState())
            }
            button.setTitle((confirmStatusArray.object(at: (indexPath as NSIndexPath).row) as? String)?.localized, for: UIControlState())
            button.layer.cornerRadius = 20;
            button.clipsToBounds = true;
            button.titleLabel?.adjustsFontSizeToFitWidth = true
            cell.selectionStyle = UITableViewCellSelectionStyle.none;
            return cell
        }
    }
    
    // MARK: - TEXTFIELD DELEGATE METHODS
    func textFieldShouldReturn(_ sender: UITextField) -> Bool {
        if sender == waitingTimeTF {
            moreKilometerTF.becomeFirstResponder()
        }
        else if sender == moreKilometerTF {
            moreKilometerTF.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ sender: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    //MARK:- TOUCH METHODS
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if self.view.endEditing(true) {
            self.view.endEditing(false)
        }
    }
    
    // MARK: - PICKER DELEGATE METHODS
    func addPickerView (_ sender : UITextField) {
        if pickerView == nil {
            pickerView = UIPickerView()
            pickerView.delegate = self
            pickerView.dataSource = self
            pickerView.backgroundColor = UIColor.white
        }
        pickerView.tag = sender.tag
        var selectedRow : Int = 0
        
        if !(sender.text ?? "").isEmpty{
            selectedRow = Int(sender.text!)! - 1
        }
        
        pickerView.selectRow(selectedRow, inComponent: 0, animated: false)
        if sender.tag == 1 {
            sender.inputAccessoryView = createPickerToolBar("Select Minutes".localized as NSString, tag: 1)
        }
        else{
            sender.inputAccessoryView = createPickerToolBar("Select Kilometer".localized as NSString, tag: 2)
        }
        
        sender.inputView = pickerView
        selectedTextField = sender
    }
    func createPickerToolBar (_ title : NSString,tag : NSInteger) -> UIToolbar {
        pickerToolBar = UIToolbar()
        pickerToolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44)
        pickerToolBar.barStyle = UIBarStyle.default
        
        let titleLabel:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.black
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.numberOfLines = 1
        titleLabel.text = title as String
        
        let titleBarItem:UIBarButtonItem = UIBarButtonItem.init(customView: titleLabel)
        let cancelButton:UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(RideInfoViewController.cancelPickerViewAction(_:)))
        let doneButton:UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(RideInfoViewController.donePickerViewAction(_:)))
        let flexiableItem:UIBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        
        doneButton.tag = tag
        
        var items = [UIBarButtonItem]()
        items.append(cancelButton)
        items.append(flexiableItem)
        items.append(titleBarItem)
        items.append(flexiableItem)
        items.append(doneButton)
        
        pickerToolBar.setItems(items, animated: false)
        return pickerToolBar
    }
    func cancelPickerViewAction (_ sender : AnyObject){
        selectedTextField?.resignFirstResponder()
    }
    func donePickerViewAction (_ sender : AnyObject){
        let selectedRow  = pickerView.selectedRow(inComponent: 0)
        selectedTextField!.text = "\(selectedRow+1)"
        selectedTextField?.resignFirstResponder()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ("\(row+1)")
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1{
            return 600
        }
        else{
            return 100
        }
        
    }
    
    // MARK: - Data Methos
    func setScreenUI(){
        infoControlSegment.setTitle("Trip Info".localized, forSegmentAt: 0)
        infoControlSegment.setTitle("Confirm Page".localized, forSegmentAt: 1)
        
        infoView.isHidden = false
        confirmView.isHidden = true
        addingView.isHidden = true
        
        // ------ Waiting Time TextField ------
        waitingTimeLBL.text = "Add Waiting Time (Minutes)".localized
        waitingTimeTF.placeholder = "Add Waiting Time (Minutes)".localized
        
        let leftViewWT = UIView.init(frame: CGRect(x: 0, y: 0, width: 5, height: waitingTimeTF.frame.size.height))
        leftViewWT.backgroundColor = waitingTimeTF.backgroundColor
        waitingTimeTF.leftView = leftViewWT
        waitingTimeTF.leftViewMode = UITextFieldViewMode.always
        
        // ------ More Kilometer TextField ------
        moreKilometerLBL.text = "Add More Kilometer".localized
        moreKilometerTF.placeholder = "Add More Kilometer".localized
        
        let leftViewMT = UIView.init(frame: CGRect(x: 0, y: 0, width: 5, height: moreKilometerTF.frame.size.height))
        leftViewMT.backgroundColor = moreKilometerTF.backgroundColor
        moreKilometerTF.leftView = leftViewMT
        moreKilometerTF.leftViewMode = UITextFieldViewMode.always
        
        
        // ------ Update button ------
        updateRideBTN.setTitle("UPDATE RIDE".localized, for: UIControlState())
        updateRideBTN.layer.cornerRadius = 20;
        updateRideBTN.clipsToBounds = true;
    }
    func setInfoTripData(){
        infoTitleArray.add("Bookings Nr.:".localized)
        infoTitleArray.add("DateTime:".localized)
        infoTitleArray.add("Order Type:".localized)
        infoTitleArray.add("From:".localized)
        
        if ((currentTripInfo.value(forKey: "RideType") as? NSInteger) == 1) {
            infoTitleArray.add("Ride Type:".localized)
        }
        else
        {
            infoTitleArray.add("Ride Type:".localized)
        }
        infoTitleArray.add("To:".localized)
        
        //ReCreate Distance / Duration
       
        infoTitleArray.add("Payment Type:".localized)
        infoTitleArray.add("Passenger:".localized)
        
        if ((currentTripInfo.value(forKey: "RideType") as? NSInteger) == 1) {
            infoTitleArray.add("Ride Type:".localized)
        }
        else
        {
            infoTitleArray.add("Ride Type:".localized)
        }
        
        if let value = currentTripInfo.value(forKey: "is_pl_airport") as? String {
            
            if value == "1" || value == "Yes"  {
                infoTitleArray.add("Flight Nr.:".localized)
                infoTitleArray.add("Gate Nr.:".localized)
            }
        }
        
        infoTitleArray.add("Category:".localized)
        infoTitleArray.add("NotesMessage".localized)
        
        if let value = currentTripInfo.value(forKey: "BookingId") as? NSInteger {//id
            infoValuesArray.add("\(value)")
        }
        else {
            infoValuesArray.add("")
        }
        
        var pickUpDate = currentTripInfo.value(forKey: "BookingDateTime") as! String
        if (pickUpDate ).isEmpty{
            pickUpDate = ""
        }
        else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateObj = dateFormatter.date(from: (currentTripInfo.value(forKey: "BookingDateTime") as? String)!)
            dateFormatter.dateFormat = "dd MMMM yyyy / HH:mm:ss"
            if dateObj != nil {
                pickUpDate = dateFormatter.string(from: dateObj!)
            } else {
                pickUpDate = (currentTripInfo.value(forKey: "BookingDateTime") as? String)!
            }
        }
        infoValuesArray.add(pickUpDate)
        
        
        if let value = currentTripInfo.value(forKey: "BookingType") as? String {
            infoValuesArray.add(value)
        }
        
        if let value = currentTripInfo.value(forKey: "BookingType") as? NSInteger {
            if value == 2 {
                infoValuesArray.add("Pre-Booking".localized)
            }else {
                infoValuesArray.add("For Now".localized)
            }
        }
        
        if let value = currentTripInfo.value(forKey: "PickupLocation") as? String {
            infoValuesArray.add(value)
        }
        else {
            infoValuesArray.add("")
        }
 
        if let value = currentTripInfo.value(forKey: "is_pl_airport") as? String
        {
            if value == "1" || value == "Yes" {
                
                if let value = currentTripInfo.value(forKey: "flight_nr") as? String {
                    infoValuesArray.add(value)
                }
                else {
                    infoValuesArray.add("")
                }
                
                if let value = currentTripInfo.value(forKey: "gate_nr") as? String {
                    infoValuesArray.add(value)
                }
                else {
                    infoValuesArray.add("")
                }
            }
        }
        
        //Set Ride distance
        setRideDuration()
        
        //Set to location
        if let value = currentTripInfo.value(forKey: "DropLocation") as? String {
            infoValuesArray.add(value)
        }
        else {
            infoValuesArray.add("")
        }
        
        //Set Ride distance
        //setRideDuration()
        
        
        var updatedPaymentTypeValue: String
        
        if let value = currentTripInfo.object(forKey: "PaymentType") as? NSInteger  {
            updatedPaymentTypeValue = (value == 1) ? "Cash" : "Card"
            infoValuesArray.add(updatedPaymentTypeValue.capitalizingFirstLetter().localized)
        }
        else {
            infoValuesArray.add("Card".capitalizingFirstLetter().localized)
        }
        
        var passengerName = ""
        if let value = (currentTripInfo.object(forKey: "RiderDetails") as! NSDictionary).value(forKey: "FirstName") as? String {
            passengerName = value;
        }
        if let value = (currentTripInfo.object(forKey: "RiderDetails") as! NSDictionary).value(forKey: "LastName") as? String {
            passengerName = "\(passengerName) \(value)";
        }
        infoValuesArray.add(passengerName)
        
        setRideDuration()
        
        if let value = currentTripInfo.value(forKey: "CarTypeId") as? NSInteger {
            var carType = ""
            if value == 1 {
                carType = "YourBudget"
            } else if value == 2 {
                carType = "YourLuxury"
            } else if value == 3 {
                carType = "YourVan"
            }
            infoValuesArray.add(carType)
        }
        else {
            infoValuesArray.add("")
        }
        

        if let value = currentTripInfo.value(forKey: "DriverNotes") as? String {
            let valueSTr = value.trimmingCharacters(in: .whitespaces)
            if valueSTr == ""
            {
                infoTitleArray.removeLastObject()
            }
            else
            {
                infoValuesArray.add(value)
            }
        }else {
            infoTitleArray.removeLastObject()
        }
    }
    
    func setRideDuration() {
        if (currentTripInfo?.value(forKey: "RideType") as? NSInteger) == 1 {
            
            let hours = "\(currentTripInfo?.value(forKey: "RideHours") as! NSInteger)"
            let inclusiveKm = getInclusiveKM(hours)
            let hoursInt = Int(hours)!
            if(hoursInt>1){
                let final_value = "\("Hourly".localized)" + " (" + "\(hours)" + " hrs - " + "\("Incl.".localized) " + "\(inclusiveKm)" + ")"
                infoValuesArray.add(final_value)
            }
            else{
                let final_value = "\("Hourly".localized)" + " (" + "\(hours)" + " hr - " + "\("Incl.".localized) " + "\(inclusiveKm)" + ")"
                infoValuesArray.add(final_value)
            }
        }
        else
        {
            var totalKM = " "
            var totalTime = " "
            
            if let tkm = currentTripInfo?.value(forKey: "TotalKm") as? NSInteger {
                totalKM = "\(tkm)"
            }
            
            if let tKm = currentTripInfo?.value(forKey: "TotalKm") as? NSString {
                totalKM = tKm as String
            }
            
            if let tTime = currentTripInfo?.value(forKey: "TotalTime") as? NSInteger {
                totalTime = "\(tTime)"
            }
            
            if let tTime = currentTripInfo?.value(forKey: "TotalTime") as? NSString {
                totalTime = tTime as String
            }
            infoValuesArray.add("Per Km".localized)
        }
    }
    
    func isPreBookingRide()-> Bool {
        
        var hideCancelBooking = false
        
        if currentRideStatus >= 3 {
            return true
        }
        
        if let value = currentTripInfo.value(forKey: "BookingType") as? String {
            hideCancelBooking = (value == "Pre-booking" || value == "Pre-Booking") ? true : false
        }
        
        if let value = currentTripInfo.value(forKey: "BookingType") as? NSInteger {
            hideCancelBooking = (value == 2) ? true : false
        }
        
        return hideCancelBooking
    }
    
    func setConfirmTripData() {
        
        confirmStatusArray.add("DRIVER ON THE WAY".localized)
        confirmStatusArray.add("ARRIVED".localized)
        confirmStatusArray.add("CALL THE RIDER".localized)
        confirmStatusArray.add("START RIDE".localized)
        confirmStatusArray.add("RIDE IS FINISHED".localized)
        confirmStatusArray.add("RIDER DID NOT SHOW UP".localized)
        confirmStatusArray.add("CANCEL THE TRIP".localized)
        confirmStatusArray.add("CALL TO YOUR TAXI".localized)
        
    }
    
    // MARK: - Actions Methods
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    func downButtonPress(_ sender: UIButton){
        if sender.tag == 1 {
            waitingTimeTF?.becomeFirstResponder()
        }
        else{
            moreKilometerTF?.becomeFirstResponder()
        }
    }
    @IBAction func segmentValueChange(_ sender: AnyObject) {
        
        if self.view.endEditing(true) {
            self.view.endEditing(false)
        }
        if infoControlSegment.selectedSegmentIndex == 0 {
            // Trip Info
            infoView.isHidden = false
            confirmView.isHidden = true
            addingView.isHidden = true
        }
        else if infoControlSegment.selectedSegmentIndex == 1 {
            // Confirm Page
            infoView.isHidden = true
            confirmView.isHidden = false
            addingView.isHidden = true
            
        }
        else{
            // Adding Page
            infoView.isHidden = true
            confirmView.isHidden = true
            addingView.isHidden = false
            waitingTimeTF.text = ""
            moreKilometerTF.text = ""
        }
    }
    @IBAction func navigateButtonAction(_ sender: AnyObject) {
        let currentCell : UITableViewCell = sender.superview?!.superview as! UITableViewCell
        let currentIndexPath : IndexPath = TripInfoTable.indexPath(for: currentCell)!
        var destLocation : CLLocation?
        
        if (currentIndexPath as NSIndexPath).row == 3 {
            if UserLocation == nil{
                getUserCurrentLocation()
            }
            else{
                let lString = currentTripInfo.value(forKey: "pick_up_location_latlong") as! String
                let content = lString.components(separatedBy: ",")
                
                if (content.first == "") || (content.last == "") {
                    
                    showSimpleAlertController("Invalid", message:"Invalid Address.")
                    return
                }else
                {
                    
                    destLocation = CLLocation.init(latitude: Double(content.first! as String)!, longitude: Double(content.last! as String)!)
                    openLocationSource(UserLocation!, destination: destLocation!)
                }
            }
        }
        else if (currentIndexPath as NSIndexPath).row == 4 {
            let slString = currentTripInfo.value(forKey: "pick_up_location_latlong") as! String
            let sContent = slString.components(separatedBy: ",")
            
            if (sContent.first == "") || (sContent.last == ""){
                
                showSimpleAlertController("Invalid", message:"Invalid Address.")
                return;
                
            }else{
                
                let sourceLocation : CLLocation? = CLLocation.init(latitude: Double(sContent.first! as String)!, longitude: Double(sContent.last! as String)!)
                
                let dlString = currentTripInfo.value(forKey: "drop_up_location_latlong") as! String
                let dContent = dlString.components(separatedBy: ",")
                destLocation = CLLocation.init(latitude: Double(dContent.first!)!, longitude: Double(dContent.last!)!)
                openLocationSource(sourceLocation!, destination: destLocation!)
            }
        }
    }
    
    @IBAction func callRiderBtnAction(_ sender: Any) {
        
        guard let riderDict = currentTripInfo.object(forKey: "RiderDetails") as? NSDictionary else {
            return
        }
        
        guard let dialCode = riderDict.value(forKey: "DialCode") as? String else {
            return
        }
        
        guard let phoneNumber = riderDict.value(forKey: "PhoneNo") as? String else {
            return
        }
        
        let riderPhoneNumber = phoneNumber.trimmingCharacters(in: .whitespaces)
        let riderDialCode = dialCode.trimmingCharacters(in: .whitespaces)
        
        let dialNumber = "\(riderDialCode)\(riderPhoneNumber)"
        
        let telString = "tel://\(dialNumber)"
        if UIApplication.shared.canOpenURL(URL(string: telString)!){
            UIApplication.shared.openURL(URL(string: telString)!)
        }
    }
    
    @IBAction func callYourTaxiBtnAction(_ sender: Any) {
        let telString = "tel://+41445209014"
        if UIApplication.shared.canOpenURL(URL(string: telString)!){
            UIApplication.shared.openURL(URL(string: telString)!)
        }
    }
    
    
    func btnActiveInactive() {
        print("btnActiveInactive -------")
        UserDefaults.standard.removeObject(forKey: "Time")
        if UserDefaults.standard.value(forKey: "rideStatus") as? String == "Go" {
            if let showArrive = currentTripInfo["ShowArriveBtn"] as? Bool {
                if showArrive{
                   UserDefaults.standard.set("Arrived", forKey: "rideStatus")
                   driveCurrentTripTimer?.invalidate()
                }
            }
        } else if UserDefaults.standard.value(forKey: "rideStatus") as? String == "Arrived" {
            UserDefaults.standard.set("StartRideon", forKey: "rideStatus")
        }  else if UserDefaults.standard.value(forKey: "rideStatus") as? String == "StartRideoff" {
            UserDefaults.standard.set("completeRide", forKey: "rideStatus")
        }
        if TripInfoTable != nil {
            self.reloadTripInfoTable()
            
        }
    }
    
    func buttonStatus(status: String, sender: MMSlidingButton) {
        let mainStoryboard = UIStoryboard(name: "Driver", bundle: nil)
        let rideInfoVc = mainStoryboard.instantiateViewController(withIdentifier: "RideInfoViewController") as! RideInfoViewController
        if sender.buttonText == "Go" || sender.buttonText == "Los" {
            if status == "Unlocked" {
                let goBtnDate = NSDate()
                UserDefaults.standard.set(goBtnDate, forKey: "Time")
                updateDriverRideWayStatus(DriverRideWayStatus.driver_on_the_way)
                
                sender.reset()
                self.btnActiveInactive()
                var destLocation : CLLocation?
                if UserLocation == nil{
                    getUserCurrentLocation()
                }
                else {
                    
                    guard let latitude = currentTripInfo.value(forKey: "PickupLatitude") as? String, let longitude = currentTripInfo.value(forKey: "PickupLongitude") as? String else {
                        showSimpleAlertController("Invalid", message:"Invalid Address.")
                        return
                    }
                    
                    UserDefaults.standard.set("Go", forKey: "rideStatus")
                    
                    destLocation = CLLocation.init(latitude: Double(latitude)!, longitude: Double(longitude)!)
                    
//                    openLocationSource(UserLocation!, destination: destLocation!)
                    let trackLocationVC = self.storyboard?.instantiateViewController(withIdentifier: "TrackLocationVC") as! TrackLocationVC
                    trackLocationVC.toLocation = destLocation!
                    trackLocationVC.currentTripData = currentTripInfo
                    trackLocationVC.trackDelegate = self
                    self.navigationController?.pushViewController(trackLocationVC, animated: true)
                    self.btnActiveInactive()
                }
            }
        }
            
        else if sender.buttonText == "Arrived" || sender.buttonText == "Angekommen" {
            if status == "Unlocked" {
                sender.reset()
                UserDefaults.standard.set("StartRideon", forKey: "rideStatus")
                updateDriverRideWayStatus(DriverRideWayStatus.arrived_at_pick_up_location)
                self.btnActiveInactive()
            }
        }
            
        else if sender.buttonText == "Start Ride" || sender.buttonText == "Fahrt Beginnen" {
            if status == "Unlocked" {
                sender.reset()
                let startRideDate = NSDate()
                UserDefaults.standard.set(startRideDate, forKey: "Time")
                updateDriverRideWayStatus(.rider_on_board)
                UserDefaults.standard.set("StartRideoff", forKey: "rideStatus")
                guard let latitude = currentTripInfo.value(forKey: "DropLatitude") as? String, let longitude = currentTripInfo.value(forKey: "DropLongitude") as? String else {
                    showSimpleAlertController("Invalid", message:"Invalid Address.")
                    return
                }
                
                let destLocation = CLLocation.init(latitude: Double(latitude)!, longitude: Double(longitude)!)
                
//                openLocationSource(UserLocation!, destination: destLocation)

                
                let trackLocationVC = self.storyboard?.instantiateViewController(withIdentifier: "TrackLocationVC") as! TrackLocationVC
                trackLocationVC.toLocation = destLocation
                trackLocationVC.currentTripData = currentTripInfo
                trackLocationVC.trackDelegate = self
                trackLocationVC.rideStarted = true
                self.navigationController?.pushViewController(trackLocationVC, animated: true)
                self.btnActiveInactive()
                
            }
        }
    }
    
    func callWebserviceForGettingCurrentTrip(){
        
        if(checkConnection()) {
            
            let parameters = [
                "UserId":getLoginDriverID()
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            printParameterLog(apiName: KDRIVER_CURRENT_RIDEINFO_URL, parameter: parameters, headerDict: getRequestHeaders())
            
            Request.CustomHTTPPostJSONWithHeader(url: KDRIVER_CURRENT_RIDEINFO_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.hideHUD()
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                
                let responseContent = getResponseDictionaryWithResult(result: result)
                print(responseContent)
                
                if let message = responseContent["Errors"] as? String {
                    self.showSimpleAlertController(nil, message: message)
                    return
                }
                
                guard let message = responseContent["Status"] as? Bool else { return }
                
                if message == true{
                    let currentTrip = responseContent["result"] as? NSDictionary
                    self.currentTripInfo = currentTrip?.mutableCopy() as! NSMutableDictionary
                    self.btnActiveInactive()
                }
            }
        } else {
            self.showAlertForInternetConnectionOff()
        }
    }
    
    @IBAction func completeRideAction(_ sender: AnyObject) {
        FinishTripAction(sender)
    }
    
    func reloadTripInfoTable() {
        print("reloadTripInfoTable")
        TripInfoTable.reloadData()
    }
    
//    func openLocationSource(_ source : CLLocation, destination: CLLocation){
//
//        if (UIApplication.shared.canOpenURL(URL(string:"https://maps.google.com/")!)) {
//            _ = "https://maps.google.com/?saddr=\(source.coordinate.latitude),\(source.coordinate.longitude)&daddr=\(destination.coordinate.latitude),\(destination.coordinate.longitude)&t=k"
//            let url = URL(string:"https://maps.google.com/?saddr=13.0351273296519,80.2450928007214&daddr=12.9792723096105,80.2149964123964"
//            )
//            UIApplication.shared.openURL(url!)
//        }
//
//        else if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)){
//            let queryStr = "comgooglemaps://?saddr=\(source.coordinate.latitude),\(source.coordinate.longitude)&daddr=\(destination.coordinate.latitude),\(destination.coordinate.longitude)"
//            let url = URL(string: queryStr)
//            UIApplication.shared.openURL(url!)
//        }
//
//        else if (UIApplication.shared.canOpenURL(URL(string:"googlechromes://")!)){
//            let queryStr = "googlechromes://?saddr=\(source.coordinate.latitude),\(source.coordinate.longitude)&daddr=\(destination.coordinate.latitude),\(destination.coordinate.longitude)"
//            let url = URL(string: queryStr)
//            UIApplication.shared.openURL(url!)
//        }
//    }
    
    func openLocationSource(_ source : CLLocation, destination: CLLocation) {
        
        if (UIApplication.shared.canOpenURL(URL(string:"https://maps.apple.com/")!)) {
            let queryStr = "https://maps.apple.com/?saddr=\(source.coordinate.latitude),\(source.coordinate.longitude)&daddr=\(destination.coordinate.latitude),\(destination.coordinate.longitude)&t=k"
            //            print("Apple map Query : \(queryStr)")
            let url = URL(string: queryStr)
            UIApplication.shared.openURL(url!)
        }
        else if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            let queryStr = "comgooglemaps://?saddr=\(source.coordinate.latitude),\(source.coordinate.longitude)&daddr=\(destination.coordinate.latitude),\(destination.coordinate.longitude)"
            //            print("Google map Query : \(queryStr)")
            let url = URL(string: queryStr)
            UIApplication.shared.openURL(url!)
        }
    }
    
    @IBAction func confirmStatusAction(_ sender: AnyObject) {
        
        let currentCell : UITableViewCell = sender.superview?!.superview as! UITableViewCell
        let currentIndexPath : IndexPath = TripConfirmTable.indexPath(for: currentCell)!
        
        switch (currentIndexPath as NSIndexPath).row{
        case 0 :
            updateDriverRideWayStatus(DriverRideWayStatus.driver_on_the_way)
            break
        case 1:
            break
        case 2 :
            var riderPhoneNumber = (currentTripInfo.object(forKey: "rider_detail") as! NSDictionary).value(forKey: "phone_number") as! String
            riderPhoneNumber = riderPhoneNumber.trimmingCharacters(in: .whitespaces)
            let telString = "tel://\(riderPhoneNumber)"
            if UIApplication.shared.canOpenURL(URL(string: telString)!){
                UIApplication.shared.openURL(URL(string: telString)!)
            }
            break
        case 3 :
            break
        case 4 :
            FinishTripAction(sender)
            break
        case 5 :
            break
        case 6 :
            cancelTripAction(sender)
            break
        case 7 :
            
            let telString = "tel://+41445209014"
            if UIApplication.shared.canOpenURL(URL(string: telString)!){
                UIApplication.shared.openURL(URL(string: telString)!)
            }
            break
        default: break
        }
    }
    
    func FinishTripAction(_ sender: AnyObject){
        let alertController: UIAlertController = UIAlertController(title: nil, message: "FinishTripMsgKey".localized , preferredStyle: .alert)
        let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
            
            self.finishTripWebService("ride_is_finished" as AnyObject, Tag: FINISHTRIPTAG)
        }
        let noAction: UIAlertAction = UIAlertAction(title: "No".localized, style: .default) { action -> Void in}
       
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func cancelTripAction(_ sender: Any) {
        
        if (self.currentTripInfo.value(forKey: "BookingType") as? Int) == 1 {
            let alertController: UIAlertController = UIAlertController(title: nil, message: "CancelInstantRideMsgKeyDriver".localized , preferredStyle: .alert)
            let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
                self.cancelTripWebService()
                UserDefaults.standard.removeObject(forKey: "Time")
                UserDefaults.standard.removeObject(forKey: "rideStatus")
            }
            let noAction: UIAlertAction = UIAlertAction(title: "No".localized, style: .default) { action -> Void in}
            
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            self.showSimpleAlertController(nil, message: "Please call to YourTaxi.".localized)
        }
    }
    
    @IBAction func cashRecivedAction(_ sender: AnyObject) {
        self.finishTripWebService(paramterName as AnyObject, Tag: FINISH)
    }
    
    @IBAction func updateRideAction(_ sender: AnyObject) {
        var result:Bool = false
        
        let waitTime =  waitingTimeTF.text! as String
        let moreKm =  moreKilometerTF.text! as String
        
        if !(waitTime).isEmpty{
            result = true
        }
        if !(moreKm).isEmpty{
            result = true
        }
        
        if result {
            waitingTimeTF.text = nil
            moreKilometerTF.text = nil
            if checkConnection(){
                
                let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
                let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
                let userDict = self.convertStringToDictionary(text: decryt!)!
                
                let driverID  = userDict.value(forKey: "id") as! NSString
                
                let parameters = [
                    "user_type":"driver",
                    "driver_id":driverID,
                    "ride_id": currentTripInfo.value(forKey: "id") as! String,
                    "add_km" : moreKm,
                    "add_waiting_time": waitTime,
                    "platform": kPlatForm,
                    "environment":kEnviroment
                    ] as [String : Any]
                
                self.showHUD()
                let Request: WebserviceHelper = WebserviceHelper()
                let headerDict = Request.getHeader(parameters: parameters)
                printParameterLog(apiName: KADDRIDETIP_URL, parameter: parameters, headerDict: headerDict)
                Request.HTTPPostJSONWithHeader(url: KADDRIDETIP_URL, paramters: parameters as NSDictionary,withIdicator: false,header: headerDict) { (result, error) in
                    
                    self.hideHUD()
                    if(error != nil){
                        self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                        return;
                    }
                    let responseObject = Request.convertStringToDictionary(text: result)
                    if let response = responseObject?["response"] as? [String:Any] {
                        if let status = response["status"] as? NSString{
                            print(status)
                            self.alreadyLogoutAction(response: response)
                        }
                    }
                }
            }
            else{
                showAlertForInternetConnectionOff()
            }
        }
        else{
            showSimpleAlertController(nil, message:"FillMsgKey".localized)
        }
    }
    
    
    //MARK:- KEYBOARD NOTIFICATION METHODS
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(RideInfoViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RideInfoViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func unregisterForKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func keyboardWillShow(_ notification: Notification) {
        
        if (((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            
            if infoControlSegment.selectedSegmentIndex == 2{
                
                if self.view.frame.size.width==320 && self.view.frame.size.height == 480 {
                    self.view.frame.origin.y = -20;
                }else
                {
                    self.view.frame.origin.y = 0;
                }
            }
            
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        self.view.frame.origin.y = 0;
    }
    
    
    //MARK:- WEB SERVICE METHODS
    func finishTripWebService(_ buttonName: AnyObject, Tag: NSInteger){
        
        if checkConnection(){
            
            UserDefaults.standard.set("0", forKey: "RideIsStart")
            UserDefaults.standard.synchronize()
            UserDefaults.standard.removeObject(forKey: "RideStartStop")
            TripConfirmTable.reloadData()
 
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            let parameters = getDriverRideWayStatusParameters(status: .ride_is_finished)
            
            printParameterLog(apiName: KRIDESTATUS_UPDATE_URL, parameter: parameters, headerDict: getRequestHeaders())
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KRIDESTATUS_UPDATE_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                
                let responseObject = getResponseDictionaryWithResult(result: result)
                print(responseObject)
                
                guard (responseObject["result"] as? [String:Any]) != nil else {
                    self.alreadyLogoutAction(response: responseObject as! [String : Any])
                    return
                }
                
                if Tag == CANCELTRIPTAG || Tag == FINISH {
                    
                    if UserDefaults.standard.value(forKey: "RideStartStop") != nil
                    {
                        UserDefaults.standard.set("0", forKey: "RideIsStart")
                        UserDefaults.standard.removeObject(forKey: "RideStartStop")
                        UserDefaults.standard.synchronize()
                    }
                    
                    let alertController: UIAlertController = UIAlertController(title: "Success".localized as String, message: responseObject["Message"] as? String , preferredStyle: .alert)
                    let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                    
                else {
                    
                    var btnTitle = "Ok".localized
                    var paramterName = ""
                    
                    if ((self.currentTripInfo.value(forKey: "RiderDetails")) != nil) {
                        if let value = self.currentTripInfo.object(forKey: "PaymentType") as? NSInteger  {
                            btnTitle = (value == 1) ? "Cash Recieved".localized : "Credit Recieved".localized
                            paramterName = (value == 1) ? "cash" : "card"
                        }
                    }
                    
                    var resultDic = responseObject["result"] as? NSDictionary
                    if resultDic?.count == 0 {
                        resultDic = [
                            "regular_price": "0.0",
                            "extra_km" : "00.00 km",
                            "extra_minute": "00.00 Minutes",
                            "paid_total": "CHF 0.00",
                            "tips" : "CHF 00.00"
                        ]
                    }
                    
                    let customAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
                    customAlertVC.resultDic = resultDic
                    customAlertVC.rideInfoVC = self
                    customAlertVC.currentTripInfo = self.currentTripInfo
                    customAlertVC.paramterName = paramterName
                    customAlertVC.btnName = btnTitle
                    customAlertVC.view.backgroundColor=UIColor.black.withAlphaComponent(0.7)
                    customAlertVC.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                    self.present(customAlertVC, animated:false, completion: nil)
                }
            }
        }
        else {
            showAlertForInternetConnectionOff()
        }
    }
    
    func cancelTripWebService(){
        if checkConnection(){
            UserDefaults.standard.synchronize()
            if (UserDefaults.standard.object(forKey: kNEW_LOCATION) != nil) {
                dicLocation = UserDefaults.standard.object(forKey: kNEW_LOCATION) as! NSDictionary
            }
            
            let parameters = [
                "Platform":1,
                "Status":3,
                "DriverId":getLoginDriverID(),
                "RideId": currentTripInfo?.value(forKey: "BookingId") as? NSInteger ?? 0,
                "Latitude" : dicLocation?.value(forKey: "lat"),
                "Longitude" : dicLocation?.value(forKey: "long")
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            printParameterLog(apiName: KACCEPTREJECT_URL, parameter: parameters, headerDict: getRequestHeaders())
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KACCEPTREJECT_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Error", message: error?.localized ?? "")
                    return;
                }
                
                let responseObject = getResponseDictionaryWithResult(result: result)
                print(responseObject)
                
                if let status = responseObject["Status"] as? Bool {
                    
                    if status {
                        
                        if UserDefaults.standard.value(forKey: "RideStartStop") != nil
                        {
                            UserDefaults.standard.set("0", forKey: "RideIsStart")
                            UserDefaults.standard.removeObject(forKey: "RideStartStop")
                            UserDefaults.standard.synchronize()
                        }
                        
                        let alertController: UIAlertController = UIAlertController(title: nil, message: responseObject["Message"] as? String , preferredStyle: .alert)
                        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                            _ = self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    }else{
                        self.showSimpleAlertController(nil, message: responseObject["Message"] as? String ?? "")
                    }
                }
            }
        }
        else{
            showAlertForInternetConnectionOff()
        }
    }
    
    func getDriverRideWayStatusParameters(status : DriverRideWayStatus) -> [String : Any] {
        
        var parameters = [String : Any]()
        
        switch status {
            
        case .driver_on_the_way:
            
            let latStr = String(format: "%f", (UserLocation?.coordinate.latitude)!)
            let longStr = String(format: "%f", (UserLocation?.coordinate.longitude)!)
            
            parameters = [
                "DriverId":getLoginDriverID(),
                "BookingId":currentTripInfo.value(forKey: "BookingId") as! NSInteger,
                "Latitude":latStr,
                "Longitude":longStr,
                "Type":"\(status)"
            ]
            break
            
        case .arrived_at_pick_up_location, .rider_on_board, .waiting_for_rider, .ride_is_finished:
            
            parameters = [
                "DriverId":getLoginDriverID(),
                "BookingId":currentTripInfo.value(forKey: "BookingId") as! NSInteger,
                "Type":"\(status)"
            ]
            break
        }
        
        return parameters
    }
    
    func updateDriverRideWayStatus(_ status : DriverRideWayStatus) {
        if UserLocation == nil {
            getUserCurrentLocation()
        }
        else {
            
            if checkConnection() {
                
                self.showHUD()
                let Request: WebserviceHelper = WebserviceHelper()
                
                let parameters = getDriverRideWayStatusParameters(status: status)
                
                printParameterLog(apiName: KRIDESTATUS_UPDATE_URL, parameter: parameters, headerDict: getRequestHeaders())
                
                let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
                
                Request.CustomHTTPPostJSONWithHeader(url: KRIDESTATUS_UPDATE_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                    
                    self.hideHUD()
                    if(error != nil) {
                        self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                        return;
                    }
                    
                    let responseObject = getResponseDictionaryWithResult(result: result)
                    print(responseObject)
                    
                    guard (responseObject["result"] as? [String:Any]) != nil else {
                        self.alreadyLogoutAction(response: responseObject as! [String : Any])
                        return
                    }
                    
                    if status == .rider_on_board {
                        self.currentRideStatus = 3
                        self.cancelTheTrip.isHidden = true
                        self.callToYourTaxiBtnTrailing.constant = ((self.view.frame.size.width - 6) / 2) - (self.callToYourTaxiBtn.frame.size.width / 2)
                        self.handleStartRideAPIResponse(responseObject: responseObject as! [String : Any])
                    }
                    
                    print("****RIDE_WAY_STATUS : \(status)***")
                }
            }
            else{
                showAlertForInternetConnectionOff()
            }
        }
    }
    
    func handleStartRideAPIResponse(responseObject : [String : Any]) {
        
        if ((currentTripInfo.value(forKey: "RideType") as? NSInteger) != 1) {//Km

            UserDefaults.standard.set("Start", forKey: "RideStartStop")
            if UserDefaults.standard.value(forKey: kNEW_LOCATION) != nil
            {
                UserDefaults.standard.removeObject(forKey: kNEW_LOCATION)
            }
            UserDefaults.standard.synchronize()
            
            if let value = currentTripInfo.value(forKey: "BookingId") as? NSInteger {//id
                let enceyptCurrentRide = AESCrypt.encrypt("\(value)", password: SECRET_KEY)
                UserDefaults.standard.set(enceyptCurrentRide, forKey: "DriverCurrentRide")
                UserDefaults.standard.set("1", forKey: "RideIsStart")
                UserDefaults.standard.set(0.0, forKey: "Distance")
                UserDefaults.standard.synchronize()
            }
        }
        self.TripConfirmTable.reloadData()
    }
    
    func alreadyLogoutAction(response:[String : Any]) {
        //  : Handle Driver Logout
        if let value = response["is_already_logout"] as? String {
            if value == "1" && UserDefaults.standard.bool(forKey: KRIDER_LOGIN) && !KAPP_RIDER{
                self.delegate?.forceFullyLogoutDriverAlert(response["Message"] as! String)
            }
            else{
                showSimpleAlertController(nil, message:response["Message"] as! String)
            }
        }
        else {
            
            if let value = response["Message"] as? String {
                self.showSimpleAlertControllerWithOkAction(nil, message: value, callback: {
                    self.backAction()
                    return
                })
            }else {
                showSimpleAlertController(nil, message:"Response error")
            }
        }
    }
    
    // MARK:- USER LOCATION METHODS
    func getUserCurrentLocation(){
        let locMgr: INTULocationManager = INTULocationManager.sharedInstance()
        locMgr.requestLocation(withDesiredAccuracy: INTULocationAccuracy.block,
                               timeout: 10.0,
                               delayUntilAuthorized: true,
                               block: {(currentLocation: CLLocation?, achievedAccuracy: INTULocationAccuracy, status: INTULocationStatus) -> Void in
                                if status == INTULocationStatus.success || status == INTULocationStatus.timedOut {
                                    if currentLocation != nil {
                                        var lat = Double((currentLocation?.coordinate.latitude)!)
                                        var long = Double((currentLocation?.coordinate.longitude)!)
                                        self.UserLocation = CLLocation.init(latitude: lat.roundToPlaces(7), longitude: long.roundToPlaces(7))
                                    }
                                }
                                else {
                                    print("Error: \(status.rawValue)")
                                }
        })
    }
    
    override func showLocationAllowAlert(){
        
        let alertController = UIAlertController(
            title: "Location",
            message: "please open this app's settings and set location access to 'While Using the App'.",
            preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- EXTENSIONS
    func didReachDestination() {
    }
}

//MARK:- EXTENSIONS
extension Double {
    mutating func roundToPlaces(_ places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Double(self * divisor) / divisor
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}


import UIKit
import GooglePlaces
import IQKeyboardManagerSwift
class DropUpLocationController: BaseViewController, UITableViewDelegate, UITextFieldDelegate,UITableViewDataSource {
    
    @IBOutlet weak var manualView : UIView!
    @IBOutlet weak var mapView : UIView!
    @IBOutlet weak var manualTextField : UITextField!
    @IBOutlet weak var fromMapButton : UIButton!
    @IBOutlet var dotView: UIView!
    @IBOutlet var tableTopConstraintWithManualView: NSLayoutConstraint!
    @IBOutlet var tableTopConstraintWithMapView: NSLayoutConstraint!
    @IBOutlet var favoriteTableView: UITableView!
    @IBOutlet var chooseByMapPinView : UIImageView!
    var homeAddressArray = [[String:Any]]()
    var workAddressArray = [[String:Any]]()
    var favoriteAddressArray = [[String:Any]]()
    var searchResultTableView:UITableView!
    var mainView : UIView!
    var placesClient = GMSPlacesClient()
    var filter = GMSAutocompleteFilter()
    var resultArray: NSMutableArray = []
    var selectedTextField : UITextField?
    var selectedRow : NSInteger?
    var locationType = ""
    var locationToEdit = ""
    var favourites: String?
    var customResultArray: NSMutableArray = []
    var favListDict : NSDictionary! = NSDictionary()
    var locationId : Int32?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(DropUpLocationController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DropUpLocationController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        manualTextField.text = locationToEdit == "Set Destination.".localized ? "" : locationToEdit
        if locationType == "drop_up"{
            fromMapButton.setTitle("Set destination by pin".localized, for: .normal)
            manualTextField.placeholder = "Set Destination.".localized
            mapView.isHidden = false
            dotView.backgroundColor = UIColor(red: 230/255.0, green: 69/255.0, blue: 11/255.0, alpha: 1.0)
            tableTopConstraintWithManualView.priority = 250
            tableTopConstraintWithMapView.priority = 750
            chooseByMapPinView.image = UIImage(named: "dropupPin")
        }
        else {
            fromMapButton.setTitle("Set pickup by pin".localized, for: .normal)
            manualTextField.placeholder = "Set pickup".localized
            mapView.isHidden = false
            dotView.backgroundColor = kLIGHT_OLIVE_COLOR
            tableTopConstraintWithManualView.priority = 250
            tableTopConstraintWithMapView.priority = 750
            chooseByMapPinView.image = UIImage(named: "pickupPin")
        }
        
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(DropUpLocationController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        self.slideMenuController()?.removeLeftGestures()
        manualView.layer.borderColor = UIColor.lightGray.cgColor
        manualView.layer.borderWidth = 1.0
        mapView.layer.borderColor = UIColor.lightGray.cgColor
        mapView.layer.borderWidth = 1.0
        initSearchTableView()
        manualTextField.delegate = self
        favoriteTableView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getFavoriteAddressList()
        if locationType == "drop_up"{
            self.title = "Set Destination".localized
        }
        else{
            self.title = "Set pickup".localized
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func chooseLocationFromMapClicked(_ sender: UIButton) {
        if locationType == "drop_up"{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dropUpLocationSelected"), object: "DropUp")
        }else if locationType == "pick_up"{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dropUpLocationSelected"), object: "PickUp")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Actions Methods
    func backAction(){
        if locationType == "drop_up"{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dropUpLocationSelected"), object: "DropUp")
        }else if locationType == "pick_up"{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dropUpLocationSelected"), object: "PickUp")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func initSearchTableView() -> Void {
        mainView = UIView.init(frame: CGRect(x: self.manualView.frame.origin.x, y: 100, width: self.manualView.frame.size.width, height: 200))
        mainView.autoresizingMask = [.flexibleTopMargin,.flexibleWidth]
        mainView.backgroundColor = UIColor.lightGray
        self.view.addSubview(mainView)
        searchResultTableView=UITableView.init(frame: CGRect(x: 1, y: 0, width: mainView.frame.size.width - 2,height: mainView.frame.size.height - 1))
        searchResultTableView.delegate = self
        searchResultTableView.dataSource=self
        searchResultTableView.tag=200
        searchResultTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        mainView.addSubview(searchResultTableView)
        mainView.isHidden=true
    }
    
    func placeAutocomplete(_ textField: NSString) {
        
        let urlString = locationType == "drop_up" ? "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(GOOGLE_WEB_API_KEY)&input=\(textField)&components=country:ch|country:li|country:de|country:at|country:in" : "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(GOOGLE_WEB_API_KEY)&input=\(textField)&components=country:ch|country:in"
        let Request: WebserviceHelper = WebserviceHelper()
        Request.HTTPGetJSON(url: urlString,withIdicator: false) { (result, error) in
            if ((error) != nil){
                
            }else{
                let responseContent = Request.convertStringToDictionary(text: result )
                if responseContent != nil {
                    if let value = responseContent?["predictions"] as? [[String:Any]] {
                        self.resultArray = NSMutableArray()
                        for data in value {
                            
                            self.resultArray.add((data as [String:Any])["description"] as! String)
                        }
                        
                        if self.resultArray.count > 0{
                            
                            self.mainView.isHidden=false
                            self.view.bringSubview(toFront: self.mainView)
                            
                            self.searchResultTableView.reloadData()
                        }else{
                            self.mainView.isHidden=true
                            
                        }
                    }
                    else {
                        self.mainView.isHidden=true
                    }
                }
                else {
                    self.mainView.isHidden=true
                }
            }
        }
    }
    
    @IBAction func editAddressButtonAction(_ sender: Any) {
        let button = sender as! UIButton
        let currentCell = button.superview?.superview?.superview as! UITableViewCell
        let currentIndexPath = favoriteTableView.indexPath(for: currentCell)
        if currentIndexPath?.row == 0{
            let addSting = homeAddressArray[0]["LocationName"] as! String
            let favid = homeAddressArray[0]["FavouriteId"] as! Int32
            addAddress("home", withAddress: addSting, favId: favid)
        }
        else if currentIndexPath?.row == 1{
            let addSting = workAddressArray[0]["LocationName"] as! String
            let favid = workAddressArray[0]["FavouriteId"] as! Int32
            addAddress("work", withAddress: addSting, favId: favid)
        }
    }
    
    @IBAction func addFavoriteAddressAction(_ sender: Any) {
        addAddress("favourite", withAddress: "", favId: 0)
    }
    // MARK: - TableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 11{
            return favoriteAddressArray.count+3
        }
        else{
            return resultArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 11{
            if indexPath.row == 0 || indexPath.row == 1{
                return 90
            }
            else if indexPath.row == 2{
                return 70
            }
            else{
                return 80
            }
            
        }
        else{
            return 34
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 11{
            
            
            if indexPath.row == 0 || indexPath.row == 1{
                let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "homeAddressCell") as UITableViewCell!
                let shadowView: UIView = cell.viewWithTag(1)!
                let imageView: UIImageView = shadowView.viewWithTag(2) as! UIImageView
                let titleLable: UILabel = shadowView.viewWithTag(3) as! UILabel
                let descLable : UILabel = shadowView.viewWithTag(4) as! UILabel
                let editButton : UIButton = shadowView.viewWithTag(5) as! UIButton
                editButton.isHidden = true
                drawShadow(shadowView)
                
                if indexPath.row == 0{
                    imageView.image = #imageLiteral(resourceName: "fav_home_add_icon")
                    titleLable.text = "Home".localized
                    descLable.text = "Add your home address.".localized
                    if homeAddressArray.count > 0{
                        if let desc = homeAddressArray[0]["LocationName"] as? String
                        {
                            descLable.text = desc
                            editButton.isHidden = false
                        }
                    }
                    
                }
                else{
                    imageView.image = #imageLiteral(resourceName: "fav_work_add_icon")
                    titleLable.text = "Work".localized
                    descLable.text = "Add your work address.".localized
                    if workAddressArray.count > 0{
                        if let desc = workAddressArray[0]["LocationName"] as? String
                        {
                            descLable.text = desc
                            editButton.isHidden = false
                        }
                    }
                }
                return cell
            }
            else if indexPath.row == 2 {
                let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "favouriteAddressCell") as UITableViewCell!
                let shadowView: UIView = cell.viewWithTag(1)!
                drawShadowForAddress(shadowView)
                let imageView: UIImageView = shadowView.viewWithTag(2) as! UIImageView
                let titleLable: UILabel = shadowView.viewWithTag(3) as! UILabel
                imageView.image = #imageLiteral(resourceName: "fav_add_icon")
                titleLable.text = "Favorites".localized
                return cell
            }
            else{
                let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "addressCell") as UITableViewCell!
                let shadowView: UIView = cell.viewWithTag(1)!
                let descLable : UILabel = shadowView.viewWithTag(2) as! UILabel
                descLable.lineBreakMode = NSLineBreakMode.byWordWrapping
                drawShadowForAddress(shadowView)
                
                let index : Int = indexPath.row - 3
                if favoriteAddressArray.count > index{
                    if let desc = favoriteAddressArray[index]["LocationName"] as? String{
                        let type = favoriteAddressArray[index]["FavouriteName"] as? String
                        descLable.text = type!+"\n"+"\n "+desc
                    }
                }
                return cell
            }
            
            
        }
        else{
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell!
            if resultArray.count>0 {
                cell.textLabel?.font=UIFont.systemFont(ofSize: 12)
                cell.textLabel?.text = resultArray[(indexPath as NSIndexPath).row] as? String
                cell.textLabel?.textColor = kDARK_OLIVE_COLOR
            }
            return cell
        }
    }
    
    func deleteLocationApi(locationId:Int32){
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [String : Any]()
            parameters = [
                "UserId" : riderID,
                "FavouriteId" : locationId,
            ]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: DELETE_LOCATION, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: DELETE_LOCATION, jsonString: jsonParam as NSString, withIdicator: false, header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    print("DeleteLocation: \(String(describing: responseContent))")
                    
                    if let value = responseContent?.value(forKey: "Status") as? Bool{
                        if value == true{
                            self.showSimpleAlertControllerWithOkAction(nil, message: responseContent!.value(forKey: "Message") as? String ?? "", callback: {
                                self.getFavoriteAddressList()
                            })
                        }else{
                            self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                        }
                        
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        print("Deleted")
        
        if (indexPath.row == 0){
            locationId = homeAddressArray[0]["FavouriteId"] as! Int32
            self.showAlertController(locationId!)
        }else if(indexPath.row == 1){
            locationId = workAddressArray[0]["FavouriteId"] as! Int32
            self.showAlertController(locationId!)
        }else{
            let index = (indexPath.row-3)
            locationId = favoriteAddressArray[index]["FavouriteId"] as! Int32
        }
        self.showAlertController(locationId!)
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if indexPath.row == 2{
            return false
        }
        if(indexPath.row == 1 && workAddressArray.count == 0){
            return false
        }else if(indexPath.row == 0 && homeAddressArray.count == 0){
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if tableView.tag == 11{
            if indexPath.row == 0{
                if homeAddressArray.count > 0 {
                    let addSting = homeAddressArray[0]["LocationName"] as! String
                    getLocationForSelectedAddress(addSting)
                }
                else{
                    addAddress("home", withAddress: "", favId: 0)
                }
            }
            else if indexPath.row == 1{
                if workAddressArray.count > 0{
                    let addSting = workAddressArray[0]["LocationName"] as! String
                    getLocationForSelectedAddress(addSting)
                }
                else{
                    addAddress("work", withAddress: "", favId: 0)
                }
            }
            else if indexPath.row == 2{
                print("Select favorite address")
            }
            else{
                let index : Int = indexPath.row - 3
                if favoriteAddressArray.count > index{
                    if let desc = favoriteAddressArray[index]["LocationName"] as? String{
                        getLocationForSelectedAddress(desc)
                    }
                }
            }
        }
        else{
            mainView.isHidden=true
            if resultArray.count>0 {
                
                let address = resultArray[(indexPath as NSIndexPath).row] as? String
                self.selectedTextField?.text = address
                self.selectedTextField?.resignFirstResponder()
                getLocationForSelectedAddress(address!)
            }
        }
    }
    
    // MARK: - TextField Methods
    func textFieldShouldBeginEditing(_ sender: UITextField) -> Bool {
        
        var height = CGFloat()
        
        if self.view.frame.size.width==320 && self.view.frame.size.height == 416 {
            height = 59
        }else  if self.view.frame.size.width==320 && self.view.frame.size.height == 504 {
            height=140
        }else {
            height=170
        }
        
        mainView.frame = CGRect(x: manualView.frame.origin.x ,y: manualView.frame.origin.y + manualView.frame.size.height + 1, width: self.manualView.frame.size.width,height: height)
        
        
        self.searchResultTableView.frame=CGRect(x: 1, y: 1, width: mainView.frame.size.width - 2, height: mainView.frame.size.height - 2)
        self.selectedTextField = sender
        filter = GMSAutocompleteFilter()
        filter.country = KSET_COUNTRY
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if newString.isEmpty {
            mainView.isHidden=true
            
        }else{
            placeAutocomplete(newString as NSString)
        }
        searchResultTableView.reloadData()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        mainView.isHidden=true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    //MARK:- ALERT CONTROLLER
    func showAlertController(_ locationId : Int32) {
        let alertController: UIAlertController = UIAlertController(title: nil, message: "Are you sure you want to delete?".localized, preferredStyle: .alert)
        let yesAction: UIAlertAction = UIAlertAction(title: "Confirm".localized, style: .default) { action -> Void in
            self.deleteLocationApi(locationId: locationId)
        }
        let noAction : UIAlertAction = UIAlertAction(title: "Cancel".localized, style: .default, handler: nil)
        
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func drawShadowForAddress(_ shadowView : UIView)
    {
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowOffset = CGSize.zero
        shadowView.layer.shadowRadius = 1
    }
    // MARK: - Address selection Methods
    func addAddress(_ type:String, withAddress selectedAddress:String, favId: Int32){
        let storyboard = UIStoryboard(name: "Rider", bundle: nil)
        let addLocationVC = storyboard.instantiateViewController(withIdentifier: "AddLocationViewController") as? AddLocationViewController
        addLocationVC?.locationType = locationType
        addLocationVC?.addressType = type
        addLocationVC?.favId = favId
        addLocationVC?.favouriteCount = self.favoriteAddressArray.count
        addLocationVC?.selectedAddress = selectedAddress
        self.navigationController?.pushViewController(addLocationVC!, animated: true)
    }
    
    
    func getLocationForSelectedAddress(_ address:String) {
        var coordinates:CLLocationCoordinate2D!
        let correctedAddress:String! = address.addingPercentEncoding(withAllowedCharacters: .symbols)
        let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(correctedAddress!)&sensor=false&mode=driving&key=\(GOOGLE_WEB_API_KEY)")
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) -> Void in
            do
            {
                if data != nil{
                    let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as!  NSDictionary
                    let result = dic["results"] as! NSArray
                    let dic1 = (((result[0] as! NSDictionary).value(forKey: "geometry") as! NSDictionary).value(forKey: "location") as! NSDictionary)
                    let lat = dic1["lat"] as! Double
                    let lon = dic1["lng"] as! Double
                    coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                    let dic2 = NSMutableDictionary()
                    dic2.setValue(coordinates, forKey: "coordinates")
                    dic2.setValue(address, forKey: "address")
                    dic2.setValue(self.locationType, forKey: "address_type")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dropUpLocationSelected"), object: dic2)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    print("Invalid address or error in response")
                }
                
            }
            catch
            {
                
            }
        }
        task.resume()
    }
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    // MARK: - Webservice Methods
    func getFavoriteAddressList()
    {
        homeAddressArray.removeAll()
        workAddressArray.removeAll()
        favoriteAddressArray.removeAll()
        if(checkConnection()) {
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "UserId" : riderID,
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KGET_FAVOURITE_LOCATION, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KGET_FAVOURITE_LOCATION, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                if ((error) != nil){
                    print(error.debugDescription)
                    self.hideHUD()
                }else{
                    if result == "[]" {
                        self.getFavoriteAddressListDidFinish()
                        return
                    }
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    guard let data = responseObject.value(forKey: "data") as? String else {
                        self.showSimpleAlertController(nil, message: "Something Went Wrong")
                        return
                    }
                    let decryptedData = AESCrypt.decrypt(data, password: SECRET_KEY)
                    
                    if let list = self.convertToDictionary(text: decryptedData!) as? [[String:Any]]{
                        self.resultArray = NSMutableArray()
                        for data in list {
                            self.resultArray.add(data as [String:Any])
                        }
                        if self.resultArray.count > 0{
                            for dict in self.resultArray
                            {
                                if (dict as AnyObject).value(forKey: "FavouriteName") as? String == "home" {
                                    self.homeAddressArray.append(dict as! [String : Any])
                                }
                                else if (dict as AnyObject).value(forKey: "FavouriteName") as? String == "work" {
                                    self.workAddressArray.append(dict as! [String : Any])
                                }
                                    
                                else {
                                    self.favoriteAddressArray.append(dict as! [String : Any])
                                }
                                
                            }
                        }
                    }
                    self.hideHUD()
                }
                self.getFavoriteAddressListDidFinish()
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    func getFavoriteAddressListDidFinish(){
        self.favoriteTableView.isHidden = false
        self.favoriteTableView.reloadData()
    }
    
    //MARK: - Keyboard delegate and Data source method
    func keyboardWillShow(_ notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
            self.favoriteTableView.contentInset = contentInsets
        }
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        self.favoriteTableView.contentInset = .zero
    }
}


// MARK: - GMSAutocompleteViewController
extension DropUpLocationController : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if manualTextField.isFirstResponder
        {
            manualTextField.text = place.formattedAddress
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Swift.Error) {
        print("Error: \(error.localizedDescription)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


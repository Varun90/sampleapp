import UIKit

class gpsActiveAlertViewController: UIViewController {
    
    @IBOutlet var okBtn: UIButton!
    @IBOutlet var labelNumber3: UILabel!
    @IBOutlet var labelNumber1: UILabel!
    @IBOutlet var titleLbl: UILabel!
    var alertTitleMessage = ""
    var alertDescMessage = ""
    var alertDesc2Message = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        alertTitleMessage = "GPSActive_Title".localized
        alertTitleMessage = alertTitleMessage.replacingOccurrences(of: "YOURTAXI", with: "<i>YOUR</i><b>TAXI</b>")
        titleLbl.attributedText = self.stringFromHtml(string: alertTitleMessage, fontSize: "14", colorType: "#000000")
        
        alertDescMessage = "GPSActive_Desc1".localized
        alertDescMessage = alertDescMessage.replacingOccurrences(of: "bullet", with: "&bull;")
        alertDescMessage = alertDescMessage.replacingOccurrences(of: "YOURTAXI", with: "<i>YOUR</i><b>TAXI</b>")
        labelNumber1.attributedText = self.stringFromHtml(string: alertDescMessage, fontSize: "12", colorType: "#000000")
        
        alertDesc2Message = "GPSActive_Desc2".localized
        alertDesc2Message = alertDesc2Message.replacingOccurrences(of: "bullet", with: "&bull;")
        labelNumber3.attributedText = self.stringFromHtml(string: alertDesc2Message, fontSize: "12", colorType: "#000000")

        okBtn.setTitle("OK".localized, for: .normal)
    }
    func stringFromHtml(string: String, fontSize: String, colorType: String) -> NSAttributedString? {
        do {
            
            let modifiedFont = NSString(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: %@; color:%@; \">%@</span>" as NSString, fontSize, colorType, string) as String
            print("modifiedFont:", modifiedFont)
            let data = modifiedFont.data(using: String.Encoding.utf16, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                return str
            }
        } catch {
        }
        return nil
    }
    @IBAction func okBtnAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}


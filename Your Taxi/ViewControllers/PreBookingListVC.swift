import UIKit

class PreBookingListVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    var preBookingObject = [PreBookingListObject]()
    @IBOutlet var tblPreBookingLIst: UITableView!
    var resultArray: NSMutableArray = []
    var customPreBookingListDict : NSDictionary! = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblPreBookingLIst.delegate = self
        tblPreBookingLIst.dataSource = self
        tblPreBookingLIst.rowHeight = UITableViewAutomaticDimension
        tblPreBookingLIst.tableFooterView = UIView()
        self.title = "PRE BOOKING".localized
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.preBookingListApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- API CALLING
    func preBookingListApi(){
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [String : Any]()
            parameters = [
                "BookingType" : 2,
                "RiderId" : riderID,
                "Status" : 1
            ]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: PREBOOKING_LIST, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: PREBOOKING_LIST, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    print(error?.localized ?? "")
                    self.showSimpleAlertController(nil, message: error?.localized ?? "")
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                guard let data = responseObject.value(forKey: "data") as? String else {
                    self.showSimpleAlertController(nil, message: "Something Went Wrong")
                    self.hideHUD()
                    return
                }
                var decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                print("decryptedData:", decryptedData)
                if let list = self.convertToDictionary(text: decryptedData!) as? NSArray {
                    if list.count > 0 {
                        self.resultArray = NSMutableArray()
                        for data in list {
                            self.resultArray.add(data as! [String:Any])
                        }
                        if self.resultArray.count > 0 {
                            self.preBookingObject.removeAll()
                            for dict in self.resultArray {
                                self.preBookingObject.append(PreBookingListObject.init(dict: dict as! NSDictionary))
                            }
                        }
                    }
                    else {
                        self.preBookingObject.removeAll()
                        self.noDataLabelView(message: self.resultArray.value(forKey: "Message") as? String ?? "No Prebooking".localized)
                    }
                    self.tblPreBookingLIst.reloadData()
                }else {
                    self.showSimpleAlertController(nil, message: "Invalid Response")
                }
            }
        }else{
            self.hideHUD()
            self.showAlertForInternetConnectionOff()
        }
    }
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
        
    }
    
    //MARK:- TABLEVIEW DATASOURCE METHODS
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "PreBookingListCell") as? PreBookingListCell
        
        if(cell == nil){
            cell = UITableViewCell(style: .default, reuseIdentifier: "PreBookingListCell") as? PreBookingListCell
        }
        cell?.loadCell(arrObject: preBookingObject[indexPath.row], Isprebook: true)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return preBookingObject.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj : PreBookingListObject = preBookingObject[indexPath.row]
        let currentRideVC = self.storyboard?.instantiateViewController(withIdentifier: "CurrentRidesViewController") as? CurrentRidesViewController
        currentRideVC!.currentRideData = obj.getObjectData()
        currentRideVC!.preBookingObject = obj;
        self.navigationController?.pushViewController(currentRideVC!, animated: true)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let selectedObject = preBookingObject[indexPath.row] as PreBookingListObject
        return (selectedObject.rideStatus >= 4) ? true : false
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        print("Deleted")
        let selectedObject = preBookingObject[indexPath.row] as PreBookingListObject
        let bookingID = selectedObject.bookingId
        self.showAlertController(bookingID!, indexPath: indexPath)
    }
    //MARK:- ALERT CONTROLLER
    func showAlertController(_ bookingId : Int64, indexPath: IndexPath) {
        let alertController: UIAlertController = UIAlertController(title: nil, message: "Are you sure you want to CancelRide?".localized, preferredStyle: .alert)
        let yesAction: UIAlertAction = UIAlertAction(title: "Confirm".localized, style: .default) { action -> Void in
            self.deletePreBookingApi(bookingId: bookingId, indexPath: indexPath)
        }
        let noAction : UIAlertAction = UIAlertAction(title: "Cancel".localized, style: .default, handler: nil)
        
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func deletePreBookingApi(bookingId:Int64, indexPath: IndexPath){
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [String : Any]()
            parameters = [
                "UserId" : riderID,
                "BookingId" : bookingId,
            ]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: DELETE_LOCATION, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: DELETE_BOOKING_HISTORY, jsonString: jsonParam as NSString, withIdicator: false, header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    if let value = responseContent?.value(forKey: "Status") as? Bool{
                        if value == true{
                            self.showSimpleAlertControllerWithOkAction(nil, message: responseContent!.value(forKey: "Message") as? String ?? "", callback: {
                                self.preBookingObject.remove(at: indexPath.row)
                                self.tblPreBookingLIst.deleteRows(at: [indexPath], with: .automatic)
                            })
                        }else{
                            self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                        }
                        
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    //MARK:- CUSTOM METHODS
    func noDataLabelView(message:String){
        
        let noDataLabel = UILabel()
        noDataLabel.translatesAutoresizingMaskIntoConstraints = false
        noDataLabel.text = message
        noDataLabel.sizeToFit()
        self.view.addSubview(noDataLabel)
        
        self.view.addConstraint(NSLayoutConstraint(item: noDataLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        self.view.addConstraint(NSLayoutConstraint(item: noDataLabel, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        
    }
}


//MARK:- PRE BOOKING CELL
class PreBookingListCell : UITableViewCell{
    
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblFrom: UILabel!
    @IBOutlet var lblPickupLocation: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblCancellationCharge: UILabel!
    @IBOutlet var lblRideStatus: UILabel!
    @IBOutlet var lblTo: UILabel!
    @IBOutlet var lblDropupLocation: UILabel!
    
    // MARK: - Overrided Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- LOAD CELL
    func loadCell(arrObject:PreBookingListObject, Isprebook:Bool){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let dateObj = dateFormatter.date(from: arrObject.pickupDate!)
        if let selectedFormate = UserDefaults.standard.value(forKey: "TimeFormate") as? String {
            if "\(selectedFormate)" == "12"
            {
                dateFormatter.dateFormat = "dd-MMM-YYYY / hh:mm:ss a"
            }
            else
            {
                dateFormatter.dateFormat = "dd-MMM-YYYY / HH:mm:ss"
            }
        }
        else {
            dateFormatter.dateFormat = "dd-MMM-YYYY / HH:mm:ss"
        }
        var pickUpDate = ""
        if dateObj != nil {
            pickUpDate = dateFormatter.string(from: dateObj!)
        } else {
            pickUpDate = arrObject.pickupDate!
        }
        
        lblDate.text = pickUpDate
        lblFrom.text = "From:".localized
        lblPickupLocation.text = arrObject.pickupLocation
        
        lblTo.text = "To:".localized
        lblDropupLocation.text = arrObject.dropLocation
        if !Isprebook {
            lblPrice.text = "\(arrObject.finalPrice!) CHF"
            if arrObject.cancelAmount != nil {
                lblCancellationCharge.text = "Cancellation Charge: \(arrObject.cancelAmount!)"
                lblCancellationCharge.isHidden = arrObject.cancelAmount! > 0 ? false : true
            }
        }
        lblRideStatus.text = "\(arrObject.rideStatusName!)"
    }
}

import UIKit
import AccountKit
import SlideMenuControllerSwift
import SafariServices
import CTKFlagPhoneNumber
import FirebaseAuth

class RegistrationViewController: BaseViewController {
    
    var accountKit: AKFAccountKit!
    @IBOutlet weak var nameTexField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet var numberTextField: CTKFlagPhoneNumberTextField!
    @IBOutlet var infoBtnOutlet: UIButton!
    @IBOutlet var repeatePasswordTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var userNameTextField: UITextField!
    @IBOutlet var scrlView: UIScrollView!
    @IBOutlet var phoneNumberTextField: UITextField!
    @IBOutlet var verifyNumberButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var agreeBtn: UIButton!
    @IBOutlet var btnTermsCondition: UIButton!
    var termsConditionText = ""
    var mobileNumber : String!
    var countryName : String!
    var countryPrefix : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Register".localized
        verifyNumberButton.setTitle("Register now".localized, for: .normal)
        nameTexField.placeholder = "FirstName *".localized
        emailTextField.placeholder = "Email *".localized
        phoneNumberTextField.placeholder = "Phone Number *".localized
        userNameTextField.placeholder = "LastName *".localized
        passwordTextField.placeholder = "Password *".localized
        repeatePasswordTextField.placeholder = "Repeate Password *".localized
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(RegistrationViewController.backAction(_:)), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        numberTextField.parentViewController = self
        numberTextField.center = view.center
        
        termsConditionText = "terms_condition_text".localized
        termsConditionText = termsConditionText.replacingOccurrences(of: "YOURTAXI", with: "<i>YOUR</i><b>TAXI</b>")
        btnTermsCondition.setAttributedTitle(stringFromHtml(string: termsConditionText, fontSize: "16", colorType: "#009578"), for: .normal)
    }
    override func viewDidLayoutSubviews() {
    }
    
    //MARK:- BUTTON ACTIONS
    @IBAction func termsofServiceAction(_ sender: AnyObject) {
        if agreeBtn.isSelected{
            agreeBtn.isSelected = false
        }else{
            agreeBtn.isSelected = true
            openSafariController(KRider_TERMSANDCONDITIONS_URL as NSString)
        }
    }
    
    @IBAction func showPasswordRules(_ sender: Any) {
        let passwordRulesAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "passwordRulesAlertVC") as? passwordRulesAlertViewController
        passwordRulesAlertVC?.modalPresentationStyle = .overCurrentContext
        self.present(passwordRulesAlertVC!, animated: false, completion: nil)
    }
    
    @IBAction func agreeTermsAction(_ sender: AnyObject) {
        if agreeBtn.isSelected{
            agreeBtn.isSelected = false
        }else{
            agreeBtn.isSelected = true
            openSafariController(KRider_TERMSANDCONDITIONS_URL as NSString)
        }
    }
    
    func backAction(_ sender : UIBarButtonItem)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func verifyNumberButtonAction(_ sender: UIButton) {
        if nameTexField.text == "" || nameTexField.text == nil {
            self.showAlertController(nil, message: "Please enter Name".localized, nameTexField)
            return
        }
        if userNameTextField.text == "" || userNameTextField.text == nil {
            self.showAlertController(nil, message: "Please enter User Name".localized, userNameTextField)
            return
        }
        if emailTextField.text == "" || emailTextField.text == nil {
            self.showAlertController(nil, message: "Please enter Email".localized, emailTextField)
            return
        }
        if passwordTextField.text == "" || passwordTextField.text == nil {
            self.showAlertController(nil, message: "Please enter Password".localized, passwordTextField)
            return
        }
        if repeatePasswordTextField.text == "" || repeatePasswordTextField.text == nil {
            self.showAlertController(nil, message: "Please enter Repeate Password".localized, repeatePasswordTextField)
            return
        }
        if phoneNumberTextField.text == "" || phoneNumberTextField.text == nil {
            self.showAlertController(nil, message: "Please enter mobile number".localized, phoneNumberTextField)
            return
        }
        
        if(self.validaData()){
            self.showHUD()
            phoneNumberTextField.text = phoneNumberTextField.text!.replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
            var parameters = [
                "FirstName" : self.nameTexField.text!,
                "LastName" : userNameTextField.text!,
                "Email" : self.emailTextField.text!,
                "PhoneNo" : phoneNumberTextField.text!,
                "DialCode" : self.numberTextField.getCountryPhoneCode(),
                "country" : self.numberTextField.getCountryPhoneCode(),
                "Password" : passwordTextField.text,
                "deviceID" : DEVICE_UUID,
                "push_token" : getPushTokenString(),
                "UserType" : "2",
                "platform" : kPlatForm,
                "Language" : systemLanguage,
                "Status" : 1,
                "CreatedBy" : "0"
                ] as [String : Any]
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = [
                "Content-Type" : "application/json",
                "Language" : systemLanguage
            ]
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            if(checkConnection()) {
                self.validateUser(header: headerDict, jsonString: jsonParam)
            }else{
                self.showAlertForInternetConnectionOff()
            }
        }
    }
    
    func validateUser(header:[String:String], jsonString : String) {
        
        
        let Request: WebserviceHelper = WebserviceHelper()
        printJsonStringLog(apiName: KValidateUser_URL, parameter: jsonString as NSString, headerDict: header)
        Request.CustomHTTPPostJSONWithHeader(url: KValidateUser_URL, jsonString: jsonString as NSString, withIdicator: false, header: header) { (result, error) in
            if error != nil{
                self.hideHUD()
                self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                return;
            }
            let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
            let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
            let response = self.convertStringToDictionary(text: decryptedData! as String)
            print("RiderRegistrationRules :", response ?? "")
            let status = response?.value(forKey: "Status") as! Bool
            if status == true {
                self.hideHUD()
                let UserName = "\(self.nameTexField.text!) \(self.userNameTextField.text!.uppercased().prefix(1))."
              
                UserDefaults.standard.set(UserName, forKey: KRIDER_USERNAME)
                UserDefaults.standard.synchronize()
                let onlyPhoneNumber = self.phoneNumberTextField.text!
                print("onlyPhoneNumber", onlyPhoneNumber)
                let phoneNumber = ((self.numberTextField.getCountryPhoneCode())! + self.phoneNumberTextField.text!)
                 print("phoneNumber", phoneNumber)
                PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                    if let error = error {
                        if error.localizedDescription == "TOO_LONG" || error.localizedDescription == "TOO_SHORT" || error.localizedDescription == "INVALID_LENGTH" || error.localizedDescription == "Invalid format." {
                            self.showSimpleAlertController(nil, message: "Invalid phone number".localized)
                            return
                        } else {
                            self.showSimpleAlertController(nil, message: error.localizedDescription)
                            return
                        }
                    }
                    encrypt(data: verificationID as AnyObject, key: "authVerificationID")
                    self.setupOtpView(view: self.view)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if let sentView = self.view.viewWithTag(SENT_VIEW_TAG){
                            sentView.removeFromSuperview()
                            
                            let addOtpVC = self.storyboard?.instantiateViewController(withIdentifier: "AddOtpVC") as? AddOtpVC
                            addOtpVC?.isFromLogin = false
                            let dict = NSMutableDictionary()
                            dict.setValue(self.nameTexField.text!, forKey: "FirstName")
                            dict.setValue(self.userNameTextField.text!, forKey: "LastName")
                            dict.setValue(self.emailTextField.text!, forKey: "Email")
                            dict.setValue(self.phoneNumberTextField.text!, forKey: "PhoneNo")
                            dict.setValue(self.numberTextField.getCountryPhoneCode(), forKey: "country")
                            dict.setValue(self.passwordTextField.text!, forKey: "Password")
                            dict.setValue(DEVICE_UUID, forKey: "deviceID")
                            dict.setValue(getPushTokenString(), forKey: "push_token")
                            dict.setValue("2", forKey: "UserType")
                            dict.setValue(kPlatForm, forKey: "platform")
                            dict.setValue(systemLanguage, forKey: "Language")
                            addOtpVC?.registerDict = NSMutableDictionary(dictionary: dict)
                            addOtpVC?.mobileNumber = self.phoneNumberTextField.text!
                            addOtpVC?.phone_number = (self.numberTextField.getCountryPhoneCode()! + self.phoneNumberTextField.text!)
                            self.navigationController?.pushViewController(addOtpVC!, animated: true)
                        }
                    }
                }
            } else if status == false {
                self.hideHUD()
                
                if response?.index(ofAccessibilityElement: "Errors") != nil {
                    if let ErrorMsg = response?.value(forKey: "Errors") as? NSDictionary{
                        if response?.index(ofAccessibilityElement: "PhoneNo") != nil {
                            if let ph = ErrorMsg.value(forKey: "PhoneNo") as? NSArray{
                                let errMessage = (response?.value(forKey: "Errors") as! NSDictionary).value(forKey: "PhoneNo") as? NSArray
                                self.showSimpleAlertController(nil, message : (errMessage![0] as? String)!)
                            }
                        }
                        if response?.index(ofAccessibilityElement: "Email") != nil {
                            if let email = ErrorMsg.value(forKey: "Email") as? NSArray{
                                let errMessage = (response?.value(forKey: "Errors") as! NSDictionary).value(forKey: "Email") as? NSArray
                                self.showSimpleAlertController(nil, message : (errMessage![0] as? String)!)
                            }
                        }
                    }
                }
                else{
                    self.hideHUD()
                    let message = response?.value(forKey: "Message") as? String
                    self.showAlertController(nil, message: message! , self.userNameTextField)
                    
                }
            }
            
        }
    }
    
    @IBAction func NextAction(_ sender: AnyObject) {
        if self.validaData() {
            self.showHUD()
            var pushTokenString=NSString()
            
            #if (arch(i386) || arch(x86_64)) && os(iOS)
                pushTokenString="1234"
            #else
                pushTokenString = getDecryptedData(key: kPUSH_TOKEN) as NSString
                
            #endif
            var parameters = [
                "FirstName" : self.nameTexField.text!,
                "LastName" : userNameTextField.text!,
                "Email" : self.emailTextField.text!,
                "PhoneNo" : phoneNumberTextField.text!,
                "DialCode" : self.numberTextField.getCountryPhoneCode(),
                "country" : self.numberTextField.getCountryPhoneCode(),
                "Password" : passwordTextField.text,
                "deviceID" : DEVICE_UUID,
                "push_token" : getPushTokenString(),
                "UserType" : "2",
                "platform" : kPlatForm,
                "Language" : systemLanguage,
                "CreatedBy" : 0
                ] as [String : Any]
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = [
                "Content-Type" : "application/json",
                "Language" : systemLanguage
            ]
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            let Request: WebserviceHelper = WebserviceHelper()
            printJsonStringLog(apiName: KRegistration_URL, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRegistration_URL, jsonString: jsonParam as NSString , withIdicator: false, header: headerDict, callback: { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    print("Rider Registration", responseContent ?? "")
                    
                    let status = responseContent?.value(forKey: "Message") as! String
                    if status != nil {
                        self.navigationController?.dismiss(animated: true, completion: {
                            
                            let visibleVC = UIApplication.topViewController()!
                            if visibleVC is SlideMenuController{
                                let slideMenu = (visibleVC as! SlideMenuController)
                                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                                let RootVC = UINavigationController.init(rootViewController: homeVC!)
                                slideMenu.changeMainViewController(RootVC, close: false)
                                self.hideHUD()
                            }
                        })
                    }
                }
            })
        }
    }
    
    func validaData() -> Bool {
        if nameTexField.text?.count == 0 && emailTextField.text?.count == 0 && userNameTextField.text?.count == 0 {
            self.showAlertController(nil, message: "Please enter required details".localized, self.nameTexField)
            return false
            
        }else if nameTexField.text?.count == 0 || nameTexField.text == nil {
            self.showAlertController(nil, message: "Please enter First Name".localized, self.nameTexField)
            return false
            
        }else if userNameTextField.text?.count == 0 &&  userNameTextField.text == nil{
            self.showAlertController(nil, message: "Please enter Last Name".localized, self.userNameTextField)
            return false
            
        }else if !self.checkEmail(emailTextField.text! as NSString){
            self.showAlertController(nil, message: "Please enter valid email address".localized, self.emailTextField)
            return false
        }else if numberTextField.text == nil{
            self.showSimpleAlertController(nil, message: "Please enter valid country code".localized)
            return false
        }else if phoneNumberTextField.text?.count == 0 || phoneNumberTextField.text == nil {
            self.showAlertController(nil, message: "Enter phone number".localized, self.phoneNumberTextField)
            return false
            
        }else if !self.isValidPassword(passwordStr: passwordTextField.text){
            //self.showAlertController(nil, message: "Please enter valid Password".localized, self.passwordTextField)
            let passwordRulesAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "passwordRulesAlertVC") as? passwordRulesAlertViewController
            passwordRulesAlertVC?.modalPresentationStyle = .overCurrentContext
            self.present(passwordRulesAlertVC!, animated: false, completion: nil)
            return false
        } else if repeatePasswordTextField.text != passwordTextField.text {
            self.showAlertController(nil, message: "Please enter same Password".localized, self.repeatePasswordTextField)
            return false
        }
        
        if !agreeBtn.isSelected {
            
            self.showSimpleAlertController(nil, message: "Please accept terms and conditions".localized)
            return false
        }
        return true
    }
    
    func showAlertController(_ title : String?, message : String, _ textfield : UITextField) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message as String, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
            textfield.becomeFirstResponder()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
    // MARK : - SafariController Methods
    func openSafariController(_ urlString : NSString){
        if #available(iOS 9.0, *){
            let svc = SFSafariViewController(url: URL(string: urlString as String)!)
            self.present(svc, animated: true, completion: nil)
        }
        else{
            if let url = URL(string: urlString as String) {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}




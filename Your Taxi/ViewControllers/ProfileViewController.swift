import UIKit
import IQKeyboardManagerSwift
import MobileCoreServices

class ProfileViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var profileImage : UIButton!
    @IBOutlet weak var shadowView : UIView!
    @IBOutlet weak var nameTextfield : UITextField!
    @IBOutlet weak var lastnameTextfield : UITextField!
    @IBOutlet weak var emailTextfield : UITextField!
    @IBOutlet weak var mobileTextfield : UITextField!
    @IBOutlet weak var FirstnameView : UIView!
    @IBOutlet weak var EmailView : UIView!
    @IBOutlet weak var LastnameView : UIView!
    let picker = UIImagePickerController()
    var editButton : UIBarButtonItem!
    var saveButton : UIBarButtonItem!
    var name : String!
    var email : String!
    var profileStatus: Int?
    var lastName: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        getRiderDetails()
        mobileTextfield.isEnabled = false
        nameTextfield.isEnabled = false
        emailTextfield.isEnabled = false
        lastnameTextfield.isEnabled = false
        drawShadow(shadowView)
        profileImage.setBackgroundImage(UIImage(named : "Member"), for: .normal)
        profileImage.layer.cornerRadius = 50.0
        profileImage.clipsToBounds = true
        profileImage.layer.borderColor = UIColor.darkGray.cgColor
        profileImage.layer.borderWidth = 1.0
        picker.delegate = self
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(ProfileViewController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        editButton = UIBarButtonItem(title: "Edit".localized, style: .plain, target: self, action: #selector(ProfileViewController.editButtonClicked(_:)))
        saveButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(ProfileViewController.saveButtonClicked(_:)))
        navigationItem.rightBarButtonItem = editButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "PROFILE".localized
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Actions Methods
    func backAction(){
        if self.navigationItem.rightBarButtonItem == saveButton
        {
            self.nameTextfield.text = name
            self.emailTextfield.text = email
            self.view.endEditing(true)
            EmailView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
            FirstnameView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
            LastnameView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
            nameTextfield.textColor = kDARK_OLIVE_COLOR
            emailTextfield.textColor = kDARK_OLIVE_COLOR
            lastnameTextfield.textColor = kDARK_OLIVE_COLOR
            mobileTextfield.isEnabled = false
            nameTextfield.isEnabled = false
            emailTextfield.isEnabled = false
            lastnameTextfield.isEnabled = false
            navigationItem.rightBarButtonItem = editButton
            return
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func editButtonClicked(_ sender: UIBarButtonItem)
    {
        //EmailView.backgroundColor = kDARK_OLIVE_COLOR
        //FirstnameView.backgroundColor = kDARK_OLIVE_COLOR
        //nameTextfield.textColor = kDARK_OLIVE_COLOR
        //LastnameView.backgroundColor = kDARK_OLIVE_COLOR
        //lastnameTextfield.textColor = kDARK_OLIVE_COLOR
        //emailTextfield.textColor = kDARK_OLIVE_COLOR
        mobileTextfield.isEnabled = false
        nameTextfield.isEnabled = true
        lastnameTextfield.isEnabled = true
        emailTextfield.isEnabled = false
        navigationItem.rightBarButtonItem = saveButton
    }
    
    func saveButtonClicked(_ sender: UIBarButtonItem)
    {
        saveRiderDetails()
    }
    
    @IBAction func imageSelected(_ sender: UIButton) {
        
    }
    
    func saveRiderDetails()
    {
        if self.validaData()
        {
            if(checkConnection()){
                
                let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
                let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
                var userDict = self.convertStringToDictionary(text: decryt!)!
                
                let riderID  = userDict.value(forKey: "id") as! Int32
                var parameters = [
                    "UserId" : riderID,
                    "FirstName": nameTextfield.text!,
                    "LastName": lastnameTextfield.text!,
                    "Status": self.profileStatus!,
                    "UpdatedBy" : riderID
                    ] as [String : Any]
                
                let jsonString = returnJsonString(param: parameters)
                var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                parameters = ["data":encryptedData!]
                let jsonParam = returnJsonString(param: parameters)
                self.showHUD()
                let Request: WebserviceHelper = WebserviceHelper()
                let headerDict = Request.getHeaderForRider(parameters: parameters)
                printJsonStringLog(apiName: KRIDER_PROFILE, parameter: jsonParam as NSString, headerDict: headerDict)
                Request.CustomHTTPPostJSONWithHeader(url: KRIDER_PROFILE, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                    
                    self.hideHUD()
                    
                    if ((error) != nil){
                        print(error.debugDescription)
                    }else{
                        let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                        
                        let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                        
                        let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                        
                        print("SaveRiderDetails :", responseContent ?? "")
                        
                        if responseContent != nil
                        {
                            if responseContent!["Status"] != nil{
                                if let status = responseContent?.value(forKey: "Status") as? Bool {
                                    if status == true{
                                        userDict = userDict.mutableCopy() as! NSMutableDictionary
                                        userDict.setValue(self.nameTextfield.text!, forKey: "FirstName")
                                        userDict.setValue(self.lastnameTextfield.text!, forKey: "LastName")
                                        var encryptData = returnJsonString(param: userDict as! [String : Any])
                                        encryptData = AESCrypt.encrypt(encryptData, password: SECRET_KEY)
                                        let UserName = "\(self.nameTextfield.text!) \(self.lastnameTextfield.text!.uppercased().prefix(1))."
                                        UserDefaults.standard.set(UserName, forKey: KRIDER_USERNAME)
                                        UserDefaults.standard.setValue(encryptData, forKey: KUSER_DETAILS)
                                        UserDefaults.standard.synchronize()
                                        self.view.endEditing(true)
                                        //self.EmailView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
                                        //self.FirstnameView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
                                        //self.LastnameView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
                                        //self.nameTextfield.textColor = kDARK_OLIVE_COLOR
                                        //self.lastnameTextfield.textColor = kDARK_OLIVE_COLOR
                                        //self.emailTextfield.textColor = kDARK_OLIVE_COLOR
                                        self.mobileTextfield.isEnabled = false
                                        self.nameTextfield.isEnabled = false
                                        self.lastnameTextfield.isEnabled = false
                                        self.emailTextfield.isEnabled = false
                                        self.navigationItem.rightBarButtonItem = self.editButton
                                        self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                                    }
                                    else{
                                        self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    
    
    
    func getRiderDetails()
    {
        if checkConnection(){
            let Request: WebserviceHelper = WebserviceHelper()
            let USUserID = Request.getUserDefaultsStandard()
            var parameters = [
                "UserId" : USUserID
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            printJsonStringLog(apiName: KRIDER_DETAIL_URL, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_DETAIL_URL, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict ) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    if responseContent != nil{
                        self.name = "\(responseContent?.value(forKey: "FirstName") as! String)"
                        self.email = "\(responseContent?.value(forKey: "Email") as! String)"
                        self.lastName = "\(responseContent?.value(forKey: "LastName") as! String)"
                        self.nameTextfield.text = "\(responseContent?.value(forKey: "FirstName") as! String)"
                        self.lastnameTextfield.text = "\(responseContent?.value(forKey: "LastName") as! String)"
                        self.emailTextfield.text = "\(responseContent?.value(forKey: "Email") as! String)"
                        self.emailTextfield.adjustsFontSizeToFitWidth = true
                        self.mobileTextfield.text = "\(responseContent?.value(forKey: "DialCode") as! String )"+"\(responseContent?.value(forKey: "PhoneNo") as! String )"
                        self.profileStatus = Int("\(responseContent?.value(forKey: "Status") as! Int)")
                    }else{
                        self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                    }
                }
            }
        }
        else{
            hideHUD()
            let alertController: UIAlertController = UIAlertController(title: nil, message: "ConnetionMsgKey".localized, preferredStyle: .alert)
            
            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                self.backAction()
            }
            alertController.addAction(okAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func validaData() -> Bool {
        
        if nameTextfield.text?.characters.count == 0 && lastnameTextfield.text?.characters.count == 0 {
            
            self.showSimpleAlertController(nil, message: "Please enter required details".localized)
            
            return false
            
        }else if (nameTextfield.text?.characters.count)! == 0 && (lastnameTextfield.text?.characters.count)! > 0{
            
            self.showSimpleAlertController(nil, message: "Please enter Name".localized)
            
            return false
            
        }else if (nameTextfield.text?.characters.count)! > 0 && (lastnameTextfield.text?.characters.count)! == 0{
            
            self.showSimpleAlertController(nil, message: "Please enter User Name".localized)
            
            return false
            
        }else if !self.checkEmail(emailTextfield.text! as NSString){
            
            self.showSimpleAlertController(nil, message: "Please enter valid email address".localized)
            
            return false
        }
        return true
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        dismiss(animated:true, completion: nil)
        showHUD()
        WebserviceHelper().myImageUploadRequest(chosenImage, url: KRIDER_PROFILE, button: self.profileImage, completionHandler: { success in
            self.hideHUD()
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if nameTextfield.isFirstResponder
        {
//            FirstnameView.backgroundColor = kLIGHT_OLIVE_COLOR
//            LastnameView.backgroundColor = kLIGHT_OLIVE_COLOR
//            EmailView.backgroundColor = kDARK_OLIVE_COLOR
//            nameTextfield.textColor = kDARK_OLIVE_COLOR
//            lastnameTextfield.textColor = kDARK_OLIVE_COLOR
//            emailTextfield.textColor = kDARK_OLIVE_COLOR
            
        }
        else if emailTextfield.isFirstResponder
        {
//            EmailView.backgroundColor = kLIGHT_OLIVE_COLOR
//            FirstnameView.backgroundColor = kDARK_OLIVE_COLOR
//            LastnameView.backgroundColor = kDARK_OLIVE_COLOR
//            nameTextfield.textColor = kDARK_OLIVE_COLOR
//            lastnameTextfield.textColor = kDARK_OLIVE_COLOR
//            emailTextfield.textColor = kDARK_OLIVE_COLOR
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == emailTextfield
        {
            if (string == " ") {
                return false
            }
        }
        return true
    }
}

extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}


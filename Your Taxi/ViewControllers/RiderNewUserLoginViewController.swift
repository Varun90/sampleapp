import UIKit
import FirebaseAuth
import JWT
import Crashlytics
import SlideMenuControllerSwift

class RiderNewUserLoginViewController: BaseViewController {
    
    var phone_number = ""
    var onlyPhoneNumber = ""
    @IBOutlet var userNameTextField: UITextField!
    
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var forgotUserNameBtn: UIButton!
    
    @IBOutlet var forgotPasswordBtn: UIButton!
    
    @IBOutlet var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Login".localized
        forgotPasswordBtn.setTitle("Forgot your password?".localized, for: .normal)
        loginBtn.setTitle("LOG IN".localized, for: .normal)
        userNameTextField.placeholder = KAPP_RIDER ? "Email *".localized : "User Name *".localized
        passwordTextField.placeholder = "Password *".localized
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
    }
    
    //MARK:- BUTTON ACTIONS
    func backAction(){
      //  _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func passwordInfoBtnAction(_ sender: Any) {
        let passwordRulesAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "passwordRulesAlertVC") as? passwordRulesAlertViewController
        passwordRulesAlertVC?.modalPresentationStyle = .overCurrentContext
        self.present(passwordRulesAlertVC!, animated: false, completion: nil)
    }
    
    @IBAction func forgotUserNameBtnAction(_ sender: Any) {
        let ForgotUsernameAndPasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotUsernameAndPasswordVC") as? ForgotUsernameAndPasswordViewController
        ForgotUsernameAndPasswordVC?.modalPresentationStyle = .overCurrentContext
        ForgotUsernameAndPasswordVC?.isForUsername = true
        self.present(ForgotUsernameAndPasswordVC!, animated: false, completion: nil)
    }
    
    @IBAction func forgotPasswordBtnAction(_ sender: Any) {
        let ForgotUsernameAndPasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotUsernameAndPasswordVC") as? ForgotUsernameAndPasswordViewController
        ForgotUsernameAndPasswordVC?.modalPresentationStyle = .overCurrentContext
        ForgotUsernameAndPasswordVC?.isForUsername = false
        self.present(ForgotUsernameAndPasswordVC!, animated: false, completion: nil)
    }
    
    @IBAction func logInBtnAction(_ sender: Any) {
        if userNameTextField.text == "" || userNameTextField.text == nil {
            self.showAlertController(nil, message: KAPP_RIDER ? "Enter username" : "Please enter Email".localized, userNameTextField)
            return
        } else if passwordTextField.text == "" || passwordTextField.text == nil {
            self.showAlertController(nil, message: KAPP_RIDER ? "Enter password" : "Please enter Password".localized, passwordTextField)
            return
        }
        if isValidPassword(passwordStr: passwordTextField.text!) {

            var parameters = [
                "Email": userNameTextField.text!,
                "Password": passwordTextField.text!,
                "UserType": 2,
                "PhoneNo": onlyPhoneNumber,
                "DeviceToken": DEVICE_UUID,
                "PushToken": getPushTokenString(),
                "PlatForm": 1,
                ] as [String : Any]
            
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = [
                "Content-Type" : "application/json",
                "Language" : systemLanguage
            ]
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            if(checkConnection()) {
                riderLoginApi(jsonString: jsonParam, headerDict: headerDict)
            }else{
                self.showAlertForInternetConnectionOff()
            }
        } else {
           // showAlertController(nil, message: "Please enter valid Password".localized, passwordTextField)
            let passwordRulesAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "passwordRulesAlertVC") as? passwordRulesAlertViewController
            passwordRulesAlertVC?.modalPresentationStyle = .overCurrentContext
            self.present(passwordRulesAlertVC!, animated: false, completion: nil)
            return
        }
    }
    func showAlertController(_ title : String?, message : String, _ textfield : UITextField) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message as String, preferredStyle: .alert)
        
        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
            textfield.becomeFirstResponder()
        }
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func riderLoginApi(jsonString : String , headerDict : [String : String]){
        
        self.showHUD()
        var tempDict = NSMutableDictionary()
        let Request: WebserviceHelper = WebserviceHelper()
        printJsonStringLog(apiName: KRIDERLOGIN_URL, parameter: jsonString as NSString, headerDict: headerDict)
        Request.CustomHTTPPostJSONWithHeader(url: KRIDERLOGIN_URL, jsonString: jsonString as NSString, withIdicator: false, header: headerDict, callback: { (result, error) in
            
            if ((error) != nil){
                self.hideHUD()
                print(error?.localized ?? "")
            }else{
                
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                let responseContent = Request.convertStringToDictionary(text: decryptedData! as String)
                print("Rider Login :", responseContent ?? "")
                if responseContent!["token"] == nil{
                    self.hideHUD()
                    if let message = responseContent?.value(forKey: "Message") as? String {
                        self.self.showSimpleAlertControllerWithOkAction(nil, message: message, callback: {
                            if message == "Already logged in another device".localized {
                                let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                appDelegate.logoutRider()
                            }
                        })
                        return;
                    }
                    if responseContent?["Errors"] != nil {
                        let errMessage = (responseContent?.value(forKey: "Errors") as! NSDictionary).value(forKey: "PushToken") as? NSArray
                        self.showSimpleAlertController(nil, message : (errMessage![0] as? String)!)
                    }else{
                       
                    }
                }else{
                    let responsetoken = responseContent?.value(forKey: "token") as! String
                    if responsetoken != nil{
                        do {
                            let claims: ClaimSet = try JWT.decode(responsetoken, algorithm: .hs256(JWT_SECRET_KEY.data(using: .utf8)!))
                            let rider_id = claims["UserId"] as! Int32
                            
                            if let id = rider_id as? Int32 {
                                tempDict.setValue(id, forKey: "id")
                            }
                            print("JWT Claims :", claims ?? "")
                        } catch {
                            print("Failed to decode JWT: \(error)")
                        }
                        var encryptData = returnJsonString(param: tempDict as! [String : Any])
                        encryptData = AESCrypt.encrypt(encryptData, password: SECRET_KEY)
                        UserDefaults.standard.set( "Bearer " + responsetoken, forKey: kAUTH_TOKEN)
                        UserDefaults.standard.set(encryptData, forKey: KUSER_DETAILS)
                        var encryptRiderlogin =
                            UserDefaults.standard.set(true, forKey: KRIDER_LOGIN)
                        UserDefaults.standard.synchronize()
                        self.getRiderDetails(riderID: tempDict.value(forKey: "id") as! Int32)
                        self.hideHUD()
                        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                        homeVC?.tempDict = tempDict
                        let RootVC = UINavigationController.init(rootViewController: homeVC!)
                        self.slideMenuController()?.changeMainViewController(RootVC, close: true)
                        self.navigationController?.navigationBar.isHidden = false
                    }
                    else {
                        self.hideHUD()
                        if let message = responseContent?.value(forKey: "Message") as? String {
                            self.showSimpleAlertController(nil, message:message)
                        }
                    }
                }
            }
        })
    }
    
    func getRiderDetails(riderID: Int32)
    {
        if checkConnection(){
            
            var parameters = [
                "UserId" : riderID
                ] as [String : Any]
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            let Request: WebserviceHelper = WebserviceHelper()
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            printJsonStringLog(apiName: KRIDER_DETAIL_URL, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_DETAIL_URL, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict ) { (result, error) in
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    if responseContent != nil{
                       // let userFName = responseContent?.value(forKey: "FirstName") as? String ?? ""
                       // let userLName = responseContent?.value(forKey: "LastName") as? String ?? ""
                       // let UserName = "\(userFName) \(userLName)"
                        let UserName = responseContent?.value(forKey: "DisplayName") as? String ?? ""
                        UserDefaults.standard.set(UserName, forKey: KRIDER_USERNAME)
                        UserDefaults.standard.synchronize()
                    }
                }
            }
        }
    }
}

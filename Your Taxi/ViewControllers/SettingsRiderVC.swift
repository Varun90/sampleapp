    import UIKit
    import SlideMenuControllerSwift
    
    class SettingsRiderVC: BaseViewController {
        
        @IBOutlet var deleteProfileLabel: UILabel!
        @IBOutlet weak var btn24hourRadio: UIButton!
        @IBOutlet weak var btn12hourRdio: UIButton!
        @IBOutlet weak var viewTimeFormate: UIView!
        @IBOutlet var sectionTitleLbl: UILabel!
        @IBOutlet var optionOneLbl: UILabel!
        @IBOutlet var optionTwoLbl: UILabel!
        @IBOutlet var profileView: UIView!
        @IBOutlet var viewProfileLbl: UILabel!
        @IBOutlet var deleteProfileView: UIView!
        var defaultSelected = 24
        var Default = UserDefaults.standard
        override func viewDidLoad() {
            super.viewDidLoad()
            deleteProfileLabel.text = "DELETE PROFILE".localized
            sectionTitleLbl.text = "Time Format.".localized
            optionOneLbl.text = "12 hours format.".localized
            optionTwoLbl.text = "24 hours format.".localized
            viewProfileLbl.text = "View Profile".localized
            if let selectedFormate = Default.value(forKey: "TimeFormate")
            {
                if "\(selectedFormate)" == "12"
                {
                    btn12hourRdio.setImage(UIImage(named:"radio_Selected"), for: UIControlState.normal)
                    btn24hourRadio.setImage(UIImage(named:"radio_Unselected"), for: UIControlState.normal)
                }
                else
                {
                    btn12hourRdio.setImage(UIImage(named:"radio_Unselected"), for: UIControlState.normal)
                    btn24hourRadio.setImage(UIImage(named:"radio_Selected"), for: UIControlState.normal)
                }
                
            }
            else
            {
                Default.set("24", forKey: "TimeFormate")
                Default.synchronize()
                btn12hourRdio.setImage(UIImage(named:"radio_Unselected"), for: UIControlState.normal)
                btn24hourRadio.setImage(UIImage(named:"radio_Selected"), for: UIControlState.normal)
            }
            drawShadow(viewTimeFormate)
            drawShadow(profileView)
            drawShadow(deleteProfileView)
            let backImage  = UIImage(named: "icn_topbar_back")
            let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            backButton.setBackgroundImage(backImage, for: UIControlState())
            backButton.addTarget(self, action: #selector(SettingsRiderVC.backAction), for: UIControlEvents.touchUpInside)
            let backBarButton = UIBarButtonItem(customView: backButton)
            self.navigationItem.leftBarButtonItem = backBarButton
            self.navigationItem.leftBarButtonItem = backBarButton
            self.slideMenuController()?.removeLeftGestures()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.title = "Settings".localized
        }
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(true)
        }
        // MARK: - Actions Methods
        @IBAction func btn12hourRadio_Click(_ sender: Any) {
            
            btn12hourRdio.setImage(UIImage(named:"radio_Selected"), for: UIControlState.normal)
            btn24hourRadio.setImage(UIImage(named:"radio_Unselected"), for: UIControlState.normal)
            Default.set("12", forKey: "TimeFormate")
            Default.synchronize()
            self.showSimpleAlertController(nil, message: "You have selected 12 hours format.".localized)
        }
        
        @IBAction func btn24hourRadio_click(_ sender: Any)
        {
            btn12hourRdio.setImage(UIImage(named:"radio_Unselected"), for: UIControlState.normal)
            btn24hourRadio.setImage(UIImage(named:"radio_Selected"), for: UIControlState.normal)
            Default.set("24", forKey: "TimeFormate")
            Default.synchronize()
            self.showSimpleAlertController(nil, message: "You have selected 24 hours format.".localized)
        }
        @IBAction func viewProfileAction(_ sender: Any) {
            let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            self.navigationController?.pushViewController(profileVC!, animated: true)
        }
        
        @IBAction func deleteProfileAction(_ sender: UIButton) {
            #if APP_RIDER
                let deleteProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "DeleteRiderProfileVC") as? DeleteRiderProfileVC
                self.navigationController?.pushViewController(deleteProfileVC!, animated: true)
            #endif
        }
        
        func backAction(){
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    

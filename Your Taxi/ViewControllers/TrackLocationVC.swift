import UIKit
import GoogleMaps
import AVFoundation
import INTULocationManager

protocol TrackLocationDelegate: class {
    func didReachDestination()
}

class TrackLocationVC: BaseViewController , CLLocationManagerDelegate {

    @IBOutlet var gMapView: GMSMapView!
    @IBOutlet var arrowImageView: UIImageView!
    @IBOutlet var lblDistance: UILabel!
    @IBOutlet var lblPlace: UILabel!
    @IBOutlet var endButton: UIButton!
    @IBOutlet var lblAngle: UILabel!

    var rotation = 0.0
    var currentTripData : NSMutableDictionary = [:]
    var rideStarted : Bool = false
//    var locationCourse : CLLocationDegrees  = 0

    let txtDirection = ["turn-left","turn-right","turn-slight-left","turn-slight-right","u-turn","u-turn","Head"]
    let imageDirection = ["TurnLeft","TurnRight","SlightLeft","SlightRight","UTurnLeft","UTurnRight","Straight"]
    
    var carImageStr: String = "Home"
    var prevRoutePolyline:String = ""
    var directionList:NSMutableArray!
    var locationManager = CLLocationManager()
    var polyline = GMSPolyline()
    var toLocation = CLLocation()
    weak var trackDelegate : TrackLocationDelegate?
    var zoomTimer : Timer?
    
//    var updateLocationTimer: Timer!
    let movement = ARCarMovement()

    var sourceMarker = GMSMarker()
    var destinationMarker = GMSMarker()

    override func viewDidLoad() {
        super.viewDidLoad()

        movement.delegate = self
        
        let titleEndRide = "End".localized
        endButton.setTitle(titleEndRide, for: UIControlState())
        
        self.navigationItem.title = "Track Location"
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        initailizeLocationManger()

        gMapView.settings.rotateGestures = false
        
        animateViewToCurrentLocation()
//        zoomTimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.animateViewToCurrentLocation), userInfo: nil, repeats: true)
        
        if let carTypeID = currentTripData["CarTypeId"] as? NSInteger {
            carImageStr = (carTypeID == 1) ? "budget": (carTypeID == 2) ? "business" : "comfort"
        }
    }
    
    func initailizeLocationManger() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
//        locationManager.distanceFilter = 10
        locationManager.startUpdatingLocation()
        locationManager1.activityType = CLActivityType.otherNavigation;
        if #available(iOS 9.0, *) {
            locationManager1.allowsBackgroundLocationUpdates = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        if updateLocationTimer == nil {
//            self.showRouteOnMap()
//            updateLocationTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.showRouteOnMap), userInfo: nil, repeats: true)
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        if updateLocationTimer != nil{
//            updateLocationTimer!.invalidate()
//            updateLocationTimer = nil
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("Deinit")
    }
    
    func animateViewToCurrentLocation() {
        
        if let location = locationManager.location {
            let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 18.0)
            gMapView.camera = camera
            gMapView.animate(to: camera)
            gMapView.isMyLocationEnabled = false
            gMapView.settings.myLocationButton = true
            gMapView.settings.compassButton = true
        }
    }
    
    func showRouteOnMap() {

        DispatchQueue.main.async {
            
            if self.destinationMarker.map == nil {
                //Destination Marker
                self.destinationMarker.position = CLLocationCoordinate2D(latitude: self.toLocation.coordinate.latitude, longitude: self.toLocation.coordinate.longitude)
                if let value = self.currentTripData.value(forKey: self.rideStarted ? "DropLocation" : "PickupLocation" ) as? String {
                    self.destinationMarker.snippet = value
                }
                self.destinationMarker.icon = UIImage(named: self.rideStarted ? "dropupPin" : "pickupPin")
                self.destinationMarker.map = self.gMapView
                self.gMapView.selectedMarker = self.destinationMarker
            }
            
            // Creates a marker in the center of the map.
            let source = (self.locationManager.location?.coordinate)!
            let destination = CLLocationCoordinate2D(latitude: self.toLocation.coordinate.latitude, longitude: self.toLocation.coordinate.longitude)

            if self.sourceMarker.map != nil {
                self.drawRoute(location: source, destinationLocation: destination)
            }
            else {

                self.sourceMarker.position = (self.locationManager.location?.coordinate)!
                let markerImageView = UIImageView(image: UIImage(named: self.carImageStr))
                self.sourceMarker.iconView = markerImageView
                self.sourceMarker.iconView?.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * (self.rotation) / 180.0))
                self.sourceMarker.map = self.gMapView
                print("\t\nlocation = \(self.rotation)")

                self.drawRoute(location: source, destinationLocation: destination)
            }
        }
    }
    
    @IBAction func finishPressed(_ sender: Any) {
        
        self.locationManager.stopUpdatingLocation()
        
//        if updateLocationTimer != nil{
//            updateLocationTimer!.invalidate()
//            updateLocationTimer = nil
//        }
        
        if zoomTimer != nil {
            zoomTimer?.invalidate()
        }

        if gMapView != nil {
            gMapView.clear()
        }

        trackDelegate?.didReachDestination()
        self.navigationController?.popViewController(animated: true)
    }
    
    func drawRoute(location: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D){
        
        let origin = "\(location.latitude),\(location.longitude)"
        let destination = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&sensor=false&mode=driving&key=\(GOOGLE_WEB_API_KEY)"
        print("ROUTE URL : \(url)")
        
        URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { (data, response, error) -> Void in
            if((error) != nil) {
                return
            }
            
            do {
                let responseObject = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(responseObject ?? "")
                
                let routes = responseObject!["routes"] as? [[String:AnyObject]]

                for route in routes!
                {
                    print(route)
                    let routeOverviewPolyline = route["overview_polyline"] as? NSDictionary
                    let points = "\(routeOverviewPolyline?["points"] as AnyObject)"
                    print()
                    let path = GMSPath.init(fromEncodedPath: points)
                    self.polyline.path = path
                    self.polyline.map = self.gMapView
                    self.polyline.strokeWidth = 8.0
                    self.polyline.strokeColor = UIColor(red: 54.0/255.0, green: 131.0/255.0, blue: 217.0/255.0, alpha: 1.0)
                    
                    // Get distance
                    let legs = route["legs"] as! NSArray
                    let leg = legs[0] as! NSDictionary

                    if let steps = leg["steps"] as? NSArray {
                        self.directionList = steps.mutableCopy() as! NSMutableArray
                    }
                    print(self.directionList)
                    
                    //After update the route show the route information
                    self.showDirectionInformation()
                    
                    //Zoom into the source and desstination location
                    DispatchQueue.main.async {
                        
                        //Setup Bounds
                        var bounds = GMSCoordinateBounds.init(coordinate: location, coordinate: destinationLocation)
                        bounds = bounds.includingPath(path!)
                        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(80.0, 50.0, 30.0, 50.0))
                        self.gMapView.animate(with: cameraUpdate)

                        //Update the pick up and Drop pin based on end point
                        self.setupMarkersAfterRoute(route: route)
                    }
                }
                
            } catch let error as NSError {
                print(error)
            }
            
        }).resume()
    }

    
    func setupMarkersAfterRoute(route : [String:AnyObject]) {
        var directionList : NSMutableArray = []
        // Get distance
        let legs = route["legs"] as! NSArray
        let leg = legs[0] as! NSDictionary
        
        guard let steps = leg["steps"] as? NSArray else {
            return
        }
        
        directionList = steps.mutableCopy() as! NSMutableArray
        print(directionList)
        
        if directionList.count > 0 {
            let startStep = directionList.firstObject as! NSDictionary
            let endStep = directionList.lastObject as! NSDictionary
            
            if let sourceDict = startStep["start_location"] as? NSDictionary, let lat = sourceDict["lat"] as? Double, let lng = sourceDict["lng"] as? Double  {
                let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                setUpDriverMarkerWithNewLocation(location: location)
            }
            
            if let destinationDict = endStep["end_location"] as? NSDictionary, let lat = destinationDict["lat"] as? Double, let lng = destinationDict["lng"] as? Double  {
                let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                destinationMarker.position = location
            }
        }
    }
    
    func setUpDriverMarkerWithNewLocation(location : CLLocationCoordinate2D) {
        movement.arCarMovement(marker: sourceMarker, oldCoordinate: sourceMarker.position, newCoordinate:location, mapView: gMapView, bearing: self.rotation, movingDuration: 0.5)
    }
    
    func showDirectionInformation() {
        
        if self.directionList.count > 0 {
            
            let firstStep = self.directionList[0] as! NSDictionary
            
            //Get Route instructions
            let instruction = firstStep["html_instructions"] ?? "Instructions not available" as String
            print(instruction)
            
            //Get Distance
            var distance = "0"
            if let value = firstStep["distance"] as? NSDictionary {
                distance = value["text"] as! String
            }
            
            //Get Route Image
            var arrowImage = UIImage(named: "Straight")
            
            if let maneuver = firstStep["maneuver"] as? String {
                if self.txtDirection.contains(maneuver) {
                    let index = self.txtDirection.index(of: maneuver)
                    if index! < self.imageDirection.count {
                        arrowImage = UIImage(named: self.imageDirection[index!])
                    }
                }
            }
            
            DispatchQueue.main.async {
                
                self.lblPlace.text = (firstStep["html_instructions"] as? String)?.withoutHtmlTags
                self.lblDistance.text = distance
                self.arrowImageView.image = arrowImage
                
                if self.prevRoutePolyline == "" {
                    if let prevRoutePolyline = firstStep["polyline"] as? NSDictionary {
                        if let prevRoutePolylinePoints = prevRoutePolyline["points"] as? String {
                            self.prevRoutePolyline = prevRoutePolylinePoints
                        }
                    }
                } else {
                    if let currentRoutePolyline = firstStep["polyline"] as? NSDictionary {
                        if let currentRoutePolylinePoints = currentRoutePolyline["points"] as? String {
                            if self.prevRoutePolyline != currentRoutePolylinePoints {
                            }
                        }
                    }
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        print("Current detected location \(String(describing: location))")
        print("Current location Course \(String(describing: location?.course))")
  
        if gMapView != nil {
            if let course = location?.course {
                self.rotation = Double(course) ?? 0.0
                self.rotation = self.rotation < 0.0 ? 0.0 : self.rotation
                print(self.rotation)
            }
            showRouteOnMap()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        print("NewHeading ***  \(newHeading)")
    }
}

extension String {
    var withoutHtmlTags: String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}

extension TrackLocationVC : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("didChange")
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("willMove")
    }
    
}

extension TrackLocationVC : ARCarMovementDelegate {
    
    func arCarMovementMoved(_ marker: GMSMarker) {
        print("Delegate")
    }
}

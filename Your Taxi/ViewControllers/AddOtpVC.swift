import UIKit
import SlideMenuControllerSwift
import FirebaseAuth
import JWT

class AddOtpVC: BaseViewController, UITextFieldDelegate {
    var phone_number = ""
    var onlyPhoneNumber = ""
    @IBOutlet var btnContinue: UIButton!
    @IBOutlet var otpTextField: UITextField!
    @IBOutlet var btnResendOTP: UIButton!
    var isFromLogin : Bool = false
    var mobileNumber = ""
    var countryPrefix = ""
    var titleStr = ""
    var seconds = 60
    var timer = Timer()
    var inVaildSeconds = 120
    var inValiedTimer = Timer()
    var registerDict : NSMutableDictionary!
    var tempDict : NSMutableDictionary!
    var tempDictNew : NSMutableDictionary!
    var inValiedOTPCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        otpTextField.delegate = self
        otpTextField.returnKeyType = .done
        otpTextField.placeholder = KAPP_RIDER ? "Enter OTP".localized : "Please enter the OTPDriver".localized
        btnContinue.setTitle("OTPNext".localized, for: .normal)
        self.btnContinue.setTitleColor(UIColor.gray, for: .normal)
        btnResendOTP.isUserInteractionEnabled = false
        self.setUpNavigationBar()
        runTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text?.count)! >= 5 {
            self.btnContinue.setTitleColor(UIColor.white, for: .normal)
        }
        else {
            self.btnContinue.setTitleColor(UIColor.white, for: .normal)
        }
        let characterLimit = 7
        let newText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        let numberOfChars = newText.characters.count
        return numberOfChars < characterLimit
    }
    
    //MARK:- TIMER METHODS
    func runTimer() {
        inValiedTimer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    func updateTimer(){
        seconds -= 1
        UIView.performWithoutAnimation {
            btnResendOTP.setTitle("Resending OTP in".localized + " \(seconds) " + "seconds".localized, for: .normal)
            btnResendOTP.layoutIfNeeded()
        }
        if (seconds == 0){
            btnResendOTP.setTitle("Resend OTP".localized, for: .normal)
            btnResendOTP.isUserInteractionEnabled = true
            timer.invalidate()
        }
    }
    
    func inValiedrunTimer() {
        timer.invalidate()
        inValiedTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(inValiedupdateTimer)), userInfo: nil, repeats: true)
    }
    
    func inValiedupdateTimer(){
        inVaildSeconds -= 1
        UIView.performWithoutAnimation {
            btnResendOTP.setTitle("Too many invalid attempts, Please try again after".localized + " \(inVaildSeconds) " + "seconds".localized, for: .normal)
            btnResendOTP.layoutIfNeeded()
            btnContinue.isEnabled = false
            otpTextField.isEnabled = false
        }
        if (inVaildSeconds == 0){
            btnResendOTP.setTitle("Resend OTP".localized, for: .normal)
            btnResendOTP.isUserInteractionEnabled = true
            inValiedTimer.invalidate()
            btnContinue.isEnabled = false
            otpTextField.isEnabled = false
        }
    }

    //MARK:- SET UP NAVIGATION BAR
    func setUpNavigationBar(){
        if isFromLogin {
            self.title = "Login".localized
            //self.title = "OTPNext".localized
        } else {
            self.title = "Register".localized
        }
        
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
    }
    
    //MARK:- BUTTON ACTIONS
    func backAction(){
//        if btnContinue.isEnabled{
//            _ = self.navigationController?.popViewController(animated: true)
//        }
    }
    
    @IBAction func resendOTPAction(_ sender: UIButton) {
        self.otpTextField.text = ""
        btnContinue.isEnabled = true
        otpTextField.isEnabled = true
        seconds = 60
        inVaildSeconds = 120
        self.inValiedOTPCount = 0
        self.inValiedTimer.invalidate()
        runTimer()
        let phoneNumber = self.phone_number
        self.showHUD()
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            self.hideHUD()
            FirebaseAuthVersionNumber
            print("error:", error)
            if let error = error {
                self.showSimpleAlertController(nil, message: "Blocked all requests".localized)
                 self.inValiedOTPCount = 0
                return
            }
            encrypt(data: verificationID as AnyObject, key: "authVerificationID")
            
            self.setupOtpView(view: self.view)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if let sentView = self.view.viewWithTag(SENT_VIEW_TAG){
                    sentView.removeFromSuperview()
                }
            }
        }
    }
    
    @IBAction func continueButtonAction(_ sender: UIButton) {
        
        inValiedOTPCount += 1
        if otpTextField.text == "" {
            self.showSimpleAlertController(nil, message: KAPP_RIDER ? "Please enter the OTP".localized : "Please enter the OTPDriver".localized)
            return
        }
        let verificationID = getDecryptedData(key: "authVerificationID")
        let verificationCode = otpTextField.text!
        print("verificationCode : ", verificationID)
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)
        Auth.auth().signInAndRetrieveData(with: credential) { (authUserData, error) in
            self.hideHUD()
            if let error = error {
                self.showSimpleAlertControllerWithOkAction(nil, message: "Invalid OTP".localized, callback: {
                    self.otpTextField.text = ""
                    if self.inValiedOTPCount >= 3 {
                        self.otpTextField.text = ""
                        self.timer.invalidate()
                        self.inValiedOTPCount = 0
                        self.inValiedrunTimer()
                        return
                    }
                })
                return
            }
            if let otpMobileNumber = authUserData?.user.phoneNumber {
                 self.phone_number = self.phone_number.replacingOccurrences(of:" " , with: "")
                if self.phone_number != otpMobileNumber {
                    self.showSimpleAlertControllerWithOkAction(nil, message: "Invalid OTP".localized, callback: {
                        self.otpTextField.text = ""
                        if self.inValiedOTPCount >= 3 {
                            self.otpTextField.text = ""
                            self.timer.invalidate()
                            self.inValiedOTPCount = 0
                            self.inValiedrunTimer()
                            return
                        }
                    })
                    return
                }
            }
            if(self.isFromLogin){
                if KAPP_RIDER {
                    let riderLoginVC = self.storyboard?.instantiateViewController(withIdentifier: "RiderNewUserLoginVC") as? RiderNewUserLoginViewController
                    riderLoginVC?.phone_number = self.phone_number
                    riderLoginVC?.onlyPhoneNumber = self.onlyPhoneNumber
                    self.navigationController?.pushViewController(riderLoginVC!, animated: true)
                }else {
                    let driverLoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                    driverLoginVC?.phone_number = self.phone_number
                    driverLoginVC?.onlyPhoneNumber = self.onlyPhoneNumber
                    self.navigationController?.pushViewController(driverLoginVC!, animated: true)
                }
            }else{
                let firstName = self.registerDict.value(forKey: "FirstName") as? String
                let lastName = self.registerDict.value(forKey: "LastName") as? String
                let email = self.registerDict.value(forKey: "Email") as? String
                let phoneNo = self.registerDict.value(forKey: "PhoneNo") as? String
                let country = self.registerDict.value(forKey: "country") as? String
                let password = self.registerDict.value(forKey: "Password") as? String
                let deviceID = self.registerDict.value(forKey: "deviceID") as? String
                let push_token = self.registerDict.value(forKey: "push_token") as? String
                let userType = self.registerDict.value(forKey: "UserType") as? String
                let platform = self.registerDict.value(forKey: "platform") as? String
                let language = self.registerDict.value(forKey: "Language") as? String
                var parameters = [
                    "FirstName" : firstName!,
                    "LastName" : lastName!,
                    "Email" : email!,
                    "PhoneNo" : phoneNo!,
                    "DialCode" : country!,
                    "country" : country!,
                    "Password" : password!,
                    "deviceID" : deviceID,
                    "push_token" : push_token,
                    "UserType" : userType,
                    "platform" : platform,
                    "Language" : language,
                    "Status" : 1,
                    "CreatedBy":"0"
                    ] as [String : Any]
                let jsonString = returnJsonString(param: parameters)
                var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                let headerDict = [
                    "Content-Type" : "application/json",
                    "Language" : systemLanguage
                ]
                parameters = ["data":encryptedData!]
                let jsonParam = returnJsonString(param: parameters)
                if(self.checkConnection()){
                    self.newRegistrationApi(jsonString: jsonParam, headerDict: headerDict)
                }else{
                    self.showAlertForInternetConnectionOff()
                }
            }
        }
    }
    
    //MARK:- WEB SERVICE METHODS
    func newRegistrationApi(jsonString : String, headerDict : [String : String]){
        self.showHUD()
        let Request: WebserviceHelper = WebserviceHelper()
        printJsonStringLog(apiName: KRegistration_URL, parameter: jsonString as NSString, headerDict: headerDict)
        Request.CustomHTTPPostJSONWithHeader(url: KRegistration_URL, jsonString: jsonString as NSString, withIdicator: false, header: headerDict, callback: { (result, error) in
            self.hideHUD()
            if ((error) != nil){
                print(error.debugDescription)
            }else{
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                
                let responseContent = Request.convertStringToDictionary(text: decryptedData! as String)
                print("OTPRegistrationRules :", responseContent ?? "")
                
                let status = responseContent?.value(forKey: "Message") as! String
                
                if status != nil{
                    UserDefaults.standard.set(true, forKey: KRIDER_LOGIN)
                    UserDefaults.standard.synchronize()
                    var parameters = [
                        "Email": self.registerDict.value(forKey: "Email") as! String ,
                        "Password": self.registerDict.value(forKey: "Password") as! String,
                        "UserType": self.registerDict.value(forKey: "UserType") as! String,
                        "PhoneNo": self.registerDict.value(forKey: "PhoneNo") as! String,
                        "DeviceToken": DEVICE_UUID,
                        "PushToken": getPushTokenString(),
                        "PlatForm": 1,
                        ] as [String : Any]
                    
                    let jsonString = returnJsonString(param: parameters)
                    var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                    let headerDict = [
                        "Content-Type" : "application/json",
                        "Language" : systemLanguage
                    ]
                    parameters = ["data":encryptedData!]
                    let jsonParam = returnJsonString(param: parameters)
                    self.riderLoginApi(jsonString: jsonParam, headerDict: headerDict)
                    self.hideHUD()
                }else{
                    self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "response") as! NSDictionary).value(forKey: "Message") as! String)
                    self.otpTextField.text = ""
                    self.hideHUD()
                }
                
            }
        })
    }
    func riderLoginApi(jsonString : String , headerDict : [String : String]){
        self.showHUD()
        let Request: WebserviceHelper = WebserviceHelper()
        printJsonStringLog(apiName: KRIDERLOGIN_URL, parameter: jsonString as NSString, headerDict: headerDict)
        Request.CustomHTTPPostJSONWithHeader(url: KRIDERLOGIN_URL, jsonString: jsonString as NSString, withIdicator: false, header: headerDict, callback: { (result, error) in
            let tempDictNew = NSMutableDictionary()
            if ((error) != nil){
                self.hideHUD()
                print(error?.localized ?? "")
            }else{
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                let responseContent = Request.convertStringToDictionary(text: decryptedData! as String)
                print("Rider Login :", responseContent ?? "")
                let responsetoken = responseContent?.value(forKey: "token") as! String
                if responsetoken != nil{
                    
                    do {
                        let claims: ClaimSet = try JWT.decode(responsetoken, algorithm: .hs256(JWT_SECRET_KEY.data(using: .utf8)!))
                        let rider_id = claims["UserId"] as! Int32
                        if let id = rider_id as? Int32 {
                            tempDictNew.setValue(rider_id, forKey: "id")
                        }
                        var encryptData = returnJsonString(param: tempDictNew as! [String : Any])
                        encryptData = AESCrypt.encrypt(encryptData, password: SECRET_KEY)
                        UserDefaults.standard.set( "Bearer " + responsetoken, forKey: kAUTH_TOKEN)
                        UserDefaults.standard.set(encryptData, forKey: KUSER_DETAILS)
                        UserDefaults.standard.synchronize()
                        
                        print("JWT Claims :", claims ?? "")
                    } catch {
                        print("Failed to decode JWT: \(error)")
                    }
                    
                    let visibleVC = UIApplication.topViewController()!
                    if visibleVC is SlideMenuController{
                        let slideMenu = (visibleVC as! SlideMenuController)
                        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                        homeVC?.tempDictNew = tempDictNew
                        let RootVC = UINavigationController.init(rootViewController: homeVC!)
                        slideMenu.changeMainViewController(RootVC, close: false)
                    }
                    self.hideHUD()
                    
                } else {
                    self.hideHUD()
                    if let message = (responseContent?.value(forKey: "response") as! NSDictionary).value(forKey: "message") as? String {
                        self.showSimpleAlertController(nil, message: message)
                        self.showHUD()
                    }
                }
            }
        })
    }
}


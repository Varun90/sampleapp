

import UIKit
import Cosmos
class CustomAlertViewController: BaseViewController {
    
    //MARK:- PROPERTY
    @IBOutlet weak var regularPriceLbl: UILabel!
    @IBOutlet weak var extraKmLbl: UILabel!
    @IBOutlet weak var extraMinutesLbl: UILabel!
    @IBOutlet weak var tipsLbl: UILabel!
    @IBOutlet weak var totalPaidLbl: UILabel!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var paidTotalLabel : UILabel!
    @IBOutlet var ratingView: CosmosView!
    
    @IBOutlet weak var titleregularPriceLbl: UILabel!
    @IBOutlet weak var titleextraKmLbl: UILabel!
    @IBOutlet weak var titleextraMinutesLbl: UILabel!
    @IBOutlet weak var titletipsLbl: UILabel!
    @IBOutlet weak var titletotalPaidLbl: UILabel!
    @IBOutlet weak var titleheaderLbl: UILabel!
    
    var paramterName = ""
    var btnName = ""
    var resultDic: NSDictionary?
    var currentTripInfo : NSDictionary?
    var startRating = 0.0
    var rideInfoVC: RideInfoViewController!
    
    let BUTTON_ENABLED_COLOR = UIColor(red: 239/255.0, green: 117/255.0, blue: 33/255.0, alpha: 1.0)
    
    //MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.regularPriceLbl.text = resultDic?.value(forKey: "regular_price") as? String
        self.extraKmLbl.text = resultDic?.value(forKey: "extra_km") as? String
        self.extraMinutesLbl.text = resultDic?.value(forKey: "extra_minute") as? String
        self.totalPaidLbl.text = resultDic?.value(forKey: "paid_total") as? String
        self.tipsLbl.text = resultDic?.value(forKey: "tips") as? String
        self.btnTitle.setTitle(btnName, for: UIControlState())
        
        self.btnTitle.isEnabled = false
        self.btnTitle.backgroundColor = UIColor.lightGray
        
        self.titleregularPriceLbl.text = "Price:".localized
        self.titleextraKmLbl.text = "Extra km:".localized
        self.titleextraMinutesLbl.text = "Extra minute:".localized
        self.titletipsLbl.text = "Tips:".localized
        self.titletotalPaidLbl.text = "Paid Total:".localized
        self.titleheaderLbl.text = "Payment".localized
        
        ratingView.didFinishTouchingCosmos = { rating in
            self.startRating = rating
            if self.startRating > 0{
                self.btnTitle.isEnabled = true
                self.btnTitle.backgroundColor = self.BUTTON_ENABLED_COLOR
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        let maskPath = UIBezierPath(roundedRect: self.headerView.bounds, byRoundingCorners: ([.topLeft, .topRight,]), cornerRadii: CGSize(width: 4, height: 4))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.view.bounds
        maskLayer.path = maskPath.cgPath
        self.headerView.layer.mask = maskLayer
    }
    
    //MARK:- BUTTON ACTION METHODS
    @IBAction func CashRecivedAction(_ sender: AnyObject) {
        callFinishRideAPI()
    }
    
    //MARK:- WEB SERVICE METHODS
    func callFinishRideAPI() {
        
        if checkConnection() {
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            let parameters = [
                "Rating":"\(ratingView.rating)",
                "DriverId":getLoginDriverID(),
                "BookingId":currentTripInfo?.value(forKey: "BookingId") as! NSInteger,
                "Type":"\(paramterName)"
                ] as [String : Any]
            
            printParameterLog(apiName: KRIDESTATUS_UPDATE_URL, parameter: parameters, headerDict: getRequestHeaders())
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KRIDESTATUS_UPDATE_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if(error != nil) {
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                
                let responseObject = getResponseDictionaryWithResult(result: result)
                print(responseObject)
                
                if let message = responseObject["Errors"] as? String {
                    self.showSimpleAlertController(nil, message: message)
                    return
                }
                
                guard (responseObject["result"] as? [String:Any]) != nil else {
                    //  : Handle Driver Logout
                    if let message = responseObject["Message"] as? NSString {
                        
                        if let value = responseObject["is_already_logout"] as? String {
                            if value == "1" && UserDefaults.standard.bool(forKey: KRIDER_LOGIN) && !KAPP_RIDER {
                                self.rideInfoVC.delegate?.forceFullyLogoutDriverAlert(message as String)
                            }
                            else {
                                self.showHomeVC(responseObject: responseObject)
                            }
                        }
                        else {
                            self.showHomeVC(responseObject: responseObject)
                        }
                    } else {
                        //self.showSimpleAlertController(nil, message:"Response Error")
                    }
                    return
                }
                
                self.showHomeVC(responseObject: responseObject)
            }
        }
        else {
            showAlertForInternetConnectionOff()
        }
    }
    
    
    func showHomeVC(responseObject : NSDictionary) {
        
        let alertController: UIAlertController = UIAlertController(title: "".localized as String, message: responseObject["Message"] as? String , preferredStyle: .alert)
        UserDefaults.standard.removeObject(forKey: "Time")
        UserDefaults.standard.removeObject(forKey: "rideStatus")
        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
            self.dismiss(animated: true, completion: {
                
                let navViewControllers = self.rideInfoVC.navigationController?.viewControllers
                let homeVCList : [UIViewController] = (navViewControllers?.filter{ $0 .isKind(of: HomeViewController.self) })!
                
                guard let homeVC = homeVCList[0] as? HomeViewController else {
                    self.rideInfoVC.navigationController?.popViewController(animated: true)
                    return
                }
                
                self.rideInfoVC.navigationController?.popToViewController(homeVC, animated: true)
            })
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- MEMORY MANAGEMENT METHOD
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


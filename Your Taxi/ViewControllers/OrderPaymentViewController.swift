import UIKit
import Stripe
import IQKeyboardManagerSwift
import SlideMenuControllerSwift
import KDCircularProgress

enum STPBackendChargeResult : Int {
    case success
    case failure
}
typealias STPTokenSubmissionHandler = (_ status: STPBackendChargeResult, _ error: Error?) -> Void

protocol STPBackendCharging {
    func createBackendCharge(with token: STPToken, completion: @escaping(STPTokenSubmissionHandler))
}
class OrderPaymentViewController: BaseViewController,STPBackendCharging, PaymentViewControllerDelegate, CardIOPaymentViewControllerDelegate,UITextFieldDelegate{
    @IBOutlet weak var circularProgressView: KDCircularProgress!
    @IBOutlet var orderButton: UIButton!
    @IBOutlet var cardButton: UIButton!
    @IBOutlet var cashButton: UIButton!
    @IBOutlet var totalPriceLable: UILabel!
    @IBOutlet var offerLabel: UILabel!
    @IBOutlet var searchDriverProgressView: UIView!
    @IBOutlet var preBookInfoView: UIView!
    @IBOutlet var lblPrebookMessage: UILabel!
    @IBOutlet var hideTextField: UITextField!
    @IBOutlet var firstTitleLbl: UILabel!
    @IBOutlet var secondTitleLbl: UILabel!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var PreBookingBtn: UIButton!
    var datePicker : UIDatePicker!
    var rideOfferDict = [String : Any]()
    var dateForRequest = ""
    var currentDateForRequest = ""
    var orderDict = [String : Any]()
    var card_type = "card"
    var isCardAvailable = ""
    var cardIOVC : CardIOPaymentViewController!
    var isRideBooked = false
    var rideID = ""
    let CARD_WITH_SELECTED_COLOR = UIColor(red: 59/255.0, green: 146/255.0, blue: 27/255.0, alpha: 1.0)
    let CARD_WITH_UNSELECTED_COLOR = UIColor(red: 152/255.0, green: 194/255.0, blue: 33/255.0, alpha: 1.0)
    var actualPrice : String? = ""
    var timer = Timer()
    var currentCount = 0.0
    let maxCount = 180.0
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "PAYMENT".localized
        firstTitleLbl.text = "Secure payments.".localized
        secondTitleLbl.text = "You will pay after the ride.".localized
        PreBookingBtn.setTitle("Pre-Booking".localized, for: .normal)
        cancelBtn.setTitle("Back".localized, for: .normal)
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(OrderPaymentViewController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        cardButton.setImage(#imageLiteral(resourceName: "card_selected"), for: .normal)
        cashButton.setImage(#imageLiteral(resourceName: "cash_unselected"), for: .normal)
        let priceArry = (orderDict["final_price"] as! String).components(separatedBy: " ")
        if priceArry.count > 1{
            totalPriceLable.text =  priceArry[1] as String +  " " + "CHF "
        }
        else if priceArry.count > 0{
            totalPriceLable.text = priceArry[0] as String +  " " + "CHF "
        }
        else{
            totalPriceLable.text = "0.00" +  " " + "CHF "
        }
        if let value = rideOfferDict["discount_msg"] as? String{
            let alertController: UIAlertController = UIAlertController(title: nil, message: value.localized, preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                self.cardButtonAction(self.cardButton)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            cardButtonAction(cardButton)
        }
        offerLabel.text = "" //rideOfferDict["OfferText"] as? String
        self.setScreenLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if(KAPP_RIDER){
            locationManager1.stopUpdatingLocation()
        }
    }
    
    func showPrebookingOptionsView( _ notification : Notification? = nil) {
        
        if notification != nil {
            lblPrebookMessage.text = ((notification?.userInfo!["aps"] as! NSDictionary).value(forKey: "alert") as! String)
        }
        
        if (delegate != nil) {
            delegate!.stopSound()
        }
        
        if timer.isValid {
            timer.invalidate()
        }
        self.circularProgressView.isHidden = true
        self.preBookInfoView.alpha = 0.0
        UIView.animate(withDuration: 0.2, animations: {
            self.preBookInfoView.alpha = 1.0
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if(KAPP_RIDER){
            locationManager1.startUpdatingLocation()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - Set Screen Design Methods
    func setScreenLayout() {
        orderButton.backgroundColor = kBUTTON_DARK_GREEN_COLOR
        orderButton.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        orderButton.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
    }
    //MARK: Action methods
    func backAction() {
        if isRideBooked{
            checkOrderCurrentStatus()
        }
        else{
            _ = self.navigationController?.popViewController(animated: true)
        }
        return
    }
    
    @IBAction func preBookAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
        let storyboard = UIStoryboard(name: "Rider", bundle: nil)
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        let swiftViewController = UINavigationController(rootViewController: homeVC!)
        self.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
    }
    
    @IBAction func cardButtonAction(_ sender: Any) {
        if self.isRideBooked == true
        {
            self.showSimpleAlertController(nil, message: "Waiting for driver response.".localized)
            return
        }
        card_type = "card"
        cardButton.setImage(#imageLiteral(resourceName: "card_selected"), for: .normal)
        cashButton.setImage(#imageLiteral(resourceName: "cash_unselected"), for: .normal)
        if self.isCardAvailable == "fail" || self.isCardAvailable == ""
        {
            let alertController: UIAlertController = UIAlertController(title: "", message: "How do you pay?".localized, preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Card1".localized, style: .default) { action -> Void in
                self.showCardActionSheet()
            }
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cash1".localized, style: .default) { action -> Void in
                self.card_type = "cash"
                
                self.cardButton.setImage(#imageLiteral(resourceName: "card_unselected"), for: .normal)
                self.cashButton.setImage(#imageLiteral(resourceName: "cash_selected"), for: .normal)
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    @IBAction func cashButtonAction(_ sender: Any) {
        if self.isRideBooked == true
        {
            self.showSimpleAlertController(nil, message: "Waiting for driver response.".localized)
            return
        }
        card_type = "cash"
        self.cardButton.setImage(#imageLiteral(resourceName: "card_unselected"), for: .normal)
        self.cashButton.setImage(#imageLiteral(resourceName: "cash_selected"), for: .normal)
        
    }
    @IBAction func orderButtonAction(_ sender: Any) {
        
        if self.isRideBooked == false
        {
            if card_type == "card"
            {
                if isCardAvailable == "fail" || isCardAvailable == ""
                {
                    let alertController: UIAlertController = UIAlertController(title: nil, message: "Please add your credit card details.".localized, preferredStyle: .alert)
                    let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .default) { action -> Void in
                        let paymentViewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                        paymentViewController.amount = 10.0
                        paymentViewController.backendCharger = self
                        paymentViewController.paymentDelegate = self
                        let navController = UINavigationController(rootViewController: paymentViewController)
                        self.present(navController, animated: true, completion: nil)
                    }
                    let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
                    alertController.addAction(okAction)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
            }
            orderDict["is_cash"] = card_type
            
            getCancellationFeesDetails()
        }
        else{
            self.showSimpleAlertController(nil, message: "Waiting for driver response.".localized)
        }
    }
    
    //MARK: Creadit Card methods
    func showCardActionSheet(){
        let alertController = UIAlertController(title: "Credit Card".localized, message: nil, preferredStyle: .actionSheet)
        
        let addCardButton = UIAlertAction(title: "Add Card".localized, style: .default, handler: { (action) -> Void in
            let paymentViewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            paymentViewController.amount = 10.0
            paymentViewController.backendCharger = self
            paymentViewController.paymentDelegate = self
            let navController = UINavigationController(rootViewController: paymentViewController)
            self.present(navController, animated: true, completion: nil)
        })
        
        let  scanCardButton = UIAlertAction(title: "Scan Card".localized, style: .default, handler: { (action) -> Void in
            self.cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
            self.cardIOVC?.collectCardholderName = true
            self.cardIOVC.hideCardIOLogo = true
            self.cardIOVC.navigationBarTintColorForCardIO = KAPP_NAV_COLOR
            self.cardIOVC.disableManualEntryButtons = true
            self.cardIOVC?.modalPresentationStyle = .formSheet
            self.present(self.cardIOVC!, animated: true, completion: nil)
        })
        
        let cancelButton = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (action) -> Void in
            self.card_type = "cash"
            self.cardButton.setImage(#imageLiteral(resourceName: "card_unselected"), for: .normal)
            self.cashButton.setImage(#imageLiteral(resourceName: "cash_selected"), for: .normal)
        })
        alertController.addAction(addCardButton)
        alertController.addAction(cancelButton)
        self.present(alertController, animated: true, completion: nil)
    }
    //MARK: UITextfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == hideTextField{
            pickUpDate(hideTextField)
        }
        
    }
    //MARK: UIDatePicker
    func preBookingAction(){
        hideTextField.becomeFirstResponder()
    }
    func pickUpDate(_ textField : UITextField){
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        if let selectedFormate = UserDefaults.standard.value(forKey: "TimeFormate")
        {
            if "\(selectedFormate)" == "12"
            {
                datePicker.locale = Locale(identifier: "en_US")
            }
            else
            {
                datePicker.locale = Locale(identifier: "en_GB")
            }
        }
        else
        {
            datePicker.locale = Locale(identifier: "en_GB")
        }
        var newDate = Calendar.current.date(byAdding: .hour, value: PRE_BOOKING_HOURS, to: Date())
        datePicker.minimumDate = Calendar.current.date(byAdding: .minute, value: PRE_BOOKING_HOURS, to: newDate!)
        datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 30, to: Date())
        hideTextField.inputView = self.datePicker
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = false
        toolBar.barTintColor = kLIGHT_OLIVE_COLOR
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(OrderPaymentViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(OrderPaymentViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    func doneClick() {
        if hideTextField.isFirstResponder{
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateForRequest = dateFormatter2.string(from: datePicker.date)
            
            let dateFormatter3 = DateFormatter()
            dateFormatter3.dateFormat = "dd/MM/yyyy HH:mm:ss"
            let dateToCompare = dateFormatter3.string(from: datePicker.date)
            
            hideTextField.resignFirstResponder()
            
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            let currentDate = dateFormatter.string(from: date)
            
            let selectedDat = dateFormatter.date(from: dateToCompare)
            let currentDat = dateFormatter.date(from: currentDate)
            if selectedDat?.compare(currentDat!) == .orderedAscending || selectedDat?.compare(currentDat!) == .orderedSame
            {
                let alertController: UIAlertController = UIAlertController(title: "Date & Time".localized, message: "Please select pick up date & time".localized, preferredStyle: .alert)
                let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                    self.hideTextField.becomeFirstResponder()
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            else{
                openPaymentScreenForOrder("pre-time")
            }
            
        }
    }
    func cancelClick()
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    func openPaymentScreenForOrder(_ bookType:String){
        orderDict["pick_up_date"] = dateForRequest
        orderDict["booking_type"] = bookType
        getCancellationFeesDetails()
    }
    
    func getCancellationFeesDetails()
    {
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "RiderId" : riderID,
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KRIDER_CANCELLATIONFEE, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_CANCELLATIONFEE, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    print("KRIDER_CANCELLATIONFEE :", responseContent ?? "")
                    
                    if(responseContent != nil)
                    {
                        let status = responseContent?.value(forKey: "Status") as? Bool
                        if(status == true)
                        {
                            let alertController: UIAlertController = UIAlertController(title: nil, message: (responseContent?.value(forKey: "Message") as? String)!, preferredStyle: .alert)
                            
                            let okAction: UIAlertAction = UIAlertAction(title: "Confirm".localized, style: .default) { action -> Void in
                                self.callOrderRequest()
                            }
                            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel".localized, style: .default) { action -> Void in
                            }
                            
                            alertController.addAction(okAction)
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else{
                            self.callOrderRequest()
                        }
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    //MARK: Webservice methods
    func getCardDetails()
    {
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "UserId" : riderID,
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KRIDER_CARD_DETAILS, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_CARD_DETAILS, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    print("getCard :", responseContent ?? "")
                    
                    if(responseContent != nil)
                    {
                        self.isCardAvailable = (responseContent?.value(forKey: "StripeCardId") as? String)!
                        if self.isCardAvailable == "fail"
                        {
                            self.card_type = "cash"
                            self.cardButton.setImage(#imageLiteral(resourceName: "card_unselected"), for: .normal)
                            self.cashButton.setImage(#imageLiteral(resourceName: "cash_selected"), for: .normal)
                        }
                        else{
                            self.card_type = "card"
                            self.cardButton.setImage(#imageLiteral(resourceName: "card_selected"), for: .normal)
                            self.cashButton.setImage(#imageLiteral(resourceName: "cash_unselected"), for: .normal)
                        }
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func callOrderRequest() {
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            orderDict["rider_id"] = riderID
            print("orderDict", orderDict)
            var bookingType = Int(orderDict["booking_type"] as! String == "on-time" ? 1 : 2)
            var rentalPId = orderDict["ride_type"] as! String == "hourly" ? orderDict["hour"] as? String : ""
            if orderDict["car_type"] as! String == "2"{
                rentalPId = "1"+rentalPId!
                rentalPId = rentalPId == "110" ? "20" : rentalPId!
            }
            if orderDict["car_type"] as! String == "3"{
                rentalPId = "2"+rentalPId!
                rentalPId = rentalPId == "210" ? "30" : rentalPId!
            }
            
            var parameters = [
                "RiderId" : orderDict["rider_id"] as! Int32,
                "CarId" : 1,
                "CarTypeId" : orderDict["car_type"] as! String,
                "PickupLocation" : orderDict["pick_up_location"] as! String,
                "PickupLatitude" : orderDict["pick_up_latitude"] as! String,
                "PickupLongitude" : orderDict["pick_up_longitude"] as! String,
                "DropLocation" : orderDict["drop_up_location"] as! String,
                "DropLatitude" : orderDict["drop_up_latitude"] as! String,
                "DropLongitude" : orderDict["drop_up_longitude"] as! String,
                "BookingDateTime" : orderDict["pick_up_date"] as! String,
                "BookingType" : bookingType,
                "RideType" : Int(orderDict["ride_type"] as! String == "hourly" ? 1 : 2),
                "RentalPlanId" : rentalPId,
                "PaymentType" : Int(self.card_type == "cash" ? 1 : 2),
                "BookingFrom" : 1,
                "RidePrice" : totalPriceLable.text!,
                "DriverNotes" : orderDict["driver_notes"] as! String
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            let headerDict = Request.getHeaderForRider(parameters: orderDict)
            printJsonStringLog(apiName: KFINAL_BOOKRIDE, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KFINAL_BOOKRIDE,jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }
                else
                {
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    guard let data = responseObject.value(forKey: "data") as? String else {
                        self.showSimpleAlertController(nil, message: "Something Went Wrong")
                        return
                    }
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    if bookingType == 2 {
                        
                        if responseContent != nil {
                            
                            if let message = responseContent?.value(forKey: "Message") as? String {
                                
                                self.showSimpleAlertControllerWithOkAction(nil, message: message, callback: {
                                    
                                    if let status = responseContent!["Status"] as? Bool {
                                        
                                        if status {
                                            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
                                            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                                            let swiftViewController = UINavigationController(rootViewController: homeVC!)
                                            self.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
                                        }else {
                                            self.isRideBooked = false
                                        }
                                    }
                                })
                            }
                        }
                    }else {
                        
                        if responseContent != nil
                        {
                            let status = "success"
                            if status == "success"
                            {
                                if bookingType == 2 {
                                    self.preBookingAction()
                                    
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
                                    self.isRideBooked = false
                                }
                                if let value = responseContent?.value(forKey: "RideId") as? NSNumber {
                                    self.rideID = value.stringValue
                                    self.orderRequestSuccessfull()
                                }
                                if let allowPreBooking = responseContent?.value(forKey: "AllowPreBooking") as? Bool, allowPreBooking {
                                    self.navigationItem.setHidesBackButton(true, animated: true)
                                    self.circularProgressView.isHidden = true
                                    self.searchDriverProgressView.isHidden = false
                                    self.preBookInfoView.alpha = 1.0
                                    self.title = "Booking".localized
                                    self.navigationItem.leftBarButtonItem = nil
                                    self.navigationItem.setHidesBackButton(true, animated: true)
                                } else {
                                    self.showSearchDriverProgress()
                                }
                            }
                            else
                            {
                                if let value = responseContent?.value(forKey: "pre_book") as? String {
                                    if value == "1" {
                                        let alertController: UIAlertController = UIAlertController(title: nil, message: (responseContent?.value(forKey: "Message") as? String)!, preferredStyle: .alert)
                                        
                                        let okAction: UIAlertAction = UIAlertAction(title: "Confirm".localized, style: .default) { action -> Void in
                                            self.preBookingAction()
                                            NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
                                            _ = self.navigationController?.popToRootViewController(animated: true)
                                        }
                                        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel".localized, style: .default) { action -> Void in
                                            NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
                                            _ = self.navigationController?.popToRootViewController(animated: true)
                                        }
                                        
                                        alertController.addAction(okAction)
                                        alertController.addAction(cancelAction)
                                        self.present(alertController, animated: true, completion: nil)
                                    }
                                    else if value == "2" {
                                        self.isCardAvailable = "fail"
                                        self.card_type = "cash"
                                        self.cardButton.setImage(#imageLiteral(resourceName: "card_unselected"), for: .normal)
                                        self.cashButton.setImage(#imageLiteral(resourceName: "cash_selected"), for: .normal)
                                        self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "Message") as? String)!)
                                    }
                                    else {
                                        self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "Message") as? String) ?? "")
                                    }
                                }
                                else {
                                    self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "Message") as? String) ?? "")
                                }
                                
                            }
                        }
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func showSearchDriverProgress() {
        
        let prebookOptionsVC = self.storyboard?.instantiateViewController(withIdentifier: "PrebookOptionsVC") as! PrebookOptionsVC
        prebookOptionsVC.prevVC = self
        prebookOptionsVC.isNeedProgress = true
        prebookOptionsVC.rideBookingID = self.rideID
//        let navController = UINavigationController(rootViewController:prebookOptionsVC)
//        navController.navigationItem.setHidesBackButton(true, animated: true)
//        self.navigationController?.present(navController, animated: true, completion: nil)
        UserDefaults.standard.set(self.rideID, forKey: RiderID_Notification)
        UserDefaults.standard.synchronize()
        self.navigationController?.pushViewController(prebookOptionsVC, animated: true)
    }
    
    func increaseProgressButtonTapped()-> Void {
        if currentCount != maxCount {
            currentCount += 1
            let newAngleValue = newAngle()
            circularProgressView.animate(Double(newAngleValue), duration: 1.0, completion: nil)
        }
        else {
            showPrebookingOptionsView(nil)
        }
    }
    
    func newAngle() -> Int {
        return Int(360 * (currentCount / maxCount))
    }
    
    func getCurrentDate()-> String{
        let date = Date()
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter2.locale = Locale(identifier: "en_GB")
        currentDateForRequest = dateFormatter2.string(from: date)
        return currentDateForRequest
    }
    func getTodayString() -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy H:mm:SS"
        
        let currentDateStr = formatter.string(from: Date())
        return currentDateStr
    }
    func orderRequestSuccessfull(){
        self.isRideBooked = true
        self.orderButton.backgroundColor = UIColor.lightGray
        self.orderButton.isEnabled = false
    }
    
    func checkOrderCurrentStatus(){
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "BookingId" : self.rideID,
                "RiderId" : riderID
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KCHECK_ORDER_STATUS, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KCHECK_ORDER_STATUS, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    print("Booking CurrentStatus :", responseContent ?? "")
                    
                    if let value = responseContent?.value(forKey: "Status") as? Bool{
                        if value == true{
                            self.checkOrderStatusDidFinish(responseContent?.value(forKey: "Message") as? String, withStatus: responseContent?.value(forKey: "rideStatus") as? String)
                        }
                        else{
                            if let isDeleted = responseContent?.value(forKey: "flag") as? Int{
                                if(isDeleted == 1){
                                    self.moveToSingupController(message: responseContent?.value(forKey: "Message") as? String ?? "")
                                }
                            }else{
                                self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                            }
                        }
                    }
                    else{
                        self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "message") as? String)!)
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    func checkOrderStatusDidFinish(_ message: String?, withStatus status:String?){
        if status == "pending"{
            let alertController: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else if status == "expire" {
            showPrebookingOptionsView(nil)
        }
        else if status == "accept"{
            let alertController: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                _ = self.navigationController?.popToRootViewController(animated: false)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
                
                let visibleVC = UIApplication.topViewController()!
                if (visibleVC is SlideMenuController) {
                    let slideMenuVC = (visibleVC as! SlideMenuController)
                    let nav = (slideMenuVC.mainViewController as! UINavigationController)
                    let topVC = nav.viewControllers.last
                    if topVC is HomeViewController{
                        (topVC as! HomeViewController).callWebserviceForGettingCurrentRide()
                    }
                }
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        else if status == "reject"{
            let alertController: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            self.showSimpleAlertController(nil, message: message!)
        }
    }
    // MARK: STPPaymentContextDelegate
    internal func createBackendCharge(with token: STPToken, completion: @escaping (STPBackendChargeResult, Error?) -> Void) {
        let error1 : NSError = NSError(domain: StripeDomain, code: 50, userInfo: [NSLocalizedDescriptionKey: "Stripe turned your credit card into a token: \(token.tokenId)"])
        
        if BackendChargeURLString == nil
        {
            completion(STPBackendChargeResult.failure, error1 as Error)
            return
        }
        completion(STPBackendChargeResult.success, nil)
        hideHUD()
    }
    
    func paymentViewController(_ controller: PaymentViewController, didFinish error: Error?) {
        
        self.dismiss(animated: true, completion: {
            if error != nil {
                self.presentError(error)
            }
            else
            {
                self.getCardDetails()
            }
        })
        
    }
    func presentError(_ error: Error?) {
        let controller = UIAlertController(title: nil, message: error!.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok".localized, style: .default, handler: nil)
        controller.addAction(action)
        self.present(controller, animated: true, completion: { _ in })
    }
    //MARK: - CardIO Methods
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        paymentViewController?.dismiss(animated: true, completion: nil)
        self.card_type = "cash"
        self.cardButton.setImage(#imageLiteral(resourceName: "card_unselected"), for: .normal)
        self.cashButton.setImage(#imageLiteral(resourceName: "cash_selected"), for: .normal)
    }
    
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        if let info = cardInfo {
            paymentViewController?.dismiss(animated: true, completion: nil)
            let card: STPCardParams = STPCardParams()
            card.number = info.cardNumber
            card.expMonth = info.expiryMonth
            card.expYear = info.expiryYear
            card.cvc = info.cvv
            card.name = info.cardholderName
            getStripeToken(card)
        }
    }
    
    func getStripeToken(_ card : STPCardParams) {
        self.showHUD()
        STPAPIClient.shared().createToken(withCard: card, completion: { (token, error) in
            if (token?.tokenId) != nil {
                self.hideHUD()
                self.postStripeToken(token: token!, card : card)
            } else {
                print(error.debugDescription)
                self.hideHUD()
            }
        })
    }
    func postStripeToken(token: STPToken, card : STPCardParams) {
        
        self.showHUD()
        
        if Stripe.defaultPublishableKey() == nil
        {
            self.cardIOVC.dismiss(animated: true, completion: nil)
            return
        }
        
        let Request: WebserviceHelper = WebserviceHelper()
        let riderID = Request.getUserDefaultsStandard()
        let number = card.number!
        let tempNo : NSMutableString = NSMutableString(string: number)
        let totalSpace = (number.characters.count / 4) - 1
        
        for i in 0 ..< totalSpace
        {
            if i == 0
            {
                tempNo.insert(" ", at: 4)
            }
            else if i == 1
            {
                tempNo.insert(" ", at: 9)
            }
            else if i == 2
            {
                tempNo.insert(" ", at: 14)
            }
            else if i == 3
            {
                tempNo.insert(" ", at: 19)
            }
            
        }
        let array : [String] = tempNo.components(separatedBy: .whitespaces)
        let lastDigits = array.last
        var cardType : String = " "
        for card in CardType.allCards {
            if (matchesRegex(card.regex, text: number)) {
                cardType = card.rawValue
                break
            }
        }
        var expirationDate = ""
        if card.expYear > 1000 {
            expirationDate = "\(card.expMonth)/\(card.expYear)"
        }
        else {
            expirationDate = "\(card.expMonth)/20\(card.expYear)"
        }
        
        var parameters = [
            "Name": card.name!,
            "Number": card.number,
            "ExpMonth": card.expMonth,
            "ExpYear": card.expYear,
            "Cvc": card.cvc,
            "UpdatedBy" : riderID,
            ] as [String : Any]
        
        if(checkConnection()){
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            let jsonString = returnJsonString(param: parameters)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KCARD_PAYMENT, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KCARD_PAYMENT, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseContent : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseContent.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseDict = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    print("Add stripeCard :", responseDict ?? "")
                    
                    self.showSimpleAlertController(nil, message: responseContent.value(forKey: "Message") as! String)
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    func rectForText(text: String, font: UIFont, maxSize: CGSize) -> CGSize {
        let attrString = NSAttributedString.init(string: text, attributes: [NSFontAttributeName:font])
        let rect = attrString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        let size = CGSize(width: rect.size.width, height: rect.size.height)
        return size
    }
    
}
extension PrebookOptionsVC {
    
    func didPrebookTapped() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func didCancelPrebookTapped() {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
        let storyboard = UIStoryboard(name: "Rider", bundle: nil)
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        let swiftViewController = UINavigationController(rootViewController: homeVC!)
        self.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
    }
}


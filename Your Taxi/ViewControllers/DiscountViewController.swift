import UIKit

class DiscountViewController: UIViewController {
    
    @IBOutlet weak var discountLable: UILabel!
    @IBOutlet weak var dicountTitleLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet var bottomFirstLbl: UILabel!
    var dicountInfoDic = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomFirstLbl.text = "Book now, get discount!".localized
        self.setPoweredByTitle()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        dicountTitleLbl.text=(dicountInfoDic.value(forKey: "aps") as AnyObject).value(forKey: "discount_label") as? String
        discountLable.text=NSString.init(format: "%@%@", ((dicountInfoDic.value(forKey: "aps") as AnyObject).value(forKey: "discount_percent") as? String)!,"%") as String
    }
    
    func dismissscreen() -> Void {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setPoweredByTitle() {
        let simpleAttributes = [NSForegroundColorAttributeName: kDARK_OLIVE_COLOR, NSFontAttributeName: UIFont.systemFont(ofSize: 17)]
        let italicAttributes = [NSForegroundColorAttributeName: kDARK_OLIVE_COLOR, NSFontAttributeName: UIFont.italicSystemFont(ofSize: 17)]
        let boldAttributes = [NSForegroundColorAttributeName: kDARK_OLIVE_COLOR, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 17)]
        let partOne = NSMutableAttributedString(string: "Powered by:".localized, attributes: simpleAttributes)
        let partTwo = NSMutableAttributedString(string: " YOUR", attributes: italicAttributes)
        let partThree = NSMutableAttributedString(string: "TAXI", attributes: boldAttributes)
        let combination = NSMutableAttributedString()
        combination.append(partOne)
        combination.append(partTwo)
        combination.append(partThree)
        messageLbl.attributedText = combination
    }
}


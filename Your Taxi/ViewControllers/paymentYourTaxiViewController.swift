
import UIKit
import MBProgressHUD

class paymentYourTaxiViewController: BaseViewController {
    
    @IBOutlet var viewForBorder: UIView!
    @IBOutlet var bankDetailLabel: UILabel!
    @IBOutlet var payAmountButton: UIButton!
    
    @IBOutlet var bankName: UILabel!
    @IBOutlet var bankAddress: UILabel!
    @IBOutlet var bankPostalCodeCity: UILabel!
    @IBOutlet var bankIBAN: UILabel!
    @IBOutlet var bankBICSWIFT: UILabel!
    
    var amountToPay : String = "0.0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bankDetailLabel.layer.borderColor = UIColor.white.cgColor
        bankDetailLabel.layer.borderWidth = 1
        viewForBorder.layer.borderColor = UIColor.white.cgColor
        viewForBorder.layer.borderWidth = 1
        self.title = "PAYMENT".localized
        bankDetailLabel.text = "Bank Details of Yourtaxi.".localized
        
        setUpPaymentInformation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        encryptData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func payAmountBtn(_ sender: Any) {
        let alertController: UIAlertController = UIAlertController(title: nil, message: "You need to pay this amount within 5 days. Otherwise you will be block. Did you pay already?".localized , preferredStyle: .alert)
        let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
            self.navigationController?.popViewController(animated: true)
        }
        let noAction: UIAlertAction = UIAlertAction(title: "No".localized, style: .default) { action -> Void in}
        
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setUpPaymentInformation() {
        
        bankName.text = getBankInformation(key: "Name")
        bankAddress.text = getBankInformation(key: "Address")
        bankPostalCodeCity.text = getBankInformation(key: "PostalCode") + " " + getBankInformation(key: "City")
        bankIBAN.text = "IBAN: " + getBankInformation(key: "IbanAccNo")
        bankBICSWIFT.text = "BIC/SWIFT: " + getBankInformation(key: "BicSwift")
        
        let payAmount = "Pay the amount CHF".localized + " \(amountToPay)"
        self.payAmountButton.setTitle(payAmount, for: .normal)
    }
    
    func getBankInformation(key : String) -> String {
        
        let bankDict = getCurrentLoginUserBankDict()
        print(bankDict)
        
        if let value = bankDict[key] as? String {
            return value
        }
        
        if let value = bankDict[key] as? NSInteger {
            return "\(value)"
        }
        
        return ""
    }
    
    func encryptData() {
        let Request: WebserviceHelper = WebserviceHelper()
        if(checkConnection()){
            let driverID = Request.getUserDefaultsStandard()
            var parameters = [
                "driver_id":driverID,
                "user_type":"driver",
                "platform": kPlatForm,
                "environment":kEnviroment
                ] as [String : Any]
            
            self.showHUD()
            
            let finalStr = getEncryptionKey(parameters: parameters, UUID: DEVICE_UUID)
            print("Encryption256:",finalStr)
            
            let jsonString = returnJsonString(param: parameters)
            print("Json String :",jsonString)
            
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            print("encryptedData :",encryptedData ?? "")
            
            encryptedData = encryptedData?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            print("Per:",encryptedData ?? "");
            
            let Request: WebserviceHelper = WebserviceHelper()
            let headerDict = Request.getHeader(parameters: parameters)
            
            parameters = ["data":encryptedData!]
            getPayAmount(header: headerDict, parameters: parameters as NSDictionary)
        } else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func getPayAmount(header:[String:String],parameters:NSDictionary) {
        
        let Request: WebserviceHelper = WebserviceHelper()
        printParameterLog(apiName: DRIVER_PAY_AMOUNT, parameter: parameters as! [String : Any], headerDict: header)
        
        Request.HTTPPostJSONWithHeader(url: DRIVER_PAY_AMOUNT, paramters: parameters, withIdicator: false, header: header) { (result, error) in
            
            self.hideHUD()
            if(error != nil){
                print(error?.localized ?? "")
                self.showSimpleAlertController("Alert", message: error?.localized ?? "")
                return
            }
            
            let responseObject = Request.convertStringToDictionary(text: result)
            
            let decryptedData = AESCrypt.decrypt(responseObject?.value(forKey: "data") as! String, password: SECRET_KEY)
            print("decryptedData :",decryptedData ?? "")
            
            let objDict = Request.convertStringToDictionary(text: decryptedData! as String)
            
            if let response = objDict!["response"] as? [String : Any] {
                let status = response["status"] as! NSString
                if status == "success"{
                    var payAmount = ""
                    if let amount = response["total_amount_to_pay"] as? String {
                        payAmount = "Pay the amount CHF".localized + " \(amount)"
                    }
                    self.payAmountButton.setTitle(payAmount, for: .normal)
                    
                } else {
                    let title = status as String
                    let msg = response["message"] as! String
                    self.showSimpleAlertController(title.localized, message: msg.localized )
                }
            }
        }
    }
}


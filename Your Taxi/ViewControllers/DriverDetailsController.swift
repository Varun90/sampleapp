import UIKit
import MapKit
import GoogleMaps


class DriverDetailsController: BaseViewController, MKMapViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var rideDetailsBtnLeading : NSLayoutConstraint!
    @IBOutlet weak var rideDetailsBtnwidth : NSLayoutConstraint!

    @IBOutlet weak var shadowView : UIView!
    @IBOutlet weak var driverImage : UIImageView!
    @IBOutlet weak var driverName : UILabel!
    @IBOutlet weak var plateNO : UILabel!

    @IBOutlet weak var routeMapView : GMSMapView!
    @IBOutlet weak var callDriver : UIButton!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var addTipButton: UIButton!
    @IBOutlet var rideDetailsBtn: UIButton!
    @IBOutlet var cancelRideBtn: UIButton!
    
    let movement = ARCarMovement()
    
    var isRouteInitiated : Bool = false
    var polyline = GMSPolyline()
    var currentRideData : NSMutableDictionary?
    var driver_id : Int32!
    var pickUpLocation : String!
    var annotationView : MKAnnotationView!
    var customAnn : CustomAnnotation!
    var annotationImage : String = "budget"
    var phoneNumber : String? = " "
    var phoneNumberDialcode : String? = " "
    var timerObject: Timer!
    var zoomInLocation : Bool = false
    let mapManager = MapManager()
    var pickUpCoordinate = CLLocationCoordinate2D()
    var reach_time = ""
    var car_typename: String? = ""
    var driverMarker = GMSMarker()
    var riderMarker = GMSMarker()
    var currentRideStatus = 1
    var directionList:NSMutableArray!
    var rotation = 0.0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        movement.delegate = self
        
        routeMapView.delegate = self
        routeMapView.settings.rotateGestures = false
        
        self.slideMenuController()?.removeLeftGestures()
        rideDetailsBtn.setTitle("Trip details".localized, for: .normal)
        cancelRideBtn.setTitle("Cancel Ride".localized, for: .normal)
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(DriverDetailsController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        let car_details = currentRideData?.value(forKey: "CarDetails") as! NSDictionary
        let driver_details = currentRideData?.value(forKey: "DriverDetails") as! NSDictionary
        driver_id = driver_details.value(forKey: "DriverId") as! Int32
        getDriverDetails(deiverDetails: driver_details)
        let ride_details = currentRideData?.value(forKey: "RideDetails") as! NSDictionary
        let car_type : Int32  = ride_details.value(forKey: "CarTypeId") as! Int32
        self.car_typename = car_details.value(forKey: "CarType") as! String
        if car_type == 1
        {
            annotationImage = "budget"
        }
        else if car_type == 2
        {
            annotationImage = "business"
        }
        else if car_type == 3
        {
            annotationImage = "comfort"
        }
        
        setUpMarkersWithRoute()
    }
    
    func setUpMarkersWithRoute() {
        
        guard let ride_details = currentRideData?.value(forKey: "RideDetails") as? NSDictionary else {
            return
        }
        guard let rideStatus = ride_details["RideStatus"] as? NSInteger else {
            return
        }
        guard let driverCurrentStatus = currentRideData?.value(forKey: "DriverStatus") as? NSInteger else {
            return
        }
        print(driverCurrentStatus)
        print(currentRideData)
        currentRideStatus = rideStatus
        let latitude = Double(ride_details.value(forKey: driverCurrentStatus < 2 ? "PickupLatitude" : "DropLatitude") as! String)
        let longitude = Double(ride_details.value(forKey: driverCurrentStatus < 2 ? "PickupLongitude": "DropLongitude") as! String)
        pickUpCoordinate = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
        riderMarker.position = pickUpCoordinate
        if driverCurrentStatus < 2 {
            riderMarker.snippet = driverCurrentStatus < 2 ? "Pickup Location" : "Drop Location"
        } else {
            if let dropLocation = ride_details["DropLocation"] as? String {
                riderMarker.snippet = dropLocation
            }
        }
        
        riderMarker.icon = UIImage(named:  driverCurrentStatus < 2 ? "pickupPin" : "dropupPin")
        riderMarker.map = routeMapView
        setUpBottomOptions()
        self.animateViewToCurrentLocation(coordinate: self.pickUpCoordinate)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "DRIVER INFO".localized
        callDriver.setTitle("Call Driver".localized, for: .normal)
        if timerObject == nil{
            self.getDriverLocation()
            timerObject = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(DriverDetailsController.getDriverLocation), userInfo: nil, repeats: true)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if timerObject != nil{
            timerObject!.invalidate()
            timerObject = nil
        }
    }
    func showSearchDriverProgress(rideID: String) {
        
        let prebookOptionsVC = self.storyboard?.instantiateViewController(withIdentifier: "PrebookOptionsVC") as! PrebookOptionsVC
        prebookOptionsVC.prevVC = self
        prebookOptionsVC.isNeedProgress = true
        print("self.rideID", rideID)
        prebookOptionsVC.rideBookingID = rideID
        self.navigationController?.pushViewController(prebookOptionsVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Set Screen Design Methods
    func setUpBottomOptions() {
        
        let driverCurrentStatus = currentRideData?.value(forKey: "DriverStatus") as? Int ?? 0

        //Hide CancelRide
        self.rideDetailsBtnwidth.constant = (self.view.frame.size.width - 24) / 2
        self.cancelRideBtn.isHidden = driverCurrentStatus >= 2 ? true : false
        self.rideDetailsBtnLeading.constant = driverCurrentStatus >= 2 ? (self.view.frame.size.width / 2) - (self.rideDetailsBtnwidth.constant / 2) : 8
    }
    
    func drawRoute(location: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D){
        let origin = "\(location.latitude),\(location.longitude)"
        let destination = "\(destinationLocation.latitude),\(destinationLocation.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(GOOGLE_WEB_API_KEY)"
        URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: { (data, response, error) -> Void in
            if((error) != nil) {
                return
            }
            do {
                let responseObject = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                let routes = responseObject!["routes"] as? [[String:AnyObject]]
                for route in routes!
                {
                    let routeOverviewPolyline = route["overview_polyline"] as? NSDictionary
                    let points = "\(routeOverviewPolyline?["points"] as AnyObject)"
                    print()
                    let path = GMSPath.init(fromEncodedPath: points)
                    self.polyline.path = path//GMSPolyline.init(path: path)
                    self.polyline.map = self.routeMapView
                    self.polyline.strokeWidth = 8.0
                    self.polyline.strokeColor = UIColor(red: 0.0/255.0, green: 179.0/255.0, blue: 253.0/255.0, alpha: 1.0)
                    
                    // Get distance
                    let legs = route["legs"] as! NSArray
                    let leg = legs[0] as! NSDictionary
                    
                    if let steps = leg["steps"] as? NSArray {
                        self.directionList = steps.mutableCopy() as! NSMutableArray
                    }
                    print(self.directionList)
                    
                    //Zoom into the source and desstination location
                    DispatchQueue.main.async {
                        //Setup Bounds
                        var bounds = GMSCoordinateBounds.init(coordinate: location, coordinate: destinationLocation)
                        bounds = bounds.includingPath(path!)
                        let cameraUpdate = GMSCameraUpdate.fit(bounds, with: UIEdgeInsetsMake(80.0, 30.0, 30.0, 30.0))
                        self.routeMapView.animate(with: cameraUpdate)
                        
                        //Update the pick up and Drop pin based on end point
                        self.setupMarkersAfterRoute(route: route)
                    }
                }
            } catch let error as NSError {
                print(error)
            }
        }).resume()
    }
    
    func setupMarkersAfterRoute(route : [String:AnyObject]) {
        var directionList : NSMutableArray = []
        // Get distance
        let legs = route["legs"] as! NSArray
        let leg = legs[0] as! NSDictionary

        guard let steps = leg["steps"] as? NSArray else {
            return
        }

        directionList = steps.mutableCopy() as! NSMutableArray
        print(directionList)

        if directionList.count > 0 {
            let startStep = directionList.firstObject as! NSDictionary
            let endStep = directionList.lastObject as! NSDictionary

            if let sourceDict = startStep["start_location"] as? NSDictionary, let lat = sourceDict["lat"] as? Double, let lng = sourceDict["lng"] as? Double  {
                let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                setUpDriverMarkerWithNewLocation(location: location)
            }

            if let destinationDict = endStep["end_location"] as? NSDictionary, let lat = destinationDict["lat"] as? Double, let lng = destinationDict["lng"] as? Double  {
                let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                riderMarker.position = location
            }
        }
    }

    func setUpDriverMarkerWithNewLocation(location : CLLocationCoordinate2D) {
        
        movement.arCarMovement(marker: driverMarker, oldCoordinate: driverMarker.position, newCoordinate: location, mapView: routeMapView, bearing: self.rotation, movingDuration: 0.5)
        
//        let tempMarker = driverMarker
//        driverMarker.map = nil
//
//        DispatchQueue.main.async {
//            self.driverMarker = GMSMarker()
//            self.driverMarker.position = tempMarker.position
//            self.driverMarker.map = self.routeMapView
//
//            let markerImageView = UIImageView(image: UIImage(named: self.annotationImage))
//            self.driverMarker.iconView = markerImageView
//            self.driverMarker.iconView?.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * (self.rotation) / 180.0))
//
//            CATransaction.begin()
//            CATransaction.setAnimationDuration(2.0)
//            self.driverMarker.position = location
//            CATransaction.commit()
//        }
    }
    
    func animateViewToCurrentLocation(coordinate : CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: 12.0)
        routeMapView.camera = camera
        routeMapView.isMyLocationEnabled = false
        routeMapView.settings.myLocationButton = true
        routeMapView.animate(to: camera)
    }
    
    func setScreenLayout() {
        callDriver.backgroundColor = kBUTTON_DARK_GREEN_COLOR
        callDriver.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        callDriver.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        addTipButton.backgroundColor = kBUTTON_DARK_GREEN_COLOR
        addTipButton.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        addTipButton.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        rideDetailsBtn.setTitle("Trip details".localized, for: .normal)
        rideDetailsBtn.backgroundColor = kBUTTON_DARK_GREEN_COLOR
        rideDetailsBtn.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        rideDetailsBtn.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        cancelRideBtn.setTitle("Cancel Ride".localized, for: .normal)
        cancelRideBtn.backgroundColor = kBUTTON_DARK_GREEN_COLOR
        cancelRideBtn.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        cancelRideBtn.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        let lan = Locale.preferredLanguages[0]
        if(lan != "en-IN"){
            callDriver.imageEdgeInsets.left = 0.0;
        }
    }
    // MARK: - Actions Methods
    func backAction(){
        if timerObject != nil{
            timerObject!.invalidate()
            timerObject = nil
        }
        _ = self.navigationController?.popViewController(animated: true)
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        let swiftViewController = UINavigationController(rootViewController: homeVC!)
        self.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
    }
    func backToHomeAction(){
        _ = self.navigationController?.popViewController(animated: true)
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        let swiftViewController = UINavigationController(rootViewController: homeVC!)
        self.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
    }
    
    @IBAction func rideDetailsButtonAction(_ sender: Any) {
        let currentRideVC = self.storyboard?.instantiateViewController(withIdentifier: "CurrentRidesViewController") as? CurrentRidesViewController
        let ridedetails = self.currentRideData?.value(forKey: "RideDetails") as! NSDictionary
        currentRideVC!.currentRideData = ridedetails.mutableCopy() as? NSMutableDictionary
        currentRideVC!.Car_TypeName = self.car_typename
        self.navigationController?.pushViewController(currentRideVC!, animated: true)
    }
    @IBAction func cancelRideButtonAction(_ sender: Any) {
        let alertController: UIAlertController = UIAlertController(title: nil, message: "CancelInstantRideMsgKeyRider".localized , preferredStyle: .alert)
        let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
            self.cancelRideService()
        }
        let noAction: UIAlertAction = UIAlertAction(title: "No".localized, style: .default) { action -> Void in}
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
       
        self.present(alertController, animated: true, completion: nil)
    }
    @IBAction func callToDriverBtnClicked(_ sender: UIButton) {
        if phoneNumber != nil
        {
            let telString = "tel://\(phoneNumberDialcode! + phoneNumber!)"
            if UIApplication.shared.canOpenURL(URL(string: telString)!){
                UIApplication.shared.openURL(URL(string: telString)!)
            }
        }
    }
    @IBAction func btnAddTip(_ sender: UIButton) {

        let vc = storyboard?.instantiateViewController(withIdentifier: "AddTipViewController") as! AddTipViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.currentRideData = currentRideData
        self.present(vc, animated: true, completion: nil)
    }
    
    func didCancelHomeTapped() {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
        let storyboard = UIStoryboard(name: "Rider", bundle: nil)
        let homeVC = storyboard.instantiateViewController(withIdentifier: "DriverDetailsController") as? DriverDetailsController
        let swiftViewController = UINavigationController(rootViewController: homeVC!)
        self.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
    }
    
    //MARK: - Get Details
    func getDriverDetails(deiverDetails : NSDictionary)
    {
        if deiverDetails != nil{
            //let name = "\(deiverDetails.value(forKey: "FirstName") as! String) \(deiverDetails.value(forKey: "LastName")as! String)"
            self.driverName.text = deiverDetails.value(forKey: "DisplayName") as? String ?? ""
            if let profile_pic_url = deiverDetails.value(forKey: "DriverProfilePic") as? String {
                downloadImage(URL(string: profile_pic_url)!, imageBtn: nil, imageView: self.driverImage, indicator : nil)
            }
            let driverCar_details = currentRideData?.value(forKey: "CarDetails") as! NSDictionary
            self.plateNO.text = "\(driverCar_details.value(forKey: "Brand") as! String)\n\(driverCar_details.value(forKey: "PlateNumber")as! String)"
            print("deiverDetails :", deiverDetails)
            print("deiver :", driverCar_details)
            self.phoneNumberDialcode = deiverDetails.value(forKey: "DialCode") as? String
            print("code :",phoneNumberDialcode)
            self.phoneNumber = deiverDetails.value(forKey: "PhoneNo") as? String
            
             if let value = deiverDetails.value(forKey: "Rating") as? String  {
                let attachment = NSTextAttachment()
                attachment.image = #imageLiteral(resourceName: "rider_star")
                let attachmentString = NSAttributedString(attachment: attachment)
                let myString = NSMutableAttributedString(string: "(\(value) ")
                let breaketStr = NSAttributedString(string: " )")
                myString.append(attachmentString)
                myString.append(breaketStr)
                self.ratingLabel.attributedText = myString
            }else{
                let value = String(format: "%.1f", (deiverDetails.value(forKey: "Rating") as? Double) ?? 0.0)
                if  value != nil || value != "0.0" {
                    let attachment = NSTextAttachment()
                    attachment.image = #imageLiteral(resourceName: "rider_star")
                    let attachmentString = NSAttributedString(attachment: attachment)
                    let myString = NSMutableAttributedString(string: "(\(value) ")
                    let breaketStr = NSAttributedString(string: " )")
                    myString.append(attachmentString)
                    myString.append(breaketStr)
                    self.ratingLabel.attributedText = myString
                }
            }
        }
        else{
            self.showSimpleAlertController(nil, message: "Can't display driver details")
        }
    }
    
    func getDriverLocation()
    {
        if !isRouteInitiated {
            self.showHUD()
        }
        let Request: WebserviceHelper = WebserviceHelper()
        let riderID = Request.getUserDefaultsStandard()
        let ride_details = currentRideData?.value(forKey: "RideDetails") as! NSDictionary
        var parameters = [
            "BookingId" : ride_details.value(forKey: "BookingId") as! Int32,
            "RiderId" : riderID
            ] as [String : Any]
        let jsonString = returnJsonString(param: parameters)
        var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
        parameters = ["data":encryptedData!]
        let jsonParam = returnJsonString(param: parameters)
        let headerDict = Request.getHeaderForRider(parameters: parameters)
        printJsonStringLog(apiName: KDRIVER_LOCATION_CONTINUOUS, parameter: jsonParam as NSString, headerDict: headerDict)
        Request.CustomHTTPPostJSONWithHeader(url: KDRIVER_LOCATION_CONTINUOUS, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
            self.hideHUD()
            self.isRouteInitiated = true
            if ((error) != nil){
                print(error.debugDescription)
            }else{
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                if responseContent != nil{
                    if((responseContent?.value(forKey: "Status") as! Bool) == true)
                    {
                        self.showDriverAnnotationOnMap(result: responseContent!)
                    }
                    
                }else{
                   // self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                }
                
            }
        }
    }
    
    //MARK: - Annotation and Mapview
    func showDriverAnnotationOnMap(result : NSDictionary) {
        
        let driver_lat = result.value(forKey: "latitude") as! String
        let driver_long = result.value(forKey: "longitude") as! String
        let driverCoordinate = CLLocationCoordinate2D(latitude: (driver_lat as NSString).doubleValue, longitude: (driver_long as NSString).doubleValue)
        
        if let value = result.value(forKey: "reach_time") as? String {
            reach_time = value
        }
        else {
            reach_time = ""
        }
        
        DispatchQueue.main.async {
            if(self.currentRideStatus <= 2) {
                self.riderMarker.snippet = self.reach_time
            }
            self.routeMapView.selectedMarker = self.riderMarker
            
            print("carDict: ", result)
            
            if let rotateAngle = result["Rotation"] as? String {
                self.rotation = Double(rotateAngle) ?? 0.0
            }
            
            if let rotateAngle = result["Rotation"] as? NSInteger {
                self.rotation = Double(rotateAngle)
            }
            
            print("MPI \(.pi * (self.rotation) / 180.0)")
            print("MPI non Rotation \(self.rotation)")
            
            self.rotation = self.rotation < 0.0 ? 0.0 : self.rotation
            
            //Setup Driver Coordinate
            if self.driverMarker.map != nil {

//                self.driverMarker.position = driverCoordinate
//
//                //                self.driverMarker.rotation = CLLocationDegrees(rotation)
//                //                self.driverMarker.icon = UIImage(named:  self.annotationImage)
//
//                let markerImageView = UIImageView(image: UIImage(named: self.annotationImage))
//                self.driverMarker.iconView = markerImageView
//                self.driverMarker.iconView?.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * (rotation) / 180.0))
                self.drawRoute(location: driverCoordinate, destinationLocation: self.pickUpCoordinate)

//                UIView.animate(withDuration: 10.0, animations: {
//                    self.driverMarker.position = driverCoordinate
//                }, completion: { (finish) in
//                    self.drawRoute(location: driverCoordinate, destinationLocation: self.pickUpCoordinate)
//                })
                
            }
            
            else {
                self.driverMarker.position = driverCoordinate
                
                //                self.driverMarker.rotation = CLLocationDegrees(rotation)
                //                self.driverMarker.icon = UIImage(named:  self.annotationImage)
                
                let markerImageView = UIImageView(image: UIImage(named: self.annotationImage))
                self.driverMarker.iconView = markerImageView
                self.driverMarker.iconView?.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * (self.rotation) / 180.0))
                self.driverMarker.map = self.routeMapView
                self.drawRoute(location: driverCoordinate, destinationLocation: self.pickUpCoordinate)
            }
        }
    }
    
    func angle(fromCoordinate first: CLLocationCoordinate2D, toCoordinate second: CLLocationCoordinate2D) -> Float {
        
        let deltaLongitude: Float = Float(second.longitude - first.longitude)
        let deltaLatitude: Float = Float(second.latitude - first.latitude)
        let angle: Float = (.pi * 0.5) - atan(deltaLatitude / deltaLongitude)
        if deltaLongitude > 0 {
            return angle
        }
        else if deltaLongitude < 0 {
            return angle + .pi
        }
        else if deltaLatitude < 0 {
            return .pi
        }
        
        return 0.0
    }
   
    //MARK: - Webservice delegate methods
    func cancelRideService(){
        if checkConnection(){
            let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
            let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
            let userDict = self.convertStringToDictionary(text: decryt!)!
            
            let riderID  = userDict.value(forKey: "id") as! Int32
            let ride_Detail = currentRideData?.value(forKey: "RideDetails") as! NSDictionary
            var parameters:[String : Any] = [
                "UserId": riderID,
                "BookingId" : ride_Detail.value(forKey: "BookingId") as! Int32,
                ]
            let jsonString = returnJsonString(param: parameters)
            
            self.showHUD()
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let Request: WebserviceHelper = WebserviceHelper()
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KCANCELRIDE_URL, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KCANCELRIDE_URL, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                if let status = responseContent?.value(forKey: "Status") as? Bool{
                    if status == true{
                        let alertController: UIAlertController = UIAlertController(title: "Cancel Ride".localized as String, message: responseContent?.value(forKey: "Message") as? String, preferredStyle: .alert)
                        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                            let swiftViewController = UINavigationController(rootViewController: homeVC!)
                            self.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else{
                        
                        self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                    }
                }
                
            }
        }
        else{
            showAlertForInternetConnectionOff()
        }
    }
}

extension String {
    func index(of target: String) -> Int? {
        if let range = self.range(of: target) {
            return characters.distance(from: startIndex, to: range.lowerBound)
        } else {
            return nil
        }
    }
    
    func lastIndex(of target: String) -> Int? {
        if let range = self.range(of: target, options: .backwards) {
            return characters.distance(from: startIndex, to: range.lowerBound)
        } else {
            return nil
        }
    }
}

//MARK:- EXTENSION GMSMAPVIEW DELEGATE
extension DriverDetailsController : GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("Target center position:\(mapView.camera.target)")
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("Target center position:\(position.target)")
    }
}

extension Array {
    func randomItem() -> Element? {
        if isEmpty { return nil }
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}

extension DriverDetailsController : ARCarMovementDelegate {

    func arCarMovementMoved(_ marker: GMSMarker) {
        print("Delegate")
    }
}

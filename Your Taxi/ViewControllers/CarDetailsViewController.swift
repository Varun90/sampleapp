

import UIKit

class CarDetailsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView : UITableView!
    
    var carDetailsArray = [ActiveCar]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(CarDetailsViewController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "CAR LIST".localized
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        self.title = "CAR LIST".localized
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailsCell", for: indexPath) as! CarDetailsCell
        
        let car : ActiveCar = carDetailsArray[indexPath.row]
        cell.carImage.image = UIImage(named: "Carlist_img")
        cell.carName.text = car.brand
        cell.carType.text = car.carTypeName
        cell.plateNo.text = getPlateNumber(plateNumber: car.plateNumber)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        self.activeCarApi(indexPath: indexPath)
    }
    
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func activeCarApi(indexPath:IndexPath) {
        
        if(checkConnection()) {
            
            let car : ActiveCar = carDetailsArray[indexPath.row]
            
            let parameters = [
                "UserId": getLoginDriverID(),
                "CarId": String(describing:car.carID),
                ] as [String : Any]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KDRIVER_ACTIVE_CAR_UPDATE, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    //  : Handle Driver Logout
                    let responseContent = getResponseDictionaryWithResult(result: result)
                    print(responseContent)
                    if let status = responseContent["Status"] as? Bool {
                        if status == true {
                            self.showSimpleAlertControllerWithOkAction(nil, message: responseContent.value(forKey: "Message") as! String, callback: {[weak self] in
                                self?.navigationController?.popViewController(animated: true)
                            })
                        }else {
                            self.showSimpleAlertController(nil, message: responseContent.value(forKey: "Message") as! String)
                        }
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
}


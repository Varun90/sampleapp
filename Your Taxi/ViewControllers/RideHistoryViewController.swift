
import UIKit

class RideHistoryListVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    var preBookingObject = [PreBookingListObject]()
    @IBOutlet var tblPreBookingLIst: UITableView!
    var resultArray: NSMutableArray = []
    var StatusInt:[Int] = [2, 3, 4, 5, 6]
    var customPreBookingListDict : NSDictionary! = NSDictionary()
    //MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        tblPreBookingLIst.delegate = self
        tblPreBookingLIst.dataSource = self
        tblPreBookingLIst.rowHeight = UITableViewAutomaticDimension
        tblPreBookingLIst.tableFooterView = UIView()
        self.title = "Ride History".localized
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.preBookingListApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- API CALLING
    func preBookingListApi(){
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let USUserID = Request.getUserDefaultsStandard()
            var parameters = [String : Any]()
            parameters = [
                "RiderId" : USUserID,
                "Status" : self.StatusInt
            ]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: PREBOOKING_LIST, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: PREBOOKING_LIST, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    print(error?.localized ?? "")
                    self.showSimpleAlertController(nil, message: error?.localized ?? "")
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                guard let data = responseObject.value(forKey: "data") as? String else {
                    self.showSimpleAlertController(nil, message: "Something Went Wrong".localized)
                    self.hideHUD()
                    return
                }
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                print("rideHistory :", decryptedData ?? "")
                if let list = self.convertToDictionary(text: decryptedData!) as? [[String:Any]] {
                    self.resultArray = NSMutableArray()
                    for data in list {
                        self.resultArray.add(data as [String:Any])
                    }
                    if self.resultArray.count > 0{
                        self.preBookingObject.removeAll()
                        for dict in self.resultArray
                        {
                            self.preBookingObject.append(PreBookingListObject.init(dict: dict as! NSDictionary))
                        }
                    }
                    self.tblPreBookingLIst.reloadData()
                    
                    if(self.preBookingObject.count == 0){
                        self.noDataLabelView(message: self.resultArray.value(forKey: "Message") as? String ?? "No Ridehistory".localized)
                    }
                }
            }
        }else{
            self.hideHUD()
            self.showAlertForInternetConnectionOff()
        }
    }
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
    
    //MARK:- TABLEVIEW DATASOURCE METHODS
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "PreBookingListCell") as? PreBookingListCell
        
        if(cell == nil){
            cell = UITableViewCell(style: .default, reuseIdentifier: "PreBookingListCell") as? PreBookingListCell
        }
        cell?.loadCell(arrObject: preBookingObject[indexPath.row], Isprebook: false)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return preBookingObject.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let selectedObject = preBookingObject[indexPath.row] as PreBookingListObject
        return selectedObject.cancelAmount != 0.0 ? 182 : 162
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let selectedObject = preBookingObject[indexPath.row] as PreBookingListObject
        return (selectedObject.rideStatus >= 4) ? true : false
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        print("Deleted")
        let selectedObject = preBookingObject[indexPath.row] as PreBookingListObject
        let bookingID = selectedObject.bookingId
        self.showAlertController(bookingID!, indexPath: indexPath)
    }
    //MARK:- ALERT CONTROLLER
    func showAlertController(_ bookingId : Int64, indexPath: IndexPath) {
        let alertController: UIAlertController = UIAlertController(title: nil, message: "Are you sure you want to delete trip history?".localized, preferredStyle: .alert)
        
        let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
            self.deleteBookingHistoryApi(bookingId: bookingId, indexPath: indexPath)
        }
        
        let noAction : UIAlertAction = UIAlertAction(title: "No".localized, style: .default, handler: nil)
        
       
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func deleteBookingHistoryApi(bookingId:Int64, indexPath: IndexPath){
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [String : Any]()
            parameters = [
                "UserId" : riderID,
                "BookingId" : bookingId,
            ]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: DELETE_LOCATION, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: DELETE_BOOKING_HISTORY, jsonString: jsonParam as NSString, withIdicator: false, header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    print("DeleteLocation: \(String(describing: responseContent))")
                    
                    if let value = responseContent?.value(forKey: "Status") as? Bool{
                        if value == true{
                            self.showSimpleAlertControllerWithOkAction(nil, message: responseContent!.value(forKey: "Message") as? String ?? "", callback: {
                                self.preBookingObject.remove(at: indexPath.row)
                                self.tblPreBookingLIst.deleteRows(at: [indexPath], with: .automatic)
                            })
                        }else{
                            self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                        }
                        
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    //MARK:- CUSTOM METHODS
    func noDataLabelView(message:String){
        
        let noDataLabel = UILabel()
        noDataLabel.translatesAutoresizingMaskIntoConstraints = false
        noDataLabel.text = message
        noDataLabel.sizeToFit()
        self.view.addSubview(noDataLabel)
        
        self.view.addConstraint(NSLayoutConstraint(item: noDataLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0))
        self.view.addConstraint(NSLayoutConstraint(item: noDataLabel, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0.0))
    }
}




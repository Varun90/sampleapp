import UIKit
import KDCircularProgress
import SlideMenuControllerSwift

class PrebookOptionsVC: BaseViewController {
    
    @IBOutlet weak var circularProgressView: KDCircularProgress!
    @IBOutlet weak var timerLable: UILabel!
    @IBOutlet var preBookInfoView: UIView!
    @IBOutlet var lblPrebookMessage: UILabel!
    @IBOutlet var lblHangMessage: UILabel!
    @IBOutlet var imgCirclePoint: UIImageView!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var PreBookingBtn: UIButton!
    var prevVC = UIViewController()
    var isNeedProgress = false
    var rideBookingID = "0"

    var userInfo = [String:Any]()
    var timer = Timer()
    var rideStatusTimer = Timer()

    var currentCount = 0.0
    var maxCount = 80.0//Progress sce
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    
    override func viewDidLoad() {
        PreBookingBtn.setTitle("Pre-Booking".localized, for: .normal)
        cancelBtn.setTitle("Back".localized, for: .normal)
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.title = "Booking".localized
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.showPrebookingOptionsView(_:)), name:NSNotification.Name(rawValue: "RideRejectedNotification"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RideRejectedNotification"), object: nil)
        if(KAPP_RIDER){
            locationManager1.startUpdatingLocation()
            stopTimers()
        }
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//
//        if isNeedProgress {
//            //Call API to get ride status
//            getRidePoolingAPI()//Initial
//            rideStatusTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.getRidePoolingAPI), userInfo: nil, repeats: true)
//
//        } else {
//            showPrebookingOptionsView(nil)
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        circularProgressView.isHidden = !isNeedProgress
        lblHangMessage.isHidden = !isNeedProgress
        imgCirclePoint.isHidden = !isNeedProgress
        if isNeedProgress {
            circularProgressView.angle = 0
            backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
                UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
            })
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.increaseProgressButtonTapped), userInfo: nil, repeats: true)
        } else {
            showPrebookingOptionsView(nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getRidePoolingAPI() {
        
        if checkConnection() {
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()

            let parameters = [
                "BookingId":Int(rideBookingID)!,
                "UserId":riderID
                ] as [String : Any]
            
            if circularProgressView.isHidden {
                self.hideHUD()
                self.showHUD()
            }
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            printParameterLog(apiName: CURRENT_RIDE_POOLING, parameter: parameters, headerDict: Request.getHeaderForRider(parameters: parameters))
            
            Request.CustomHTTPPostJSONWithHeader(url: CURRENT_RIDE_POOLING, jsonString: requestJsonString as NSString, withIdicator: false, header: Request.getHeaderForRider(parameters: parameters)) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    print(error?.localized ?? "")
                    self.showSimpleAlertController("Alert", message: error?.localized ?? "")
                    return
                }

                let responseContent = getResponseDictionaryWithResult(result: result)
                print(responseContent)
                //Handle Response
                self.handleRidePoolingResponse(responseContent: responseContent)
            }
        } else {
            showAlertForInternetConnectionOff()
        }
    }
    
    func handleRidePoolingResponse(responseContent:NSDictionary) {
        
        guard let totalDrivers = responseContent.value(forKey: "TotalDrivers") as? NSInteger, let rideStatus = responseContent.value(forKey: "RideStatus") as? NSInteger else {
            return
        }
        
        if rideStatusTimer.isValid && self.circularProgressView.isHidden {
            let totalDriverInt = Double(totalDrivers)
            self.maxCount = totalDriverInt * 25.0
            self.startSearchingProgress()
        }
            
        else {
            self.ridePoolingDidFinishWith(status: rideStatus)
        }
    }
    
    func ridePoolingDidFinishWith(status:NSInteger?){
//    {
//        "result": {
//            "TotalDrivers": "2",
//            "RideStatus": "1:Search OR 2:Assigned OR 6:Expired"
//        }
//        }
       if status == 2 {
            let alertController: UIAlertController = UIAlertController(title: nil, message: "Ride Booked Successfully".localized, preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                _ = self.navigationController?.popToRootViewController(animated: false)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
                
                let visibleVC = UIApplication.topViewController()!
                if (visibleVC is SlideMenuController) {
                    let slideMenuVC = (visibleVC as! SlideMenuController)
                    let nav = (slideMenuVC.mainViewController as! UINavigationController)
                    let topVC = nav.viewControllers.last
                    if topVC is HomeViewController{
                        (topVC as! HomeViewController).callWebserviceForGettingCurrentRide()
                    }
                }
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            stopTimers()
            print("***** Driver Assigned *****")
        }
            
        else if status == 6 {
            showPrebookingOptionsView(nil)
            print("***** Ride Expired *****")
        }
    }
    
    func startSearchingProgress() {
        
        circularProgressView.isHidden = false
        lblHangMessage.isHidden = false
        imgCirclePoint.isHidden = false
        circularProgressView.angle = 0
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
        })
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.increaseProgressButtonTapped), userInfo: nil, repeats: true)
    }
    
    func increaseProgressButtonTapped()-> Void {
        if currentCount != maxCount {
            currentCount += 1
            let newAngleValue = newAngle()
            circularProgressView.animate(Double(newAngleValue), duration: 1.0, completion: nil)
            let str = NSString(format:"%.0fs", currentCount)
            timerLable.text=str as String
        }
        else {
            showPrebookingOptionsView(nil)
        }
    }
    
    @IBAction func preBookAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        if self.prevVC.isKind(of: OrderPaymentViewController.self) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
            _ = self.prevVC.navigationController?.popViewController(animated: true)
        }
        else if !self.prevVC.isKind(of: HomeViewController.self) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            let swiftViewController = UINavigationController(rootViewController: homeVC!)
            self.prevVC.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
        } else {
            print("HomeVC")
        }
        
//        self.dismiss(animated: true) {
//
//            if self.prevVC.isKind(of: OrderPaymentViewController.self) {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
//                _ = self.prevVC.navigationController?.popViewController(animated: true)
//            }
//            else if !self.prevVC.isKind(of: HomeViewController.self) {
//                NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
//                let storyboard = UIStoryboard(name: "Rider", bundle: nil)
//                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
//                let swiftViewController = UINavigationController(rootViewController: homeVC!)
//                self.prevVC.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
//            } else {
//                print("HomeVC")
//            }
//        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
        if self.prevVC.isKind(of: OrderPaymentViewController.self) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            let swiftViewController = UINavigationController(rootViewController: homeVC!)
            self.prevVC.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
        } else {
            print("Other VC's")
        }
        
        if self.prevVC.isKind(of: DriverDetailsController.self) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            let swiftViewController = UINavigationController(rootViewController: homeVC!)
            self.prevVC.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
        } else {
            print("Other VC's")
        }
//        self.dismiss(animated: true) {
//
//            if self.prevVC.isKind(of: OrderPaymentViewController.self) {
//                NotificationCenter.default.post(name: Notification.Name(rawValue: "RideBooked"), object: nil)
//                let storyboard = UIStoryboard(name: "Rider", bundle: nil)
//                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
//                let swiftViewController = UINavigationController(rootViewController: homeVC!)
//                self.prevVC.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
//            } else {
//                print("Other VC's")
//            }
//        }
    }
    
    func showPrebookingOptionsView( _ notification : Notification? = nil) {
        
        if notification != nil {
            userInfo = notification?.userInfo as! [String : Any]
        }else{
            callUserInformationAPIWithUserID()
        }
        
        if let notificationDict = self.userInfo["aps"] as? NSDictionary {
            lblPrebookMessage.text = (notificationDict.value(forKey: "alert") as! String)
        }
        
        if (delegate != nil) {
            delegate!.stopSound()
        }
        
        stopTimers()
        
        self.circularProgressView.isHidden = true
        self.imgCirclePoint.isHidden = true
        self.lblHangMessage.isHidden = true
        self.preBookInfoView.alpha = 0.0
        UIView.animate(withDuration: 0.2, animations: {
            self.preBookInfoView.alpha = 1.0
        })
       
    }
    func callUserInformationAPIWithUserID() {
        
        if(checkConnection()){

            let lRID = rideBookingID != ""  ? rideBookingID : UserDefaults.standard.string(forKey: RiderID_Notification)
            
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [String : Any]()
            parameters = [
                "BookingId":Int(lRID!) ?? 0,
                "UserId":riderID] as [String : Any]
            self.showHUD()
            UserDefaults.standard.set("", forKey: RiderID_Notification)
            UserDefaults.standard.synchronize()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KRIDER_RIDEEXPIRE, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_RIDEEXPIRE, jsonString: jsonParam as NSString, withIdicator: false, header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    print("responseContent: ", responseContent)
                    if let value = responseContent?.value(forKey: "Status") as? Bool{
                        if value == true{
                        }else{
                           // self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                        }
                        
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func newAngle() -> Int {
        return Int(360 * (currentCount / maxCount))
    }
    
    func stopTimers() {
        if timer.isValid {
            timer.invalidate()
        }
        
        if rideStatusTimer.isValid {
            rideStatusTimer.invalidate()
        }
    }

}


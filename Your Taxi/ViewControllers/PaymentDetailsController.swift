import UIKit
import Stripe

class PaymentDetailsController: BaseViewController, STPBackendCharging, PaymentViewControllerDelegate, CardIOPaymentViewControllerDelegate {
    
    @IBOutlet weak var shadowView  : UIView!
    @IBOutlet weak var cardHolderName : UITextField!
    @IBOutlet weak var cardNumber : UITextField!
    @IBOutlet weak var expDate : UITextField!
    @IBOutlet weak var paymentButton : UIButton!
    var cardIOVC : CardIOPaymentViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        drawShadow(shadowView)
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(PaymentDetailsController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        shadowView.isHidden = true
        cardHolderName.isEnabled = false
        cardNumber.isEnabled = false
        expDate.isEnabled = false
        getCardDetails()
        self.slideMenuController()?.removeLeftGestures()
        setScreenLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "CARD DETAILS".localized
    }
    
    // MARK: - Set Screen Design Methods
    func setScreenLayout() {
        paymentButton.backgroundColor = kBUTTON_DARK_GREEN_COLOR
        paymentButton.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        paymentButton.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
    }
    
    func getCardDetails()
    {
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "UserId" : riderID
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KRIDER_CARD_DETAILS_WITH_DATA, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_CARD_DETAILS_WITH_DATA, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    
                    if((responseContent?.value(forKey: "StripeCardId") as? String) == "fail")
                    {
                        self.shadowView.isHidden = true
                    }
                    else
                    {
                        self.shadowView.isHidden = false
                        if((responseContent?.value(forKey: "Status") as? Bool) != false)
                        {
                            if ((responseContent?.value(forKey: "Status") as? Bool) == true)
                            {
                                let cardNo = responseContent?.value(forKey: "Number") as! String
                                let tempNo : NSMutableString = NSMutableString(string: cardNo)
                                let totalSpace = (cardNo.count / 4) - 1
                                for i in 0 ..< totalSpace
                                {
                                    if i == 0
                                    {
                                        tempNo.insert(" ", at: 4)
                                    }
                                    else if i == 1
                                    {
                                        tempNo.insert(" ", at: 9)
                                    }
                                    else if i == 2
                                    {
                                        tempNo.insert(" ", at: 14)
                                    }
                                    else if i == 3
                                    {
                                        tempNo.insert(" ", at: 19)
                                    }
                                }
                                self.cardNumber.text = tempNo as String
                                self.cardHolderName.text = responseContent?.value(forKey: "Name") as? String
                                self.expDate.text = "\(responseContent?.value(forKey: "ExpMonth") as! String)/\(responseContent?.value(forKey: "ExpYear") as! Int64)"
                                self.paymentButton.setTitle("Change Card".localized, for: .normal)
                                
                            }else{
                                
                                if let isDeleted = responseContent?.value(forKey: "flag") as? Int{
                                    if(isDeleted == 1){
                                        self.moveToSingupController(message: responseContent?.value(forKey: "Message") as? String ?? "")
                                    }
                                }else{
                                    self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                                }
                                
                            }
                        }
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    @IBAction func btnAddCard(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Credit Card".localized, message: nil, preferredStyle: .actionSheet)
        
        let addCardButton = UIAlertAction(title: "Add Card".localized, style: .default, handler: { (action) -> Void in
            let paymentViewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            paymentViewController.amount = 10.0
            paymentViewController.backendCharger = self
            paymentViewController.paymentDelegate = self
            let navController = UINavigationController(rootViewController: paymentViewController)
            self.present(navController, animated: true, completion: nil)
        })
        
        let  scanCardButton = UIAlertAction(title: "Scan Card".localized, style: .default, handler: { (action) -> Void in
            self.cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
            self.cardIOVC?.collectCardholderName = true
            self.cardIOVC.hideCardIOLogo = true
            self.cardIOVC.navigationBarTintColorForCardIO = KAPP_NAV_COLOR
            self.cardIOVC.disableManualEntryButtons = true
            self.cardIOVC?.modalPresentationStyle = .formSheet
            self.present(self.cardIOVC!, animated: true, completion: nil)
        })
        
        let cancelButton = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (action) -> Void in
        })
        alertController.addAction(addCardButton)
        alertController.addAction(cancelButton)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Actions Methods
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func paymentViewController(_ controller: PaymentViewController, didFinish error: Error?) {
        
        self.dismiss(animated: true, completion: {
            if error != nil {
                self.presentError(error)
            }
            else
            {
                self.getCardDetails()
            }
        })
        
    }
    
    func getStripeToken(_ card : STPCardParams) {
        self.showHUD()
        STPAPIClient.shared().createToken(withCard: card, completion: { (token, error) in
            
            if (token?.tokenId) != nil {
                self.hideHUD()
                self.postStripeToken(token: token!, card : card)
            } else {
                print(error.debugDescription)
                self.hideHUD()
            }
        })
    }
    func postStripeToken(token: STPToken, card : STPCardParams) {
        
        self.showHUD()
        
        if Stripe.defaultPublishableKey() == nil
        {
            self.cardIOVC.dismiss(animated: true, completion: nil)
            return
        }
        let Request: WebserviceHelper = WebserviceHelper()
        let riderID = Request.getUserDefaultsStandard()
        let number = card.number!
        
        let tempNo : NSMutableString = NSMutableString(string: number)
        let totalSpace = (number.characters.count / 4) - 1
        for i in 0 ..< totalSpace
        {
            if i == 0
            {
                tempNo.insert(" ", at: 4)
            }
            else if i == 1
            {
                tempNo.insert(" ", at: 9)
            }
            else if i == 2
            {
                tempNo.insert(" ", at: 14)
            }
            else if i == 3
            {
                tempNo.insert(" ", at: 19)
            }
        }
        let array : [String] = tempNo.components(separatedBy: .whitespaces)
        let lastDigits = array.last
        var cardType : String = " "
        for card in CardType.allCards {
            if (matchesRegex(card.regex, text: number)) {
                cardType = card.rawValue
                break
            }
        }
        var expirationDate = ""
        if card.expYear > 1000 {
            expirationDate = "\(card.expMonth)/\(card.expYear)"
        }
        else {
            expirationDate = "\(card.expMonth)/20\(card.expYear)"
        }
        if(checkConnection()) {
            var parameters = [
                "UserId" : riderID,
                "Name": card.name!,
                "Number": card.number,
                "ExpMonth": card.expMonth,
                "ExpYear": card.expYear,
                "Cvc": card.cvc,
                "UpdatedBy" : riderID
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KCARD_PAYMENT, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KCARD_PAYMENT, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseContent : NSDictionary = Request.convertStringToDictionary(text: result )!
                    if responseContent["Message"] as! String != "fail"
                    {
                        self.getCardDetails()
                    }
                    self.showSimpleAlertController(nil, message: responseContent["Message"] as! String)
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func presentError(_ error: Error?) {
        let controller = UIAlertController(title: nil, message: error!.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok".localized, style: .default, handler: nil)
        controller.addAction(action)
        self.present(controller, animated: true, completion: { _ in })
    }
    
    //MARK: - CardIO Methods
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        
        paymentViewController?.dismiss(animated: true, completion: nil)
    }
    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        
        if let info = cardInfo {
            paymentViewController?.dismiss(animated: true, completion: nil)
            let card: STPCardParams = STPCardParams()
            card.number = info.cardNumber
            card.expMonth = info.expiryMonth
            card.expYear = info.expiryYear
            card.cvc = info.cvv
            card.name = info.cardholderName
            getStripeToken(card)
            
        }
    }
    // MARK: STPPaymentContextDelegate
    internal func createBackendCharge(with token: STPToken, completion: @escaping (STPBackendChargeResult, Error?) -> Void) {
        let error1 : NSError = NSError(domain: StripeDomain, code: 50, userInfo: [NSLocalizedDescriptionKey: " "])
        
        if BackendChargeURLString == nil
        {
            completion(STPBackendChargeResult.failure, error1 as Error)
            return
        }
        completion(STPBackendChargeResult.success, nil)
        hideHUD()
    }
}


import UIKit
import IQKeyboardManagerSwift

class ForgotUsernameAndPasswordViewController: BaseViewController {
    
    var isForUsername: Bool = true
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var okBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        emailTextField.placeholder = KAPP_RIDER ? "LoginEmailAddress".localized : "ForgotEmail".localized
        cancelBtn.setTitle("Back".localized, for: .normal)
        okBtn.setTitle("Ok".localized, for: .normal)
        titleLabel.text = "Forgot password?".localized
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func okBtnAction(_ sender: Any) {
        if checkEmail(emailTextField.text as! NSString) {
            
            var parameters = [String:Any]()
            
            if isForUsername {
                parameters = [
                    "email": emailTextField.text,
                    "mode": "forgot_username",
                    "deviceID": DEVICE_UUID,
                    "user_type":"rider",
                    "platform": kPlatForm,
                    "language":systemLanguage,
                ]
            } else {
                parameters = [
                    "UserType" : 2,
                    "Email" : emailTextField.text,
                ]
            }
            
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = [
                "Content-Type" : "application/json",
                "Language" : systemLanguage
            ]
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            if(checkConnection()) {
                forgetCaseApi(jsonString: jsonParam as NSString, headerDict: headerDict)
            }else{
                self.showAlertForInternetConnectionOff()
            }
            
        } else {
            if emailTextField.text == "" {
                showSimpleAlertController(nil, message: "Please enter Email".localized)
                return
            }
            showSimpleAlertController(nil, message: "Please enter a valid email address.".localized)
            return
        }
    }
    
    func forgetCaseApi(jsonString : NSString, headerDict : [String : String]) {
        self.showHUD()
        var tempDict = NSMutableDictionary()
        let Request: WebserviceHelper = WebserviceHelper()
        printJsonStringLog(apiName: FORGET_CASE_URL, parameter: jsonString, headerDict: headerDict)
        Request.CustomHTTPPostJSONWithHeader(url: FORGET_CASE_URL, jsonString: jsonString as NSString, withIdicator: false, header: headerDict, callback: { (result, error) in
            
            if ((error) != nil){
                self.hideHUD()
                print(error?.localized ?? "")
            }else{
                self.hideHUD()
                self.emailTextField.text = ""
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                let responseContent = Request.convertStringToDictionary(text: decryptedData! as String)
                print("Forget Password :", responseContent ?? "")
                if responseContent?.value(forKey: "Status") as? Bool == true {
                    if let message = responseContent?.value(forKey: "Message") as? String {
                        let alertController: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                        
                        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                            self.dismiss(animated: false, completion: nil)
                        }
                        alertController.addAction(okAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                } else if responseContent?.value(forKey: "Status") as? Bool == false {
                    if let message = responseContent?.value(forKey: "Message") as? String {
                        self.showSimpleAlertController(nil, message: message)
                    }
                }
                
            }
        })
    }
}


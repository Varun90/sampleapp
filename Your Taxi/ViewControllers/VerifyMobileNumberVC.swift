import UIKit
import CTKFlagPhoneNumber
import FirebaseAuth
import SlideMenuControllerSwift
import AccountKit
import MapKit
import INTULocationManager
import KDCircularProgress
import CoreLocation
import Crashlytics

class VerifyMobileNumberVC: BaseViewController {
    var phone_number = ""
    var onlyPhoneNumber = ""
    @IBOutlet var countyCodeTextField: CTKFlagPhoneNumberTextField!
    @IBOutlet var phoneNumberTextField: UITextField!
    @IBOutlet var verifyNumberButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        verifyNumberButton.setTitle("LOG IN".localized, for: UIControlState())
        verifyNumberButton.titleLabel?.adjustsFontSizeToFitWidth = true;
        if KAPP_RIDER {
            verifyNumberButton.backgroundColor = kBUTTON_DARK_GREEN_COLOR
            verifyNumberButton.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
            verifyNumberButton.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        }
        
        phoneNumberTextField.placeholder = KAPP_RIDER ? "Phone Number".localized : "Mobile Number".localized
        countyCodeTextField.parentViewController = self
        countyCodeTextField.center = view.center
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                let alertController = UIAlertController(
                    title: "Background Location Access Disabled".localized,
                    message: "In order to be notified, please open this app's settings and set location access to 'Always'.".localized,
                    preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
                alertController.addAction(cancelAction)
                
                let openAction = UIAlertAction(title: "Open Settings".localized, style: .default) { (action) in
                    if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                alertController.addAction(openAction)
                self.present(alertController, animated: true, completion: nil)
                
            case .authorizedAlways, .authorizedWhenInUse:
                if (delegate?.userLocation?.coordinate != nil){
                    let long = delegate?.userLocation?.coordinate.longitude
                    let lat = delegate?.userLocation?.coordinate.latitude
                    let location = CLLocation(latitude: lat!, longitude: long!)
                    CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                        if error != nil {
                            return
                        }else if let country = placemarks?.first?.isoCountryCode,
                            let city = placemarks?.first?.locality {
                        }
                    })
                    break
                }
            }
        } else {
            print("Location services are not enabled".localized)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SET UP NAVIGATION BAR
    func setUpNavigationBar(){
        self.title = "Login".localized
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
    }
    
    
    //MARK:- BUTTON ACTIONS
    @IBAction func verifyMobileNumberAction(_ sender: UIButton) {
        
        if phoneNumberTextField.text == "" || (phoneNumberTextField.text == nil) {
            self.showSimpleAlertController(nil, message: "Please enter mobile number".localized)
            return
        }
        if self.countyCodeTextField.text == nil {
            self.showSimpleAlertController(nil, message: "Enter phone number".localized)
            return
        } else {
            if(checkConnection()) {
                self.showHUD()
               phoneNumberTextField.text = phoneNumberTextField.text!.replacingOccurrences(of: "^0+", with: "", options: .regularExpression)
                var parameters = [
                    "PhoneNo" : phoneNumberTextField.text!,
                    "UserType":KAPP_RIDER ? 2 : 3,
                    ] as [String : Any]
                
                let jsonString = returnJsonString(param: parameters)
                var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                let headerDict = [
                    "Content-Type" : "application/json",
                    "Language" : systemLanguage
                ]
                parameters = ["data":encryptedData!]
                let jsonParam = returnJsonString(param: parameters)
                let Request: WebserviceHelper = WebserviceHelper()
                printJsonStringLog(apiName: KValidatePhone_URL, parameter: jsonString as NSString, headerDict: headerDict)
                Request.CustomHTTPPostJSONWithHeader(url: KValidatePhone_URL, jsonString: jsonParam as NSString, withIdicator: false, header: headerDict) { (result, error) in
                    self.hideHUD()
                    if error != nil{
                        self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                        return;
                    }
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    if responseContent?.value(forKey: "Status")as? Bool == true {
                        
//                        if KAPP_RIDER {
//                            let riderLoginVC = self.storyboard?.instantiateViewController(withIdentifier: "RiderNewUserLoginVC") as? RiderNewUserLoginViewController
//                            riderLoginVC?.phone_number = ((self.countyCodeTextField.text)! + self.phoneNumberTextField.text!)
//                            riderLoginVC?.onlyPhoneNumber = self.phoneNumberTextField.text!
//                            self.navigationController?.pushViewController(riderLoginVC!, animated: true)
//                        }else {
//                            let driverLoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
//                            driverLoginVC?.phone_number = ((self.countyCodeTextField.text)! + self.phoneNumberTextField.text!)
//                            driverLoginVC?.onlyPhoneNumber = self.phoneNumberTextField.text!
//                            self.navigationController?.pushViewController(driverLoginVC!, animated: true)
//                        }

                        
                        PhoneAuthProvider.provider().verifyPhoneNumber(((self.countyCodeTextField.text)! + self.phoneNumberTextField.text!), uiDelegate: nil) { (verificationID, error) in

                            if let error = error {
                                if error.localizedDescription == "TOO_LONG" || error.localizedDescription == "TOO_SHORT" || error.localizedDescription == "INVALID_LENGTH" || error.localizedDescription == "Invalid format." {
                                    self.showSimpleAlertController(nil, message: "Invalid phone number".localized)
                                    return
                                } else {
                                    self.showSimpleAlertController(nil, message: error.localizedDescription)
                                    print("Error message : ", error.localizedDescription)
                                    return
                                }
                            }
                            encrypt(data: verificationID as AnyObject, key: "authVerificationID")
                            self.setupOtpView(view: self.view)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if let sentView = self.view.viewWithTag(SENT_VIEW_TAG) {
                                    sentView.removeFromSuperview()

                                    let addOtpVC = self.storyboard?.instantiateViewController(withIdentifier: "AddOtpVC") as? AddOtpVC
                                    addOtpVC?.isFromLogin = true
                                    addOtpVC?.phone_number = ((self.countyCodeTextField.text)! + self.phoneNumberTextField.text!)
                                    addOtpVC?.onlyPhoneNumber = (self.phoneNumberTextField.text)!
                                    addOtpVC?.mobileNumber = ((self.countyCodeTextField.text)! + self.phoneNumberTextField.text!);
                                    self.navigationController?.pushViewController(addOtpVC!, animated: true)
                                }
                            }
                        }
                    }
                    else {
                        if let message = responseContent?.value(forKey: "Message") as? String {
                            self.showSimpleAlertController(nil, message: message)
                            self.hideHUD()
                        }
                    }

                }
            }else{
                self.showAlertForInternetConnectionOff()
            }
        }
    }
    
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}


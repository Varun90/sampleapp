import UIKit

class passwordRulesAlertViewController: UIViewController {

    @IBOutlet var okBtn: UIButton!
    @IBOutlet var labelNumber5: UILabel!
    @IBOutlet var labelNumber4: UILabel!
    @IBOutlet var labelNumber3: UILabel!
    @IBOutlet var labelNumber2: UILabel!
    @IBOutlet var labelNumber1: UILabel!
    @IBOutlet var titleLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        titleLbl.text = "Password Rules".localized
        labelNumber1.text = "1. At least one upper case English letter.".localized
        labelNumber2.text = "2. At least one lower case English letter.".localized
        labelNumber3.text = "3. At leas one digit.".localized
        labelNumber4.text = "4. At least one special character.".localized
        labelNumber5.text = "5. Minimum eight in length.".localized
        okBtn.setTitle("OK".localized, for: .normal)
    }

    @IBAction func okBtnAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

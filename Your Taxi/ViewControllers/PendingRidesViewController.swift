

import UIKit

class PendingRidesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView : UITableView!
    var pendingRideList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.estimatedRowHeight = 100
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(PendingRidesViewController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
    }
    
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Later Booking".localized
    }
    
    func getPendingRideList()
    {
        if(checkConnection()){
            
            let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
            let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
            let userDict = self.convertStringToDictionary(text: decryt!)!
            
            let driverID  = userDict.value(forKey: "id") as! String
            
            let parameters : [String:Any] = [
                "user_type":"driver",
                "driver_id": driverID,
                "environment":kEnviroment,
                "platform": kPlatForm,
                "deviceID": DEVICE_UUID,
                "push_token":getPushTokenString()
            ]
            
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            print("Request : \(KDRIVER_PENDING_RIDE) \n Parameters: \(parameters)")
            let headerDict = Request.getHeader(parameters: parameters)
            Request.HTTPPostJSONWithHeader(url: KDRIVER_PENDING_RIDE, paramters: parameters as NSDictionary, withIdicator: false, header: headerDict, callback: { (result, error) in
                
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                    
                }else{
                    let responseContent = Request.convertStringToDictionary(text: result )
                    print("responseContent:",responseContent ?? [:])
                    if let response = responseContent?.value(forKey: "response") as? NSDictionary
                    {
                        if let status = (response.value(forKey: "status") as? String)
                        {
                            if status == "success"
                            {
                                if let result = (response.value(forKey: "result") as? NSArray)
                                {
                                    self.pendingRideList = (result.mutableCopy() as? NSMutableArray)!
                                    self.tableView.reloadData()
                                }
                            }
                            else
                            {
                                //  : Handle Driver Logout
                                if let value = response["is_already_logout"] as? String {
                                    if value == "1" && UserDefaults.standard.bool(forKey: KRIDER_LOGIN) && !KAPP_RIDER{
                                        self.delegate?.forceFullyLogoutDriverAlert(response["message"] as! String)
                                    }
                                    else {
                                        _ = self.navigationController?.popViewController(animated: true)
                                    }
                                }
                                else{
                                    _ = self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                    }
                }
            })
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pendingRideList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.separatorColor = UIColor.darkGray
        let cell = tableView.dequeueReusableCell(withIdentifier: "PendingRideCell", for: indexPath) as! PendingRideCell
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        cell.fromLabel.text = "From:".localized
        cell.toLabel.text = "To:".localized
        
        if let dic = pendingRideList[indexPath.row] as? NSDictionary {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.locale = Locale(identifier: "en_GB")

            if let pickupDateTime = dic["BookingDateTime"] as? String {
                cell.dateTime.text = pickupDateTime
            } else {
                cell.dateTime.text = "00-00-0000 00:00:00"
            }
            
            cell.pickUpLocation.text = dic["PickupLocation"] as? String
            cell.dropUpLocation.text = dic["DropLocation"] as? String
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let dic = pendingRideList[indexPath.row] as? NSDictionary {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PendingRideDetailsViewController") as! PendingRideDetailsViewController
            vc.detailDictionary = dic.mutableCopy() as! NSMutableDictionary
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}


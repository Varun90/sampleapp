import UIKit
import Stripe
import IQKeyboardManagerSwift

protocol PaymentViewControllerDelegate {
    func paymentViewController(_ controller: PaymentViewController, didFinish error: Error?)
}

class PaymentViewController: BaseViewController, STPPaymentCardTextFieldDelegate {
    
    var paymentTextField = STPPaymentCardTextField()
    var amount : Double = 0.0
    var backendCharger : STPBackendCharging?
    var paymentDelegate : PaymentViewControllerDelegate?
    
    @IBOutlet weak var paymentView : UIView!
    @IBOutlet weak var cardHolderName : UITextField!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet var secondTilteLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.view.backgroundColor = UIColor.white
        self.title = "PAYMENT".localized
        titleLabel.text = "Secure payments.".localized
        secondTilteLabel.text = "You will pay after the ride.".localized
        titleLabel.adjustsFontSizeToFitWidth = true
        cardHolderName.autocorrectionType = .no
        if self.responds(to: #selector(setter: UIViewController.edgesForExtendedLayout)){
            self.edgesForExtendedLayout = .all
        }
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .done, target: self, action: #selector(PaymentViewController.saveButtonClicked(_:)))
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(PaymentViewController.cancelButtonClicked(_:)))
        doneButton.isEnabled = false
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.rightBarButtonItem = doneButton
        paymentTextField = STPPaymentCardTextField(frame: CGRect(x: 0, y: 0, width: self.paymentView.frame.size.width, height: self.paymentView.frame.size.height))
        paymentTextField.delegate = self
        paymentTextField.cursorColor = UIColor.green
        self.paymentView.addSubview(paymentTextField)
        self.cardHolderName.placeholder = "Card Holder".localized
    }
    override func viewDidLayoutSubviews() {
        paymentTextField.frame = CGRect(x: 0, y: 0, width: self.paymentView.frame.size.width, height: self.paymentView.frame.size.height)
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        paymentTextField.frame = CGRect(x: 0, y: 0, width: self.paymentView.frame.size.width, height: self.paymentView.frame.size.height)
    }
    func saveButtonClicked(_ sender : UIBarButtonItem)
    {
        self.showHUD()
        if cardHolderName.text?.trimmingCharacters(in: .whitespaces).characters.count == 0
        {
            self.hideHUD()
            self.showSimpleAlertController(nil, message: "Please Enter Holder Name.".localized)
            return
        }
        else
        {
            if self.paymentTextField.isValid == false
            {
                self.hideHUD()
                return
            }
        }
        
        if Stripe.defaultPublishableKey() == nil
        {
            let error : NSError = NSError(domain: StripeDomain, code: 50, userInfo: [NSLocalizedDescriptionKey: "Public key not found"])
            
            self.paymentDelegate?.paymentViewController(self, didFinish: error)
            self.hideHUD()
            return
        }
        
        STPAPIClient.shared().createToken(withCard: self.paymentTextField.cardParams, completion: { (token, error) in
            
            if error != nil{
                self.hideHUD()
                self.showSimpleAlertController(nil, message: error?.localizedDescription ?? "")
                return
            }
            
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            let number = self.paymentTextField.cardNumber!
            let lastDigits = String(number.characters.suffix(4))
            
            var cardType : String = " "
            for card in CardType.allCards {
                if (matchesRegex(card.regex, text: number)) {
                    cardType = card.rawValue
                    break
                    
                }
            }
            var expirationDate = ""
            if self.paymentTextField.expirationYear > 1000 {
                expirationDate = "\(self.paymentTextField.expirationYear)"
            }
            else {
                expirationDate = "20\(self.paymentTextField.expirationYear)"
            }
            var expirationMonth = "\(self.paymentTextField.expirationMonth)"
            if expirationMonth.characters.count < 2
            {
                expirationMonth = "0\(self.paymentTextField.expirationMonth)"
            }
            var parameters = [
                "UserId" : riderID,
                "Name": self.cardHolderName.text!,
                "Number": self.paymentTextField.cardNumber!,
                "ExpMonth": expirationMonth,
                "ExpYear": "20\(self.paymentTextField.expirationYear)",
                "Cvc": self.paymentTextField.cvc!,
                "UpdatedBy" : riderID
                ] as [String : Any]
            
            if (error != nil)
            {
                self.hideHUD()
                self.paymentDelegate?.paymentViewController(self, didFinish: error)
            }
            self.backendCharger?.createBackendCharge(with: token!, completion: { (result, error) in
                
                if (error != nil)
                {
                    self.paymentDelegate?.paymentViewController(self, didFinish: error)
                    self.hideHUD()
                    return
                }
                
                if(self.checkConnection()){
                    
                    self.showHUD()
                    let jsonString = returnJsonString(param: parameters)
                    var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                    parameters = ["data":encryptedData!]
                    let jsonParam = returnJsonString(param: parameters)
                    let headerDict = Request.getHeaderForRider(parameters: parameters)
                    printJsonStringLog(apiName: KCARD_PAYMENT, parameter: jsonParam as NSString, headerDict: headerDict)
                    Request.CustomHTTPPostJSONWithHeader(url: KCARD_PAYMENT, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                        self.hideHUD()
                        if ((error) != nil){
                            print(error.debugDescription)
                            self.showAlertForInternetConnectionOff()
                        }else{
                            let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                            
                            let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                            
                            let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                            print("AddCard :", responseContent ?? "")
                            if let response = responseContent?.value(forKey: "Status") as? Bool {
                                if response == true
                                {
                                    self.showSimpleAlertControllerWithOkAction(nil, message: responseContent!.value(forKey: "Message") as? String ?? "", callback: {
                                        self.paymentDelegate?.paymentViewController(self, didFinish: nil)
                                        self.presentingViewController?.dismiss(animated: true, completion: nil)
                                    })
                                }
                                else{
                                    self.hideHUD()
                                    if responseContent?.index(ofAccessibilityElement: "Errors") != nil {
                                        if let ErrorMsg = responseContent?.value(forKey: "Errors") as? NSDictionary{
                                            if let fn = ErrorMsg.value(forKey: "Cvc") as? NSArray{
                                                self.showSimpleAlertController(nil, message: (fn[0] as? String)!)
                                            }
                                        }else{
                                            self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "Message") as? String)!)
                                        }
                                    }
                                    else{
                                        self.showSimpleAlertController(nil, message: (responseContent?.value(forKey: "Message") as? String)!)
                                    }
                                }
                            }
                        }
                    }
                }else{
                    self.showAlertForInternetConnectionOff()
                }
            })
        })
    }
    
    func cancelButtonClicked(_ sender : UIBarButtonItem)
    {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        self.navigationItem.rightBarButtonItem?.isEnabled = textField.isValid
    }
}

enum CardType: String {
    case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
    
    static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
}


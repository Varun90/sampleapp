

import UIKit
import SafariServices
import SlideMenuControllerSwift
import AccountKit
import IQKeyboardManagerSwift
import Crashlytics
import Firebase

class LoginViewController: BaseViewController,UITextFieldDelegate,AKFViewControllerDelegate {
    var phone_number = ""
    var onlyPhoneNumber = ""

    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passTextField: UITextField!
    
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var passwordLabel: UILabel!
    
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var lostPassButton: UIButton!
    @IBOutlet weak var rememberMeButton: UIButton!
    
    @IBOutlet var loginView: UIView!
    @IBOutlet var loginViewBottom: NSLayoutConstraint!

    
    var accountKit: AKFAccountKit!
    var diallingCodesDictionary: NSDictionary!
    var rider_id : String = " "
    
    // MARK: - Overrided Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        
        if KAPP_RIDER
        {
            emailTextField.delegate = self
            passTextField.delegate = self
            emailTextField.placeholder = "Email Address".localized
            passTextField.placeholder = "Password".localized
            loginButton.setTitle("LOG IN".localized, for: UIControlState())
            lostPassButton.setTitle("Lost Password?".localized, for: UIControlState())
            IQKeyboardManager.shared.enable = true
            IQKeyboardManager.shared.enableAutoToolbar = true
            IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        }
        
        if UserDefaults.standard.bool(forKey: KRIDER_LOGIN) {
            
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            
            if KAPP_RIDER {
                let RootVC = UINavigationController.init(rootViewController: homeVC!)
                self.slideMenuController()?.changeMainViewController(RootVC, close: true)
            }else{
                self.navigationController?.pushViewController(homeVC!, animated: false)
            }
        }
        
        if !KAPP_RIDER {
            
            setData()
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
            view.addGestureRecognizer(tap)
            
            if (UserDefaults.standard.object(forKey: kREMEMBER_ME) != nil){
                rememberMeButton.isSelected=true;
                
                let dic = UserDefaults.standard.object(forKey: kREMEMBER_ME) as? String
                let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
                let userDict = self.convertStringToDictionary(text: decryt!)!
                
                emailTextField.text = userDict.value(forKey: "email") as? String
                passTextField.text = userDict.value(forKey: "password") as? String
            }
        }
        
        if !(KAPP_RIDER) {
            registerButton.setTitle("REGISTER".localized, for: UIControlState())
            registerButton.titleLabel?.adjustsFontSizeToFitWidth = true;
            registerButton.layer.cornerRadius = 20;
            registerButton.clipsToBounds = true;
        }
        else
        {
            registerButton.setTitle("Sign Up".localized, for: UIControlState())
            registerButton.titleLabel?.adjustsFontSizeToFitWidth = true;
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if KAPP_RIDER {
            self.title = "Rider".localized
            slideMenuController()?.removeLeftGestures()
        }
        else{
            self.title = "Login".localized
            loginButton.tag = 1
            emailTextField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
            passTextField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
            registerForKeyboardNotifications()
        }
        
        if(!KAPP_RIDER){
            locationManager1.stopUpdatingLocation()
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        if !KAPP_RIDER {
            unregisterForKeyboardNotifications()
            locationManager1.startUpdatingLocation()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- BUTTON ACTIONS
    func backAction(){
       // _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Data Methos
    func setData() {
        
        emailLabel.text = "Email Address".localized
        passwordLabel.text = "Password".localized
        emailTextField.placeholder = "Email Address".localized
        passTextField.placeholder = "Password".localized
        loginButton.setTitle("LOG IN".localized, for: UIControlState())
        loginButton.layer.cornerRadius = 20;
        loginButton.clipsToBounds = true;
        lostPassButton.setTitle("Lost Password?".localized, for: UIControlState())
        loginButton.titleLabel?.adjustsFontSizeToFitWidth = true;
        lostPassButton.titleLabel?.adjustsFontSizeToFitWidth = true;
    }
    
    // MARK: - TextField Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passTextField.becomeFirstResponder()
        }
        else if textField == passTextField {
            passTextField.resignFirstResponder()
            loginAction(loginButton)
        }
        return true
    }
    
    // MARK: - Actions Methods
    @IBAction func loginAction(_ sender: AnyObject) {
        emailTextField.resignFirstResponder()
        passTextField.resignFirstResponder()
        let senderButton = sender as! UIButton
        
        let email =  emailTextField.text! as String
        let password =  passTextField.text! as String
        if (email ).isEmpty && (password ).isEmpty{
            showSimpleAlertController(nil, message:"LoginVdltKey1".localized)
        }
        else if (email ).isEmpty{
            showSimpleAlertController(nil, message:"LoginVdltKey2".localized)
        }
        else if (password ).isEmpty{
            showSimpleAlertController(nil, message:"LoginVdltKey3".localized)
        }
        else if !checkEmail(email as NSString){
            showSimpleAlertController(nil, message:"Please enter a valid email address.".localized)
        }
        else {
            if checkConnection(){
                self.showHUD()
                var loginType = ""
                if KAPP_RIDER{
                    loginType = "Rider"
                }
                else{
                    loginType = "Driver"
                }
                
                var pushTokenString=NSString()
                #if (arch(i386) || arch(x86_64)) && os(iOS)
                    pushTokenString="1234"
                #else
                    pushTokenString = getDecryptedData(key: kPUSH_TOKEN) as NSString
                #endif
                
                var parameters = [String : Any]()
                if senderButton.tag == 2{
                    parameters = [
                        "login_type": loginType,
                        "email": email,
                        "password": password.sha256(),
                        "deviceID": DEVICE_UUID,
                        "push_token":getPushTokenString(),
                        "platform": kPlatForm,
                        "environment" : kEnviroment,
                        "login_forcefully" : "1"
                    ]
                }
                else {
                    
                    parameters = [
                        "Email":email,
                        "Password":password,
                        "PhoneNo":onlyPhoneNumber,
                        "PlatForm":kPlatFormID,
                        "UserType":3,
                        "DeviceToken":DEVICE_UUID,
                        "PushToken":getPushTokenString()
                    ]
                    
                }
                printParameterLog(apiName: KLOGIN_URL, parameter: parameters, headerDict: kLoginRequestHeader)
                loginApi(header: kLoginRequestHeader, jsonParam: getJsonStringWithEncryptedRequestParameters(requestParameters: parameters))
            }
            else{
                showAlertForInternetConnectionOff()
            }
        }
    }
    
    func loginApi(header:[String:String],jsonParam: String) {
        
        let Request: WebserviceHelper = WebserviceHelper()
        Request.CustomHTTPPostJSONWithHeader(url: KLOGIN_URL, jsonString: jsonParam as NSString, withIdicator: false, header: header) { (result, error) in
            
            self.hideHUD()
            
            if error != nil {
                self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                return;
            }
            
            //Get the Result Set
            PrepareClaimSetWithResult(result: result) { (resultClaimSet, statusFlag, message) in
                if statusFlag {
                    UserDefaults.standard.set(true, forKey: KRIDER_LOGIN)
                    UserDefaults.standard.synchronize()
                    //Call API to fetch the user information
                    if let userID = resultClaimSet["UserId"] as? NSInteger {
                        self.callUserInformationAPIWithUserID(userID: userID)
                    }
                }else if message == "" {
                    return;
                } else {
                    self.self.showSimpleAlertControllerWithOkAction(nil, message: message, callback: {
                        if message == "You are already logged in another device. So you can not perform this action from this device." {
                            let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                            appDelegate.performLogout()
                        }
                    })
                    return;
                }
            }
        }
    }
    
    func callUserInformationAPIWithUserID(userID: NSInteger) {
        
        if(checkConnection()) {
            
            let parameters = [
                "UserId":userID] as [String : Any]
            
            self.showHUD()
            
            let Request: WebserviceHelper = WebserviceHelper()
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: kGET_DRIVER_DETAILS, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                
                if error != nil {
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                let responseContent = getResponseDictionaryWithResult(result: result)
                print(responseContent)
                if self.setLoginUserDataWithDictionary(responseDict: responseContent) {
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
                    self.navigationController?.pushViewController(homeVC!, animated: true)
                }
            }
        }
        else {
            showAlertForInternetConnectionOff()
        }
    }
    
    func setLoginUserDataWithDictionary(responseDict: NSDictionary) -> Bool {
        
        print(responseDict)
        
        if (responseDict["user"] as? NSDictionary) != nil {
            
            let kUserDict = responseDict["user"] as! NSDictionary
           
            encrypt(data: kUserDict, key: KUSER_DETAILS)
            UserDefaults.standard.set(true, forKey: KRIDER_LOGIN)
            
            if let bankDict = responseDict["bank"] as? NSDictionary {
                encrypt(data: bankDict, key: KUSER_BANK_DETAILS)
            }
            
            UserDefaults.standard.synchronize()
            return true
        }
        
        self.showSimpleAlertController("Fail".localized, message:"Server response error".localized)
        return false
    }
    
    @IBAction func registerAction(_ sender: AnyObject) {
        
        if KAPP_RIDER {
            
            self.accountKit = AKFAccountKit(responseType: AKFResponseType.accessToken)
            let inputState: String = UUID().uuidString
            let viewController:AKFViewController = accountKit.viewControllerForPhoneLogin(with: nil, state: inputState)  as! AKFViewController
            viewController.enableSendToFacebook = true
            viewController.defaultCountryCode = "CH"
            self.prepareLoginViewController(viewController)
            self.present(viewController as! UIViewController, animated: true, completion:nil)
            
        }
        else{
            let registerUrl = (systemLanguage == "en") ? KDriver_REGISTER_EN_URL : KDriver_REGISTER_DE_URL
            openSafariController(registerUrl as NSString)
        }
    }
    @IBAction func lostPasswordAction(_ sender: AnyObject) {
        openSafariController(KLOSSPASS_URL as NSString)
    }
    
    // MARK : - SafariController Methods
    func openSafariController(_ urlString : NSString){
        if #available(iOS 9.0, *){
            let svc = SFSafariViewController(url: URL(string: urlString as String)!)
            self.present(svc, animated: true, completion: nil)
        }
        else{
            if let url = URL(string: urlString as String) {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // MARK: - Remember Me Button method
    @IBAction func rememberMeAction(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected=false;
            
        }else{
            sender.isSelected=true;
        }
    }
    
    // MARK: - Keyboard method
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func unregisterForKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if KAPP_RIDER {
                
                if self.view.frame.size.width==320 && self.view.frame.size.height == 480 {
                    self.view.frame.origin.y = 0;
                }else
                {
                    self.view.frame.origin.y = (0 - keyboardSize.height) + 120;
                }
            }
            else{
                if loginView.frame.origin.y > keyboardSize.height {
                    loginViewBottom.constant = keyboardSize.height
                }
            }
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        loginViewBottom.constant = 0
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: - Account Kit Section
    func viewController(_ viewController: UIViewController!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!){
        DispatchQueue.main.async(execute: {
            self.showHUD()
            self.accountKit = AKFAccountKit(responseType: AKFResponseType.accessToken)
            self.accountKit.requestAccount{
                (account, error) -> Void in
                
                if ((error) != nil){
                    
                    self.showSimpleAlertController(nil, message: "Error while fetching mobile number abd country code")
                    self.hideHUD()
                    
                }
            }
        })
    }
    
    func viewController(_ viewController: UIViewController!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("Login succcess with AuthorizationCode")
    }
    
    func viewController(_ viewController: UIViewController!, didFailWithError error: Error!) {
        print("We have an error \(error)")
    }
    
    func viewControllerDidCancel(_ viewController: UIViewController!) {
        print("The user cancel the login")
        self.registerButton.isHidden = false
    }
    
    func prepareLoginViewController(_ loginViewController: AKFViewController) {
        
        loginViewController.delegate = self
        loginViewController.advancedUIManager = nil
        //Costumize the theme
        let theme:AKFTheme = AKFTheme.init(primaryColor: KAPP_NAV_COLOR, primaryTextColor: UIColor(red: 1, green: 1, blue: 1, alpha: 1), secondaryColor: KAPP_NAV_COLOR, secondaryTextColor: UIColor(red: 1, green: 1, blue: 1, alpha: 1), statusBarStyle: .default)
        theme.textColor = .darkGray
        theme.titleColor = .darkGray
        theme.inputBackgroundColor = .white
        theme.inputTextColor = .darkGray
        loginViewController.theme = theme
    }
}



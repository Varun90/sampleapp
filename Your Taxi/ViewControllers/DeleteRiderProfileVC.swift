import UIKit
import SlideMenuControllerSwift

enum CellType {
    case NormalCell
    case OtherCell
}

class DeleteRiderProfileVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var tblHeaderView: UIView!
    @IBOutlet var lblTitle: UILabel!
    //@IBOutlet var lblSecondTitle: UILabel!
    //@IBOutlet var borderView: UIView!
    @IBOutlet var tblFooterView: UIView!
    @IBOutlet var attentationView: UIView!
    @IBOutlet var lblAttationTitle: UILabel!
    @IBOutlet var saperatorView: UIView!
    @IBOutlet var lblAttationDesc: UILabel!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet var btnCancel: UIButton!
    var arrReason : NSMutableArray!
    var currentIndexPath : IndexPath!
    var alertMessage = ""
    
    //MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetUpUI()
        setCellData()
        tblView.rowHeight = UITableViewAutomaticDimension
    }
    
    func setCellData(){
        
        arrReason = NSMutableArray()
        let dict1 = NSMutableDictionary()
        dict1.setValue(false, forKey: "isSelected")
        dict1.setValue("reason_complicated".localized, forKey: "reason")
        dict1.setValue(CellType.NormalCell, forKey: "cellType")
        arrReason.add(dict1)
        let dict2 = NSMutableDictionary()
        dict2.setValue(false, forKey: "isSelected")
        dict2.setValue("reason_another_provider".localized, forKey: "reason")
        dict2.setValue(CellType.NormalCell, forKey: "cellType")
        arrReason.add(dict2)
        let dict3 = NSMutableDictionary()
        dict3.setValue(false, forKey: "isSelected")
        dict3.setValue("reason_security".localized, forKey: "reason")
        dict3.setValue(CellType.NormalCell, forKey: "cellType")
        arrReason.add(dict3)
        let dict4 = NSMutableDictionary()
        dict4.setValue(false, forKey: "isSelected")
        dict4.setValue("reason_expensive".localized, forKey: "reason")
        dict4.setValue(CellType.NormalCell, forKey: "cellType")
        arrReason.add(dict4)
        let dict5 = NSMutableDictionary()
        dict5.setValue(false, forKey: "isSelected")
        dict5.setValue("reason_other".localized, forKey: "reason")
        dict5.setValue(CellType.OtherCell, forKey: "cellType")
        arrReason.add(dict5)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- SET UP UI
    func SetUpUI(){
        self.lblTitle.text = "first_title".localized
       // self.lblSecondTitle.text = "second_title".localized
       //self.borderView.layer.borderColor = UIColor.lightGray.cgColor
       // self.borderView.layer.borderWidth = 1.0
       //self.borderView.layer.cornerRadius = 5.0
        self.lblAttationTitle.text = "attentation_title".localized
        alertMessage = "attentation_desc".localized
        alertMessage = alertMessage.replacingOccurrences(of: "YOURTAXI", with: "<i>YOUR</i><b>TAXI</b>")
        self.self.lblAttationDesc.attributedText = stringFromHtml(string: alertMessage, fontSize: "14", colorType: "#A9A9A9")
        self.attentationView.layer.borderColor = UIColor.lightGray.cgColor
        self.attentationView.layer.borderWidth = 1.0
        self.attentationView.layer.cornerRadius = 5.0
        self.btnConfirm.setTitle("Delete".localized, for: .normal)
        self.title = "DELETE PROFILE".localized
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        
    }
    
    //MARK:- BUTTON ACTIONS METHODS
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        
        var reason = ""
        var otherText = ""
        if(self.currentIndexPath == nil){
            self.showSimpleAlertController(nil, message: "validation_select_one".localized)
            return;
        }
        if let arrObj = arrReason[self.currentIndexPath.row] as? NSDictionary{
            
            if(arrObj.value(forKey: "cellType") as? CellType == CellType.OtherCell){
                let currentCell = tblView.cellForRow(at: self.currentIndexPath) as? OtherReasonCell
                if(currentCell?.reasonTxtView.text.isEmpty)!{
                    self.showSimpleAlertController(nil, message: "validation_empty".localized)
                    return;
                }else{
                    reason = "Other"
                    otherText = (currentCell?.reasonTxtView.text)!
                }
            }else{
                let currentCell = tblView.cellForRow(at: self.currentIndexPath) as? DefaultReasonCell
                reason = (currentCell?.lblReason.text!)!
            }
        }
        if(checkConnection()){
            
            let alertController = UIAlertController(title: nil, message: "msg_delete_account".localized, preferredStyle: .alert)
            
            let yesAction = UIAlertAction(title: "Confirm".localized, style: .default, handler: { (action) in
                self.deleteProfileApi(reason: reason, otherText: otherText)
            })
            let noAction = UIAlertAction(title: "Cancel".localized, style: .default, handler: nil)
            
            
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            hideHUD()
            showAlertForInternetConnectionOff()
        }
        
    }
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TABLEVIEW DELEGATE/DATASOURCE METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReason.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let arrObj = arrReason[indexPath.row] as! NSDictionary
        if(self.currentIndexPath != nil){
            if (self.currentIndexPath.row != indexPath.row){
                arrObj.setValue(false, forKey: "isSelected")
            }
        }
        
        if(arrObj.value(forKey: "cellType") as? CellType == CellType.NormalCell){
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultReasonCell") as? DefaultReasonCell
            cell?.setUpCellData(dict: arrObj)
            
            cell?.onRadioPress = {(sender) in
                self.selectSingleReason(indexPath: indexPath)
            }
            return cell!
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherReasonCell") as? OtherReasonCell
            cell?.setUpCellData(dict: arrObj)
            
            cell?.onRadioPress = {(sender) in
                self.selectSingleReason(indexPath: indexPath)
            }
            
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectSingleReason(indexPath: indexPath)
    }
    
    //MARK:- SELECT REASON METHOD
    func selectSingleReason(indexPath:IndexPath){
        let arrObj = arrReason[indexPath.row] as! NSMutableDictionary
        arrObj.setValue(true, forKey: "isSelected")
        self.currentIndexPath = indexPath
        self.tblView.reloadData()
    }
    
    //MARK:- WEB SERVICE METHODS
    func deleteProfileApi(reason:String,otherText:String){
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "UserId": riderID,
                "Reason" : reason,
                "UpdatedBy" : riderID
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            printJsonStringLog(apiName: DELETE_USER_PROFILE, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: DELETE_USER_PROFILE, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController(nil, message: (error?.localized)!)
                    return;
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                
                let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                
                if let dict = responseContent{
                    
                    if let value = dict.value(forKey: "Status") as? Bool{
                        
                        if (value == true){
                            
                            let alertController: UIAlertController = UIAlertController(title: "Success".localized as String, message: dict.value(forKey: "Message") as? String ?? "", preferredStyle: .alert)
                            
                            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .default) { action -> Void in
                                
                                UserDefaults.standard.set(false, forKey: KRIDER_LOGIN)
                                UserDefaults.standard.removeObject(forKey: KUSER_DETAILS)
                                UserDefaults.standard.removeObject(forKey: KRIDE_DETAILS)
                                UserDefaults.standard.set("", forKey: KRIDER_USERNAME)
                                UserDefaults.standard.synchronize()
                                let appdel = UIApplication.shared.delegate as! AppDelegate
                                self.firbaseAuthSignOut()
                                let storyboard = UIStoryboard(name: "Rider", bundle: nil)
                                let loginVC = storyboard.instantiateViewController(withIdentifier: "RiderLoginViewController") as! RiderLoginViewController
                                let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
                                appdel.homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                let nvc: UINavigationController = UINavigationController(rootViewController: loginVC)
                                let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
                                slideMenuController.delegate = appdel.homeViewController
                                slideMenuController.automaticallyAdjustsScrollViewInsets = true;                                    slideMenuController.slideMenuController()?.changeLeftViewWidth((appdel.window?.bounds.width)!-90)
                                slideMenuController.removeLeftGestures()
                                appdel.window?.backgroundColor = UIColor(red: 0.0, green: 149.0, blue: 120.0, alpha: 1.0)
                                appdel.window?.rootViewController = slideMenuController
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            if let isDeleted = dict.value(forKey: "flag") as? Int{
                                if(isDeleted == 1){
                                    self.moveToSingupController(message: dict.value(forKey: "Message") as? String ?? "")
                                }
                            }else{
                                self.showSimpleAlertController(nil, message: dict.value(forKey: "Message") as? String ?? "")
                            }
                        }
                    } else{
                        
                        self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
}

//MARK:- DEFAULT REASON TABLEVIEW CELL CLASS
class DefaultReasonCell : UITableViewCell{
    
    @IBOutlet var btnRadio: UIButton!
    @IBOutlet var lblReason: UILabel!
    var reasonText = ""
    var onRadioPress : ((UIButton) -> Void)? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func stringFromHtml(string: String) -> NSAttributedString? {
        do {
            
            let modifiedFont = NSString(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: 16; color:#009578; \">%@</span>" as NSString, string) as String
            let data = modifiedFont.data(using: String.Encoding.utf16, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                return str
            }
        } catch {
        }
        return nil
    }
    func setUpCellData(dict:NSDictionary){
        btnRadio.isSelected = dict.value(forKey: "isSelected") as! Bool
        reasonText = (dict.value(forKey: "reason") as? String)!
        reasonText = reasonText.replacingOccurrences(of: "YOURTAXI", with: "<i>YOUR</i><b>TAXI</b>")
        lblReason.attributedText = stringFromHtml(string: reasonText)
    }
    //MARK:- BUTTON ACTION METHODS
    @IBAction func btnRadioAction(_ sender: UIButton) {
        if(onRadioPress != nil){
            self.onRadioPress!(sender)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
//MARK:- OTHER REASON TABLEVIEW CELL CLASS
class OtherReasonCell : UITableViewCell{
    //MARK:- PROPERTY
    @IBOutlet var btnOtherRadio: UIButton!
    @IBOutlet var lblOtherReason: UILabel!
    @IBOutlet var reasonTxtView: UITextView!
    @IBOutlet var txtViewHeightConstraints: NSLayoutConstraint!
    var onRadioPress : ((UIButton) -> Void)? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- SETUP DATA
    func setUpCellData(dict:NSDictionary){
        btnOtherRadio.isSelected = dict.value(forKey: "isSelected") as! Bool
        lblOtherReason.text = dict.value(forKey: "reason") as? String
        self.reasonTxtView.layer.borderColor = UIColor.lightGray.cgColor
        self.reasonTxtView.layer.borderWidth = 1.0
        self.reasonTxtView.layer.cornerRadius = 5.0
        self.reasonTxtView.text = ""
        self.reasonTxtView.placeholder = "placeholder_reason".localized
        self.reasonTxtView.placeholderColor = UIColor.lightGray
        let isSel = dict.value(forKey: "isSelected") as! Bool
        if(isSel){
            self.reasonTxtView.isHidden = false
            txtViewHeightConstraints.constant = 130.0
        }else{
            self.reasonTxtView.isHidden = true
            txtViewHeightConstraints.constant = 0.0
        }
    }
    //MARK:- BUTTON ACTION METHODS
    @IBAction func btnRadioAction(_ sender: UIButton) {
        
        if(onRadioPress != nil){
            self.onRadioPress!(sender)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}



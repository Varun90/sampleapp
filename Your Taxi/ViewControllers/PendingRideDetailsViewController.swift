
import UIKit

class PendingRideDetailsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView : UITableView!
    
    var infoTitleArray = NSMutableArray()
    var infoValuesArray = NSMutableArray()
    
    var detailDictionary = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInfoTripData()
        // tableView.estimatedRowHeight = 50
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(PendingRideDetailsViewController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        // Do any additional setup after loading the view.
        //        print("detail dic : \(detailDictionary)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "RIDE INFO".localized
    }
    
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoTitleArray.count + 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "PendingRideDetailsCell", for: indexPath) as! PendingRideDetailsCell
        cell.titleLabel.adjustsFontSizeToFitWidth = true
        if indexPath.row < infoTitleArray.count
        {
            cell.titleLabel.text = infoTitleArray[indexPath.row] as? String
            cell.valueLabel.text = infoValuesArray[indexPath.row] as? String
            cell.startBtn.isHidden = true
            cell.titleLabel.isHidden = false
            cell.valueLabel.isHidden = false
        }
        else if indexPath.row == infoTitleArray.count + 1
        {
            cell.titleLabel.isHidden = true
            cell.valueLabel.isHidden = true
            cell.startBtn.isHidden = false
            cell.startBtn.setTitle("Start".localized, for: .normal)
            cell.startBtn.addTarget(self, action: #selector(PendingRideDetailsViewController.startRideButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        }
        else
        {
            cell.titleLabel.isHidden = true
            cell.valueLabel.isHidden = true
            cell.startBtn.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row < infoTitleArray.count
        {
            let inputStr : NSString = infoValuesArray.object(at: (indexPath as NSIndexPath).row) as! NSString
            let cellSize = getTextSize(inputStr, fontSize: CGFloat(17.0), contrainSize: CGSize(width: tableView.frame.size.width - 180, height: CGFloat(Float.greatestFiniteMagnitude)))
            if cellSize.height>40{
                return cellSize.height
            }
            else
            {
                return 40
            }
        }
        return 40
    }
    
    func startRideButtonClicked(_ sender : UIButton)
    {
        
        if(checkConnection()){
            
            var pushTokenString = "1234"
            pushTokenString = (getDecryptedData(key: kPUSH_TOKEN) as NSString) as String
            guard let bookingID = detailDictionary.value(forKey: "BookingId") as? NSInteger else {
                return
            }
            
            let parameters = [
                "BookingId":bookingID,
                "DriverId":getLoginDriverID(),
                "DeviceToken":DEVICE_UUID,
                "PushToken":pushTokenString
                ] as [String : Any]
            
            self.showHUD()
            
            let Request: WebserviceHelper = WebserviceHelper()
            
            let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
            
            Request.CustomHTTPPostJSONWithHeader(url: KSTART_RIDE, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
                
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                } else {
                    //  : Handle Driver Logout
                    let responseObject = getResponseDictionaryWithResult(result: result)
                    print(responseObject)
                    
                    guard let status = responseObject.value(forKey: "Status") as? Bool else {
                        
                        if let _ = responseObject["Message"] as? NSString {
                            let rideInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "RideInfoViewController") as? RideInfoViewController
                            rideInfoVC?.currentTripInfo = self.detailDictionary.mutableCopy() as! NSMutableDictionary
                            self.navigationController?.pushViewController(rideInfoVC!, animated: true)
                        }
                        return
                    }
                    
                    if status == false {
                        if let message = responseObject["Message"] as? NSString {
                            self.showSimpleAlertController(nil, message: message as String)
                        }
                    } else {
                        let rideInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "RideInfoViewController") as? RideInfoViewController
                        rideInfoVC?.currentTripInfo = self.detailDictionary.mutableCopy() as! NSMutableDictionary
                        self.navigationController?.pushViewController(rideInfoVC!, animated: true)
                    }
                }
            }
        } else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func setInfoTripData()
    {
        infoTitleArray.add("Date & Time:".localized)
        infoTitleArray.add("Order Type:".localized)
        infoTitleArray.add("From:".localized)
        infoTitleArray.add("To:".localized)
        
        if ((detailDictionary.value(forKey: "RideType") as? NSInteger) == 1) {//Hourly
            infoTitleArray.add("Selected hour & km:".localized)
        }
        else
        {
            infoTitleArray.add("Distance / Duration:".localized)
        }
        infoTitleArray.add("Category:".localized)
        infoTitleArray.add("Bookings Nr.:".localized)
        infoTitleArray.add("Payment Type:".localized)
        infoTitleArray.add("Passenger:".localized)
        infoTitleArray.add("NotesMessage".localized)

        var pickUpDate = "00-00-0000 00:00:00"
        
        if let pTime = detailDictionary["BookingDateTime"] as? String {
            pickUpDate = pTime
        }
        
        infoValuesArray.add(pickUpDate)
        
        if detailDictionary["BookingType"] as? Int == 2 {
            infoValuesArray.add("Pre-Booking".localized)
        }else {
            infoValuesArray.add("Instant Booking".localized)
        }
        
        if let value = detailDictionary.value(forKey: "PickupLocation") as? String {
            infoValuesArray.add(value)
        }
        else {
            infoValuesArray.add("")
        }
        
        if let value = detailDictionary.value(forKey: "DropLocation") as? String {
            infoValuesArray.add(value)
        }
        else {
            infoValuesArray.add("")
        }
        
        if ((detailDictionary.value(forKey: "RideType") as? NSInteger) == 1) {//Hourly
            
            if let hours = detailDictionary.value(forKey: "RideHours") as? NSInteger {
                
                let inclusiveKm = getInclusiveKM("\(hours)")
                let hoursSubStr = (hours > 1) ? "Hours" : "Hour"
                let final_value = "\(hours) " + "\(hoursSubStr)".localized + " (" + "Incl.".localized + " \(inclusiveKm))"
                infoValuesArray.add(final_value)
            }
        }
        else
        {
            var totalKM = " "
            var totalTime = " "
            
            if let tkm = detailDictionary.value(forKey: "TotalKm") as? NSInteger {
                totalKM = "\(tkm)"
            }
            
            if let tKm = detailDictionary.value(forKey: "TotalKm") as? NSString {
                totalKM = tKm as String
            }
            
            if let tTime = detailDictionary.value(forKey: "TotalTime") as? NSInteger {
                totalTime = "\(tTime)"
            }
            
            if let tTime = detailDictionary.value(forKey: "TotalTime") as? NSString {
                totalTime = tTime as String
            }
            
            infoValuesArray.add(NSString(format: "\(totalKM) km/ \(totalTime)" as NSString))
        }
        
        if let value = detailDictionary.value(forKey: "CarTypeId") as? NSInteger {
            
            var car : String = ""
            if value == 1 {
                car = "Budget"
            }
            else if value == 2 {
                car = "Luxury"
            }
            else {
                car = "Van"
            }
            
            infoValuesArray.add(car)
        }
        else {
            infoValuesArray.add("")
        }
        
        
        if let value = detailDictionary.value(forKey: "BookingId") as? NSInteger {//id
            infoValuesArray.add("\(value)")
        }
        else {
            infoValuesArray.add("")
        }
       
        var updatedPaymentTypeValue: String
        
        if let value = detailDictionary.object(forKey: "PaymentType") as? NSInteger  {
            updatedPaymentTypeValue = (value == 1) ? "Cash" : "Card"
            infoValuesArray.add(updatedPaymentTypeValue.capitalizingFirstLetter().localized)
        }
        else {
            infoValuesArray.add("Card".capitalizingFirstLetter().localized)
        }
       
        var passengerName = ""
        if let value = (detailDictionary.object(forKey: "RiderDetails") as! NSDictionary).value(forKey: "FirstName") as? String {
            passengerName = value;
        }
        if let value = (detailDictionary.object(forKey: "RiderDetails") as! NSDictionary).value(forKey: "LastName") as? String {
            passengerName = "\(passengerName) \(value)";
        }
        infoValuesArray.add(passengerName)
        
        if let value = detailDictionary.value(forKey: "DriverNotes") as? String {
            let valueSTr = value.trimmingCharacters(in: .whitespaces)
            if valueSTr == ""
            {
                infoTitleArray.removeLastObject()
            }
            else
            {
                infoValuesArray.add(value)
            }
        }else {
            infoTitleArray.removeLastObject()
        }
    }
}


import UIKit

class CurrentRidesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate,UITextFieldDelegate,CurrentRideDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    func setLocationData(strLocation: String?, type: String) {
        var pickLocation = ""
        var dropLocation = ""
        var pickUpDate = ""
        if type == "edit_pickup_address"{
            pickLocation = strLocation ?? ""
            dropLocation = currentRideData?.value(forKey: "DropLocation") as? String ?? ""
            pickUpDate = currentRideData?.value(forKey: "BookingDateTime") as? String ?? ""
        }else if (type == "edit_dropup_address"){
            pickLocation = currentRideData?.value(forKey: "PickupLocation") as? String ?? ""
            dropLocation = strLocation ?? ""
            pickUpDate = currentRideData?.value(forKey: "BookingDateTime") as? String ?? ""
        }
        let carType = currentRideData?.value(forKey: "CarTypeId") as? Int32
        let planId = currentRideData?.value(forKey: "RentalPlanId") as? Int32 ?? 0
        self.editPrebookingApi(date: pickUpDate, pickupAddress: pickLocation, dropupAddress: dropLocation, carTypeId: carType!, rentalPlanId: planId, IsRentalplan: false)
    }
    @IBOutlet weak var tableView : UITableView!
    var datePicker : UIDatePicker!
    var categoryPicker : UIPickerView!
    var editOptionArray = NSMutableArray()
    var hourlyRatePicker : UIPickerView!
    var titleArray = NSMutableArray()
    var titleImageArray = [String]()
    var valuesArray = NSMutableArray()
    var currentRideData : NSMutableDictionary?
    var preBookingObject : PreBookingListObject?
    var resultArray: NSMutableArray = []
    var hourlyRateArray: NSMutableArray = []
    var carCategoryArray: NSMutableArray = []
    @IBOutlet var txtField: UITextField!
    var payment_Type: Int32?
    var carTypeId: Int32?
    var resRideType: Int32?
    var rideType: Int32?
    var Car_TypeName: String? = ""
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnCancel: UIButton!
    var isCategoryPickerSelected : Bool = false
    var isHourlyPickerSelected : Bool = false
    //let carCategoryArray = ["YourBudget", "YourLuxury", "YourVan"]
    @IBOutlet var optionsViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("currentRideData:", currentRideData ?? "")
        txtField.isHidden = true
        tableView.isHidden = true
        self.txtField.delegate = self
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        self.slideMenuController()?.removeLeftGestures()
        setCurrentRideData()
        optionsViewHeight.constant = 0
        if let isDelete = self.currentRideData?.value(forKey: "canUpdate") as? Bool {
            optionsViewHeight.constant = isDelete ? 44 : 44
        }
        GetCarTypeDetails()
        let carTypeId = currentRideData?.value(forKey: "CarTypeId") as? Int32
        GetPriceofHourlyDetails(carType: carTypeId ?? 0)
    }
    
    func swiped(_ gesture: UIGestureRecognizer)
    {
        self.view.removeGestureRecognizer(gesture)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Trip details".localized
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SET RIDE DATA
    func setCurrentRideData() {
        
        titleArray.removeAllObjects()
        titleImageArray.removeAll()
        valuesArray.removeAllObjects()
        tableView.isHidden = false
        titleImageArray = ["ride_booking","ride_date&time","ride_from","ride_to","ride_category","ride_order","ride_distance","ride_price"]
        titleArray = NSMutableArray()
        titleArray.add("Booking Nr.:".localized)
        titleArray.add("Date & Time:".localized)
        titleArray.add("From:".localized)
        titleArray.add("To:".localized)
        titleArray.add("Category:".localized)
        titleArray.add("Payment Type:".localized)
        self.rideType = currentRideData?.value(forKey: "RideType") as! Int32
        if self.rideType == 2 {
            titleArray.add("Distance & Duration:".localized)
        }
        else
        {
            titleArray.add("Distance / Duration:".localized)
        }
        titleArray.add("Price:".localized)
        valuesArray = NSMutableArray()
        let bookId = String(currentRideData?.value(forKey: "BookingId") as! Int64)
        valuesArray.add(bookId)
        var pickUpDate = currentRideData?.value(forKey: "BookingDateTime") as! String
        if (pickUpDate).isEmpty{
            pickUpDate = ""
        }
        else{
            pickUpDate = changeDateForamteWebToApp(pickUpDate)
        }
        valuesArray.add(pickUpDate)
        valuesArray.add(currentRideData?.value(forKey: "PickupLocation") as! String)
        valuesArray.add(currentRideData?.value(forKey: "DropLocation") as! String)
        if(currentRideData!["CarType"] != nil){
            valuesArray.add(currentRideData?.value(forKey: "CarType") as! String)
        }
        else{
            valuesArray.add(self.Car_TypeName)
        }
        if(currentRideData!["Payment"] != nil){
            valuesArray.add(currentRideData?.value(forKey: "Payment") as! String)
        }
        if(currentRideData!["PaymentType"] != nil){
            var type = currentRideData?.value(forKey: "PaymentType") as! Int32
            valuesArray.add(type == 1 ? "Cash" : "Card")
        }
        if  self.rideType == 1 {
            var hours = 0
            if currentRideData!["AdditionalRef"] != nil {
                hours = Int((currentRideData?.value(forKey: "AdditionalRef") as! NSDictionary).value(forKey: "PlanHour") as? Int32 ?? 0)
            }
            var inclusiveKm = 0
            if currentRideData!["AdditionalRef"] != nil {
                let value = (currentRideData?.value(forKey: "AdditionalRef") as! NSDictionary).value(forKey: "PlanKm") as? Int32 ?? 0
                inclusiveKm = Int(value)
            }
            let hoursInt = Int32(hours)
            if(hoursInt>1){
                let final_value = "\(hours) " + "Hours".localized + " (" + "Incl.".localized + " \(inclusiveKm) km)"
                valuesArray.add(final_value)
            }
            else{
                let final_value = "\(hours) " + "Hour".localized + " (" + "Incl.".localized + " \(inclusiveKm) km)"
                valuesArray.add(final_value)
            }
        }
        else
        {
            let totalKms = String(currentRideData?.value(forKey: "TotalKm") as! Int)
            let totalTimes = String("\(currentRideData?.value(forKey: "TotalTime") as! String)")
            valuesArray.add(NSString(format: "%@ km / %@ ",  totalKms, totalTimes!))
        }
        valuesArray.add("CHF \(currentRideData?.value(forKey: "FinalPrice") as? String ?? "00.00")")
        tableView.reloadData()
    }
    
    //MARK:- UITableview Data source and Delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if titleArray.count != 0
        {
            return titleArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentRidesCell", for: indexPath) as! CurrentRidesCell
        drawShadow(cell.shadowView)
        cell.titleImageview.image = UIImage.init(named: titleImageArray[indexPath.row] as String)
        cell.titleLabel.text = (titleArray[indexPath.row] as? String)?.localized
        
        cell.valueLabel.text = (valuesArray[indexPath.row] as? String)?.localized
        
        if (currentRideData?.value(forKey: "isEditable") as? Bool) != nil{
            if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4 {
                
                cell.btnEdit.isHidden = true
                
                if let Isupdate = self.currentRideData?.value(forKey: "canUpdate") as? Bool {
                    cell.btnEdit.isHidden = Isupdate ? false : true
                }
                
                cell.onEditPress = {(sender) in
                    
                    if(indexPath.row == 1){
                        self.txtField.becomeFirstResponder()
                    }else if(indexPath.row == 2){
                        self.editAddress("edit_pickup_address", withAddress: cell.valueLabel.text ?? "", locationType: "")
                    }else if(indexPath.row == 3){
                        self.editAddress("edit_dropup_address", withAddress: cell.valueLabel.text ?? "", locationType: "drop_up")
                    }
                    else if(indexPath.row == 4){
                        self.isCategoryPickerSelected = true
                        self.txtField.becomeFirstResponder()
                    }
                    
                }
            }else{
                cell.btnEdit.isHidden = true
            }
        }else{
            cell.btnEdit.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func editAddress(_ type:String, withAddress selectedAddress:String,locationType:String){
        let storyboard = UIStoryboard(name: "Rider", bundle: nil)
        let addLocationVC = storyboard.instantiateViewController(withIdentifier: "AddLocationViewController") as? AddLocationViewController
        addLocationVC?.locationType = locationType
        addLocationVC?.addressType = type
        addLocationVC?.selectedAddress = selectedAddress
        addLocationVC?.locationDelegate = self
        self.navigationController?.pushViewController(addLocationVC!, animated: true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if isCategoryPickerSelected{
             self.pickCategoryRate(txtField)
        }else if isHourlyPickerSelected{
            self.pickHourlyRate(txtField)
        }else{
             self.pickupDate(txtField)
        }
    }
    
    //MARK:- DATA PICKER
    func pickupDate(_ textField:UITextField){
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        
        if let selectedFormate = UserDefaults.standard.value(forKey: "TimeFormate")
        {
            if "\(selectedFormate)" == "12"
            {
                self.datePicker.locale = Locale(identifier: "en_US")
            }
            else
            {
                self.datePicker.locale = Locale(identifier: "en_GB")
            }
        }
        else
        {
            self.datePicker.locale = Locale(identifier: "en_GB")
        }
        
        self.datePicker.minimumDate = Calendar.current.date(byAdding: .hour, value: 1, to: Date())
        txtField.inputView = self.datePicker
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = false
        toolBar.barTintColor = kLIGHT_OLIVE_COLOR
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtField.inputAccessoryView = toolBar
    }
    func doneClick() {
        
        if txtField.isFirstResponder{
            
            txtField.resignFirstResponder()
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "dd-MM-yyyy HH:mm:ss"
            let date = dateFormatter2.string(from: datePicker.date)
            print(date)
            
            let pickLocation = currentRideData?.value(forKey: "PickupLocation") as? String ?? ""
            let dropLocation = currentRideData?.value(forKey: "DropLocation") as? String ?? ""
            let carType = currentRideData?.value(forKey: "CarTypeId") as? Int32
            
            let planId = currentRideData?.value(forKey: "RentalPlanId") as? Int32 ?? 0
            print("planId:",planId)
            self.editPrebookingApi(date: date, pickupAddress: pickLocation, dropupAddress: dropLocation, carTypeId: carType!, rentalPlanId: planId, IsRentalplan: false)
        }
    }
    
    func cancelClick()
    {
        if txtField.isFirstResponder
        {
            txtField.resignFirstResponder()
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == categoryPicker {
            return carCategoryArray.count
        } else if pickerView == hourlyRatePicker{
            return hourlyRateArray.count
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == categoryPicker {
            let value = carCategoryArray[row] as! String
            return value.localized
        } else if pickerView == hourlyRatePicker{
            print("Hourly", hourlyRateArray)
            let value = hourlyRateArray[row] as! String
            return value.localized
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    func pickCategoryRate(_ textfield : UITextField)
    {
        categoryPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 216))
        categoryPicker.backgroundColor = .white
        categoryPicker.showsSelectionIndicator = true
        categoryPicker.delegate = self
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.barTintColor = kLIGHT_OLIVE_COLOR
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(categoryDoneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(categoryCancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textfield.inputView = categoryPicker
        textfield.inputAccessoryView = toolBar
    }
    
    func categoryDoneClick() {
        var pickUpDate = ""
        pickUpDate = currentRideData?.value(forKey: "BookingDateTime") as? String ?? ""
        let pickLocation = currentRideData?.value(forKey: "PickupLocation") as? String ?? ""
        let dropLocation = currentRideData?.value(forKey: "DropLocation") as? String ?? ""
        let planId = currentRideData?.value(forKey: "RentalPlanId") as? Int32 ?? 0
        if txtField.isFirstResponder{
            txtField.resignFirstResponder()
        }
        isCategoryPickerSelected = false
        let selectedIndex = categoryPicker.selectedRow(inComponent: 0)
        let value : String = carCategoryArray[selectedIndex] as! String
        print(value)
        print("carTy:", selectedIndex + 1)
        if self.rideType == 1 {
        self.isHourlyPickerSelected = true
        self.txtField.becomeFirstResponder()
        }else{
            self.editPrebookingApi(date: pickUpDate, pickupAddress: pickLocation, dropupAddress: dropLocation, carTypeId: selectedIndex + 1, rentalPlanId: planId, IsRentalplan: false)
        }
    }
    
    func categoryCancelClick()
    {
        if txtField.isFirstResponder {
            txtField.resignFirstResponder()
        }
        isCategoryPickerSelected = false
    }
    
    func pickHourlyRate(_ textfield : UITextField)
    {
        hourlyRatePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 216))
        hourlyRatePicker.backgroundColor = .white
        hourlyRatePicker.showsSelectionIndicator = true
        hourlyRatePicker.delegate = self
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.barTintColor = kLIGHT_OLIVE_COLOR
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(hourlyDoneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(hourlyCancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textfield.inputView = hourlyRatePicker
        textfield.inputAccessoryView = toolBar
    }
    func hourlyDoneClick() {
        var pickUpDate = ""
        pickUpDate = currentRideData?.value(forKey: "BookingDateTime") as? String ?? ""
        let pickLocation = currentRideData?.value(forKey: "PickupLocation") as? String ?? ""
        let dropLocation = currentRideData?.value(forKey: "DropLocation") as? String ?? ""
      //  let carType = currentRideData?.value(forKey: "CarTypeId") as? Int32 ?? 1
        if txtField.isFirstResponder{
            txtField.resignFirstResponder()
        }
        isHourlyPickerSelected = false
        let categoryselectedIndex = categoryPicker.selectedRow(inComponent: 0)
        let selectedIndex = hourlyRatePicker.selectedRow(inComponent: 0)
        let value : String = hourlyRateArray[selectedIndex] as! String
        print(value)
        print("carTy:", selectedIndex + 1)
        self.editPrebookingApi(date: pickUpDate, pickupAddress: pickLocation, dropupAddress: dropLocation, carTypeId: categoryselectedIndex + 1, rentalPlanId: selectedIndex + 1, IsRentalplan: true)
    }
    func hourlyCancelClick()
    {
        if txtField.isFirstResponder {
            txtField.resignFirstResponder()
        }
        isHourlyPickerSelected = false
    }
    
    //MARK:- API CALLING
    func editPrebookingApi(date:String,pickupAddress:String,dropupAddress:String,carTypeId:Int32, rentalPlanId: Int32, IsRentalplan: Bool ){
        var dateBooking = date
        if(checkConnection()){
            if(dateBooking == ""){
                let BookingDate = currentRideData?.value(forKey: "BookingDateTime") as? String ?? ""
            }else{
                dateBooking = date
            }
            let rideID = currentRideData?.value(forKey: "BookingId") as? Int32
            self.resRideType = currentRideData?.value(forKey: "RideType") as? Int32
            let types = currentRideData?.value(forKey: "Payment") as? String ?? ""
            let BookingDate = currentRideData?.value(forKey: "BookingDateTime") as? String ?? ""
            if(types == "Card"){
                self.payment_Type = 2
            }
            else{
                self.payment_Type = 1
            }
            var selectedHour = rentalPlanId == 0 ? String("") : String(rentalPlanId)
            if(IsRentalplan){
                if(selectedHour != ""){
                    if carTypeId == 2{
                        if selectedHour == "10"{
                            selectedHour = "20"
                        }else{
                            selectedHour = "1"+selectedHour
                            print("selectedHour1:", selectedHour)
                        }
                    }else if carTypeId == 3{
                        if selectedHour == "10"{
                            selectedHour = "30"
                        }else{
                            selectedHour = "2" + selectedHour
                            print("selectedHour:", selectedHour)
                        }
                    }
                }
            }
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [String : Any]()
            parameters = [
                "BookingId" : String(rideID!),
                "RiderId" : riderID,
                "CarTypeId": carTypeId,
                "PickupLocation":pickupAddress,
                "PickupLatitude":"",
                "PickupLongitude":"",
                "DropLocation":dropupAddress,
                "DropLatitude":"",
                "DropLongitude":"",
                "BookingDateTime": dateBooking,
                "RideType": self.resRideType,
                "RentalPlanId": selectedHour == "" ? String("") : Int32(selectedHour),
                "PaymentType": self.payment_Type,
                "BookingFrom": 1]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: EDIT_PREBOOKING_DETAILS_URL, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: EDIT_PREBOOKING_DETAILS_URL, jsonString: jsonParam as NSString,withIdicator: false, header: headerDict) { (result, error) in
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Error", message: error?.localized ?? "")
                    return;
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                let responseDict = self.convertStringToDictionary(text: decryptedData! as String)
                if responseDict!.value(forKey: "Status") as! Bool == false
                {
                    if responseDict!["Errors"] != nil{
                        let ErrorDis = responseDict!.value(forKey: "Errors") as? NSDictionary
                        self.showSimpleAlertController(nil, message:  "Can't updated")
                    }else{
                        self.showSimpleAlertController(nil, message: responseDict!.value(forKey: "Message") as? String ?? "")
                    }
                    
                }else{
                    
                    self.showSimpleAlertControllerWithOkAction(nil, message: responseDict!.value(forKey: "Message") as? String ?? "", callback: {
                        self.preBookingListApi()
                    })
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    func preBookingListApi(){
        
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            let bookId = String(currentRideData?.value(forKey: "BookingId") as! Int64)
            var parameters = [String : Any]()
            parameters = [
                "BookingType" : 2,
                "RiderId" : riderID,
                "BookingId" : bookId
            ]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: PREBOOKING_LIST, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: PREBOOKING_LIST, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if(error != nil){
                    print(error?.localized ?? "")
                    self.showSimpleAlertController(nil, message: error?.localized ?? "")
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                guard let data = responseObject.value(forKey: "data") as? String else {
                    self.showSimpleAlertController(nil, message: "Something Went Wrong")
                    self.hideHUD()
                    return
                }
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                print("PreBooking List :", decryptedData ?? "")
                if let list = self.convertToDictionary(text: decryptedData!) as? [[String:Any]] {
                    self.resultArray = NSMutableArray()
                    for data in list {
                        self.resultArray.add(data as [String:Any])
                    }
                    if self.resultArray.count > 0{
                        for dict in self.resultArray
                        {
                            self.preBookingObject!.setObjectData(dict: dict as! NSDictionary)
                            self.currentRideData?.removeAllObjects()
                            self.currentRideData = NSMutableDictionary(dictionary: self.preBookingObject!.getObjectData())
                            print(self.currentRideData!)
                            self.setCurrentRideData()
                        }
                    }
                }
            }
        }else{
            self.hideHUD()
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func GetPriceofHourlyDetails(carType: Int32)
    {
        if(checkConnection()){
            var parameters = [
                "CarTypeId" : carType,
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let Request: WebserviceHelper = WebserviceHelper()
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KRIDER_RENTAL_PLAN, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_RENTAL_PLAN, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                print("Result :", result)
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                guard let data = responseObject.value(forKey: "data") as? String else {
                    self.showSimpleAlertController(nil, message: "Something Went Wrong")
                    self.hideHUD()
                    return
                }
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                
                let responseContent = convertStringToArray(text: decryptedData as! String)
                
                print("GetPriceofHourly :", responseContent ?? "")
                if responseContent != nil{
                    DispatchQueue.main.async {
                        //"1 H. (Incl. 30 km)"
                        self.hourlyRateArray = NSMutableArray()
                        for data in responseContent as! [[String:Any]]{
                            let fixedHR = data["FixedHour"] as? Int ?? 0
                            let fixedKM = data["FixedKm"] as? Int32 ?? 0
                            let hourly = "\(fixedHR) \("h".localized). (\("Incl.".localized) \(fixedKM) km)"
                            self.hourlyRateArray.add(hourly)
                        }
                        print("hourlyRateArray :",self.hourlyRateArray)
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    func GetCarTypeDetails()
    {
        if(checkConnection()){
            var parameters = [
                "Status" : 1,
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let Request: WebserviceHelper = WebserviceHelper()
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KCARTYPE_LIST, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KCARTYPE_LIST, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                print("Result :", result)
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                guard let data = responseObject.value(forKey: "data") as? String else {
                    self.showSimpleAlertController(nil, message: "Something Went Wrong")
                    self.hideHUD()
                    return
                }
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                
                let responseContent = convertStringToArray(text: decryptedData as! String)
                
                print("GetCarTypes :", responseContent ?? "")
                if responseContent != nil{
                    DispatchQueue.main.async {
                        self.carCategoryArray = NSMutableArray()
                        for data in responseContent as! [[String:Any]]{
                            let type = data["CarType"] as? String ?? ""
                            print("type :", type)
                            self.carCategoryArray.add(type)
                        }
                         print("CarTypes :", self.carCategoryArray)
                    }
                }
               
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    // MARK: - Actions Methods
    func backAction(){
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
        
    }
    
    @IBAction func cancelPressed(_ sender : UIButton) {
        self.backAction()
    }
    
    @IBAction func deletePressed(_ sender : UIButton) {
        let alertController: UIAlertController = UIAlertController(title: nil, message: "Are you sure you want to CancelRide?".localized, preferredStyle: .alert)
        
        let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
            
            let bookId = self.currentRideData?.value(forKey: "BookingId") as? Int64
            self.deletePreBookingApi(bookingId: bookId!)
        }
        
        let noAction : UIAlertAction = UIAlertAction(title: "No".localized, style: .default, handler: nil)
        
        
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deletePreBookingApi(bookingId:Int64){
        
        if(checkConnection()){
            
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [String : Any]()
            parameters = [
                "UserId" : riderID,
                "BookingId" : bookingId,
            ]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KCANCELRIDE_URL, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KCANCELRIDE_URL, jsonString: jsonParam as NSString, withIdicator: false, header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    if let value = responseContent?.value(forKey: "Status") as? Bool{
                        if value == true{
                            self.showSimpleAlertControllerWithOkAction(nil, message: responseContent!.value(forKey: "Message") as? String ?? "", callback: {
                                self.backAction()
                            })
                        }else{
                            self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                        }
                        
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
}



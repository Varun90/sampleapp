import UIKit
import IQKeyboardManagerSwift

class AddTipViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var outerView : UIView!
    @IBOutlet weak var tipView : UIView!
    @IBOutlet weak var tipTextField : UITextField!
    @IBOutlet weak var addTipLabel : UILabel!
    @IBOutlet weak var submitBtn : UIButton!
    @IBOutlet weak var cancelBtn : UIButton!
    var currentRideData : NSMutableDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        tipTextField.placeholder = "0.00"
        tipTextField.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)
        tipTextField.delegate = self
        tipTextField.becomeFirstResponder()
        self.outerView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        tipView.layer.masksToBounds = true
        tipView.layer.cornerRadius = 3.0
        submitBtn.setTitle("Send".localized, for: .normal)
        cancelBtn.setTitle("Back".localized, for: .normal)
        addTipLabel.text = "Tip".localized
        setScreenLayout()
        tipTextField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setScreenLayout() {
        submitBtn.backgroundColor = kBUTTON_OLIVE_GREEN_COLOR
        submitBtn.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        submitBtn.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        
        cancelBtn.backgroundColor = kBUTTON_OLIVE_GREEN_COLOR
        cancelBtn.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
        cancelBtn.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let characterLimit = 6
        let newText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        let numberOfChars = newText.characters.count
        return numberOfChars < characterLimit
    }
    @IBAction func submitClicked(_ sender: UIButton) {
        if tipTextField.text?.trimmingCharacters(in: .whitespaces).characters.count != 0
        {
            addTip(tipTextField.text!)
        }
        else
        {
            self.showSimpleAlertController(nil, message: "Please Add Tip.".localized)
        }
    }
    
    @IBAction func cancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addTip(_ tip : String)
    {
        let bookingId = ((currentRideData?.value(forKey: "RideDetails") as? NSDictionary)?.value(forKey: "BookingId") as! Int32)
        let RiderIdAPI = ((currentRideData?.value(forKey: "RideDetails") as? NSDictionary)?.value(forKey: "RiderId") as! Int32)
        var parameters = [
            "BookingId" : bookingId,
            "RiderId" : RiderIdAPI,
            "Tip": systemLanguage == "de" ? tip.replacingOccurrences(of: ",", with: ".") : tip
            ] as [String : Any]
        let jsonString = returnJsonString(param: parameters)
        print(jsonString)
        var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
        parameters = ["data":encryptedData!]
        let jsonParam = returnJsonString(param: parameters)
        if checkConnection(){
            self.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            printJsonStringLog(apiName: KADDRIDETIP_URL, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KADDRIDETIP_URL, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                }else{
                    let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                    let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                    if responseContent != nil
                    {
                        if responseContent?.value(forKey: "Status") as! Bool == true
                        {
                            let finalPriceStr = ((self.currentRideData?.value(forKey: "RideDetails") as? NSDictionary)?.value(forKey: "FinalPrice") as? String ?? "")
                            print(tip)
                            let newPrice = Double(systemLanguage == "de" ? tip.replacingOccurrences(of: ",", with: ".") : tip)! + Double(finalPriceStr)!
                            
                            let fp = self.currentRideData?.value(forKey: "RideDetails") as? NSMutableDictionary
                            fp?.setValue(String(Double(newPrice)), forKey: "FinalPrice")
                            
                            self.hideHUD()
                            let alert = UIAlertController(
                                title: nil,
                                message: (responseContent?.value(forKey: "Message") as! String).localized,
                                preferredStyle: UIAlertControllerStyle.alert
                            )
                            let ok = UIAlertAction(
                                title: "Ok".localized,
                                style: UIAlertActionStyle.default,
                                handler: { action in
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            alert.addAction(ok)
                            UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
                        }
                        else {
                            if let message = responseContent?.value(forKey: "Message") as? String {
                                self.showSimpleAlertController(nil, message:message)
                            }
                            else{
                                self.showSimpleAlertController(nil, message:"The tip format is invalid.")
                            }
                        }
                    }
                    else
                    {
                        self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    }
                }
            }
        }
        else{
            showAlertForInternetConnectionOff()
        }
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//        let inverseSet = NSCharacterSet(charactersIn:"0123456789.").inverted
//        let components = string.components(separatedBy: inverseSet)
//        let filtered = components.joined(separator: "")
//        return string == filtered
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func myTextFieldDidChange(_ textField: UITextField) {
        
        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
        }
    }
}
extension String {
    
    // formatting text for currency textField
    func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        if #available(iOS 9.0, *) {
            formatter.numberStyle = .decimal
        } else {
            // Fallback on earlier versions
        }
       // formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        
        return formatter.string(from: number)!
    }
}


import UIKit
import IQKeyboardManagerSwift

class OrderRideViewController: BaseViewController, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet var hideTextField: UITextField!
    var pickUPLocation = SelectedLocation()
    var dropUPLocation = SelectedLocation()
    var carType : String = ""
    var totalPrice : String = ""
    var actualPrice : String = ""
    var driver_id : String = ""
    var bookTime : String = ""
    var rideWith : String = "kilometer"
    var resultArray: NSMutableArray = []
    var datePicker : UIDatePicker!
    var hourlyPicker : UIPickerView!
    var datePickerView : UIView!
    var hourlyTextField : UITextField!
    var selectedHour = 0
    var isKeyboardOpened = false
    var dateForRequest = " "
    //let hourlyRateArray = ["1 H. (Incl. 30 km)", "2 H. (Incl. 50 km)", "3 H. (Incl. 80 km)", "4 H. (Incl. 120 km)", "5 H. (Incl. 150 km)", "6 H. (Incl. 180 km)", "7 H. (Incl. 210 km)", "8 H. (Incl. 240 km)", "9 H. (Incl. 270 km)", "10 H. (Incl. 300 km)"]
    var driver_notes = "Type your text here...".localized
    var cellheight : CGFloat = 200
    var card_type = "cash"
    var isCardAvailable = "fail"
    var reachTimeToRider : String = ""
    var isRideBooked = false
    var forNowAvailable = false
    var cardIOVC : CardIOPaymentViewController!
    var rideOfferDict = [String:Any]()
    let RIDE_WITH_SELECTED_COLOR = UIColor(red: 59/255.0, green: 146/255.0, blue: 27/255.0, alpha: 1.0)
    let RIDE_WITH_UNSELECTED_COLOR = UIColor(red: 152/255.0, green: 194/255.0, blue: 33/255.0, alpha: 1.0)
    var isPrebook = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        self.slideMenuController()?.removeLeftGestures()
        self.title = "ORDER RIDE".localized
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        getCurrentDate()
        let backImage  = UIImage(named: "icn_topbar_back")
        let backButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setBackgroundImage(backImage, for: UIControlState())
        backButton.addTarget(self, action: #selector(OrderRideViewController.backAction), for: UIControlEvents.touchUpInside)
        let backBarButton = UIBarButtonItem(customView: backButton)
        self.navigationItem.leftBarButtonItem = backBarButton
        getFareEstimate(rideWith, hour: "0", driver_id: driver_id)
        getCardDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(OrderRideViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(OrderRideViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        GetPriceofHourlyDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CardIOUtilities.preload()
        if(KAPP_RIDER){
            locationManager1.stopUpdatingLocation()
        }
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if(KAPP_RIDER){
            locationManager1.startUpdatingLocation()
        }
    }
    func loadList(){
        forNowAvailable = false
        self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func getCardDetails()
    {
        if(checkConnection()){
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var parameters = [
                "UserId" : riderID,
                ] as [String : Any]
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KRIDER_CARD_DETAILS, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_CARD_DETAILS, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                
                let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                
                if responseContent?.value(forKey: "Status")as? Bool == true {
                    self.isCardAvailable = (responseContent?.value(forKey: "StripeCardId") as? String)!
                }else{
                    self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func GetPriceofHourlyDetails()
    {
        if(checkConnection()){
            var parameters = [
                "CarTypeId" : Int32(carType) ?? 0,
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let Request: WebserviceHelper = WebserviceHelper()
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KRIDER_RENTAL_PLAN, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KRIDER_RENTAL_PLAN, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                print("Result :", result)
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                guard let data = responseObject.value(forKey: "data") as? String else {
                    self.showSimpleAlertController(nil, message: "Something Went Wrong")
                    self.hideHUD()
                    return
                }
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                
                let responseContent = convertStringToArray(text: decryptedData as! String)
                
                print("GetPriceofHourly :", responseContent ?? "")
                if responseContent != nil{
                    DispatchQueue.main.async {
                        //"1 H. (Incl. 30 km)"
                        self.resultArray = NSMutableArray()
                        for data in responseContent as! [[String:Any]]{
                            let fixedHR = data["FixedHour"] as? Int ?? 0
                            let fixedKM = data["FixedKm"] as? Int32 ?? 0
                            let hourly = "\(fixedHR) \("h".localized). (\("Incl.".localized) \(fixedKM) km)"
                            self.resultArray.add(hourly)
                        }
                        print("resultArray :",self.resultArray)
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
    
    //MARK: Action methods
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK: - tableview datasource and delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // ----------- New code -----------
        if indexPath.row == 0
        {
            //Pick up cell
            let pickUplocationCell = tableView.dequeueReusableCell(withIdentifier: "PickUpLocationCell") as! PickUpLocationCell
            drawShadow(pickUplocationCell.shadowView)
            pickUplocationCell.setSelected(false, animated: false)
            
            pickUplocationCell.heading.text = "From:".localized
            pickUplocationCell.locationLabel.text = pickUPLocation.address.localized
            
            pickUplocationCell.lblDestinationTitle.text = "To:".localized
            pickUplocationCell.lblDestinationLocation.text = dropUPLocation.address.localized
            return pickUplocationCell
        }
            
        else if indexPath.row == 1
        {
            let priceDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PriceDetailsCell") as! PriceDetailsCell
            priceDetailsCell.message.text = "Price will change if you change destination.".localized
            priceDetailsCell.setSelected(false, animated: false)
            drawShadow(priceDetailsCell.shadowView)
            if rideWith == "hourly"
            {
                priceDetailsCell.priceHeading.text = "Hourly Price:".localized
            }
            else if rideWith == "kilometer"
            {
                if carType == "1"{
                    priceDetailsCell.priceHeading.text = "YourBudget Price:".localized
                }
                else if carType == "2"{
                    priceDetailsCell.priceHeading.text = "YourBusiness Price:".localized
                }
                else if carType == "3"{
                    priceDetailsCell.priceHeading.text = "YourComfort Price:".localized
                }
                else {
                    priceDetailsCell.priceHeading.text = "YourBudget Price:".localized
                }
                
            }
            priceDetailsCell.priceLabel.text = "\(actualPrice)".localized + " " + "CHF"
            
            return priceDetailsCell
        }
        else if indexPath.row == 2
        {
            let longTripCell = tableView.dequeueReusableCell(withIdentifier: "LongTripCell") as! LongTripCell
            drawShadow(longTripCell.shadowView)
            longTripCell.cellTitleLabel.text = "Long dist. shopping or meeting?".localized
            hourlyTextField = longTripCell.hourlyTextField
            return longTripCell
        }
        else if indexPath.row == 3
        {
            let noteToDriverCell = tableView.dequeueReusableCell(withIdentifier: "NoteToDriverCell" ) as! NoteToDriverCell
            self.isKeyboardOpened = true
            drawShadow(noteToDriverCell.shadowView)
            noteToDriverCell.noteField.delegate = self
            noteToDriverCell.noteField.autocorrectionType = .no
            
            noteToDriverCell.noteField.tag = indexPath.row
            if driver_notes != "Type your text here...".localized
            {
                noteToDriverCell.noteField.text = driver_notes
                noteToDriverCell.noteField.textColor = UIColor.black
            }
            else
            {
                noteToDriverCell.noteField.text = driver_notes.localized
            }
            return noteToDriverCell
        }
        else
        {
            let orderNowCell = tableView.dequeueReusableCell(withIdentifier: "OrderNowButtonCell" ) as! OrderNowButtonCell
            orderNowCell.forNowButton.backgroundColor = kBUTTON_DARK_GREEN_COLOR
            orderNowCell.forNowButton.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
            orderNowCell.forNowButton.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
            orderNowCell.forNowButton.setTitle("For Now".localized, for: .normal)
            orderNowCell.forLaterButton.setTitle("For Later".localized, for: .normal)
            orderNowCell.forLaterButton.backgroundColor = kBUTTON_DARK_GREEN_COLOR
            orderNowCell.forLaterButton.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
            orderNowCell.forLaterButton.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
            if forNowAvailable
            {
                orderNowCell.forNowButton.isEnabled = true
            }
            else{
                orderNowCell.forNowButton.isEnabled = false
                orderNowCell.forNowButton.backgroundColor = UIColor.lightGray
            }
            orderNowCell.forNowButton.addTarget(self, action: #selector(OrderRideViewController.forNowButtonClicked(_:)), for: .touchUpInside)
            
            orderNowCell.forLaterButton.addTarget(self, action: #selector(OrderRideViewController.forLaterButtonClicked(_:)), for: .touchUpInside)
            return orderNowCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
            self.selectedHour = 0
            hourlyTextField.becomeFirstResponder()
            self.isKeyboardOpened = true
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return UITableViewAutomaticDimension
        }
        else if indexPath.row == 1 {
            return 110
        }
        else if indexPath.row == 2 {
            return 110
        }
        else if indexPath.row == 3
        {
            return 110
        }
        return 66
    }
    
    func getCurrentDate()
    {
        let date = Date()
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter2.locale = Locale(identifier: "en_GB")
        dateForRequest = dateFormatter2.string(from: date)
        print("dateForRequest.....",dateForRequest)
    }
    
    //MARK: UITextfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.tintColor = UIColor.clear
        hourlyTextField.tintColor = UIColor.clear
        
        if hourlyTextField.isFirstResponder
        {
            pickHourlyRate(hourlyTextField)
        }
        else
        {
            pickUpDate(textField)
        }
    }
    
    //MARK: UITextview delegate methods
    func textViewDidChange(_ textView: UITextView)
    {
        
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        tableView.beginUpdates()
        let index : NSIndexPath = NSIndexPath(row: textView.tag , section: 0)
        tableView.scrollToRow(at: index as IndexPath, at: .top, animated: true)
        tableView.endUpdates()
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type your text here...".localized
            textView.textColor = UIColor.lightGray
            driver_notes = "Type your text here...".localized
        }
        else
        {
            driver_notes = textView.text!
        }
    }
    
    //MARK: UIDatePicker
    func pickUpDate(_ textField : UITextField){
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
        if let selectedFormate = UserDefaults.standard.value(forKey: "TimeFormate")
        {
            if "\(selectedFormate)" == "12"{
                datePicker.locale = Locale(identifier: "en_US")
            }
            else{
                datePicker.locale = Locale(identifier: "en_GB")
            }
        }
        else{
            datePicker.locale = Locale(identifier: "en_GB")
        }
        
        var newDate = Calendar.current.date(byAdding: .hour, value: PRE_BOOKING_HOURS, to: Date())
        datePicker.minimumDate = Calendar.current.date(byAdding: .minute, value: PRE_BOOKING_HOURS, to: newDate!)
        datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 30, to: Date())
        textField.inputView = self.datePicker
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = false
        toolBar.barTintColor = kLIGHT_OLIVE_COLOR
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(OrderRideViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(OrderRideViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return resultArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let value = resultArray[row] as? String ?? ""
        return value.localized
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedHour = row
    }
    func pickHourlyRate(_ textfield : UITextField)
    {
        hourlyPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 216))
        hourlyPicker.backgroundColor = .white
        hourlyPicker.showsSelectionIndicator = true
        hourlyPicker.delegate = self
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.barTintColor = kLIGHT_OLIVE_COLOR
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(OrderRideViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(OrderRideViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textfield.inputView = hourlyPicker
        textfield.inputAccessoryView = toolBar
    }
    
    func doneClick() {
        if hideTextField.isFirstResponder {
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "dd-MM-yyyy HH:mm:ss"
            dateFormatter2.locale = Locale(identifier: "en_GB")
            dateForRequest = dateFormatter2.string(from: datePicker.date)
            let dateFormatter3 = DateFormatter()
            dateFormatter3.dateFormat = "dd/MM/yyyy HH:mm:ss"
            let dateToCompare = dateFormatter3.string(from: datePicker.date)
            hideTextField.resignFirstResponder()
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            let currentDate = dateFormatter.string(from: date)
            let selectedDat = dateFormatter.date(from: dateToCompare)
            let currentDat = dateFormatter.date(from: currentDate)
            if selectedDat?.compare(currentDat!) == .orderedAscending || selectedDat?.compare(currentDat!) == .orderedSame
            {
                showAlert("Date & Time".localized, message: "Please select pick up date & time".localized)
                return
            }
            else{
                openPaymentScreenForOrder("pre-time")
            }
        }
        if hourlyTextField.isFirstResponder
        {
            self.rideWith = "hourly"
            self.isKeyboardOpened = false
            getFareEstimate(rideWith, hour: "\(selectedHour + 1)", driver_id: driver_id)
            hourlyTextField.resignFirstResponder()
        }
    }
    
    func cancelClick()
    {
        if hourlyTextField.isFirstResponder{
            hourlyTextField.resignFirstResponder()
        }
        if hideTextField.isFirstResponder{
            hideTextField.resignFirstResponder()
        }
    }
    
    //Api calling method for fare estimate
    func getFareEstimate(_ rideWith : String, hour : String, driver_id : String)
    {
        var selectedHour = hour
        if(selectedHour != "0"){
            if self.carType == "2"{
                if selectedHour == "10"{
                    selectedHour = "20"
                }else{
                    selectedHour = "1\(hour)"
                }
            }else if self.carType == "3"{
                if selectedHour == "10"{
                    selectedHour = "30"
                }else{
                    selectedHour = "2\(hour)"
                }
            }
        }
        if(checkConnection()) {
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            let pickup_latlong = "\(pickUPLocation.latitude!),\(pickUPLocation.longitude!)"
            let dropup_latlong = "\(dropUPLocation.latitude!),\(dropUPLocation.longitude!)"
            var parameters = [
                "PickupLatitude" : pickUPLocation.latitude!,
                "PickupLongitude" : pickUPLocation.longitude!,
                "DropLatitude" : dropUPLocation.latitude!,
                "DropLongitude": dropUPLocation.longitude!,
                "RiderId" : riderID,
                "RideType" : Int(rideWith == "hourly" ? 1 : 2),
                "RentalPlanId" : selectedHour,
                "CarTypeId" : Int32(carType) ?? 0
                ] as [String : Any]
            
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            printJsonStringLog(apiName: KGET_FARE_ESTIMATE_URL, parameter: jsonString as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KGET_FARE_ESTIMATE_URL, jsonString: jsonParam as NSString, withIdicator: false,header: headerDict) { (result, error) in
                
                self.hideHUD()
                if ((error) != nil){
                    print(error.debugDescription)
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                guard let data = responseObject.value(forKey: "data") as? String else {
                    self.showSimpleAlertController(nil, message: "Something Went Wrong")
                    self.hideHUD()
                    return
                }
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                
                if(responseContent != nil){
                   let result = "Success"
                   if result == "Success"{
                        let resultDic = responseContent as! NSDictionary
                       // if let value = resultDic.value(forKey: "OfferText") as? String{
                       //     self.rideOfferDict["OfferText"] = value
                       // }
                      //  if let value = resultDic.value(forKey: "OfferText") as? String{
                      //      self.rideOfferDict["OfferText"] = value
                      //  }
                        let Finalprice:String = resultDic.value(forKey: "FinalPrice") as? String ?? ""
                        let FinalpriceString:String = String(format:"%.1f", Finalprice)
                        self.totalPrice = Finalprice
                        let Actualprice:String = resultDic.value(forKey: "ActualPrice") as? String ?? ""
                        let ActualpriceString:String = String(Actualprice)
                        self.actualPrice = ActualpriceString
                        self.tableView.reloadData()
                    }else{
                        self.showSimpleAlertController(nil, message: responseContent?.value(forKey: "Message") as? String ?? "")
                    }
                }
            }
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func forNowButtonClicked(_ sender : UIButton){
        view.endEditing(true)
        openPaymentScreenForOrder("on-time")
    }
    func forLaterButtonClicked(_ sender : UIButton){
        if hourlyTextField.isFirstResponder{
            hourlyTextField.resignFirstResponder()
        }
        hideTextField.becomeFirstResponder()
    }
    func openPaymentScreenForOrder(_ bookType:String) {
        if driver_notes == "Type your text here...".localized
        {
            driver_notes = " "
        }
        let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
        let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
        let userDict = self.convertStringToDictionary(text: decryt!)!
        let riderID  = userDict.value(forKey: "id") as! Int32
        let pickup_latlong = "\(pickUPLocation.latitude!),\(pickUPLocation.longitude!)"
        let dropup_latlong = "\(dropUPLocation.latitude!),\(dropUPLocation.longitude!)"
        var parameters = [String : Any]()
        parameters["ride_type"] = rideWith
        parameters["rider_id"] = riderID
        parameters["pick_up_location"] = pickUPLocation.address!
        parameters["drop_up_location"] = dropUPLocation.address!
        parameters["pick_up_latitude"] = pickUPLocation.latitude!
        parameters["pick_up_longitude"] = pickUPLocation.longitude!
        parameters["drop_up_latitude"] = dropUPLocation.latitude!
        parameters["drop_up_longitude"] = dropUPLocation.longitude!
        parameters["car_type"] = carType
        parameters["driver_notes"] = driver_notes.trimmingCharacters(in: .whitespacesAndNewlines)
        parameters["final_price"] = totalPrice
        parameters["pick_up_date"] = dateForRequest
        parameters["language"] = systemLanguage
        parameters["platform"] = kPlatForm
        parameters["environment"] = kEnviroment
        parameters["hour"] = "\(selectedHour + 1)"
        parameters["driver_id"] = driver_id
        parameters["booking_type"] = bookType
        parameters["time_to_reach"] = reachTimeToRider
        let orderPaymentVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderPaymentViewController") as? OrderPaymentViewController
        orderPaymentVC?.orderDict = parameters
        orderPaymentVC?.isCardAvailable = self.isCardAvailable
        orderPaymentVC?.rideOfferDict = self.rideOfferDict
        orderPaymentVC?.actualPrice = self.actualPrice //AK
        self.navigationController?.pushViewController(orderPaymentVC!, animated: true)
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func showSuccessAlert(_ message:NSString)  {
        
        let alertController: UIAlertController = UIAlertController(title: "Success".localized as String, message: message as String, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
            let controllers = self.navigationController?.viewControllers
            for viewController:UIViewController in controllers! {
                if viewController.isKind(of: HomeViewController.self){
                    _ = self.navigationController?.popToViewController(viewController, animated: true)
                    break
                }
            }
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func keyboardWillShow(_ notification: NSNotification) {
        if isKeyboardOpened == true
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.tableView.frame.origin.y == 0{
                    self.tableView.frame.origin.y -= keyboardSize.height
                }
            }
        }
    }
    func keyboardWillHide(_ notification: NSNotification) {
        
        if isKeyboardOpened == true
        {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.tableView.frame.origin.y != 0{
                    self.tableView.frame.origin.y += keyboardSize.height
                }
            }
        }
    }
}


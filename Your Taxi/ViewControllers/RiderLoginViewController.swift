 import UIKit
 import SafariServices
 import SlideMenuControllerSwift
 import UIKit
 import Crashlytics
 class RiderLoginViewController: BaseViewController {
    
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var registerButton: UIButton!
    @IBOutlet var tapTheButtonLbl: UILabel!
    @IBOutlet var fixCostLabel: UILabel!
    
    // MARK: - Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.bool(forKey: KRIDER_LOGIN) {
            
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            if KAPP_RIDER {
                let RootVC = UINavigationController.init(rootViewController: homeVC!)
                self.slideMenuController()?.changeMainViewController(RootVC, close: true)
            }else {
                self.navigationController?.pushViewController(homeVC!, animated: false)
            }
        }else{
            print("GPSActive", UserDefaults.standard.bool(forKey: GPSActive))
            if !UserDefaults.standard.bool(forKey: GPSActive){
                let GPSActiveAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "GPSActiveAlertVC") as? gpsActiveAlertViewController
                GPSActiveAlertVC?.modalPresentationStyle = .overCurrentContext
                self.present(GPSActiveAlertVC!, animated: false, completion: nil)
                UserDefaults.standard.set(true, forKey: GPSActive)
                UserDefaults.standard.synchronize()
            }
        }
        
        registerButton.setTitle("REGISTER".localized, for: UIControlState())
        registerButton.titleLabel?.adjustsFontSizeToFitWidth = true;
        
        loginButton.setTitle("LOG IN".localized, for: UIControlState())
        loginButton.titleLabel?.adjustsFontSizeToFitWidth = true;
        
        if KAPP_RIDER {
            tapTheButtonLbl.text = "Tap the button, get a ride.".localized
            fixCostLabel.text = "Fix & Low Cost!".localized
        }else {
            tapTheButtonLbl.text = "Tap the button, make the money.".localized
        }
        
        setScreenLayout()
        
        if(KAPP_RIDER){
            if locationManager1 != nil {
                locationManager1.stopUpdatingLocation()
            }
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
        self.title = "WELCOME".localized
        slideMenuController()?.removeLeftGestures()
        DispatchQueue.main.async {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Set Screen Design Methods
    func setScreenLayout() {
        
        if(KAPP_RIDER) {
            registerButton.backgroundColor = kBUTTON_DARK_GREEN_COLOR
            registerButton.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
            registerButton.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
            loginButton.backgroundColor = kBUTTON_DARK_GREEN_COLOR
            loginButton.titleLabel?.font = kBUTTON_DARK_GREEN_FONT
            loginButton.tintColor = kBUTTON_DARK_GREEN_TINT_COLOR
        }
    }
    
    // MARK: - Button Actions Methods
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        let verifyNumberVC = self.storyboard?.instantiateViewController(withIdentifier: "VerifyMobileNumberVC") as? VerifyMobileNumberVC
        self.navigationController?.pushViewController(verifyNumberVC!, animated: true)
    }
    @IBAction func registerButtonAction(_ sender: Any) {
        if KAPP_RIDER {
            let RegisterVC = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as? RegistrationViewController
            self.navigationController?.pushViewController(RegisterVC!, animated: true)
            self.navigationController?.navigationBar.isHidden = false
        }
        else {
            let registerUrl = (systemLanguage == "en") ? KDriver_REGISTER_EN_URL : KDriver_REGISTER_DE_URL
            openSafariController(registerUrl as NSString)
        }
    }
    
    func viewController(_ viewController: UIViewController!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
    }
    
    func viewController(_ viewController: UIViewController!, didFailWithError error: Error!) {
        print("We have an error \(error)")
    }
    
    func viewControllerDidCancel(_ viewController: UIViewController!) {
        print("The user cancel the login")
        self.registerButton.isHidden = false
    }
    
    // MARK : - SafariController Methods
    func openSafariController(_ urlString : NSString){
        if #available(iOS 9.0, *){
            let svc = SFSafariViewController(url: URL(string: urlString as String)!)
            self.present(svc, animated: true, completion: nil)
        }
        else{
            if let url = URL(string: urlString as String) {
                UIApplication.shared.openURL(url)
            }
        }
    }
 }
 


import UIKit
import MobileCoreServices
import MessageUI
import FirebaseAuth
import SlideMenuControllerSwift
import SafariServices

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

enum LeftMenu: Int {
    case home = 0
    case currentRide
    case preBookingList
    case rideHistoryList
    case payment
    case lostFound
    case contact
    case settings
    case help
    case logout
    case viewProfile
}


class LeftViewController: BaseViewController,LeftMenuProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var profileImage : UIButton!
    @IBOutlet weak var fullNameLbl: UILabel!
    let picker = UIImagePickerController()
    var UserName : String = ""
    var selectedImage : UIImage? = nil
    var menus = ["MenuHome".localized, "MenuBooking".localized,"Pre Booking".localized, "Ride History".localized, "Payment".localized,"Lost & Found".localized,"Contact us".localized, "Settings".localized, "Help".localized, "LogoutKey".localized]
    var iconName = ["Menuhome", "CurrentRide","ic_pre_booking","ic_ride_history","Payment","ic_lost_found","ic_Contact","settings", "Help", "ic_logout"]
    
    
    //MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: KRIDER_LOGIN) {
            self.fullNameLbl.text = UserDefaults.standard.string(forKey: KRIDER_USERNAME)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- BUTTON ACTIONS
    @IBAction func changeProfileImageClick(_ sender: UIButton) {
    }
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
            
        case .home:
            self.slideMenuController()?.closeLeft()
            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            let swiftViewController = UINavigationController(rootViewController: homeVC!)
            self.slideMenuController()?.changeMainViewController(swiftViewController, close: true)
            
        case .currentRide:
            self.slideMenuController()?.closeLeft()
            
            let slideMenuVC1 = self.slideMenuController()
            let nav1 = (slideMenuVC1?.mainViewController as! UINavigationController)
            let rootCon = nav1.viewControllers.last
            if rootCon is HomeViewController
            {
                (rootCon as! HomeViewController).callWebserviceForGettingCurrentRide()
            }
            
        case .settings:
            self.slideMenuController()?.closeLeft()
            let slideMenuVC1 = self.slideMenuController()
            let nav1 = (slideMenuVC1?.mainViewController as! UINavigationController)
            let rootCon = nav1.viewControllers.last
            if rootCon is HomeViewController
            {
                (rootCon as! HomeViewController).openSettingScreen()
            }
            
        case .payment:
            self.slideMenuController()?.closeLeft()
            let slideMenuVC1 = self.slideMenuController()
            let nav1 = (slideMenuVC1?.mainViewController as! UINavigationController)
            let rootCon = nav1.viewControllers.last
            if rootCon is HomeViewController
            {
                (rootCon as! HomeViewController).openPaymentDetailsScreen()
            }
            
        case .preBookingList:
            self.slideMenuController()?.closeLeft()
            let slideMenuVC1 = self.slideMenuController()
            let nav1 = (slideMenuVC1?.mainViewController as! UINavigationController)
            let rootCon = nav1.viewControllers.last
            if rootCon is HomeViewController
            {
                (rootCon as! HomeViewController).openPreBookingListScreen()
            }
            break;
        case .rideHistoryList:
            self.slideMenuController()?.closeLeft()
            let slideMenuVC1 = self.slideMenuController()
            let nav1 = (slideMenuVC1?.mainViewController as! UINavigationController)
            let rootCon = nav1.viewControllers.last
            if rootCon is HomeViewController
            {
                (rootCon as! HomeViewController).openRideHistoryListScreen()
            }
            break;
        case .lostFound:
            self.openMailComposer(recipientsArr: ["care@yourtaxi.ch"], strSubject: "Tell us, what you have lost.".localized, strBody: "")
            break;
            
        case .contact:
            self.openMailComposer(recipientsArr: ["service@yourtaxi.ch"], strSubject: "", strBody: "")
            break;
            
        case .help:
            self.openSafariController(KRider_HelpText_URL as NSString)
            break;
            
        case .logout:
            self.slideMenuController()?.closeLeft()
            let alertController: UIAlertController = UIAlertController(title: nil, message: KAPP_RIDER ? "RiderLogOutMsgKey".localized : "LogOutMsgKey".localized , preferredStyle: .alert)
            let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
                self.riderLogoutApi()
            }
            let noAction: UIAlertAction = UIAlertAction(title: "No".localized, style: .default) { action -> Void in}
           
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
            break;
            
        default:
            break
        }
    }
    // MARK : - SafariController Methods
    func openSafariController(_ urlString : NSString){
        if #available(iOS 9.0, *){
            let svc = SFSafariViewController(url: URL(string: urlString as String)!)
            self.present(svc, animated: true, completion: nil)
        }
        else{
            if let url = URL(string: urlString as String) {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK:- MFMailComposeViewController Delegate Methods
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Open Mail Composer
    func openMailComposer(recipientsArr:[String],strSubject:String,strBody:String){
        self.slideMenuController()?.closeLeft()
        if !MFMailComposeViewController.canSendMail() {
            showSimpleAlertController("Alert", message: "Mail services are not available.".localized)
            return
        }
        else{
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self
            mailVC.setToRecipients(recipientsArr)
            mailVC.setSubject(strSubject)
            mailVC.setMessageBody(strBody, isHTML: false)
            self.present(mailVC, animated: true, completion: nil)
        }
    }
    
    //MARK:- WEBSERVICE METHODS
    func riderLogoutApi()  {
        
        let appdel = UIApplication.shared.delegate as! AppDelegate
        
        if checkConnection(){
            appdel.showHUD()
            let Request: WebserviceHelper = WebserviceHelper()
            let riderID = Request.getUserDefaultsStandard()
            var loginType = ""
            if KAPP_RIDER{
                loginType = "rider"
            }
            else{
                loginType = "driver"
            }
            
            var parameters = [
                "PlatForm": 1,
                ] as [String : Any]
            self.showHUD()
            let jsonString = returnJsonString(param: parameters)
            var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
            parameters = ["data":encryptedData!]
            let jsonParam = returnJsonString(param: parameters)
            let headerDict = Request.getHeaderForRider(parameters: parameters)
            printJsonStringLog(apiName: KLOGOUT_URL, parameter: jsonParam as NSString, headerDict: headerDict)
            Request.CustomHTTPPostJSONWithHeader(url: KLOGOUT_URL, jsonString: jsonParam as NSString, withIdicator: false, header: headerDict, callback: { (result, error) in
                
                self.hideHUD()
                if(error != nil){
                    self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                    return;
                }
                let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                if let dict = responseContent{
                    if let value = dict.value(forKey: "Status") as? Bool{
                        if (value == true){
                            locationManager1.stopUpdatingLocation()
                            UserDefaults.standard.set(false, forKey: KRIDER_LOGIN)
                            UserDefaults.standard.removeObject(forKey: KUSER_DETAILS)
                            UserDefaults.standard.removeObject(forKey: KRIDE_DETAILS)
                            UserDefaults.standard.set("", forKey: KRIDER_USERNAME)
                            UserDefaults.standard.synchronize()
                            self.firbaseAuthSignOut()
                            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
                            let loginVC = storyboard.instantiateViewController(withIdentifier: "RiderLoginViewController") as! RiderLoginViewController
                            let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
                            appdel.homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                            let nvc: UINavigationController = UINavigationController(rootViewController: loginVC)
                            let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
                            slideMenuController.delegate = appdel.homeViewController
                            slideMenuController.automaticallyAdjustsScrollViewInsets = true
                            slideMenuController.slideMenuController()?.changeLeftViewWidth((appdel.window?.bounds.width)!-90)
                            slideMenuController.removeLeftGestures()
                            appdel.window?.backgroundColor = UIColor(red: 0.0, green: 149.0, blue: 120.0, alpha: 1.0)
                            appdel.window?.rootViewController = slideMenuController
                            
                        }else{
                            if let isDeleted = dict.value(forKey: "flag") as? Int{
                                if(isDeleted == 1){
                                    self.moveToSingupController(message: dict.value(forKey: "Message") as? String ?? "")
                                }
                            }else{
                                self.showSimpleAlertController(nil, message: dict.value(forKey: "Message") as? String ?? "")
                            }
                        }
                    }
                }
                
            })
        }
        else{
            showAlertForInternetConnectionOff()
        }
    }
    
    //MARK: - UIImagePickerController Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        dismiss(animated:true, completion: nil)
        self.slideMenuController()?.removeLeftGestures()
        showHUD()
        WebserviceHelper().myImageUploadRequest(chosenImage, url: KRIDER_PROFILE, button: self.profileImage, completionHandler: { success in
            self.hideHUD()
            self.slideMenuController()?.addLeftGestures()
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


extension LeftViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        let menuTitle = cell?.viewWithTag(1) as! UILabel
        menuTitle.text = menus[indexPath.row]
        let icons = cell?.viewWithTag(2) as! UIImageView
        let imageName =  iconName[indexPath.row] as String
        icons.image = UIImage.init(named: imageName)
        return cell!
    }
    
}
//MARK:- MFMailComposeViewController
extension MFMailComposeViewController {
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    open override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isTranslucent = false
        navigationBar.isOpaque = false
        navigationBar.barTintColor = UIColor.white
        navigationBar.tintColor = UIColor.white
    }
}


import Foundation
import UIKit

class SelectedLocation : NSObject
{
    var latitude : String!
    var longitude : String!
    var address : String!
    
    func setLocationData(latitude : String, longitude : String, address : String)
    {
        self.latitude = latitude
        self.longitude = longitude
        self.address = address
    }
}

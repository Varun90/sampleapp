import UIKit

class PickUpLocationCell: UITableViewCell {

    @IBOutlet weak var heading : UILabel!
    @IBOutlet weak var locationLabel : UILabel!
    @IBOutlet weak var shadowView : UIView!
    
    @IBOutlet var lblDestinationTitle: UILabel!
    @IBOutlet var lblDestinationLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

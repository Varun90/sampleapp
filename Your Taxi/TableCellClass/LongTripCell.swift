import UIKit

class LongTripCell: UITableViewCell {

    @IBOutlet var shadowView: UIView!
    @IBOutlet var hourlyTextField: UITextField!
    @IBOutlet var cellTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

import UIKit

class NoteToDriverCell: UITableViewCell {

    @IBOutlet var noteToDriverLabel: UILabel!
    @IBOutlet weak var noteField : UITextView!
    @IBOutlet weak var shadowView : UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        noteField.textColor = UIColor.lightGray
        noteField.text = "Type your text here... \n \n \n"
        noteToDriverLabel.text = "Note to Driver:".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
}

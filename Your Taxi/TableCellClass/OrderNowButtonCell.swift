import UIKit

class OrderNowButtonCell: UITableViewCell {
    
    @IBOutlet var forLaterButton: UIButton!
    @IBOutlet var forNowButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        forNowButton.setTitle("For Now".localized, for: .normal)
        forLaterButton.setTitle("For Later".localized, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

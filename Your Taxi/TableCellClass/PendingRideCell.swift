import UIKit
class PendingRideCell: UITableViewCell {

    @IBOutlet weak var dateTime : UILabel!
    @IBOutlet weak var pickUpLocation : UILabel!
    @IBOutlet weak var dropUpLocation : UILabel!
    @IBOutlet weak var fromLabel : UILabel!
    @IBOutlet weak var toLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

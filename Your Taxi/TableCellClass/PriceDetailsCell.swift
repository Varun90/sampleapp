import UIKit

class PriceDetailsCell: UITableViewCell {

    @IBOutlet weak var priceLabel : UILabel!
    @IBOutlet weak var shadowView : UIView!
    @IBOutlet weak var priceHeading : UILabel!
    @IBOutlet weak var message : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

import UIKit

class infoTripRedirectTableViewCell: UITableViewCell {
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet var startRideSlideBtn: MMSlidingButton!
    @IBOutlet var arrivedSlideBtn: MMSlidingButton!
    @IBOutlet var goSlideBtn: MMSlidingButton!
    @IBOutlet var goSlideBtnHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

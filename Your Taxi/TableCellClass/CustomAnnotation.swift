import UIKit
import CoreLocation
import MapKit

class CustomAnnotation: MKPointAnnotation {


     var id : String!
     var annotationImageName : String!
    
    func addAnnotationWithTitle(title: String, coordinate: CLLocationCoordinate2D, id: String, annotationImageName: String) -> Any {
        
      self.title = title
      self.coordinate = coordinate
      self.id = id
      self.annotationImageName = annotationImageName

      return self
        
    }
}

import UIKit

class CarDetailsCell: UITableViewCell {

    @IBOutlet weak var carImage : UIImageView!
    @IBOutlet weak var carName : UILabel!
    @IBOutlet weak var plateNo : UILabel!
    @IBOutlet weak var carType : UILabel!
    @IBOutlet weak var activityIndicatior : UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

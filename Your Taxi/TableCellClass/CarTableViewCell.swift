import UIKit

class CarTableViewCell: UITableViewCell {
    @IBOutlet var entityImage: UIImageView!
    @IBOutlet var entityName: UILabel!
    @IBOutlet var entityPrice: UILabel!
    @IBOutlet var entityButton: UIButton!
    @IBOutlet var maxPerson: UILabel!
    @IBOutlet var maxHandbag: UILabel!
    @IBOutlet var maxLuggage: UILabel!
    @IBOutlet weak var entityDiscription: UILabel!
    @IBOutlet weak var ourCarTitle: UILabel!
    
    
    // MARK: - Overrided Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

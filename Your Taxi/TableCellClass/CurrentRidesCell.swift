import UIKit
class CurrentRidesCell: UITableViewCell {

    @IBOutlet var shadowView: UIView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var valueLabel : UILabel!
    @IBOutlet var titleImageview: UIImageView!
    
    @IBOutlet var btnEdit: UIButton!
    
    var onEditPress : ((UIButton) -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func btnEditAction(_ sender: UIButton) {
        if(onEditPress != nil){
            self.onEditPress!(sender)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

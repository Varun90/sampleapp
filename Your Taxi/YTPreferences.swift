import Foundation

func encrypt(data : AnyObject, key: String) {
    
    var encryptData : String
    
    switch data {
        
    case is String:
        
        encryptData = AESCrypt.encrypt(data as! String, password: SECRET_KEY)
        
        print("None")
        break
        
    case is NSDictionary:
        
        let convertedString = returnJsonString(param: data as! [String : Any])
        encryptData = AESCrypt.encrypt(convertedString, password: SECRET_KEY)
        
        print("None")
        break
        
    default:
        encryptData = ""
        
        print("None")
        break
    }
    
    UserDefaults.standard.set(encryptData, forKey: key)
    UserDefaults.standard.synchronize()
}

func getDecryptedData(key: String)-> String {
    
    let encryptedData = UserDefaults.standard.value(forKey: key)
    
    if let preferenceStr = encryptedData as? String {
        return AESCrypt.decrypt(preferenceStr, password: SECRET_KEY)
    }
    
    return ""
}

func getAuthToken()-> String {
    let token = getDecryptedData(key: kAUTH_TOKEN)
    return token
}


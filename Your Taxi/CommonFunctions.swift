import Foundation
import JWT
import CTKFlagPhoneNumber

//MARK:- AUTHENTICAION DETAILS
let authDict = UserDefaults.standard.object(forKey: AUTH_DETAILS) as? NSDictionary

var homeVcDidLoad = false

func getDriverAuthToken() -> String{
    let authDict = UserDefaults.standard.object(forKey: AUTH_DETAILS) as? NSDictionary
    let encryptedAUTH_TOKEN = authDict!.value(forKey: "auth_token") as! String
    let AUTH_TOKEN = AESCrypt.decrypt(encryptedAUTH_TOKEN, password: SECRET_KEY)
    return AUTH_TOKEN!
}

func getDriverRandomString() -> String{
    let authDict = UserDefaults.standard.object(forKey: AUTH_DETAILS) as? NSDictionary
    let RANDOM_STRING = AESCrypt.decrypt(authDict!.value(forKey: "random_string") as! String, password: SECRET_KEY)
    return RANDOM_STRING!
}

//MARK:- GET PUSH TOKEN STRING
func getPushTokenString() -> NSString{
    var pushTokenString=NSString()
    #if (arch(i386) || arch(x86_64)) && os(iOS)
        pushTokenString="1234"
    #else
        pushTokenString = getDecryptedData(key: kPUSH_TOKEN) as NSString
        
    #endif
    
    return pushTokenString
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    
    
}
func DLog(_ message: String, function: String = #function)
{
    #if DEBUG
        NSLog("\(function): \(message)")
    #endif
}
func showAlert(_ title : String?, message: String) {
    let alert = UIAlertController(
        title: title,
        message: message,
        preferredStyle: UIAlertControllerStyle.alert
    )
    let ok = UIAlertAction(
        title: "Ok",
        style: UIAlertActionStyle.default,
        handler: nil
    )
    alert.addAction(ok)
    UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
}

func drawShadow(_ shadowView : UIView)
{
    shadowView.layer.shadowColor = UIColor.black.cgColor
    shadowView.layer.shadowOpacity = 0.5
    shadowView.layer.shadowOffset = CGSize.zero
    shadowView.layer.shadowRadius = 1
    shadowView.layer.cornerRadius = 2
}

func replace(_ myString: String, _ index: Int, _ newChar: Character) -> String {
    var modifiedString = String()
    for (i, char) in myString.characters.enumerated() {
        modifiedString += String((i == index) ? newChar : char)
    }
    return modifiedString
}

func getDataFromUrl(_ url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
    URLSession.shared.dataTask(with: url) {
        (data, response, error) in
        completion(data, response, error)
        }.resume()
}

func downloadImage(_ url: URL, imageBtn : UIButton?, imageView : UIImageView?, indicator : UIActivityIndicatorView?) {
    if indicator != nil
    {
        indicator?.isHidden = false
        indicator?.startAnimating()
    }
    getDataFromUrl(url) { (data, response, error)  in
        guard let data = data, error == nil else { return }
        print(response?.suggestedFilename ?? url.lastPathComponent)
        
        DispatchQueue.main.async() { () -> Void in
            let image = UIImage(data: data)
            
            if imageBtn != nil
            {
                imageBtn?.setBackgroundImage(image, for: .normal)
            }
            else if imageView != nil
            {
                imageView?.image = image
            }
            if indicator != nil
            {
                indicator?.stopAnimating()
                indicator?.isHidden = true
            }
        }
    }
}

func getInclusiveKM(_ hours : String) -> String
{
    var inclusiveKm = "0"
    
    if hours == "1"
    {
        inclusiveKm = "30 km"
    }
    else if hours == "2"
    {
        inclusiveKm = "50 km"
    }
    else if hours == "3"
    {
        inclusiveKm = "80 km"
    }
    else if hours == "4"
    {
        inclusiveKm = "120 km"
    }
    else if hours == "5"
    {
        inclusiveKm = "150 km"
    }
    else if hours == "6"
    {
        inclusiveKm = "180 km"
    }
    else if hours == "7"
    {
        inclusiveKm = "210 km"
    }
    else if hours == "8"
    {
        inclusiveKm = "240 km"
    }
    else if hours == "9"
    {
        inclusiveKm = "270 km"
    }
    else if hours == "10"
    {
        inclusiveKm = "300 km"
    }
    return inclusiveKm
}

func matchesRegex(_ regex: String!, text: String!) -> Bool {
    do {
        let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
        let nsString = text as NSString
        let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
        return (match != nil)
    } catch {
        return false
    }
}
func getCountryNameString(from countryCode: String) -> String {
    if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
        // Country name was found
        return name
    } else {
        // Country name cannot be found
        return countryCode
    }
}
func writeToFile(content: String, fileName: String) {
    
    let contentToAppend = content+"\n"
    let filePath = NSHomeDirectory() + "/Documents/" + fileName
    
    //Check if file exists
    if let fileHandle = FileHandle(forWritingAtPath: filePath) {
        //Append to file
        fileHandle.seekToEndOfFile()
        fileHandle.write(contentToAppend.data(using: String.Encoding.utf8)!)
    }
    else {
        //Create new file
        do {
            try contentToAppend.write(toFile: filePath, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("Error creating \(filePath)")
        }
    }
}

//MARK:- STRING EXTENSTION
extension String {
    
    //MARK:- MD5 ENCRYPTION
    func md5() -> String! {
        let str = self.cString(using: String.Encoding.utf8)
        let strLen = CUnsignedInt(self.lengthOfBytes(using: String.Encoding.utf8))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLen)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.deinitialize()
        
        return String(format: hash as String)
    }
    
    //MARK:- SHA256 ENCRYPTION
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
    
    func aesEncrypt(key:String, iv:String, options:Int = kCCOptionPKCS7Padding) -> String? {
        if let keyData = key.data(using: String.Encoding.utf8),
            let data = self.data(using: String.Encoding.utf8),
            let cryptData    = NSMutableData(length: Int(((data as NSData).length)) + kCCBlockSizeAES128) {
            
            let keyLength              = size_t(kCCKeySizeAES128)
            let operation: CCOperation = UInt32(kCCEncrypt)
            let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES128)
            let options:   CCOptions   = UInt32(options)
            
            var numBytesEncrypted :size_t = 0
            
            let cryptStatus = CCCrypt(operation,
                                      algoritm,
                                      options,
                                      (keyData as NSData).bytes, keyLength,
                                      iv,
                                      (data as NSData).bytes, (data as NSData).length,
                                      cryptData.mutableBytes, cryptData.length,
                                      &numBytesEncrypted)
            
            if UInt32(cryptStatus) == UInt32(kCCSuccess) {
                cryptData.length = Int(numBytesEncrypted)
                let base64cryptString = cryptData.base64EncodedString(options: .lineLength64Characters)
                return base64cryptString
            }
            else {
                return nil
            }
        }
        return nil
    }
}
func sha256EncryptionWithSecretKey(data:NSData) -> String{
    let keyData = SECRET_KEY.data(using: .utf8)!
    let hash = NSMutableData(length: Int(CC_SHA256_DIGEST_LENGTH))
    CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), (keyData as NSData).bytes, (keyData as NSData).length, data.bytes, data.length, hash!.mutableBytes)
    return hash!.base64EncodedString(options: .lineLength64Characters)
}


func getEncryptionKey(parameters:[String:Any],randomString:String? = nil ,UUID:String) -> String{
    
    let jsonstr = returnJsonString(param: parameters)
    var dynamicString = ""
    if randomString == nil{
        dynamicString = "\(UUID)\(jsonstr)" //Concate String
    }else{
        dynamicString = "\(randomString!)\(jsonstr)\(UUID)" //Concate String
    }
    let data = dynamicString.data(using: String.Encoding.utf8) //UTF Encoding
    
    let finalStr = sha256EncryptionWithSecretKey(data: data! as NSData) //SHA256 Encryption with secret key
    return finalStr
}

func returnJsonString(param:[String:Any]) -> String
{
    let jsonData = try! JSONSerialization.data(withJSONObject: param, options: [])
    let string = String(data: jsonData, encoding: .utf8)!
    return string
}

func printParameterLog(apiName:String,parameter:[String:Any],headerDict:[String:Any]){
    
    print("Request URL:\n \(apiName) \n \n Request Parameter:\n \(parameter) \n \n Headers:\n \(headerDict)\n")
}
func printJsonStringLog(apiName:String,parameter:NSString,headerDict:[String:Any]){
    
    print("Request URL:\n \(apiName) \n \n Request Parameter:\n \(parameter) \n \n Headers:\n \(headerDict)\n")
}


//MARK:- API Add-On Methods

func getRequestHeaders()-> [String:String] {
    return [
        "Content-Type" : "application/json",
        "Authorization" : getAuthToken(),
        "Language" : systemLanguage
    ]
}

func getJsonStringWithEncryptedRequestParameters(requestParameters : [String : Any]) -> String {
    
    let jsonString = returnJsonString(param: requestParameters)
    print("Json String :",jsonString)
    
    let encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
    print("encryptedData :",encryptedData ?? "")
    
    let parameters = ["data":encryptedData!]
    print(parameters)
    
    let jsonParam = returnJsonString(param: parameters)
    print("jsonParam:", jsonParam);
    
    return jsonParam
}

func getResponseDictionaryWithResult(result: String) -> NSDictionary {
    
    let responseObject : NSDictionary = convertStringToDictionary(text: result as String)!
    
    guard (responseObject["data"] as? String) != nil else {
        return responseObject
    }
    
    let decryptedData = AESCrypt.decrypt(responseObject["data"] as! String, password: SECRET_KEY)
    print("decryptedData :",decryptedData ?? "")
    
    let responseContent = convertStringToDictionary(text: decryptedData! as String)
    print("responseContent :", responseContent ?? "")
    
    return responseContent!
}

func getResponseArrayWithResult(result: String) -> NSArray {
    
    let responseObject : NSDictionary = convertStringToDictionary(text: result as String)!
    
    let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
    print("decryptedData :",decryptedData ?? "")
    
    let responseContent = convertStringToArray(text: decryptedData! as String)
    print("responseContent :", responseContent ?? "")
    
    return responseContent!
}


func PrepareClaimSetWithResult(result: String, callback: @escaping (ClaimSet, Bool, String) -> Void) {
    
    var claims = ClaimSet()
    
    let responseDict = getResponseDictionaryWithResult(result: result)
    
    if let token = responseDict.value(forKey: "token") as? String {
        
        let responsetoken = token
        
        let tokenString = "Bearer " + responsetoken
        encrypt(data: tokenString as AnyObject, key: kAUTH_TOKEN)
        do {
            claims = try JWT.decode(responsetoken, algorithm: .hs256(JWT_SECRET_KEY.data(using: .utf8)!))
            callback(claims, true, "Success")
            return;
            
        } catch {
            print("Failed to decode JWT: \(error)")
            callback(claims, false, "Response decode Error")
            return;
        }
        
    }
    guard let errMsg = responseDict.value(forKey: "Message") as? String else {
        var displayMessage = ""
        if responseDict["Errors"] != nil {
            let errMessage = (responseDict.value(forKey: "Errors") as! NSDictionary).value(forKey: "PushToken") as? NSArray
            displayMessage = (errMessage![0] as? String)!
        }
        callback(claims, false, displayMessage)
        return;
    }
    
    callback(claims, false, errMsg)
}

func convertStringToDictionary(text: String) -> NSDictionary? {
    
    let tempDict : NSDictionary = ["Errors" : "Response data error"]
    var encryptData = returnJsonString(param: tempDict as! [String : Any])
    encryptData = AESCrypt.encrypt(encryptData, password: SECRET_KEY)
    let errorDict : NSDictionary = ["data" : encryptData]
    
    if let data = text.data(using: String.Encoding.utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
        } catch let error as NSError {
            print(error)
        }
    }
    return errorDict
}

func convertStringToArray(text: String) -> NSArray? {
    if let data = text.data(using: String.Encoding.utf8) {
        
        do {
            if let responseArray = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                return responseArray
            }
        } catch let error as NSError {
            print(error)
        }
    }
    return []
}

func getCurrentLoginUserDict()-> NSDictionary {
    let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS)
    let decryt = AESCrypt.decrypt(dic as! String, password: SECRET_KEY)
    return convertStringToDictionary(text: decryt!)!
}

func getCurrentLoginUserBankDict()-> NSDictionary {
    let dic = UserDefaults.standard.object(forKey: KUSER_BANK_DETAILS)
    let decryt = AESCrypt.decrypt(dic as! String, password: SECRET_KEY)
    return convertStringToDictionary(text: decryt!)!
}

func getPlateNumber(plateNumber:PlateNumber) -> String? {
    
    var plateNumberStr:String?
    
    switch plateNumber {
    case .integer(let int_plateNumber):
        plateNumberStr =  "\(int_plateNumber)"
    case .string(let string_plateNumber):
        plateNumberStr =  string_plateNumber
    }
    return plateNumberStr
}

func getLoginDriverID()-> NSInteger {
    let userDict: NSDictionary = getCurrentLoginUserDict()
    let strDriverID = userDict.value(forKey: "UserId") as! NSInteger
    return strDriverID
}


import Foundation
import UIKit

//Api live
//let KBASE_URL = "https://www.yourtaxi.ch/api"
//let KBASE_WEB_URL = "https://www.yourtaxi.ch"
//let StripePublishableKey : String? = "pk_live_OBXgXaop5Z0TNqSTvyGcpVfW"
//let GMS_TOKEN = "AIzaSyA6nJYfqHtAz2eM3mnhkH  HiWv5OYHOOZb4"
//let GOOGLE_WEB_API_KEY = "AIzaSyA6nJYfqHtAz2eM3mnhkHHiWv5OYHOOZb4"
//let kEnviroment = "live"

// ------ Local Changes ------ //
let KBASE_URL = "https://test.yourtaxi.ch/api"
let KBASE_WEB_URL = "https://test.yourtaxi.ch"
let StripePublishableKey : String? = "pk_test_1q1f9P22jX3Btkj6HCULkUr9"
let GMS_TOKEN = "AIzaSyAJWLCVmd9TAKE1CXA9c9X1-RAJbebrJug" // Client google API key
let GOOGLE_WEB_API_KEY = "AIzaSyAJWLCVmd9TAKE1CXA9c9X1-RAJbebrJug" // Client  API key
let kEnviroment = "sandbox"

let SECRET_KEY = "zR88SENhs53rapOTl2ShV7WaHAKuB+sVyUlcnSiHpQ8="

let JWT_SECRET_KEY = "61YCwGeSQCNbsngVTqeBVTVTLWW3hRYH"

let KSET_COUNTRY = "CHE"

let PRE_BOOKING_HOURS = 2

//Some new API
let KLOGIN_URL = "\(KBASE_URL)/login"
let KLOGOUT_URL = "\(KBASE_URL)/logout"
let FORGET_CASE_URL = "\(KBASE_URL)/forgotpassword"
let KADDRIDETIP_URL = "\(KBASE_URL)/booking/addtip"
let KCURRENTRIDE_URL = "\(KBASE_URL)/booking/currentride"
let KACCEPTREJECT_URL = "\(KBASE_URL)/booking/rideaccept"
let KCANCELRIDE_URL = "\(KBASE_URL)/booking/ridecancel"
let ONLINE_DRIVER_COUNT = "\(KBASE_URL)/booking/closestdriver"
let PREBOOKING_COUNT = "\(KBASE_URL)/booking/prebookingcount"
let PREBOOKING_LIST = "\(KBASE_URL)/booking/list"
let EDIT_PREBOOKING_DETAILS_URL = "\(KBASE_URL)/booking/updateprebookride"
let DELETE_LOCATION = "\(KBASE_URL)/userlocations/delete"
let DELETE_USER_PROFILE = "\(KBASE_URL)/users/delete"
let DELETE_DRIVER_PROFILE = "\(KBASE_URL)/users/delete"
let DRIVER_PAY_AMOUNT = "\(KBASE_URL)/driver/totalpay"
let KGET_FARE_ESTIMATE_URL = "\(KBASE_URL)/booking/getrideprice"
let KRIDERLOGIN_URL = "\(KBASE_URL)/login"
let KValidatePhone_URL = "\(KBASE_URL)/verifyphonenumber"
let KValidateUser_URL = "\(KBASE_URL)/riderregistrationrules"
let KRegistration_URL = "\(KBASE_URL)/riderregistration"
let KSendDriverLocation_URL = "\(KBASE_URL)/userlocations/locationupdate"
let KNearestDriverList_URL = "\(KBASE_URL)/booking/closestdriver"
let KDriverReachingTime_URL = "\(KBASE_URL)/booking/getcartypepickdriverdis"
let KCheckServiceAvailability = "\(KBASE_URL)/checkserviceavailability"
let KGET_MIN_PRICE = "\(KBASE_URL)/booking/getcartypesrideprice"
let KFINAL_BOOKRIDE = "\(KBASE_URL)/booking/ride"
let KRIDER_DETAIL_URL = "\(KBASE_URL)/users/show"
let KRIDER_PROFILE = "\(KBASE_URL)/users/update"
let KDRIVER_LOCATION_CONTINUOUS = "\(KBASE_URL)/booking/driverlocation"
let KCARD_PAYMENT = "\(KBASE_URL)/users/addcard"
let KRIDER_CARD_DETAILS = "\(KBASE_URL)/users/show"
let KRIDER_RENTAL_PLAN = "\(KBASE_URL)/rentalplan"
let KRIDER_CARD_DETAILS_WITH_DATA = "\(KBASE_URL)/users/showcard"
let KDRIVER_PENDING_RIDE = "\(KBASE_URL)/booking/pendingride"
let KSTART_RIDE = "\(KBASE_URL)/booking/startride"
let KDRIVER_CAR_LIST = "\(KBASE_URL)/driver/activecars"
let KDRIVER_ACTIVE_CAR_UPDATE = "\(KBASE_URL)/car/activate"
let KGET_FAVOURITE_LOCATION = "\(KBASE_URL)/userlocations/list"
let KSET_FAVOURITE_LOCATION = "\(KBASE_URL)/userlocations"
let KUPDATE_FAVOURITE_LOCATION = "\(KBASE_URL)/userlocations/update"
let KSET_DRIVER_AVAILABILITY = "\(KBASE_URL)/driver/availabilitychange"
let KSET_DRIVER_RATING = "\(KBASE_URL)/booking/rating"
let KCHECK_ORDER_STATUS = "\(KBASE_URL)/booking/ridestatus"
let kGET_DRIVER_DETAILS = "\(KBASE_URL)/users/showdriver"
let kGET_CSRF_TOKEN = "\(KBASE_URL)/gettoken"
let KDRIVER_CURRENT_RIDEINFO_URL = "\(KBASE_URL)/booking/currentride"
let KRIDESTATUS_UPDATE_URL = "\(KBASE_URL)/ride"
let DELETE_BOOKING_HISTORY  = "\(KBASE_URL)/booking/delete"
let CURRENT_RIDE_POOLING = "\(KBASE_URL)/ridePooling"
let KCARTYPE_LIST = "\(KBASE_URL)/cartypes/list"
let KDRIVER_STILLAWAKE = "\(KBASE_URL)/driver/stillawake"
let KRIDER_CANCELLATIONFEE = "\(KBASE_URL)/booking/cancellationfee"
let KRIDER_RIDEEXPIRE = "\(KBASE_URL)/booking/rideexpire"
let KAPPVERSION = "\(KBASE_URL)/appversion"
//// Web Url //

let KDriver_REGISTER_EN_URL = "\(KBASE_WEB_URL)/en/driver/registration?type=app"
let KDriver_REGISTER_DE_URL = "\(KBASE_WEB_URL)/de/driver/registration?type=app"
let KLOSSPASS_URL = "\(KBASE_WEB_URL)/\(systemLanguage)/forgotPassword?usertype=3"
let KRider_TERMSANDCONDITIONS_URL = "\(KBASE_WEB_URL)/\(systemLanguage)/page/termOfUse"
let KRider_HelpText_URL = "\(KBASE_WEB_URL)/\(systemLanguage)/page/help"

let KRIDER_FEEDBACK_INFO = "KRIDER_FEEDBACK_INFO"
let KRIDER_LOGIN = "rider_login"
let KUSER_DETAILS = "user_details"
let KUSER_BANK_DETAILS = "user_bank_details"
let KRIDER_USERNAME = "rider_username"
let KRIDE_DETAILS = "ride_details"
let AUTH_DETAILS = "auth_details"
let KDRIVER_LOGIN = "driver_login"
let kREMEMBER_ME  = "RememberMe"
let kPUSH_TOKEN  =   "devicePushToken"
let kNEW_LOCATION =  "IsNewLocation"
let kAUTH_TOKEN  =   "deviceAutoToken"
let DRIVER1_LASTCOUNT = "driver1LastCount"
let DRIVER2_LASTCOUNT = "driver2LastCount"
let DRIVER3_LASTCOUNT = "driver3LastCount"
let GPSActive = "GPSstatus"
let kPlatForm = "iOS"
let kPlatFormID = 1
let RiderID_Notification = "riderIdNotification"

let DEVICE_UUID = UIDevice.current.identifierForVendor!.uuidString;

let Authorization = "Basic VGF4aUBaYWhhbmdpcjEyMzp2VlZhdTMoV35qdCZzRTRV"

let NewAuthorization = "Bearer "

let language = Bundle.main.preferredLocalizations.first! as NSString
let systemLanguage = language == "de" ? "de" : "en"

let KBundleName = Bundle.main.bundleIdentifier
let KAPP_RIDER = (KBundleName == "com.yourtaxigmbh.yourtaxirider") ? true : false

let KAPP_NAV_COLOR = (KAPP_RIDER) ? UIColor.init(red: 0/255.0, green: 149/255.0, blue: 120/255.0, alpha: 1.0) : UIColor.init(red: 239/255.0, green: 117/255.0, blue: 33/255.0, alpha: 1.0)

let KAPP_COLOR = (KAPP_RIDER) ? UIColor.init(red: 0/255.0, green: 149/255.0, blue: 120/255.0, alpha: 1.0) : UIColor.init(red: 239/255.0, green: 117/255.0, blue: 33/255.0, alpha: 1.0)


let MoreDetailsTableTag = 111

let APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
let APP_BUILD = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

//Stripe Payment
let BackendChargeURLString : String? = KCARD_PAYMENT
let AppleMerchantId : String? = nil

// new design
let kDARK_OLIVE_COLOR = UIColor.init(red: 0/255.0, green: 149/255.0, blue: 120/255.0, alpha: 1.0)
let kLIGHT_OLIVE_COLOR = UIColor.init(red: 0/255.0, green: 149/255.0, blue: 120/255.0, alpha: 1.0)
let kLIGHT_SELECT_COLOR = UIColor.init(red: 55/255.0, green: 70/255.0, blue: 31/255.0, alpha: 1.0)

let kSTATUS_BAR_COLOR = kDARK_OLIVE_COLOR
let kNAVIGATION_BAR_COLOR = kLIGHT_OLIVE_COLOR

let kBUTTON_DARK_GREEN_COLOR = kDARK_OLIVE_COLOR
let kBUTTON_DARK_GREEN_FONT = UIFont.boldSystemFont(ofSize: 17.0)
let kBUTTON_DARK_GREEN_TINT_COLOR = UIColor.white

let kBUTTON_OLIVE_GREEN_COLOR = UIColor.init(red: 0/255.0, green: 149/255.0, blue: 120/255.0, alpha: 1.0) //kLIGHT_OLIVE_COLOR

//MARK: - API Headers
let kLoginRequestHeader = [
    "Content-Type" : "application/json",
    "Language" : systemLanguage
]

//MARK: - Notification Identifiers
struct kNotificationIdentifier {
    
    static let NewRideNotification = "new_ride_notification"
    static let RideAccepted = "driver_accept_ride_notification"
    static let RideFinished = "ride_finish_notification"
    static let RideCancelled = "AIzaSyBLaj-UnHUDA0hsZnrcxCjnDX1zCiI5sdQ"
    static let RideRejected = "XANZY2I6-I5SMZ3IU-Q3V6EMF7-V6RK4AY3-F4HLCET7-Q6ZF2GLX-OX3RNXWT-EKPL6LPH"
}


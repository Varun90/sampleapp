import UIKit
import MBProgressHUD
import SlideMenuControllerSwift
import FirebaseAuth
import IQKeyboardManagerSwift

let SENT_VIEW_TAG = 11123

class BaseViewController: UIViewController {
    
    var delegate : AppDelegate?
    
    // MARK: - Overrided Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: false)
        delegate = UIApplication.shared.delegate as? AppDelegate
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - AlertController Methods
    func showSimpleAlertController(_ title : String?, message : String) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - AlertController Methods
    func showSimpleAlertControllerWithOkAction(_ title : String?, message : String, callback: @escaping (()->Void)) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction(title: "OK".localized, style: .cancel) { action -> Void in
            callback()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertForInternetConnectionOff() {
        let alertController: UIAlertController = UIAlertController(title: nil, message: "ConnetionMsgKey".localized, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - MBProgressHud Methods
    func showHUD() {
        DispatchQueue.main.async{
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.navigationController?.navigationBar.isUserInteractionEnabled = false
        }
    }
    
    func hideHUD(){
        DispatchQueue.main.async{
            MBProgressHUD.hide(for: self.view, animated: true)
            
            self.navigationController?.navigationBar.isUserInteractionEnabled = true
        }
    }
    
    // MARK: - Connection Methods
    func checkConnection() -> Bool{
        var value:Bool = false
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            value = false
        case .online(.wwan):
            value = true
        case .online(.wiFi):
            value = true
        }
        return value
    }
    
    // MARK: - Password Check
    func isValidPassword(passwordStr:String?) -> Bool {
        guard passwordStr != nil else { return false }
        let passwordRegex = "^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\\d]){1,})(?=(.*[\\W]){1,})(?!.*\\s).{8,}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: passwordStr)
    }
    
    // MARK: - Email Check
    func checkEmail(_ emailString : NSString) -> Bool {
        let emailString = emailString.trimmingCharacters(in: CharacterSet.whitespaces)
        let emailRegex = "^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@",emailRegex)
        return emailTest.evaluate(with: emailString)
    }
    
    // MARK: - content height
    func getTextSize(_ inputStr:NSString, fontSize: CGFloat, contrainSize : CGSize) -> CGSize {
        let text : NSString = inputStr as NSString
        if text .isKind(of: NSString.self){
            let constraint : CGSize = CGSize(width: contrainSize.width, height: contrainSize.height)
            var attributes = [String: AnyObject]()
            attributes  = [NSFontAttributeName : UIFont.systemFont(ofSize: fontSize)]
            
            let size : CGSize = text.boundingRect(with: constraint, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil).size
            return size
        }
        return CGSize.zero
    }
    
    // MARK: - System Date Formate
    func getSystemDateFormate() -> String{
        let formatter: DateFormatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = DateFormatter.Style.medium
        formatter.timeStyle = DateFormatter.Style.medium
        let dateString : NSString = formatter.string(from: Date()) as NSString
        let amRange : NSRange = dateString.range(of: formatter.amSymbol)
        let pmRange : NSRange = dateString.range(of: formatter.pmSymbol)
        let is24h : Bool = (amRange.location == NSNotFound && pmRange.location == NSNotFound)
        if is24h{
            print("System data foramte 24 hours")
        }
        else{
            print("System data foramte 12 hours")
        }
        return ""
    }
    
    func changeDateForamteAppToWeb(_ orignalStr : String) -> String{
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let orignalDate = dateFormatter1.date(from: orignalStr)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let replceStr = dateFormatter2.string(from: orignalDate!)
        return replceStr
    }
    func changeDateForamteWebToApp(_ orignalStr : String) -> String{
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let orignalDate = dateFormatter1.date(from: orignalStr)
        
        let dateFormatter2 = DateFormatter()
        if let selectedFormate = UserDefaults.standard.value(forKey: "TimeFormate")
        {
            if "\(selectedFormate)" == "12"
            {
                dateFormatter2.dateFormat = "dd.MMMM.YYYY / hh:mm:ss a"
            }
            else
            {
                dateFormatter2.dateFormat = "dd.MMMM.YYYY / HH:mm:ss"
            }
        }
        else
        {
            dateFormatter2.dateFormat = "dd.MMMM.YYYY / HH:mm:ss"
        }
        var replceStr = ""
        if orignalDate != nil {
            replceStr = dateFormatter2.string(from: orignalDate!)
        } else {
            replceStr = orignalStr
        }
        return replceStr
    }
    
    
    func calculateContentHeight(_ size : CGSize, contentText : String) -> CGFloat{
        
        let maxLabelSize: CGSize = CGSize(width: size.width, height: CGFloat(9999))
        let contentNSString = contentText
        
        let expectedLabelSize = contentNSString.boundingRect(with: maxLabelSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17.0)], context: nil)
        
        return expectedLabelSize.size.height
        
    }
    
    // MARK: - String To Dictionary Method
    func convertStringToDictionary(text: String) -> NSDictionary? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    func stringFromHtml(string: String, fontSize: String, colorType: String) -> NSAttributedString? {
        do {
            
            let modifiedFont = NSString(format:"<span style=\"font-family: '-apple-system', 'HelveticaNeue'; font-size: %@; color:%@; \">%@</span>" as NSString, fontSize, colorType, string) as String
            print("modifiedFont:", modifiedFont)
            let data = modifiedFont.data(using: String.Encoding.utf16, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                return str
            }
        } catch {
        }
        return nil
    }
    
    func showLocationAllowAlert(){
        
    }
    func moveToSingupController(message:String){

        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
            UserDefaults.standard.set(false, forKey: KRIDER_LOGIN)
            UserDefaults.standard.removeObject(forKey: KUSER_DETAILS)
            UserDefaults.standard.removeObject(forKey: KRIDE_DETAILS)
            UserDefaults.standard.set("", forKey: KRIDER_USERNAME)
            UserDefaults.standard.synchronize()
            let appdel = UIApplication.shared.delegate as! AppDelegate
            self.firbaseAuthSignOut()
            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
            let loginVC = storyboard.instantiateViewController(withIdentifier: "RiderLoginViewController") as! RiderLoginViewController
            let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
            appdel.homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
            let nvc: UINavigationController = UINavigationController(rootViewController: loginVC)
            let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
            
            slideMenuController.delegate = appdel.homeViewController
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            slideMenuController.slideMenuController()?.changeLeftViewWidth((appdel.window?.bounds.width)!-90)
            slideMenuController.removeLeftGestures()
            appdel.window?.backgroundColor = UIColor(red: 0.0, green: 149.0, blue: 120.0, alpha: 1.0)
            appdel.window?.rootViewController = slideMenuController
            
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func firbaseAuthSignOut(){
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func setupOtpView(view:UIView){
        
        let sentView = UIView()
        sentView.tag = SENT_VIEW_TAG
        sentView.translatesAutoresizingMaskIntoConstraints = false
        sentView.backgroundColor = UIColor.white
        view.addSubview(sentView)
        let imgView = UIImageView(image: KAPP_RIDER ? #imageLiteral(resourceName: "img_tick") : #imageLiteral(resourceName: "Driver_Image_Tick"))
        imgView.translatesAutoresizingMaskIntoConstraints = false
        sentView.addSubview(imgView)
        let lblTitle = UILabel()
        lblTitle.text = "OTP Sent".localized
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        sentView.addSubview(lblTitle)
        
        let dict = ["sentView":sentView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[sentView]|", options: [], metrics: nil, views: dict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[sentView]|", options: [], metrics: nil, views: dict))
        
        view.addConstraints([NSLayoutConstraint(item: imgView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0.0)])
        view.addConstraints([NSLayoutConstraint(item: imgView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 0.4, constant: 0.0)])
        
        view.addConstraints([NSLayoutConstraint(item: imgView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: (imgView.image?.size.height)!)])
        view.addConstraints([NSLayoutConstraint(item: imgView, attribute: .width, relatedBy: .equal, toItem: nil, attribute:.height, multiplier: 1.0, constant: (imgView.image?.size.width)!)])
        
        view.addConstraints([NSLayoutConstraint(item: lblTitle, attribute: .top, relatedBy: .equal, toItem: imgView, attribute: .bottom, multiplier: 1.0, constant: 20.0)])
        view.addConstraints([NSLayoutConstraint(item: lblTitle, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0.0)])
    }
    
}

extension UITextField {
    
    func useUnderline() {
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    
    func useBorderLine(left : CGFloat , rigth : CGFloat, top: CGFloat, bottom : CGFloat) {
        
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: self.bounds.origin.y, width: self.bounds.size.width, height: top)
        topBorder.backgroundColor = UIColor.lightGray.cgColor
        self.layer.addSublayer(topBorder)
        
        let leftBorder = CALayer()
        leftBorder.frame = CGRect(x: self.bounds.origin.x, y: 0, width: left, height: self.bounds.size.height)
        leftBorder.backgroundColor = UIColor.lightGray.cgColor
        self.layer.addSublayer(leftBorder)
        
        let rightBorder = CALayer()
        rightBorder.frame = CGRect(x:  self.bounds.size.width-rigth , y: 0, width: rigth, height: self.bounds.size.height)
        rightBorder.backgroundColor = UIColor.lightGray.cgColor
        self.layer.addSublayer(rightBorder)
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0, y: self.bounds.size.height-bottom, width: self.bounds.size.width, height: bottom)
        bottomBorder.backgroundColor = UIColor.lightGray.cgColor
        self.layer.addSublayer(bottomBorder)
    }
    
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.masksToBounds = true
        self.layer.mask = mask
    }
    
}


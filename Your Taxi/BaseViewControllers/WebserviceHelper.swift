import Foundation
import UIKit
import MBProgressHUD
import SlideMenuControllerSwift


class WebserviceHelper : NSObject, URLSessionDelegate {
    
    var tag : NSInteger!
    typealias CompletionHandler = (_ success:Bool) -> Void
    var window: UIWindow?
    
    func getSession()-> URLSession {
        return URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                
                if (errSecSuccess == status) {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData);
                        let size = CFDataGetLength(serverCertificateData);
                        let cert1 = NSData(bytes: data, length: size)
                        let file_der = Bundle.main.path(forResource: "YourTaxiCert", ofType: "der")
                        
                        if let file = file_der {
                            if let cert2 = NSData(contentsOfFile: file) {
                                if cert1.isEqual(to: cert2 as Data) {
                                    completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
                                    return
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // Pinning failed
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
    
    //MARK:- POST REQUEST WITH HEADER
    func CustomHTTPPostJSONWithHeader(url: String, jsonString: NSString,withIdicator: Bool,header:[String:String],
                                      callback: @escaping (String, String?) -> Void) {
        
        if self.checkConnection() {
            
            if UserDefaults.standard.bool(forKey: KRIDER_LOGIN) && url !=  KNearestDriverList_URL && url != KSendDriverLocation_URL && url != KDRIVER_LOCATION_CONTINUOUS && url != KDriverReachingTime_URL && url != KRIDERLOGIN_URL {
                
                self.getCSRFToken(url: kGET_CSRF_TOKEN, header: header, callback: { (result, error) in
                    
                    let updatedHeader = NSMutableDictionary(dictionary: header)
                    
                    if(error == nil) {
                        
                        let responseContent = getResponseDictionaryWithResult(result: result)
                        print(responseContent)
                        
                        if let CSRFToken = responseContent["Token"] as? String {
                            updatedHeader.setValue(CSRFToken, forKey: "Token")
                        }
                    }
                    
                    self.makeRequestWithHeader(url: url, jsonString: jsonString, withIdicator: withIdicator, header: updatedHeader as! [String : String], callback: callback)
                })
            }
            else {
                makeRequestWithHeader(url: url, jsonString: jsonString, withIdicator: withIdicator, header: header, callback: callback)
            }
            
        } else{
           // self.showAlertForInternetConnectionOff()
        }
    }
    
    func makeRequestWithHeader(url: String, jsonString: NSString,withIdicator: Bool,header:[String:String],
                               callback: @escaping (String, String?) -> Void) {
        
        let requestData = jsonString.data(using: String.Encoding.ascii.rawValue)! as NSData?
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.addValue(DEVICE_UUID,forHTTPHeaderField: "deviceID")
        if header.count > 0{
            for item in header{
                request.addValue(item.value, forHTTPHeaderField : item.key)
            }
        }
        request.httpBody = requestData! as Data
        self.HTTPsendRequest(request: request, callback: callback)
    }
    
    
    //MARK:- POST REQUEST WITH HEADER
    func getCSRFToken(url: String,header:[String:String], callback: @escaping (String, String?) -> Void) {
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        
        request.addValue(DEVICE_UUID,forHTTPHeaderField: "deviceID")
        if header.count > 0{
            for item in header{
                request.addValue(item.value, forHTTPHeaderField : item.key)
            }
        }
        HTTPsendRequest(request: request, callback: callback)
    }
    
    
    //MARK:- POST REQUEST WITH HEADER
    func HTTPPostJSONWithHeader(url: String,  paramters: NSDictionary,withIdicator: Bool,header:[String:String],
                                callback: @escaping (String, String?) -> Void) {
        
        if self.checkConnection() {
            var jsonString = ""
            jsonString =  paramters.stringFromHttpParameters()
            let requestData = jsonString.data(using: String.Encoding.utf8)! as NSData?
            var request = URLRequest(url: URL(string: url)!)
            request.httpMethod = "POST"
            request.addValue(DEVICE_UUID,forHTTPHeaderField: "deviceID")
            if header.count > 0{
                for item in header{
                    request.addValue(item.value, forHTTPHeaderField : item.key)
                }
            }
            request.httpBody = requestData! as Data
            HTTPsendRequest(request: request, callback: callback)
            
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    //MARK:- GET REQUEST METHOD
    func HTTPGetJSON(url: String,withIdicator: Bool ,callback: @escaping (String, String?) -> Void) {
        
        if self.checkConnection() {
            let urlString = NSURL(string: url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!
            var request = URLRequest(url: urlString as URL)
            request.httpMethod = "GET"
            request.addValue("application/x-www-form-urlencoded; charset=utf-8",forHTTPHeaderField: "Content-Type")
            request.addValue("application/json",forHTTPHeaderField: "Accept")
            HTTPsendRequest(request: request, callback: callback)
            
        }else{
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func HTTPsendRequest(request: URLRequest, callback: @escaping (String, String?) -> Void) {
        let session = getSession()
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) -> Void in
            if let httpStatus = response as? HTTPURLResponse {
                print("status code = \(httpStatus.statusCode)")
                if httpStatus.statusCode == 401{
                    do {
                        if UserDefaults.standard.object(forKey: KUSER_DETAILS) != nil{
                            let message = "Session Expired"
                            if message == "Session Expired" {
                                DispatchQueue.main.async{
                                    let appDelegate = UIApplication.shared.delegate! as! AppDelegate
                                    KAPP_RIDER ? appDelegate.logoutRider() : appDelegate.performLogout()
                                }
                            }
                            return;
                        }
                    }catch {
                        print(error.localizedDescription)
                    }
                }
            }
            OperationQueue.main.addOperation(){
                if (error != nil) {
                    callback("Parsing Error", error?.localizedDescription)
                    
                } else {
                    callback(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String, nil)
                }
            }
        }
        task.resume()
    }
    
    //MARK:- CUSTOM METHODS
    func convertStringToDictionary(text: String) -> NSDictionary? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func showSimpleAlertControllerWithOkAction(_ title : String?, message : String, callback: @escaping (()->Void)) {
        DispatchQueue.main.async {
            let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                callback()
            }
        
        alertController.addAction(okAction)
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK:- MULTIPART POST REQUEST METHODS
    func myImageUploadRequest(_ image : UIImage, url  : String, button : UIButton, completionHandler: @escaping CompletionHandler)
    {
        
        if self.checkConnection()
        {
            let myUrl = NSURL(string: url);
            
            let request = NSMutableURLRequest(url:myUrl! as URL);
            request.httpMethod = "POST";
            
            let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
            let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
            let userDict = self.convertStringToDictionary(text: decryt!)!
            
            let riderID  = userDict.value(forKey: "id") as! NSString
            
            let param = [
                "rider_id": riderID as String
                ] as [String : String]
            
            let boundary = generateBoundaryString()
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let imageData = UIImageJPEGRepresentation(image, 1)
            
            if(imageData==nil)  { return; }
            
            request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "profile_pic", imageDataKey: imageData! as NSData, boundary: boundary) as Data
            
            let session = getSession()
            
            let task = session.dataTask(with: request as URLRequest) {
                (data, response, error) in
                
                if error != nil {
                    print(error as Any)
                    return
                }
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    
                    if json?.value(forKey: "status") as! String == "success"
                    {
                        DispatchQueue.main.async(execute: {
                            button.setBackgroundImage(image, for: .normal)
                            let profile_img = (json?.value(forKey: "result") as! NSDictionary).value(forKey: "profile_pic") as! String
                            let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
                            let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
                            var userDict = self.convertStringToDictionary(text: decryt!)!
                            
                            userDict = userDict.mutableCopy() as! NSMutableDictionary
                            userDict.setValue(profile_img, forKey: "profile_pic")
                            
                            var encryptData = returnJsonString(param: userDict as! [String : Any])
                            encryptData = AESCrypt.encrypt(encryptData, password: SECRET_KEY)
                            
                            UserDefaults.standard.setValue(encryptData, forKey: KUSER_DETAILS)
                            UserDefaults.standard.synchronize()
                            completionHandler(true)
                        });
                    }
                    
                    
                }catch
                {
                    print(error)
                }
            }
            
            task.resume()
        }
        else
        {
            self.showAlertForInternetConnectionOff()
        }
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString("\r\n")
        
        
        
        body.appendString("--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    // MARK: - Connectivity Methods
    
    func checkConnection() -> Bool{
        var value:Bool = false
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            value = false
        case .online(.wwan):
            value = true
        case .online(.wiFi):
            value = true
        }
        return value
    }
    
    func showAlertForInternetConnectionOff() {
        DispatchQueue.main.async {
            let alertController: UIAlertController = UIAlertController(title: nil, message: "ConnetionMsgKey".localized, preferredStyle: .alert)
            
            let okAction: UIAlertAction = UIAlertAction(title: "Ok".localized, style: .cancel) { action -> Void in
                
            }
            alertController.addAction(okAction)
            let appdel = UIApplication.shared.delegate
            if let viewcontroller = appdel?.window??.topMostController() {
                if viewcontroller is UIAlertController{
                }
                else{
                    appdel?.window??.topMostController()?.present(alertController, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    
    //MARK:- CUSTOM HEADER METHOD (DRIVER)
    func getHeader(parameters:[String:Any]) -> [String:String]{
        let headerDict = [
            "authtoken":getDriverAuthToken(),
          //  "Authorization":Authorization,
            "language": systemLanguage
        ]
        return headerDict
    }
    
    //MARK:- CUSTOM HEADER METHOD (RIDER)
    func getHeaderForRider(parameters:[String:Any]) -> [String:String]{
        let headerDict = [
            "Content-Type" : "application/json",
            "Authorization" : UserDefaults.standard.string(forKey: kAUTH_TOKEN)! as String,
            "Language" : systemLanguage
        ]
        return headerDict
    }
    
    //MARK:- CUSTOM HEADER METHOD
    func getUserDefaultsStandard() -> Int32{
        let dic = UserDefaults.standard.object(forKey: KUSER_DETAILS) as! String
        let decryt = AESCrypt.decrypt(dic, password: SECRET_KEY)
        let userDict = self.convertStringToDictionary(text: decryt!)!
        let riderID  = userDict.value(forKey: "id") as! Int32
        return riderID
    }
}

// MARK:- Dictionary to String Extension
extension NSDictionary {
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlHostAllowed)!
            let percentEscapedValue = ("8" as! String).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.alphanumerics)!
            
            return "\(percentEscapedKey)=\(percentEscapedValue)" as String
        }
        return parameterArray.joined(separator: "&")
    }
}


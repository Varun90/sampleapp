import UIKit

class CustomTextField: UITextField {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if KAPP_RIDER {
            
            let currentCell : UITableViewCell = self.superview?.superview as! UITableViewCell
            let bookRideTable : UITableView = self.superview?.superview?.superview?.superview as! UITableView
            let currentIndexPath : IndexPath = bookRideTable.indexPath(for: currentCell)!
            
            if bookRideTable.tag == MoreDetailsTableTag {
                
                if action == #selector(UIResponderStandardEditActions.paste(_:)) {
                    return false
                }
                
            }else{
                
                if (currentIndexPath as NSIndexPath).row == 2 {
                    
                    if action == #selector(UIResponderStandardEditActions.paste(_:)) {
                        return false
                    }
                }
            }
            
        }else{
            
            if action == #selector(UIResponderStandardEditActions.paste(_:)) {
                return false
            }
        }
        
        return super.canPerformAction(action, withSender: sender)
    }
}


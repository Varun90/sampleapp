import Foundation

typealias DriversCarList = [ActiveCar]

struct ActiveCar: Codable {
    let carID: Int
    let brand, model: String
    let year: Int
    let plateNumber: PlateNumber
    let carType, userID, status, selectedCar: Int
    let createdDate: String
    let createdBy: Int
    let updatedDate: String?
    let updatedBy: Int?
    let carTypeName: String?
    
    enum CodingKeys: String, CodingKey {
        case carID = "CarId"
        case brand = "Brand"
        case model = "Model"
        case year = "Year"
        case plateNumber = "PlateNumber"
        case carType = "CarType"
        case userID = "UserId"
        case status = "Status"
        case selectedCar = "SelectedCar"
        case createdDate = "CreatedDate"
        case createdBy = "CreatedBy"
        case updatedDate = "UpdatedDate"
        case updatedBy = "UpdatedBy"
        case carTypeName = "CarTypeName"
    }
}

enum PlateNumber: Codable {
    case integer(Int)
    case string(String)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(PlateNumber.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for PlateNumber"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

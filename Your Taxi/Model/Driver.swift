// To parse the JSON, add this file to your project and do:
//
//   let driver = try? newJSONDecoder().decode(Driver.self, from: jsonData)

import Foundation

struct Driver: Codable {
    let user: User
    let company: Company
    let car: Car
    let bank: Bank
}

struct Bank: Codable {
    let bankID: Int
    let name, address, city: String
    let postalCode: Int
    let ibanAccNo: String
    let bicSwift: Int
    
    enum CodingKeys: String, CodingKey {
        case bankID = "BankId"
        case name = "Name"
        case address = "Address"
        case city = "City"
        case postalCode = "PostalCode"
        case ibanAccNo = "IbanAccNo"
        case bicSwift = "BicSwift"
    }
}

struct Car: Codable {
    let carID, brand, year, model: JSONNull?
    let plateNumber, carType, idOfCar: JSONNull?
    let status: Int
    
    enum CodingKeys: String, CodingKey {
        case carID = "CarId"
        case brand = "Brand"
        case year = "Year"
        case model = "Model"
        case plateNumber = "PlateNumber"
        case carType = "CarType"
        case idOfCar = "IdOfCar"
        case status = "Status"
    }
}

struct Company: Codable {
    let companyID: Int
    let name: String
    let noOfDriver, stNo: Int
    let stName: String
    let postalCode: Int
    let city, vat: String
    let commercialRegister: String
    
    enum CodingKeys: String, CodingKey {
        case companyID = "CompanyId"
        case name = "Name"
        case noOfDriver = "NoOfDriver"
        case stNo = "StNo"
        case stName = "StName"
        case postalCode = "PostalCode"
        case city = "City"
        case vat = "Vat"
        case commercialRegister = "CommercialRegister"
    }
}

struct User: Codable {
    let userID: Int
    let driverRating: Double
    let title, firstName, lastName, dob: String
    let email: String
    let phoneNo, country: Int
    let countryName, dialCode, language, address1: String
    let address2: String
    let postalCode: Int
    let city: String
    let userType: Int
    let legalPermitType: String
    let status: Int
    let profilePic, legalPermit1, legalPermit2, driverLicence1: String
    let driverLicence2, taxiOperatingLicence1, taxiOperatingLicence2, selfEmploymentSva: String
    let userAsCompany: Int
    
    enum CodingKeys: String, CodingKey {
        case userID = "UserId"
        case driverRating = "DriverRating"
        case title = "Title"
        case firstName = "FirstName"
        case lastName = "LastName"
        case dob = "Dob"
        case email = "Email"
        case phoneNo = "PhoneNo"
        case country = "Country"
        case countryName = "CountryName"
        case dialCode = "DialCode"
        case language = "Language"
        case address1 = "Address1"
        case address2 = "Address2"
        case postalCode = "PostalCode"
        case city = "City"
        case userType = "UserType"
        case legalPermitType = "LegalPermitType"
        case status = "Status"
        case profilePic = "ProfilePic"
        case legalPermit1 = "LegalPermit1"
        case legalPermit2 = "LegalPermit2"
        case driverLicence1 = "DriverLicence1"
        case driverLicence2 = "DriverLicence2"
        case taxiOperatingLicence1 = "TaxiOperatingLicence1"
        case taxiOperatingLicence2 = "TaxiOperatingLicence2"
        case selfEmploymentSva = "SelfEmploymentSva"
        case userAsCompany = "UserAsCompany"
    }
}

// MARK: Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}


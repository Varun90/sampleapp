import UIKit

class PreBookingListObject: NSObject {
    
    var id : String? = ""
    var bookingId : Int64?
    var pickupLocation : String? = ""
    var pickupLatlong : String? = ""
    var pickupDate : String? = ""
    var CarTypeId : Int32?
    var dropLocation : String? = ""
    var dropLocationLatlong : String? = ""
    var finalPrice : String?
    var cancelAmount: Double?
    var paymentStatus: String?
    var totalKm : Int?
    var rideStatus : Int = 0
    var totalTime : String?
    var rideStatusName : String?
    var carType : String? = ""
    var rideType : Int32?
    var bookingType : String? = ""
    var rideHour : String? = ""
    var canUpdate : Bool?
    var payment : String? = ""
    var additionalRef: NSDictionary?
    var rentalPlanId: Int32?
    var showArriveBtn : Bool?
    convenience public init(dict:NSDictionary) {
        self.init()
        self.setObjectData(dict: dict)
    }
    
    //MARK:- SET OBJECT DATA
    func setObjectData(dict:NSDictionary){
        self.additionalRef = dict["AdditionalRef"] as? NSDictionary
        self.rentalPlanId = dict["RentalPlanId"] as? Int32
        self.bookingId = dict["BookingId"] as? Int64
        self.payment = dict["Payment"] as? String
        self.pickupLocation = dict["PickupLocation"] as? String
        self.pickupLatlong = dict["PickupLatitude"] as? String
        self.pickupDate = dict["BookingDateTime"] as? String
        self.CarTypeId = dict["CarTypeId"] as? Int32
        self.dropLocation = dict["DropLocation"] as? String
        self.dropLocationLatlong = dict["DropLatitude"] as? String
        self.rideStatusName = dict["RideStatusName"] as? String
        self.totalKm = dict["TotalKm"] as? Int
        self.totalTime = dict["TotalTime"] as? String
        self.finalPrice = dict["FinalPrice"] as? String
        self.cancelAmount = dict["CancelAmount"] as? Double
        self.paymentStatus = dict["PaymentStatus"] as? String
        self.canUpdate = dict["canUpdate"] as? Bool
        self.showArriveBtn = dict["ShowArriveBtn"] as? Bool
        self.rideStatus = (dict["RideStatus"] as? Int)!
        self.carType = dict["CarType"] as? String
        self.rideType = dict["RideType"] as? Int32
        if dict["BookingType"] as? Int == 2 {
            self.bookingType = "Pre-Booking".localized
        }
        else {
            self.bookingType = dict["booking_type"] as? String
        }
        self.rideHour = dict["TotalTime"] as? String
    }
    
    //MARK:- GET OBJECT DATA
    func getObjectData() -> NSMutableDictionary {
        
        let dict = NSMutableDictionary()
        dict.setValue(self.additionalRef, forKey: "AdditionalRef")
        dict.setValue(self.rentalPlanId, forKey: "RentalPlanId")
        dict.setValue(self.bookingId, forKey: "BookingId")
        dict.setValue(self.payment, forKey: "Payment")
        dict.setValue(self.pickupLocation, forKey: "PickupLocation")
        dict.setValue(self.pickupLatlong, forKey: "pickupLatlong")
        dict.setValue(self.pickupDate, forKey: "BookingDateTime")
        dict.setValue(self.CarTypeId, forKey: "CarTypeId")
        dict.setValue(self.dropLocation, forKey: "DropLocation")
        dict.setValue(self.dropLocationLatlong, forKey: "DropLatitude")
        dict.setValue(self.rideStatusName, forKey: "RideStatusName")
        dict.setValue(self.totalKm, forKey: "TotalKm")
        dict.setValue(self.totalTime, forKey: "TotalTime")
        dict.setValue(self.finalPrice, forKey: "FinalPrice")
        dict.setValue(self.cancelAmount, forKey: "CancelAmount")
        dict.setValue(self.paymentStatus, forKey: "PaymentStatus")
        dict.setValue(self.canUpdate, forKey: "canUpdate")
        dict.setValue(self.showArriveBtn, forKey: "ShowArriveBtn")
        dict.setValue(self.rideStatus, forKey: "RideStatus")
        dict.setValue(self.carType, forKey: "CarType")
        dict.setValue(self.rideType, forKey: "RideType")
        dict.setValue(self.bookingType, forKey: "BookingType")
        dict.setValue(self.rideHour, forKey: "TotalTime")
        dict.setValue(true, forKey: "isEditable")
        
        return dict
    }
}


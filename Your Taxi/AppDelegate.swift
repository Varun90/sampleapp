//import UIKit
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import INTULocationManager
import MBProgressHUD
import AVFoundation
import GooglePlaces
import UserNotifications
import SlideMenuControllerSwift
import AccountKit
import CoreLocation
import MapKit
import Stripe
import Firebase
import GoogleMaps
import UserNotifications
import MediaPlayer
var locationManager1 : CLLocationManager!
var myLocationArrayInPlist : NSMutableArray!
var myLocationDictInPlist : NSMutableDictionary!

enum RideRequestStatus {
    case rideAccepted
    case rideRejected
    case other
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var closestCarMarkers:[GMSMarker] = []
    var window: UIWindow?
    var riderAddressInfo: NSDictionary?
    var paramDic: NSDictionary?
    var userLocation : CLLocation?
    var audioPlayer: AVAudioPlayer!
    var accountKit: AKFAccountKit!
    var location: CLLocation!
    var requestID : INTULocationRequestID!
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var carType : String = "1"
    var dis = 0.0
    var startRide : Bool = false
    var seenError : Bool = false
    var locationFixAchieved : Bool = false
    var locationStatus : NSString = "Not Started"
    var afterResume : Bool!
    var homeViewController : HomeViewController!
    var driverAnnotation : CustomAnnotation!
    var pinAnnotationView:MKAnnotationView!
    var busDirection: CLLocationDirection!
    var logoutAlertController : UIAlertController!
    var driveListTimer : Timer?
    var lastDate : Date?
    var locDisctnace = 0.0
    var old_Location: CLLocation?
    var resultArray: NSMutableArray = []
    var userIDString: String?
    var userIDDisString: String?
    var rideRequestStatus:RideRequestStatus?
    var isRiderFeedBackPresented : Bool = false
    var prvVC = UIViewController()
    var driver1ReachTimeCount = 0
    var driver2ReachTimeCount = 0
    var driver3ReachTimeCount = 0
    
    //MARK: - Application Life Cycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey(GOOGLE_WEB_API_KEY)
        FirebaseApp.configure()
       
        if KAPP_RIDER {
            UINavigationBar.appearance().barTintColor = kNAVIGATION_BAR_COLOR
            self.setStatusBarBackgroundColor(color: kNAVIGATION_BAR_COLOR) //kSTATUS_BAR_COLOR
        }
        else{
            UINavigationBar.appearance().barTintColor = UIColor.init(red: 239/255.0, green: 117/255.0, blue: 33/255.0, alpha: 1.0)
        }
        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18)]
        UISegmentedControl.appearance().tintColor = KAPP_COLOR
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: UIControlState())
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: UIControlState.selected)
        UISwitch.appearance().onTintColor = KAPP_COLOR
        
        if StripePublishableKey != nil
        {
            Stripe.setDefaultPublishableKey(StripePublishableKey!)
        }
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        GMSPlacesClient.provideAPIKey(GMS_TOKEN)
        if(!KAPP_RIDER){
            if(launchOptions != nil){
                if let _ = launchOptions?[UIApplicationLaunchOptionsKey.location] {
                    
                    self.initailizeLocationManger()
                    
                }
            }
        }
        
        if KAPP_RIDER {
            UIApplication.shared.statusBarStyle = .lightContent
            
            self.initailizeLocationManger()
            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
            
            let loginVC = storyboard.instantiateViewController(withIdentifier: "RiderLoginViewController") as! RiderLoginViewController
            let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
            
            self.homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            
            let nvc: UINavigationController = UINavigationController(rootViewController: loginVC)
            
            let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
            
            slideMenuController.delegate = self.homeViewController
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            
            slideMenuController.slideMenuController()?.changeLeftViewWidth((self.window?.bounds.width)!-90)
            slideMenuController.removeLeftGestures()
            
            self.window?.backgroundColor = UIColor(red: 0.0, green: 149.0, blue: 120.0, alpha: 1.0)
            self.window?.rootViewController = slideMenuController
           
            if UserDefaults.standard.value(forKey: "TimeFormate") == nil
            {
                UserDefaults.standard.set("24", forKey: "TimeFormate")
                UserDefaults.standard.synchronize()
            }
        }
        else
        {
            self.initailizeLocationManger()
            
        }
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
        }
        Fabric.with([Crashlytics.self])
        self.initializeNotificationServices(application: application)
        return true
    }
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    func startOrStopRide(_ notification : Notification)
    {
        if let val = notification.object as? String
        {
            if val == "true"
            {
                dis = 0.0
                startRide = true
            }
            else
            {
                startRide = false
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RideIsStart"), object: nil)
                UserDefaults.standard.set(dis, forKey: "Distance")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func initailizeLocationManger() {
        
        if(locationManager1 != nil){
            locationManager1.stopMonitoringSignificantLocationChanges()
        }
        locationManager1 = CLLocationManager()
        locationManager1.delegate = self
        if #available(iOS 9.0, *) {
            locationManager1.requestLocation()
        }
        if #available(iOS 9.0, *) {
            locationManager1.allowsBackgroundLocationUpdates = true
        }
        locationManager1.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager1.requestAlwaysAuthorization()
        
        locationManager1.startUpdatingLocation()
        locationManager1.activityType = CLActivityType.otherNavigation;
        locationManager1.startMonitoringSignificantLocationChanges()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
        })
        if (!KAPP_RIDER){
            self.restartMonitoring()
        }
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        let pushToken = getDecryptedData(key: kPUSH_TOKEN)
        
        if pushToken == "" {
            self.initializeNotificationServices(application: application)
        }
        
        #if APP_RIDER
            if UserDefaults.standard.bool(forKey: KRIDER_LOGIN){
                
            }
        #endif
        
        if !KAPP_RIDER {
            
            let rootViewController = window!.rootViewController
            if rootViewController?.presentedViewController != nil {
                
                let visibleViewController:UIViewController = (rootViewController?.presentedViewController)!
                
                if visibleViewController.isKind(of: OrderDetailsViewController.self) {
                    
                    let orderDetailsObject:OrderDetailsViewController = visibleViewController as! OrderDetailsViewController
                    if orderDetailsObject.timer.isValid {
                        let str = NSString(format:"%.0fs", orderDetailsObject.currentCount)
                        orderDetailsObject.timerLable.text=str as String!
                    }
                    
                }
            }
            
        }
        else if KAPP_RIDER{
            let visibleVC = UIApplication.topViewController()!
            if (visibleVC is SlideMenuController) {
                let slideMenuVC = (visibleVC as! SlideMenuController)
                let nav = (slideMenuVC.mainViewController as! UINavigationController)
                let topVC = nav.viewControllers.last
                if topVC is HomeViewController{
                    (topVC as! HomeViewController).setCurrentLocation()
                }
            }
        }
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    @available(iOS 10.0, *)
    func handleDeliveredNotificationWithType(notifyType : String) {
        
        var filteredNotifications:[UNNotification]?
        
        UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
            
            DispatchQueue.main.async {
                UIApplication.shared.applicationIconBadgeNumber = notifications.count
            }
            
            if notifications.count > 0 {
                switch(notifyType) {
                    
                case kNotificationIdentifier.NewRideNotification :
                    
                    filteredNotifications = notifications.filter {$0.request.content.categoryIdentifier == notifyType}
                    
                    if let firstNotification = filteredNotifications?.first {
                        let interval = NSDate().timeIntervalSince(firstNotification.date as Date)
                        print(interval)
                        if interval < 30 {
                            firstNotification.removeFromNotificationsList()
                            self.remoteNotificationHandle(UIApplication.shared, userInfo: (firstNotification.request.content.userInfo))
                        }
                    }
                    
                case kNotificationIdentifier.RideAccepted :
                    
                    filteredNotifications = notifications.filter {
                        if let aps = $0.request.content.userInfo["aps"] as? NSDictionary, let nType = aps["notify_type"] as? String, nType == notifyType {
                            return true
                        }
                        return false
                    }
                    
                    if let firstNotification = filteredNotifications?.first {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            firstNotification.removeFromNotificationsList()
                            self.redirectRideAcceptNotification((firstNotification.request.content.userInfo) as NSDictionary)
                        }
                    }
                    
                case kNotificationIdentifier.RideFinished :
                    
                    filteredNotifications = notifications.filter {
                        if let aps = $0.request.content.userInfo["aps"] as? NSDictionary, let nType = aps["notify_type"] as? String, nType == notifyType {
                            return true
                        }
                        return false
                    }
                    
                    if let firstNotification = filteredNotifications?.first {
                        firstNotification.removeFromNotificationsList()
                        self.redirectRideFinishNotification((firstNotification.request.content.userInfo) as NSDictionary)
                    }
                    
                case kNotificationIdentifier.RideCancelled :
                    
                    filteredNotifications = notifications.filter {
                        if let aps = $0.request.content.userInfo["aps"] as? NSDictionary, let nType = aps["notify_type"] as? String, nType == notifyType {
                            return true
                        }
                        return false
                    }
                    
                    if let firstNotification = filteredNotifications?.first {
                        firstNotification.removeFromNotificationsList()
                        self.redirectRideCancelNotification((firstNotification.request.content.userInfo) as NSDictionary)
                    }
                    
                default:
                    return
                }
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if !KAPP_RIDER{
            MPVolumeView.setVolume(1.0)
        }
        if (UserDefaults.standard.object(forKey: KUSER_DETAILS) != nil){
            NotificationCenter.default.post(name: Notification.Name(rawValue: "NotifyAppVersion"), object: nil)
        }
        
        DispatchQueue.main.async {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        if UserDefaults.standard.bool(forKey: KRIDER_LOGIN) {
            
            if !KAPP_RIDER {
                if #available(iOS 10.0, *) {
                    handleDeliveredNotificationWithType(notifyType: kNotificationIdentifier.NewRideNotification)
                }
                self.initailizeLocationManger()
                DispatchQueue.main.async {
                      self.homeViewController.callUserInformationAPIWithUserID(userID: getLoginDriverID())
                }
            }
            else {
                
                if #available(iOS 10.0, *) {
                    handleDeliveredNotificationWithType(notifyType: kNotificationIdentifier.RideAccepted)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    //Get the Feedback Form and Show if available
                    if let rideFeedbackUserInfo = UserDefaults.standard.value(forKey: KRIDER_FEEDBACK_INFO) as? NSDictionary, !self.isRiderFeedBackPresented {
                        self.redirectRideFinishNotification(rideFeedbackUserInfo)
                    }
                    else {
                        if #available(iOS 10.0, *) {
                            self.handleDeliveredNotificationWithType(notifyType: kNotificationIdentifier.RideFinished)
                        }
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
        }
    }
    
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: - Indicator Methods
    func showHUD() {
        DispatchQueue.main.async{
            MBProgressHUD.showAdded(to: self.window!.rootViewController!.view, animated: true)
        }
    }
    
    func hideHUD(){
        DispatchQueue.main.async{
            MBProgressHUD.hideAllHUDs(for: self.window!.rootViewController!.view, animated: true)
        }
    }
    
    // MARK: - Push Notfication Delegate
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let categoryIdentifier = userInfo["category"] as? NSString, categoryIdentifier == "new_ride_notification" {
            rideRequestStatus = .other
        }
        #if APP_RIDER
            if Auth.auth().canHandleNotification(userInfo) {
                completionHandler(UIBackgroundFetchResult.noData)
                return
            }
        #endif
//        let state = UIApplication.shared.applicationState
//
//        switch  state{
//        case .background:
//            print("sate:", state)
//            self.remoteNotificationHandle(application, userInfo: userInfo)
//        default:
//            self.remoteNotificationHandle(application, userInfo: userInfo)
//
//        }
        self.remoteNotificationHandle(application, userInfo: userInfo)
    }
    
    // MARK: - Push Notfication Delegate for >= iOS10
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //self.playSound(UIApplication.shared, userInfo: response.notification.request.content.userInfo)
        
        if UserDefaults.standard.bool(forKey: KRIDER_LOGIN){
            
            if response.notification.request.content.categoryIdentifier == "new_ride_notification" {
                rideRequestStatus = .other
            }
            if response.actionIdentifier == "accept.action" || response.actionIdentifier == "reject.action" {
                self.handleCategoryNotificationWithResponse(response: response)
            }
            self.remoteNotificationHandle(UIApplication.shared, userInfo: response.notification.request.content.userInfo)
        }
    }
    
    @available(iOS 10.0, *)
    func handleCategoryNotificationWithResponse(response: UNNotificationResponse) {
        
        let categoryIdentifier = response.notification.request.content.categoryIdentifier
        
        if categoryIdentifier == "new_ride_notification" {
            
            if response.actionIdentifier == "accept.action" {
                rideRequestStatus = .rideAccepted
                print("Ride Accepted")
                self.stopSound()
            } else if response.actionIdentifier == "reject.action"{
                rideRequestStatus = .rideRejected
                print("Ride Rejected")
                self.stopSound()
            }
        }
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
        let application = UIApplication.shared
        
        self.remoteNotificationHandle(application, userInfo: notification.request.content.userInfo)
    }
    
    
    // MARK: - Push Token Register Methods
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
        
        encrypt(data: tokenString as AnyObject, key: kPUSH_TOKEN)
        //print("\n\nPushToken\t\(tokenString)\t\n\n")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        encrypt(data: "" as AnyObject, key: kPUSH_TOKEN)
        
    }
    
    // MARK: - Push NotficationServices Methods
    
    func initializeNotificationServices() {
        let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func initializeNotificationServices(application : UIApplication) -> Void {
        
        // iOS 12 support
        if #available(iOS 12, *) {  
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granteds, error) in
            if granteds {
              self.setCategorie()
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
                }
            }
        }
        
        // iOS 10 support
        if #available(iOS 10, *) {
            
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in
                if granted {
                    self.setCategories()
                    
                    DispatchQueue.main.async {
                        application.registerForRemoteNotifications()
                    }
                }
            }
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
        
    }
    
    @available(iOS 10.0, *)
    func setCategories(){
        
        let acceptAction = UNNotificationAction(
            identifier: "accept.action",
            title: "Accept",
            options: [.foreground])
        
        let rejectAction = UNNotificationAction(
            identifier: "reject.action",
            title: "Reject",
            options: [.foreground,.destructive])
        
        let pizzaCategory = UNNotificationCategory(
            identifier: "new_ride_notification",
            actions: [acceptAction,rejectAction],
            intentIdentifiers: [],
            options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories(
            [pizzaCategory])
    }
    @available(iOS 12, *)
    func setCategorie(){
        
        let acceptAction = UNNotificationAction(
            identifier: "accept.action",
            title: "Accept",
            options: [.foreground])
        
        let rejectAction = UNNotificationAction(
            identifier: "reject.action",
            title: "Reject",
            options: [.foreground,.destructive])
        
        let pizzaCategory = UNNotificationCategory(
            identifier: "new_ride_notification",
            actions: [acceptAction,rejectAction],
            intentIdentifiers: [],
            options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories(
            [pizzaCategory])
    }
    
    func discountPushNotificationHandler(_ userInfo: NSDictionary) -> Void {
        
        let rootViewController = window!.rootViewController
        let visibleViewController:UIViewController = getVisibleViewController(rootViewController)!
        
        if visibleViewController.isKind(of: DiscountViewController.self) {
            
            let discountvcObj:DiscountViewController = visibleViewController as! DiscountViewController
            discountvcObj.dicountInfoDic=userInfo
            discountvcObj.viewWillAppear(true)
            
        }else{
            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
            let discountvcObj = storyboard.instantiateViewController(withIdentifier: "DiscountViewController") as? DiscountViewController
            discountvcObj!.dicountInfoDic=userInfo
            discountvcObj?.view.backgroundColor=UIColor.black.withAlphaComponent(0.7)
            discountvcObj!.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            window?.rootViewController?.present(discountvcObj!, animated:true, completion: nil)
        }
    }
    
    // MARK: - Push Notfication Handler Methods
    func OrderPushNotficationhandler(_ riderInfo: NSDictionary) -> Void {
        
        self.hideHUD();
        let visibleViewController:UIViewController = UIApplication.topViewController()!
        if visibleViewController.isKind(of: UIAlertController.self) {
            visibleViewController.dismiss(animated: false, completion: nil)
        }
        if visibleViewController.isKind(of: OrderDetailsViewController.self) {
            print("Order Details Screen already open")
            let orderDetailsObject:OrderDetailsViewController = visibleViewController as! OrderDetailsViewController
            orderDetailsObject.dismiss(animated: false, completion: {
                if self.window?.rootViewController?.storyboard != nil {
                    let orderDetailsObject = self.window?.rootViewController?.storyboard!.instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
                    orderDetailsObject!.riderInfoDic=riderInfo
                    let nvc: UINavigationController = UINavigationController(rootViewController: orderDetailsObject!)
                    self.window?.rootViewController?.present(nvc, animated:false, completion: nil)
                } else {
                    let storyboard = UIStoryboard(name: "Driver", bundle: nil)
                    let orderDetailsObject = storyboard.instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
                    orderDetailsObject!.riderInfoDic=riderInfo
                    let nvc: UINavigationController = UINavigationController(rootViewController: orderDetailsObject!)
                    self.window?.rootViewController?.present(nvc, animated:false, completion: nil)
                    
                }
            })
            
        }else{
            print("Presenting new order details screen")
            let rootVC = UIApplication.shared.keyWindow?.rootViewController
            if rootVC?.storyboard != nil {
                let orderDetailsObject = rootVC!.storyboard!.instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
                orderDetailsObject!.riderInfoDic = riderInfo
                let nvc: UINavigationController = UINavigationController(rootViewController: orderDetailsObject!)
                UIApplication.topViewController()?.present(nvc, animated:false, completion: nil)
            } else {
                let storyboard = UIStoryboard(name: "Driver", bundle: nil)
                let orderDetailsObject = storyboard.instantiateViewController(withIdentifier: "OrderDetailsViewController") as? OrderDetailsViewController
                orderDetailsObject!.riderInfoDic = riderInfo
                let nvc: UINavigationController = UINavigationController(rootViewController: orderDetailsObject!)
                rootVC!.present(nvc, animated:false, completion: nil)
            }
        }
        
    }
    
    func redirectRideAcceptNotification(_ userInfo : NSDictionary)
    {
        let rootViewController = window!.rootViewController
        let visibleViewController:UIViewController = getVisibleViewController(rootViewController)!
        if (visibleViewController.presentingViewController != nil) {
            visibleViewController.dismiss(animated: false, completion: {() -> Void in
                self.redirectRideAcceptNotification(userInfo)
            })
        }
        else if (visibleViewController is SlideMenuController) {
            
            
            let slideMenuVC = (visibleViewController as! SlideMenuController)
            let nav = (slideMenuVC.mainViewController as! UINavigationController)
            let rootCon = nav.viewControllers.last
            if !(rootCon is HomeViewController){
                _ = nav.popToRootViewController(animated: false)!
            }
            let visibleViewController1 : UIViewController = self.getVisibleViewController(self.window!.rootViewController)!
            if visibleViewController1 is SlideMenuController
            {
                let slideMenuVC1 = (visibleViewController1 as! SlideMenuController)
                let nav1 = (slideMenuVC1.mainViewController as! UINavigationController)
                let rootCon = nav1.viewControllers.last
                if rootCon is HomeViewController
                {
                    (rootCon as! HomeViewController).callWebserviceForGettingCurrentRide()
                }
            }
            
        }
    }
    
    func redirectRideOtherStatusNotification(_ userInfo : NSDictionary)
    {
        let rootViewController = window!.rootViewController
        let visibleViewController:UIViewController = getVisibleViewController(rootViewController)!
        if (visibleViewController.presentingViewController != nil) {
            visibleViewController.dismiss(animated: false, completion: {() -> Void in
                self.redirectRideAcceptNotification(userInfo)
            })
        }
        else if (visibleViewController is SlideMenuController) {
            let alertController: UIAlertController = UIAlertController(title: nil, message: ((userInfo["aps"] as! NSDictionary).value(forKey: "alert") as! String as NSString) as String, preferredStyle: .alert)
            
            //Create and add the Cancel action
            let okAction: UIAlertAction = UIAlertAction(title: "OK".localized, style: .cancel) { action -> Void in
                
                let slideMenuVC = (visibleViewController as! SlideMenuController)
                let nav = (slideMenuVC.mainViewController as! UINavigationController)
                let rootCon = nav.viewControllers.last
                if !(rootCon is HomeViewController){
                    _ = nav.popToRootViewController(animated: false)!
                }
                let visibleViewController1 : UIViewController = self.getVisibleViewController(self.window!.rootViewController)!
                if visibleViewController1 is SlideMenuController
                {
                    let slideMenuVC1 = (visibleViewController1 as! SlideMenuController)
                    let nav1 = (slideMenuVC1.mainViewController as! UINavigationController)
                    let rootCon = nav1.viewControllers.last
                    if rootCon is HomeViewController
                    {
                        (rootCon as! HomeViewController).callWebserviceForGettingCurrentRide()
                    }
                }
            }
            alertController.addAction(okAction)
            
            self.window?.rootViewController!.present(alertController, animated: true, completion: nil)
        }
    }
    
    func redirectRideCancelNotification(_ userInfo : NSDictionary)
    {
        if (userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String == "rider_cancel_ride_notification"{
            if(userInfo["aps"] as AnyObject).value(forKey: "againSearch") as! String == "Yes"{
                if (userInfo["aps"] as AnyObject).value(forKey: "driverCancel") as! String == "Yes"{
                    let rootViewController = window!.rootViewController
                    let visibleViewController:UIViewController = getVisibleViewController(rootViewController)!
                    if (visibleViewController.presentingViewController != nil) {
                        visibleViewController.dismiss(animated: false, completion: {() -> Void in
                            self.redirectRideCancelNotification(userInfo)
                        })
                    }
                    else if (visibleViewController is SlideMenuController) {
                        let slideMenuVC = (visibleViewController as! SlideMenuController)
                        let nav = (slideMenuVC.mainViewController as! UINavigationController)
                        let rootCon = nav.viewControllers.last
                        
                        guard let rootVC = rootCon else {
                            return
                        }
                        
                        if rootVC.isKind(of: PrebookOptionsVC.self) {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "RideRejectedNotification"), object: nil, userInfo: userInfo as? [AnyHashable : Any])
                        } else {
                            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
                            let dropUpLocationSelect = storyboard.instantiateViewController(withIdentifier: "PrebookOptionsVC") as! PrebookOptionsVC
                            dropUpLocationSelect.prevVC = rootVC
                            dropUpLocationSelect.isNeedProgress = false
                            dropUpLocationSelect.userInfo = userInfo as! [String : Any]
                            let message1 = (userInfo["aps"]! as AnyObject).value(forKey: "alert") as! String as String
                            //self.showSimpleAlertController(nil, message: message1)
                            //return
                            //                let navController = UINavigationController(rootViewController:dropUpLocationSelect) // Creating a navigation controller with VC1 at the root of the navigation stack.
                            //                navController.navigationItem.setHidesBackButton(true, animated: true)
                            //                rootVC.navigationController?.present(navController, animated: true, completion: nil)
                           // rootVC.navigationController?.pushViewController(dropUpLocationSelect, animated: true)
                        }
                        
                    }
                }
                let rootViewController = self.window!.rootViewController
                let visibleViewController:UIViewController = self.self.getVisibleViewController(rootViewController)!
                if (visibleViewController.presentingViewController != nil) {
                    visibleViewController.dismiss(animated: false, completion: {() -> Void in
                        self.redirectRideCancelNotification(userInfo)
                    })
                }
                else if (visibleViewController is SlideMenuController) {
                    let slideMenuVC = (visibleViewController as! SlideMenuController)
                    let nav = (slideMenuVC.mainViewController as! UINavigationController)
                    let rootCon = nav.viewControllers.last
                    
                    guard let rootVC = rootCon else {
                        return
                    }
                    
                    if rootVC.isKind(of: PrebookOptionsVC.self) {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "RideRejectedNotification"), object: nil, userInfo: userInfo as? [AnyHashable : Any])
                    }
                    else {
                        let storyboard = UIStoryboard(name: "Rider", bundle: nil)
                        let prebookOptionsVC = storyboard.instantiateViewController(withIdentifier: "PrebookOptionsVC") as! PrebookOptionsVC
                        prebookOptionsVC.prevVC = rootVC
                        prebookOptionsVC.isNeedProgress = true
                       // let apsRideID = (userInfo["aps"]! as AnyObject).value(forKey: "ride_id") as! String
                       // print("apsRideID", apsRideID)
                        let apsRideID1 = (userInfo["aps"]! as AnyObject).value(forKey: "ride_id") as! NSInteger
                         print("apsRideID1", apsRideID1)
                          print("apsRideID2", String(apsRideID1))
                        prebookOptionsVC.rideBookingID = String(apsRideID1) ?? "0"
                        rootVC.navigationController?.pushViewController(prebookOptionsVC, animated: true)
                    }
                    
                }
            }else{
                let message = (userInfo["aps"]! as AnyObject).value(forKey: "alert") as! String as String
                self.self.showSimpleAlertControllerWithOkAction(nil, message: message, callback: {
                    
                    let visibleVC = UIApplication.topViewController()!
                    if (visibleVC is SlideMenuController) {
                        let slideMenuVC = (visibleVC as! SlideMenuController)
                        let nav = (slideMenuVC.mainViewController as! UINavigationController)
                        let topVC = nav.viewControllers.last
                        print("topVC", topVC)
                        if topVC is DriverDetailsController{
                            (topVC as! DriverDetailsController).backToHomeAction()
                        }
                    }
                })
                return;
            }
        }else{
            let rootViewController = window!.rootViewController
            let visibleViewController:UIViewController = getVisibleViewController(rootViewController)!
            if (visibleViewController.presentingViewController != nil) {
                visibleViewController.dismiss(animated: false, completion: {() -> Void in
                    self.redirectRideCancelNotification(userInfo)
                })
            }
            else if (visibleViewController is SlideMenuController) {
                let slideMenuVC = (visibleViewController as! SlideMenuController)
                let nav = (slideMenuVC.mainViewController as! UINavigationController)
                let rootCon = nav.viewControllers.last
                
                guard let rootVC = rootCon else {
                    return
                }
                
                if rootVC.isKind(of: PrebookOptionsVC.self) {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "RideRejectedNotification"), object: nil, userInfo: userInfo as? [AnyHashable : Any])
                } else {
                    let storyboard = UIStoryboard(name: "Rider", bundle: nil)
                    let dropUpLocationSelect = storyboard.instantiateViewController(withIdentifier: "PrebookOptionsVC") as! PrebookOptionsVC
                    dropUpLocationSelect.prevVC = rootVC
                    dropUpLocationSelect.isNeedProgress = false
                    dropUpLocationSelect.userInfo = userInfo as! [String : Any]
                    //                let navController = UINavigationController(rootViewController:dropUpLocationSelect) // Creating a navigation controller with VC1 at the root of the navigation stack.
                    //                navController.navigationItem.setHidesBackButton(true, animated: true)
                    //                rootVC.navigationController?.present(navController, animated: true, completion: nil)
                    rootVC.navigationController?.pushViewController(dropUpLocationSelect, animated: true)
                }
                
            }
        }
        }

    func redirectRideFinishNotification(_ userInfo : NSDictionary){
        let rootViewController = window!.rootViewController
        let visibleViewController:UIViewController = getVisibleViewController(rootViewController)!
        if (visibleViewController.presentingViewController != nil) {
            visibleViewController.dismiss(animated: false, completion: {() -> Void in
                self.redirectRideFinishNotification(userInfo)
            })
        }
        else if (visibleViewController is SlideMenuController) {
            let slideMenuVC = (visibleViewController as! SlideMenuController)
            let nav = (slideMenuVC.mainViewController as! UINavigationController)
            let rootCon = nav.viewControllers.last
            
            
            let storyboard = UIStoryboard(name: "Rider", bundle: nil)
            let alertController = storyboard.instantiateViewController(withIdentifier: "CustomeFinishRideAlert") as! CustomeFinishRideAlert
            alertController.alertMessage = (userInfo["aps"] as! NSDictionary).value(forKey: "alert") as! String
            alertController.totalPrice = (userInfo["aps"] as! NSDictionary).value(forKey: "total_price") as! String
            
            var rideID = ""
            if let ID = (userInfo["aps"] as! NSDictionary).value(forKey: "ride_id") as? NSString {
                rideID = "\(ID)"
            }
            
            if let IDInt = (userInfo["aps"] as! NSDictionary).value(forKey: "ride_id") as? NSInteger {
                rideID = "\(IDInt)"
            }
            
            alertController.rideId = rideID
            alertController.rootCon = rootCon
            alertController.nav = nav
            let nvc: UINavigationController = UINavigationController(rootViewController: alertController)
            self.window?.rootViewController?.present(nvc, animated:true, completion: nil)
            
            UserDefaults.standard.set(userInfo, forKey: KRIDER_FEEDBACK_INFO)
            UserDefaults.standard.synchronize()
            isRiderFeedBackPresented = true
        }
    }
    
    func remoteNotificationHandle(_ application: UIApplication,userInfo: [AnyHashable: Any]) {
        let Request:WebserviceHelper = WebserviceHelper()
        
        print(userInfo)

        guard let notificationDict = userInfo["aps"] as? NSDictionary else {
            self.showSimpleAlertController(nil, message: "Test Notification Received")
            return
        }
        
        guard ((notificationDict.value(forKey: "notify_type") as? String) != nil) else {
            self.showSimpleAlertController(nil, message: "Test Notification Received")
            return
        }
        
        if UserDefaults.standard.bool(forKey: KRIDER_LOGIN) {
            
            if KAPP_RIDER {
                print("userInfo: \((userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String)")
                
                if (userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String == "discount_notification" {
                    discountPushNotificationHandler(userInfo as NSDictionary)
                }
                else if (userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String == "driver_accept_ride_notification" {
                    
                    redirectRideAcceptNotification(userInfo as NSDictionary)
                }
                else if (userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String == "driver_on_the_way_notification" {
                    redirectRideOtherStatusNotification(userInfo as NSDictionary)
                }
                else if (userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String == "driver_arrived_at_notification" {
                    redirectRideOtherStatusNotification(userInfo as NSDictionary)
                }
                else if (userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String == "driver_cancel_ride_notification"
                {
                    redirectRideCancelNotification(userInfo as NSDictionary)
                }
                else if (userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String == "ride_finish_notification"{
                    redirectRideFinishNotification(userInfo as NSDictionary)
                }
                else if(userInfo["aps"]! as AnyObject).value(forKey: "notify_type") as! String == "driver_reject_ride_notification"{
                    
                    let userDict = userInfo["aps"] as? NSDictionary
                    
                    let rootViewController = window!.rootViewController
                    let visibleViewController = self.getVisibleViewController(rootViewController)!
                    
                    let slideMenuVC = (visibleViewController as? SlideMenuController)
                    let nav = (slideMenuVC?.mainViewController as? UINavigationController)
                    let rootCon = nav?.viewControllers.last
                    
                    if let value = userDict!["is_ride_expired"] as? String, value == "Yes", let type = userDict!["BookingType"] as? Int, type == 2  {
                        self.showSimpleAlertController(nil, message:((userInfo["aps"] as! NSDictionary).value(forKey: "alert") as! String))
                        return
                    }
                    
                    if let value = userDict!["is_expire"] as? Int, value == 1 {
                        
                        if rootCon is OrderPaymentViewController {
                            let detailVC = (rootCon as! OrderPaymentViewController)
                            let alertController: UIAlertController = UIAlertController(title: nil, message: ((userInfo["aps"] as! NSDictionary).value(forKey: "alert") as! String as NSString) as String, preferredStyle: .alert)
                            
                            //Expire ride action
                            let yesAction: UIAlertAction = UIAlertAction(title: "Confirm".localized, style: .default) { action -> Void in
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                                detailVC.navigationController?.popViewController(animated: true)
                            }
                            let noAction: UIAlertAction = UIAlertAction(title: "Cancel".localized, style: .cancel) { action -> Void in
                                detailVC.navigationController?.popToRootViewController(animated: true)
                            }
                            
                            alertController.addAction(yesAction)
                            alertController.addAction(noAction)
                            self.window?.rootViewController!.present(alertController, animated: true, completion: nil)
                        }
                        else {
                            self.showSimpleAlertController(nil, message:((userInfo["aps"] as! NSDictionary).value(forKey: "alert") as! String))
                        }
                        
                    }
                        
                    else{
                        redirectRideCancelNotification(userInfo as NSDictionary)
                    }
                }
                else
                {
                    redirectRideCancelNotification(userInfo as NSDictionary)
                }
                
            }else{
                
                DispatchQueue.main.async(execute: {
                    
                    if (userInfo["aps"] as! NSDictionary).value(forKey: "notify_type") as! String == "new_ride_notification" {
                        self.playSound(UIApplication.shared, userInfo: userInfo)
                        self.showHUD()
                        self.getUserCurrentLocation(userInfo as NSDictionary)
                    }
                    else if (userInfo["aps"] as AnyObject).value(forKey: "notify_type") as! String == "rider_cancel_ride_notification"{
                        
                            let alertController: UIAlertController = UIAlertController(title:nil, message: (userInfo["aps"]! as AnyObject).value(forKey: "alert") as! String as String, preferredStyle: .alert)
                            
                            //Create and add the Cancel action
                            let okAction: UIAlertAction = UIAlertAction(title: "OK".localized, style: .cancel) { action -> Void in
                                if self.homeViewController != nil {
                                    self.homeViewController.preBookingCountApi()
                                }
                                let rootViewController = self.window!.rootViewController
                                
                                let visibleViewController:UIViewController = self.getVisibleViewController(rootViewController)!
                                
                                if visibleViewController is TrackLocationVC {
                                     _ = visibleViewController.navigationController?.popViewController(animated: false)
                                     _ = visibleViewController.navigationController?.popViewController(animated: false)
                                    
                                }
                                if visibleViewController is RideInfoViewController {
                                    _ = visibleViewController.navigationController?.popViewController(animated: true)
                                }
                                
                            }
                            alertController.addAction(okAction)
                            self.window?.rootViewController!.present(alertController, animated: true, completion: nil)
                    }
                    else if ((userInfo["aps"] as AnyObject).value(forKey: "notify_type") as! String == "driver_offline"){
                        
                        let alertController: UIAlertController = UIAlertController(title:nil, message: (userInfo["aps"]! as AnyObject).value(forKey: "alert") as! String as String, preferredStyle: .alert)
                        
                        let yesAction: UIAlertAction = UIAlertAction(title: "Yes".localized, style: .default) { action -> Void in
                            let refId = (userInfo["aps"] as AnyObject).value(forKey: "NotifyId") as! Int32
                             let driId = (userInfo["aps"] as AnyObject).value(forKey: "driverID") as! Int64
                            self.callWebserviceForGettingStillawake(notifyId: refId, driverId : driId)
                            }

                        let noAction: UIAlertAction = UIAlertAction(title: "No".localized, style: .cancel) { action -> Void in
                            
                            let visibleViewController1 : UIViewController = self.getVisibleViewController(self.window!.rootViewController)!
                            if !(visibleViewController1 is HomeViewController){
                                _ = visibleViewController1.navigationController?.popViewController(animated: true)
                            }
                            self.homeViewController.setDriverAvailibility("0")
                        }
                        
                        alertController.addAction(yesAction)
                        alertController.addAction(noAction)
                        self.window?.rootViewController!.present(alertController, animated: true, completion: nil)
                        
                    }
                    else
                    {
                        let alertController: UIAlertController = UIAlertController(title:nil, message: (userInfo["aps"]! as AnyObject).value(forKey: "alert") as! String as String, preferredStyle: .alert)
                        
                        //Create and add the Cancel action
                        let okAction: UIAlertAction = UIAlertAction(title: "OK".localized, style: .cancel) { action -> Void in
                            
                            let rootViewController = self.window!.rootViewController
                            
                            let visibleViewController:UIViewController = self.getVisibleViewController(rootViewController)!
                            
                            if visibleViewController is RideInfoViewController {
                                _ = visibleViewController.navigationController?.popViewController(animated: true)
                            }
                            
                        }
                        alertController.addAction(okAction)
                        self.window?.rootViewController!.present(alertController, animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    // MARK: - Alert Message
    
    func showSimpleAlertController(_ title : String?, message : String) {
        //Create the AlertController
        let alertController: UIAlertController = UIAlertController(title: title, message: message as String, preferredStyle: .alert)
        
        //Create and add the Cancel action
        let okAction: UIAlertAction = UIAlertAction(title: "OK".localized, style: .cancel) { action -> Void in
            
        }
        alertController.addAction(okAction)
        
        self.window?.rootViewController!.present(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: - Get Distance and Duration upon Address Methods
    
    func data_request(_ notficationDic:NSDictionary?) {
        
        let destinationAddress:NSString = (notficationDic!.value(forKey: "aps")! as! NSDictionary).value(forKey: "pick_up_location") as! String as NSString
        let orginAddress:NSString = NSString(format:"%.2f,%.2f",(self.userLocation?.coordinate.latitude)!,(self.userLocation?.coordinate.longitude)!)
        
        let urlString = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(orginAddress)&destinations=\(destinationAddress)&mode=driving&sensor=false&key=\(GOOGLE_WEB_API_KEY)"
        
        let Request: WebserviceHelper = WebserviceHelper()
        
        Request.HTTPGetJSON(url: urlString,withIdicator: false) { (result, error) in
            
            if ((error) != nil){
                
            }else{
                
                let responseContent = Request.convertStringToDictionary(text: result )
                let rowArray = responseContent?["rows"]! as! NSArray
                if(rowArray.count>0){
                    let status = ((((responseContent?["rows"]! as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "elements") as! NSArray).object(at: 0) as! NSDictionary).object(forKey: "status") as! String
                    
                    if status == "OK"{
                        
                        let responseInfo = ((((responseContent?["rows"]! as! NSArray).object(at: 0) as! NSDictionary).value(forKey: "elements") as! NSArray).object(at: 0) as! NSDictionary)
                        
                        
                        self.riderAddressInfo = [ "Distance" : (responseInfo.value(forKey: "distance")! as! NSDictionary).value(forKey: "text")!,
                                                  "Duration" : (responseInfo.value(forKey: "duration")! as AnyObject).value(forKey: "text")!,
                                                  "drop_up_location": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "drop_up_location"))!,
                                                  "pick_up_location": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "pick_up_location"))!,
                                                  "pick_up_date": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "pick_up_date"))!,
                                                  "ride_id": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "ride_id"))!,
                                                  "total_km": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "total_km"))!,
                                                  "total_time": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "total_time"))!,
                                                  "car_type": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "car_type"))!,
                                                  "order_type": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "order_type"))!]
                        
                        self.perform(#selector(AppDelegate.OrderPushNotficationhandler(_:)), with: self.riderAddressInfo!, afterDelay: 0.2)
                    }
                    else{
                        
                        
                        self.riderAddressInfo = [ "Distance" : "Not able to find distance",
                                                  "Duration" : "Not able to find duration",
                                                  "drop_up_location": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "drop_up_location"))!,
                                                  "pick_up_location": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "pick_up_location"))!,
                                                  "pick_up_date": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "pick_up_date"))!,
                                                  "ride_id": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "ride_id"))!,
                                                  "total_time": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "total_time"))!,
                                                  "total_km": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "total_km"))!,
                                                  "car_type": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "car_type"))!,
                                                  "order_type": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "order_type"))!]
                        
                        
                        self.OrderPushNotficationhandler(self.riderAddressInfo!)
                        
                    }
                }
                else{
                    self.riderAddressInfo = [ "Distance" : "Not able to find distance",
                                              "Duration" : "Not able to find duration",
                                              "drop_up_location": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "drop_up_location"))!,
                                              "pick_up_location": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "pick_up_location"))!,
                                              "pick_up_date": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "pick_up_date"))!,
                                              "ride_id": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "ride_id"))!,
                                              "total_time": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "total_time"))!,
                                              "total_km": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "total_km"))!,
                                              "car_type": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "car_type"))!,
                                              "order_type": ((notficationDic!.value(forKey: "aps") as! NSDictionary).value(forKey: "order_type"))!]
                    
                    
                    self.OrderPushNotficationhandler(self.riderAddressInfo!)
                }
                
            }
        }
    }
   
    
    // MARK: - Play Notfication sound Method
    
    func playSound(_ application: UIApplication, userInfo: [AnyHashable: Any]) {
        let state = application.applicationState
        if state == .active {
            let soundDic = userInfo["aps"] as! NSDictionary
            
            let playSoundOnAlert = soundDic.value(forKey: "sound") as! String
            
            if (playSoundOnAlert == "DriverAppSound.mp3")
            {
                let url = URL(fileURLWithPath: "\(Bundle.main.resourcePath!)/\(playSoundOnAlert)")
                
                do {
                    self.audioPlayer = try AVAudioPlayer(contentsOf: url)
                    self.audioPlayer.numberOfLoops = -1
                    self.audioPlayer.play()
                }
                catch {
                }
            }
            else if (playSoundOnAlert == "RiderAppSound.mp3")
            {
                let url = URL(fileURLWithPath: "\(Bundle.main.resourcePath!)/\(playSoundOnAlert)")
                
                do {
                    self.audioPlayer = try AVAudioPlayer(contentsOf: url)
                    self.audioPlayer.numberOfLoops = -1
                    self.audioPlayer.play()
                }
                catch {
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(20)) {
            self.stopSound()
        }
    }
   
    func setupAudio() {
        let audioSession = AVAudioSession.sharedInstance()
        _ = try? audioSession.setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
        _ = try? audioSession.setActive(true)
    }
    func stopSound() {
        
        if self.audioPlayer != nil {
            self.audioPlayer.stop()
            self.audioPlayer = nil
        }
    }
    
    func printLog(log: AnyObject?) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS "
        print(formatter.string(from: NSDate() as Date), terminator: "")
        if log == nil {
            print("nil")
        }
        else {
            print(log!)
        }
    }
    // MARK: - AlertController Methods
    func showSimpleAlertControllerWithOkAction(_ title : String?, message : String, callback: @escaping (()->Void)) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction(title: "OK".localized, style: .cancel) { action -> Void in
            callback()
        }
        alertController.addAction(okAction)
         self.window?.rootViewController!.present(alertController, animated: true, completion: nil)
    }
    
    func callWebserviceForGettingStillawake(notifyId: Int32, driverId : Int64){
        
        let Request: WebserviceHelper = WebserviceHelper()
        self.showHUD()
        var parameters = [
            "UserId" : driverId,
            "RefId": notifyId,
            "Language" : systemLanguage
            ] as [String : Any]
        let jsonString = returnJsonString(param: parameters)
        var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
        parameters = ["data": encryptedData!]
        let jsonParam = returnJsonString(param: parameters)
       // let headerDict = Request.getHeaderForRider(parameters: parameters)
        printJsonStringLog(apiName: KDRIVER_STILLAWAKE, parameter: jsonString as NSString, headerDict: getRequestHeaders())
        Request.CustomHTTPPostJSONWithHeader(url: KDRIVER_STILLAWAKE,jsonString: jsonParam as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
            self.hideHUD()
            if(error != nil){
                self.showSimpleAlertController("Fail".localized, message:"Server connection fail, please try again.".localized)
                return;
            }
            let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
            
            let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
            
            let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
            
            print("Still Awake :", responseContent ?? "")
            
            if let status = responseContent!["Status"] as? Bool{
                if status == true
                {
                 //   self.showSimpleAlertControllerWithOkAction(nil, message: responseContent!.value(forKey: "Message") as? String ?? "", callback: {
                  //  })
                }
                else {
                    if let message = responseContent?.value(forKey: "Message") as? String {
                        self.showSimpleAlertController(nil, message: message)
                        self.hideHUD()
                    }
                }
                
            }
        }
    }
    
    func getNearByAvailableDriver(carType: String, location: CLLocation) {
        
        let Request: WebserviceHelper = WebserviceHelper()
        DispatchQueue.global(qos: .background).async {
            
            if((UserDefaults.standard.object(forKey: KUSER_DETAILS)) != nil){
                let latitudeStr = String(format: "%f", location.coordinate.latitude)
                let longitudeStr = String(format: "%f", location.coordinate.longitude)
                var parameters = [
                    "Latitude" : self.homeViewController.pickUpisSet ?  self.homeViewController.pickUpLocationData.latitude : latitudeStr,
                    "Longitude" : self.homeViewController.pickUpisSet ?  self.homeViewController.pickUpLocationData.longitude : longitudeStr
                    ] as [String:Any]
                let Request: WebserviceHelper = WebserviceHelper()
                let jsonString = returnJsonString(param: parameters)
                var encryptedData  = AESCrypt.encrypt(jsonString, password: SECRET_KEY)
                let headerDict = Request.getHeaderForRider(parameters: parameters)
                parameters = ["data":encryptedData!]
                let jsonParam = returnJsonString(param: parameters)
                Request.CustomHTTPPostJSONWithHeader(url: KNearestDriverList_URL, jsonString: jsonParam as NSString,withIdicator: false,header: headerDict) { (result, error) in
                    if ((error) != nil){
                        print("Parsing error %@",error.debugDescription)
                    }else{
                        let responseObject : NSDictionary = self.convertStringToDictionary(text: result as String)!
                        
                        guard let data = responseObject.value(forKey: "data") as? String else {
                            self.showSimpleAlertController(nil, message: "Something Went Wrong")
                            return
                        }
                        let decryptedData = AESCrypt.decrypt(responseObject.value(forKey: "data") as! String, password: SECRET_KEY)
                        let responseContent = self.convertStringToDictionary(text: decryptedData! as String)
                        //self.rotation = responseContent?.value(forKey: "Rotation") as? Double ?? 0.0
                        let weather = responseContent!["drivers"] as? NSArray
                        DispatchQueue.main.async {
                            if weather!.count > 0 {
                                self.showDriversOnGoogleMap(LocationsArray: weather!)
                            }else {
                                self.removeMarkers()
                            }
                            self.driver1ReachTimeCount = responseContent?.value(forKey: "driverscount_1") as? Int ?? 0
                            self.driver2ReachTimeCount = responseContent?.value(forKey: "driverscount_2") as? Int ?? 0
                            self.driver3ReachTimeCount = responseContent?.value(forKey: "driverscount_3") as? Int ?? 0
                            let driver1ReachTime = UserDefaults.standard.integer(forKey: DRIVER1_LASTCOUNT)
                            let driver2ReachTime = UserDefaults.standard.integer(forKey: DRIVER2_LASTCOUNT)
                            let driver3ReachTime = UserDefaults.standard.integer(forKey: DRIVER3_LASTCOUNT)
                            UserDefaults.standard.set(self.driver1ReachTimeCount, forKey: DRIVER1_LASTCOUNT)
                            UserDefaults.standard.set(self.driver2ReachTimeCount, forKey: DRIVER2_LASTCOUNT)
                            UserDefaults.standard.set(self.driver3ReachTimeCount, forKey: DRIVER3_LASTCOUNT)
                            UserDefaults.standard.synchronize()
                            if driver1ReachTime != self.driver1ReachTimeCount || driver2ReachTime != self.driver2ReachTimeCount || driver3ReachTime != self.driver3ReachTimeCount {
                                let centerCoordinate = CLLocationCoordinate2DMake((self.userLocation!.coordinate.latitude), (self.userLocation!.coordinate.longitude))
                                let visibleVC = UIApplication.topViewController()!
                                if (visibleVC is SlideMenuController) {
                                    let slideMenuVC = (visibleVC as! SlideMenuController)
                                    let nav = (slideMenuVC.mainViewController as! UINavigationController)
                                    let topVC = nav.viewControllers.last
                                    if topVC is HomeViewController{
                                        (topVC as! HomeViewController).getReachTime(location: centerCoordinate, pickUpSet: self.homeViewController.pickUpisSet ? false : true, delegatelatitude: self.homeViewController.pickUpLocationData.latitude, delegatelongitude: self.homeViewController.pickUpLocationData.longitude)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func convertToDictionary(text: String) -> Any? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? Any
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
        
    }
    
    func convertStringToDictionary(text: String) -> NSDictionary? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func removeMarkers() {
        
        for (_, prevMarker) in closestCarMarkers.enumerated() {
            
            prevMarker.map = nil
        }
        
        closestCarMarkers.removeAll()
    }
    
    func showDriversOnGoogleMap(LocationsArray : NSArray) {
        
        print(LocationsArray)
        
        removeMarkers()
        
        for (_,value) in LocationsArray.enumerated() {
            
            if let carDict = value as? NSDictionary {
                
                var carTypeString = "1"
                
                if let currentCarType = carDict["CarType"] as? NSInteger {
                    carTypeString = "\(currentCarType)"
                }
                
                if let currentCarType = carDict["CarType"] as? NSString {
                    carTypeString = currentCarType as String
                }
                
                if carTypeString != self.homeViewController.carType {
                    continue
                }
                
                guard let latitude = carDict["Latitude"] as? String, let longitude = carDict["Longitude"] as? String else {
                    return
                }
                
                var rotation = 0.0
                print("carDict: ", carDict)
                if let rotateAngle = carDict["Rotation"] as? String {
                    rotation = Double(rotateAngle) ?? 0.0
                }
                
                if let rotateAngle = carDict["Rotation"] as? NSInteger {
                    rotation = Double(rotateAngle)
                }
                
                rotation = rotation < 0.0 ? 0.0 : rotation

                let newMarker = GMSMarker()
                let markerLocation = CLLocation.init(latitude: Double(latitude)!, longitude: Double(longitude)!)
                newMarker.position =  CLLocationCoordinate2D(latitude: markerLocation.coordinate.latitude, longitude: markerLocation.coordinate.longitude)

                var annotationImage = ""
                
                if let carTypeID = carDict["CarType"] as? NSInteger {
                    
                    if carTypeID == 1 {
                        annotationImage = "budget"
                    }
                    else if carTypeID == 2 {
                        annotationImage = "business"
                    }
                    else if carTypeID == 3 {
                        annotationImage = "comfort"
                    }
                }
                
//                newMarker.icon = UIImage(named: annotationImage)
//                newMarker.rotation = rotation

                let markerImageView = UIImageView(image: UIImage(named: annotationImage))
                newMarker.iconView = markerImageView
                newMarker.iconView?.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * (rotation) / 180.0))
                
                newMarker.map = homeViewController.rideMapView
                
                closestCarMarkers.append(newMarker)
            }
        }
    }

    func sendDriverLocationToServer(location : CLLocation, dataDict:[String:Any]?) {
        
        let Request: WebserviceHelper = WebserviceHelper()
        
        let latitudeStr = String(format: "%f", location.coordinate.latitude)
        let longitudeStr = String(format: "%f", location.coordinate.longitude)
        
        let course = "\(location.course)"
        
        let parameters = [
            "DriverId":getLoginDriverID(),
            "Latitude":latitudeStr,
            "Longitude":longitudeStr,
            "Rotation": course
            ] as [String : Any]
        
        
//        print("Request Parameter:",parameters)
        
        let requestJsonString = getJsonStringWithEncryptedRequestParameters(requestParameters: parameters)
        
        Request.CustomHTTPPostJSONWithHeader(url: KSendDriverLocation_URL, jsonString: requestJsonString as NSString, withIdicator: false, header: getRequestHeaders()) { (result, error) in
            
            if ((error) != nil){
                print(error.debugDescription)
            }else{
                
                let responseContent = getResponseDictionaryWithResult(result: result)
                print("Location update Server Response:",responseContent)
                
                if UserDefaults.standard.bool(forKey: KRIDER_LOGIN) && !KAPP_RIDER {
                    if let value = responseContent["is_already_logout"] as? String {
                        if value == "1"{
                            self.forceFullyLogoutDriverAlert(responseContent["message"] as! String)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - User Location
    func getUserCurrentLocation(_ notficationDic :NSDictionary )
    {
        let locMgr: INTULocationManager = INTULocationManager.sharedInstance()
        
        locMgr.requestLocation(withDesiredAccuracy: INTULocationAccuracy.block,
                               timeout: 10.0,
                               delayUntilAuthorized: true,
                               block: {(currentLocation: CLLocation?, achievedAccuracy: INTULocationAccuracy, status: INTULocationStatus) -> Void in
                                if status == INTULocationStatus.success || status == INTULocationStatus.timedOut {
                                    
                                    var lat = Double((currentLocation?.coordinate.latitude)!)
                                    var long = Double((currentLocation?.coordinate.longitude)!)
                                    
                                    self.userLocation = CLLocation.init(latitude: lat.roundToPlaces(7), longitude: long.roundToPlaces(7))
                                    
                                    self.data_request(notficationDic)
                                }
                                else
                                {
                                    self.riderAddressInfo =
                                        ["Distance" : "Not able to find distance",
                                         "Duration" : "Not able to find duration",
                                         "drop_up_location": ((notficationDic.value(forKey: "aps") as AnyObject).value(forKey: "drop_up_location"))!,
                                         "pick_up_location": ((notficationDic.value(forKey: "aps") as AnyObject).value(forKey: "pick_up_location"))!,
                                         "pick_up_date": ((notficationDic.value(forKey: "aps") as AnyObject).value(forKey: "pick_up_date"))!,
                                         "ride_id": ((notficationDic.value(forKey: "aps") as AnyObject).value(forKey: "ride_id"))!,
                                         "total_km": ((notficationDic.value(forKey: "aps") as AnyObject).value(forKey: "total_km"))!,
                                         "total_time": ((notficationDic.value(forKey: "aps") as AnyObject).value(forKey: "total_time"))!,
                                         "car_type": ((notficationDic.value(forKey: "aps") as AnyObject).value(forKey: "car_type"))!,
                                         "order_type": ((notficationDic.value(forKey: "aps") as AnyObject).value(forKey: "order_type"))!]
                                    
                                    self.OrderPushNotficationhandler(self.riderAddressInfo!)
                                }
        })
    }
    
    // MARK: - Get Visible Viewcontroller Methods
    func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        
        if rootViewController!.isKind(of: UINavigationController.self) {
            let navigationController = rootViewController as! UINavigationController
            return navigationController.viewControllers.last!
        }
        else if rootViewController!.isKind(of: UITabBarController.self) {
            let tabBarController = rootViewController as! UITabBarController
            return tabBarController.selectedViewController!
        }else {
            
            if ((rootViewController?.presentedViewController) != nil) {
                
                return getVisibleViewController(rootViewController?.presentedViewController)
                
            }else{
                return rootViewController
            }
        }
    }
    func startTimer() {
        self.getNearByAvailableDriver(carType: self.carType, location: self.userLocation!)
    }
    
    //MARK:- CREATE PLIST FILE FOR LOG
    func addLocationToPList(fromResume:Bool){
        myLocationDictInPlist = NSMutableDictionary()
        myLocationDictInPlist.setValue(locationManager1?.location?.coordinate.latitude, forKey: "Latitude")
        myLocationDictInPlist.setValue(locationManager1?.location?.coordinate.longitude, forKey: "Longitude")
        myLocationDictInPlist.setValue(locationManager1?.location?.horizontalAccuracy, forKey: "Accuracy")
        myLocationDictInPlist.setValue(appState(), forKey: "AppState")
        
        if(fromResume){
            myLocationDictInPlist.setValue("YES", forKey: "AddFromResume")
        }
        else{
            myLocationDictInPlist.setValue("NO", forKey: "AddFromResume")
        }
        myLocationDictInPlist.setValue(Date(), forKey: "Time")
        saveLocationsToPlist()
    }
    
    func addResumeLocationToPList() {
        myLocationDictInPlist = NSMutableDictionary()
        myLocationDictInPlist.setValue("UIApplicationLaunchOptionsLocationKey", forKey: "Resume")
        myLocationDictInPlist.setValue(appState(), forKey: "AppState")
        myLocationDictInPlist.setValue(Date(), forKey: "Time")
        
        saveLocationsToPlist()
    }
    
    func saveLocationsToPlist() {
        let plistName = "LocationArray.plist"
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docDir: String = paths[0]
        let fullPath = "\(docDir)/\(plistName)"
        var savedProfile = NSMutableDictionary(contentsOfFile: fullPath)
        
        if(savedProfile == nil){
            savedProfile = NSMutableDictionary()
            myLocationArrayInPlist = NSMutableArray()
        }else{
            myLocationArrayInPlist = savedProfile?.object(forKey:"LocationArray") as! NSMutableArray
        }
        
        if let _ = myLocationArrayInPlist{
            myLocationArrayInPlist.add(myLocationDictInPlist)
            savedProfile?.setObject(myLocationArrayInPlist, forKey: "LocationArray" as NSCopying)
        }
        if(!(savedProfile?.write(toFile: fullPath, atomically: false))!){
            print("Couldn't save location in plist file")
        }
    }
    func addApplicationStatusToPList(applicationStatus:NSString){
        myLocationDictInPlist = NSMutableDictionary()
        
        myLocationDictInPlist.setValue(applicationStatus, forKey: "applicationStatus")
        myLocationDictInPlist.setValue(appState(), forKey: "AppState")
        myLocationDictInPlist.setValue(Date(), forKey: "Time")
        saveLocationsToPlist()
    }
    func appState() -> String {
        let application = UIApplication.shared
        var appState = ""
        if application.applicationState == .active {
            appState = "UIApplicationStateActive"
        }
        if application.applicationState == .background {
            appState = "UIApplicationStateBackground"
        }
        if application.applicationState == .inactive {
            appState = "UIApplicationStateInactive"
        }
        return appState
    }
}

@available(iOS 10.0, *)
extension UNNotification {
    func removeFromNotificationsList() {
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [self.request.identifier])
    }
}

extension Array {
    subscript( index: Int) -> AnyObject? {
        if index >= self.count {
            NSLog("Womp!")
            return nil
        }
        return index as AnyObject?
        
    }
}
extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
            try AVAudioSession.sharedInstance().setActive(true)
            print("Output volume1: \(AVAudioSession.sharedInstance().outputVolume)")
        }
        catch {
        }
        
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            slider?.value = volume
            print("given volume: \(volume)")
            let volume1 = AVAudioSession.sharedInstance().outputVolume
            print("Output volume: \(volume1)")
        }
    }
}

extension AppDelegate : CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.userLocation = locations.last
        
        if  UserDefaults.standard.bool(forKey: KRIDER_LOGIN) {
            
            if KAPP_RIDER {
                if driveListTimer != nil{
                    
                }
                else{
                    print("driver timer is nill")
                    driveListTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(AppDelegate.startTimer), userInfo: nil, repeats: true)
                }
            } else {
                
                if (UserDefaults.standard.object(forKey: kNEW_LOCATION) != nil) {
                    let dicLocation = UserDefaults.standard.object(forKey: kNEW_LOCATION) as! NSDictionary
                    
                    let lat = dicLocation.value(forKey: "lat")
                    let lon = dicLocation.value(forKey: "long")
                    
                    let oldLocation = CLLocation(latitude: lat as! CLLocationDegrees, longitude: lon as! CLLocationDegrees)
                    
                    if (oldLocation.coordinate.latitude != (locations.last?.coordinate.latitude)! && oldLocation.coordinate.longitude != locations.last?.coordinate.longitude ) {
                        
                        let lat = NSNumber(value: (locations.last?.coordinate.latitude)!)
                        let lon = NSNumber(value: (locations.last?.coordinate.longitude)!)
                        
                        
                        let dicLocation: NSDictionary = ["lat": lat, "long": lon]
                        var recordDict : [String:Any]?
                        
                        if let value = UserDefaults.standard.value(forKey: "RideIsStart") {
                            if (value as! String) == "1" {
                                let newLoc = locations.last!
                                
                                let distanceMeters = (newLoc.distance(from: oldLocation))
                                if distanceMeters > 100 {
                                    var currentDisctnace = 0.0
                                    if let disValue = UserDefaults.standard.value(forKey: "Distance"){
                                        currentDisctnace = disValue as! Double
                                    }
                                    let distanceKiloMeters = (newLoc.distance(from: oldLocation)) / 1000.0
                                    
                                    currentDisctnace += distanceKiloMeters
                                    print("distance is : \(currentDisctnace)")
                                    
                                    recordDict = [String:Any]()
                                    
                                    let Request : WebserviceHelper = WebserviceHelper()
                                    
                                    let driverID  = getLoginDriverID()
                                    var driverCurrentRide = UserDefaults.standard.value(forKey: "DriverCurrentRide") as! String
                                    driverCurrentRide = AESCrypt.decrypt(driverCurrentRide, password: SECRET_KEY)
                                    recordDict?["startRide"] = "1"
                                    recordDict?["ride_id"] = driverCurrentRide
                                    recordDict?["logString"] = "DriverID: \(driverID), RideID: \(driverCurrentRide), Distance: \(currentDisctnace), Lat: \(lat), Long: \(lon)"
                                    
                                    UserDefaults.standard.set(dicLocation, forKey: kNEW_LOCATION)
                                    UserDefaults.standard.synchronize()
                                    
                                    UserDefaults.standard.set(currentDisctnace, forKey: "Distance")
                                    UserDefaults.standard.synchronize()
                                }
                                
                            }
                        }
                        self.sendDriverLocationToServer(location: locations.last!, dataDict:recordDict)
                    }
                    
                }else{
                    
                    let lat = NSNumber(value: (locations.last?.coordinate.latitude)!)
                    let lon = NSNumber(value: (locations.last?.coordinate.longitude)!)
                    let dicLocation: NSDictionary = ["lat": lat, "long": lon]
                    
                    UserDefaults.standard.set(dicLocation, forKey: kNEW_LOCATION)
                    UserDefaults.standard.synchronize()
                    self.sendDriverLocationToServer(location: locations.last!, dataDict: nil)
                }
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("Fail to get location:")
    }
    
    func forceFullyLogoutDriverAlert(_ alertMessage: String){
        print("force Fully Logout Driver Alert")
        let alertView =  UIApplication.topViewController()!
        if alertView is UIAlertController{
            print("is already presneted ")
        }
        else{
            print("is not Presented")
            logoutAlertController = UIAlertController(title: nil, message: alertMessage, preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK".localized, style: .cancel) { action -> Void in
                self.forceFullyLogoutAction()
            }
            logoutAlertController.addAction(okAction)
            UIApplication.topViewController()!.present(logoutAlertController, animated: true, completion: nil)
        }
    }
    
    func forceFullyLogoutAction(){
        print("force Fully Logout Action")
        let alertView =  UIApplication.topViewController()!
        if alertView.presentingViewController != nil{
            alertView.dismiss(animated: false, completion: {
                self.forceFullyLogoutAction()
            })
        }
        else{
            //Remove user local Data
            UserDefaults.standard.set(false, forKey: KRIDER_LOGIN)
            UserDefaults.standard.removeObject(forKey: KUSER_DETAILS)
            UserDefaults.standard.removeObject(forKey: AUTH_DETAILS)
            UserDefaults.standard.removeObject(forKey: kAUTH_TOKEN)
            UserDefaults.standard.set("", forKey: KRIDER_USERNAME)
            if !KAPP_RIDER{
                UserDefaults.standard.removeObject(forKey: kNEW_LOCATION)
            }
            UserDefaults.standard.synchronize()
            _ = alertView.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
    
    func showNotificationAllowAlert(){
        
        let alertController = UIAlertController(
            title: "Notification",
            message: "please open this app's settings and set notification access to 'While Using the App'.",
            preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
        }
        alertController.addAction(openAction)
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func restartMonitoring(){
        locationManager1.stopMonitoringSignificantLocationChanges()
        locationManager1.requestAlwaysAuthorization()
        locationManager1.startMonitoringSignificantLocationChanges()
    }
    
    func performLogout() {
        
        //Remove user local Data
        UserDefaults.standard.set(false, forKey: KRIDER_LOGIN)
        UserDefaults.standard.removeObject(forKey: KUSER_DETAILS)
        UserDefaults.standard.removeObject(forKey: AUTH_DETAILS)
        UserDefaults.standard.removeObject(forKey: kAUTH_TOKEN)
        UserDefaults.standard.set("", forKey: KRIDER_USERNAME)
        if !KAPP_RIDER{
            UserDefaults.standard.removeObject(forKey: kNEW_LOCATION)
        }
        UserDefaults.standard.synchronize()
        
        self.firbaseAuthSignOut()
        
        let rootNavigationController = window?.rootViewController as? UINavigationController
        
        let rootViewController = rootNavigationController?.viewControllers.first
        
        if (rootViewController?.isKind(of: RiderLoginViewController.self))! {
            rootNavigationController?.popToRootViewController(animated: true)
        } else {
            let storyBoard = UIStoryboard(name: "Driver", bundle: nil)
            let loginViewController = storyBoard.instantiateViewController(withIdentifier: "RiderLoginViewController") as! RiderLoginViewController
            rootNavigationController?.viewControllers = [loginViewController]
        }
    }
    
    func logoutRider() {
        
        locationManager1.stopUpdatingLocation()
        
        UserDefaults.standard.set(false, forKey: KRIDER_LOGIN)
        UserDefaults.standard.removeObject(forKey: KUSER_DETAILS)
        UserDefaults.standard.removeObject(forKey: KRIDE_DETAILS)
        UserDefaults.standard.set("", forKey: KRIDER_USERNAME)
        UserDefaults.standard.synchronize()
        
        self.firbaseAuthSignOut()
        
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        
        let storyboard = UIStoryboard(name: "Rider", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: "RiderLoginViewController") as! RiderLoginViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        appDelegate.homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: loginVC)
        
        let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
        slideMenuController.delegate = appDelegate.homeViewController
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.slideMenuController()?.changeLeftViewWidth((appDelegate.window?.bounds.width)!-90)
        slideMenuController.removeLeftGestures()
        appDelegate.window?.backgroundColor = UIColor(red: 0.0, green: 149.0, blue: 120.0, alpha: 1.0)
        appDelegate.window?.rootViewController = slideMenuController
    }
    
    func firbaseAuthSignOut(){
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
}

